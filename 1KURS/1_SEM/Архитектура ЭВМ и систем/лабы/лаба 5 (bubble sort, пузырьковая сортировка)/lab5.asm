s_s segment stack "stack"
	dw 5 dup(?)
s_s ends

d_s segment
	arr1 db 6, 3, 8, 2, 5, 9, 1, 7
	arr2 db 8 dup(0)
	temp db 0 ; временная переменная
d_s ends

c_s segment
	assume ss:s_s, ds: d_s, cs: c_s
	
	bubble_sort proc near ; объявление процедуры ближнего вызова
		mov cx, 8
		run:
			mov bx, cx
			mov si, 0
			mov cx, 8
			m1:
				mov ah, arr1[si]
				cmp ah, arr1[si+1]
				jg m2 ; переход в случае arr[si] > arr[si+1]
				add si, 1
			m2: ; обмен значениями arr[si] и arr[si+1]
				mov al, arr1[si+1]
				mov temp, al
				mov al, arr1[si]
				mov arr1[si+1], al
				mov al, temp
				mov arr1[si], al
			loop m1
			mov cx, bx
		loop run
		ret
	bubble_sort endp ; конец процедуры
	
	begin:
	mov ax, d_s
	mov ds, ax
	xor ax, ax
	
	call bubble_sort ; вызов процедуры
	
	mov ah, 4ch
	int 21h
c_s ends
	end begin