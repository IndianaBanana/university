import random
import math
import numpy as np
import matplotlib.pyplot as plt

table_size = 1009
alp = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789" 

def getkey():
    key = ""
    for i in range(6): 
        key += random.choice(alp)
    return key

r_table = []
for i in range(6):
    r_table.append(chr(random.randint(0, 255)))
    
def xor_method(key):
    hash = 0
    for i in range(len(key)):
        hash += ord(key[i]) ^ ord(r_table[i])
    return hash

def division_hash(key):
    hash = xor_method(key)  
    return hash % table_size

def multiplication_hash(key, A=0.6180339887):
    hash = xor_method(key)
    return int(table_size * math.modf(hash * ((5**0.5-1)/2))[0])  

def linear_probing(hash_value, i, c=251):
    return (hash_value + c * i) % table_size

def quadratic_probing(hash_value, i, c=251, d=3):
    return (hash_value + c * i + d * i**2) % table_size

def insert_into_table(keys, hash_function, probing_method):
    table = [None] * table_size
    attempts = []
    
    for key in keys:
        h = hash_function(key)
        for i in range(table_size):
            pos = probing_method(h, i)
            if table[pos] is None:
                table[pos] = key
                attempts.append(i + 1)
                break
    
    return table, np.mean(attempts)

def run_experiment(num_keys, runs=20):
    keys = [getkey() for _ in range(num_keys)]
    methods = [
        ("Division + Linear", division_hash, linear_probing),
        ("Division + Quadratic", division_hash, quadratic_probing),
        ("Multiplication + Linear", multiplication_hash, linear_probing),
        ("Multiplication + Quadratic", multiplication_hash, quadratic_probing),
    ]
    
    results = {method[0]: {"Avg Attempts": 0} for method in methods}

    for _ in range(runs):
        for name, hash_func, probing_method in methods:
            table, avg_attempts = insert_into_table(keys, hash_func, probing_method)
            results[name]["Avg Attempts"] += avg_attempts
    
    for name in results:
        results[name]["Avg Attempts"] /= runs
    
    return results

# Run the experiment and collect data
experiment_results = {}

for N in range(10, table_size, 10):
    results = run_experiment(N, runs=20)
    experiment_results[N] = {
        "Division + Linear": results["Division + Linear"]['Avg Attempts'],
        "Division + Quadratic": results["Division + Quadratic"]['Avg Attempts'],
        "Multiplication + Linear": results["Multiplication + Linear"]['Avg Attempts'],
        "Multiplication + Quadratic": results["Multiplication + Quadratic"]['Avg Attempts']
    }
    division_linear_avg_attempts = results["Division + Linear"]['Avg Attempts']
    division_quadratic_avg_attempts = results["Division + Quadratic"]['Avg Attempts']
    multiplication_linear_avg_attempts = results["Multiplication + Linear"]['Avg Attempts']
    multiplication_quadratic_avg_attempts = results["Multiplication + Quadratic"]['Avg Attempts']
    # Store results for graph


    # Print the results
    print(f"{N} {division_linear_avg_attempts} {division_quadratic_avg_attempts} {multiplication_linear_avg_attempts} {multiplication_quadratic_avg_attempts}") 

# Plot all graphs in a single window as bar charts
methods = ["Division + Linear", "Division + Quadratic", "Multiplication + Linear", "Multiplication + Quadratic"]

# Create a figure with a large size
plt.figure(figsize=(16, 9))

# Plot each method in a separate subplot
for i, method in enumerate(methods, 1):
    plt.subplot(2, 2, i)  # Create a 2x2 grid of subplots
    x = list(experiment_results.keys())
    y = [experiment_results[N][method] for N in x]
    plt.ylim(top=35)  # Set minimum y-axis limit to 35
    plt.bar(x, y, color='orange', width=6)  # Set bars to be orange and thicker
    plt.xlabel("Number of keys (N)")
    plt.ylabel("Average Attempts")
    plt.title(f"Average Attempts for {method}")
    plt.grid(True)

plt.tight_layout()  # Adjust layout to prevent overlap
plt.show()