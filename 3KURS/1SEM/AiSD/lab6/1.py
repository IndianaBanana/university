import random
import string
import matplotlib.pyplot as plt

def generate_random_keys(num_keys, key_length=6):
    """Генерирует список случайных строк длиной key_length."""
    characters = string.ascii_letters + string.digits
    return [''.join(random.choices(characters, k=key_length)) for _ in range(num_keys)]

def xor_to_number(key):
    """Преобразует строковый ключ в числовое значение методом исключающего ИЛИ."""
    result = 0
    for char in key:
        result ^= ord(char)
    return result

def division_hash(value, table_size):
    """Хеш-функция: метод деления."""
    return value % table_size

def multiplication_hash(value, table_size, A=0.6180339887):
    """Хеш-функция: метод умножения."""
    fractional_part = (value * A) % 1
    return int(table_size * fractional_part)

def linear_probing(hash_table, hash_func, key, table_size, c=251):
    """Метод открытой адресации с линейным опробованием."""
    value = xor_to_number(key)  # Преобразуем ключ в числовое значение.
    index = hash_func(value, table_size)  # Вычисляем начальный индекс.
    initial_index = index
    path_length = 1  # Считаем количество шагов при поиске.
    step = 0

    while hash_table[index] is not None:
        path_length += 1
        index = (initial_index + step * c) % table_size  # Линейное пробирование с шагом c.
        step += 1
        if index == initial_index:  # Проверяем, не обошли ли мы таблицу полностью.
            return path_length, False  # Таблица полна.

    hash_table[index] = key  # Вставляем ключ.
    return path_length, True

def quadratic_probing(hash_table, hash_func, key, table_size, c=251, d=3):
    """Метод открытой адресации с квадратичным опробованием."""
    value = xor_to_number(key)
    index = hash_func(value, table_size)
    initial_index = index
    path_length = 1
    step = 0

    while hash_table[index] is not None:
        path_length += 1
        index = (initial_index + c * step + d * step**2) % table_size
        step += 1
        if index == initial_index:  # Таблица полна
            return path_length, False

    hash_table[index] = key
    return path_length, True 

def experiment(num_keys, table_size, hash_func, probing_method):
    """Проводит один эксперимент и возвращает среднюю длину пути поиска."""
    hash_table = [None] * table_size
    total_path_length = 0
    successful_inserts = 0

    keys = generate_random_keys(num_keys)
    for key in keys:
        path_length, success = probing_method(hash_table, hash_func, key, table_size)
        total_path_length += path_length
        if success:
            successful_inserts += 1

    return total_path_length / successful_inserts if successful_inserts > 0 else float('inf')

def experiment_multiple_runs(num_keys, table_size, hash_func, probing_method, runs=100):
    """Проводит эксперимент несколько раз и возвращает среднюю длину пути поиска."""
    total_path_length = 0

    for _ in range(runs):
        total_path_length += experiment(num_keys, table_size, hash_func, probing_method)

    return total_path_length / runs

def run_experiments_with_repeats(runs=100):
    """Запускает эксперименты для всех комбинаций методов с несколькими прогонками."""
    # table_size = 317
    # table_size = 71
    table_size = 1009
    key_counts = range(10, table_size + 1, 10)
    hash_methods = [division_hash, multiplication_hash]
    probing_methods = [linear_probing, quadratic_probing]

    results = {}

    for hash_func in hash_methods:
        for probing_method in probing_methods:
            method_name = f"{hash_func.__name__} + {probing_method.__name__}"
            avg_path_lengths = []

            for num_keys in key_counts:
                avg_path_length = experiment_multiple_runs(num_keys, table_size, hash_func, probing_method, runs=runs)
                avg_path_lengths.append(avg_path_length)

            results[method_name] = avg_path_lengths

    return key_counts, results

def plot_results(key_counts, results):
    """Строит графики зависимости длины пути поиска от количества ключей."""
    plt.figure(figsize=(14, 10))

    for method_name, avg_path_lengths in results.items():
        plt.plot(key_counts, avg_path_lengths, label=method_name)

    plt.title("Зависимость длины пути поиска от количества ключей")
    plt.xlabel("Количество ключей")
    plt.ylabel("O(n)")
    plt.legend()
    plt.grid(True)
    plt.show()

if __name__ == "__main__":
    # Запускаем эксперименты с 100 прогонками
    key_counts, averaged_results = run_experiments_with_repeats(runs=10)
    # Строим графики
    plot_results(key_counts, averaged_results)