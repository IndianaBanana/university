import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class StoreManager  {
    private Map<String, Double> inventory;

    public StoreManager (  )   {
        inventory  =  new HashMap<> (  ) ;
    }

    public void addProduct ( String name, double price )   {
        inventory.put ( name, price ) ;
        System.out.println ( "Product added: " + name + " with price: " + price ) ;
    }

    public void removeProduct ( String name )   {
        if  ( inventory.containsKey ( name )  )   {
            inventory.remove ( name ) ;
            System.out.println ( "Product removed: " + name ) ;
        }  else  {
            System.out.println ( "Product not found: " + name ) ;
        }
    }

    public void displayInventory (  )   {
        if  ( inventory.isEmpty (  )  )   {
            System.out.println ( "No products available." ) ;
        }  else  {
            System.out.println ( "Inventory:" ) ;
            inventory.forEach (  ( name, price )  -> System.out.println ( "Product: " + name + ", Price: " + price )  ) ;
        }
    }

    public static void main ( String[] args )   {
        StoreManager storeManager  =  new StoreManager (  ) ;
        Scanner scanner  =  new Scanner ( System.in ) ;

        while  ( true )   {
            System.out.println ( "\nSelect an option:" ) ;
            System.out.println ( "1. Add Product" ) ;
            System.out.println ( "2. Remove Product" ) ;
            System.out.println ( "3. Display Inventory" ) ;
            System.out.println ( "4. Exit" ) ;

            int choice  =  scanner.nextInt (  ) ;
            scanner.nextLine (  ) ;  // Consume newline

            switch  ( choice )   {
                case 1:
                    System.out.print ( "Enter product name: " ) ;
                    String productName  =  scanner.nextLine (  ) ;
                    System.out.print ( "Enter product price: " ) ;
                    double price  =  scanner.nextDouble (  ) ;
                    storeManager.addProduct ( productName, price ) ;
                    break;
                case 2:
                    System.out.print ( "Enter product name to remove: " ) ;
                    String productToRemove  =  scanner.nextLine (  ) ;
                    storeManager.removeProduct ( productToRemove ) ;
                    break;
                case 3:
                    storeManager.displayInventory (  ) ;
                    break;
                case 4:
                    System.out.println ( "Exiting..." ) ;
                    scanner.close (  ) ;
                    return;
                default:
                    System.out.println ( "Invalid choice, please try again." ) ;
            }
        }
    }
}
