package org.banana;

public class AvlTree<T extends Comparable<T>> implements IBinaryTree<T> {

    private class Node {
        T data;
        int height;
        Node left;
        Node right;

        Node(T data) {
            this.data = data;
            this.height = 1;
        }
    }

    private Node root;

    // Вставка элемента

    public void insert(T value) {
        root = insert(root, value);
    }

    private Node insert(Node node, T value) {
        if (node == null) {
            return new Node(value);
        }

        if (value.compareTo(node.data) < 0) {
            node.left = insert(node.left, value);
        } else if (value.compareTo(node.data) > 0) {
            node.right = insert(node.right, value);
        } else {
            return node;
        }

        // Обновляем высоту текущего узла
        node.height = fixHeight(node);

        // Проверяем баланс
        int balance = getBalance(node);

        // 4 случая дисбаланса и соответствующие повороты
        // Левый левый случай
        if (balance > 1 && value.compareTo(node.left.data) < 0) {
            return rightRotate(node);
        }

        // Правый правый случай
        if (balance < -1 && value.compareTo(node.right.data) > 0) {
            return leftRotate(node);
        }

        // Левый правый случай
        if (balance > 1 && value.compareTo(node.left.data) > 0) {
            node.left = leftRotate(node.left);
            return rightRotate(node);
        }

        // Правый левый случай
        if (balance < -1 && value.compareTo(node.right.data) < 0) {
            node.right = rightRotate(node.right);
            return leftRotate(node);
        }

        return node; // Возвращаем текущий узел
    }

    // Поиск элемента
    @Override
    public boolean search(T data) {
        return search(root, data);
    }

    private boolean search(Node node, T data) {
        if (node == null) {
            return false;
        }
        if (data.compareTo(node.data) == 0) {
            return true;
        }
        if (data.compareTo(node.data) < 0) {
            return search(node.left, data);
        } else {
            return search(node.right, data);
        }
    }

    @Override
    public void display() {
        display(root, "", true);
    }

    private void display(Node node, String prefix, boolean isTail) {
        if (node != null) {
            System.out.println(prefix + (isTail ? "└── " : "├── ") + node.data);
            display(node.right, prefix + (isTail ? "    " : "│   "), false);
            display(node.left, prefix + (isTail ? "    " : "│   "), true);
        }
    }

    // Высота узла
    private int height(Node node) {
        return node == null ? 0 : node.height;
    }

    // Баланс узла
    private int getBalance(Node node) {
        return node == null ? 0 : height(node.left) - height(node.right);
    }

    // Правый поворот
    private Node rightRotate(Node y) {
        Node x = y.left;
        Node T2 = x.right;

        // Выполняем поворот
        x.right = y;
        y.left = T2;

        // Обновляем высоты
        y.height = fixHeight(y);
        x.height = fixHeight(x);

        // Возвращаем новый корень
        return x;
    }

    // Левый поворот
    private Node leftRotate(Node x) {
        Node y = x.right;
        Node T2 = y.left;

        // Выполняем поворот
        y.left = x;
        x.right = T2;

        // Обновляем высоты
        x.height = fixHeight(x);
        y.height = fixHeight(y);

        // Возвращаем новый корень
        return y;
    }

    private int fixHeight(Node node) {
        return Math.max(height(node.left), height(node.right)) + 1;
    }
}
