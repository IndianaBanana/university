package org.banana;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

public class Main {
    private final static List<String> JAVA_KEYWORDS = new ArrayList<>(List.of(
            "abstract", "assert", "boolean", "break", "byte",
            "case", "catch", "char", "class", "const",
            "continue", "default", "do", "double", "else",
            "enum", "extends", "final", "finally", "float",
            "for", "goto", "if", "implements", "import",
            "instanceof", "int", "interface", "long", "native",
            "new", "package", "private", "protected", "public",
            "return", "short", "static", "strictfp", "super",
            "switch", "synchronized", "this", "throw", "throws",
            "transient", "try", "void", "volatile", "while",
            "var"
    ));

    public static Map<String, Integer> readFile(String fileName) throws IOException {
        Map<String, Integer> frequencies = new HashMap<>();
        BufferedReader br = new BufferedReader(new FileReader(fileName));
        String line;

        while ((line = br.readLine()) != null) {
            String[] parts = line.split(": ");
            String word = parts[0].trim();
            int frequency = Integer.parseInt(parts[1].trim());
            frequencies.put(word, frequency);
        }

        br.close();
        return frequencies;
    }

    public static void main(String[] args) throws IOException {
        Collections.sort(JAVA_KEYWORDS);
        IBinaryTree<String> balancedTree = new BalancedBinaryTree<>(JAVA_KEYWORDS);
        try {
            KeywordAnalyzer keywordAnalyzer = new KeywordAnalyzer(balancedTree, JAVA_KEYWORDS);
            keywordAnalyzer.getKeyWordsFrequency();
            Map<String, Integer> frequencies = readFile("keyword_frequencies.txt");
            IBinaryTree<String> stringOptimalBinarySearchTree = new OptimalBinarySearchTree<>(frequencies);
            System.out.println("Balanced Tree:");
            balancedTree.display();
            System.out.println("Optimal Binary Search Tree:");
            stringOptimalBinarySearchTree.display();
            keywordAnalyzer.setTree(stringOptimalBinarySearchTree);
            keywordAnalyzer.findKeyWordsInFiles();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
