package org.banana;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

public class OptimalBinarySearchTree<T extends Comparable<T>> implements IBinaryTree<T> {
    private class Node {
        T data;
        Node left, right;

        public Node(T data) {
            this.data = data;
            this.left = this.right = null;
        }
    }
    private final Map<T, Integer> frequencies;
    private final List<T> keys = new ArrayList<>();
    private final int[][] cost;  // Таблица для стоимости поиска
    private final int[][] roots; // Таблица для корней
    private final int[][] knuthBounds; // Границы поиска корней
    private final int[] prefixSum; // Префиксные суммы частот
    private final int SIZE;

    public final Node root;

    public OptimalBinarySearchTree(Map<T, Integer> frequencies) {
        this.frequencies = frequencies;
        keys.addAll(frequencies.keySet());
        Collections.sort(keys);
        SIZE = keys.size();
        cost = new int[SIZE][SIZE];
        roots = new int[SIZE][SIZE];
        knuthBounds = new int[SIZE][SIZE]; // Для хранения границ корней
        prefixSum = new int[SIZE + 1];

        calculatePrefixSum();
        initializeCostAndRoots();
        root = buildOptimalBST();
    }

    @Override
    public boolean search(T value) {
        return search(root, value);
    }

    private boolean search(Node node, T value) {
        if (node == null) {
            return false;
        }

        int cmp = value.compareTo(node.data);
        if (cmp == 0) {
            return true;
        } else if (cmp < 0) {
            return search(node.left, value);
        } else {
            return search(node.right, value);
        }
    }

    private void calculatePrefixSum() {
        for (int i = 0; i < SIZE; i++) {
            prefixSum[i + 1] = prefixSum[i] + frequencies.get(keys.get(i));
        }
    }

    private int sumFrequency(int i, int j) {
        return prefixSum[j + 1] - prefixSum[i];
    }

    private void initializeCostAndRoots() {
        for (int i = 0; i < SIZE; i++) {
            cost[i][i] = frequencies.get(keys.get(i));
            roots[i][i] = i;
            knuthBounds[i][i] = i; // Для диапазонов из одного элемента границы равны корню
        }
    }

    public Node buildOptimalBST() {
        for (int length = 2; length <= SIZE; length++) {
            for (int i = 0; i <= SIZE - length; i++) {
                int j = i + length - 1;
                cost[i][j] = Integer.MAX_VALUE;

                int kStart = (i + 1 <= j) ? knuthBounds[i][j - 1] : i;
                int kEnd = (i <= j - 1) ? knuthBounds[i + 1][j] : j;

                findMinCostRoots(kStart, kEnd, i, j);

                knuthBounds[i][j] = roots[i][j];
            }
        }
        return buildTree(0, SIZE - 1);
    }

    private void findMinCostRoots(int kStart, int kEnd, int i, int j) {
        for (int r = kStart; r <= kEnd; r++) {
            int leftCost = (r > i) ? cost[i][r - 1] : 0;
            int rightCost = (r < j) ? cost[r + 1][j] : 0;
            int totalCost = leftCost + rightCost + sumFrequency(i, j);

            if (totalCost < cost[i][j]) {
                cost[i][j] = totalCost;
                roots[i][j] = r;
            }
        }
    }

    private Node buildTree(int i, int j) {
        if (i > j) return null;

        int r = roots[i][j];
        Node node = new Node(keys.get(r));
        node.left = buildTree(i, r - 1);
        node.right = buildTree(r + 1, j);
        return node;
    }

    @Override
    public void display() {
        display(root, "", true);
    }

    private void display(Node node, String prefix, boolean isTail) {
        if (node != null) {
            System.out.println(prefix + (isTail ? "└── " : "├── ") + node.data);
            display(node.right, prefix + (isTail ? "    " : "│   "), false);
            display(node.left, prefix + (isTail ? "    " : "│   "), true);
        }
    }
}
