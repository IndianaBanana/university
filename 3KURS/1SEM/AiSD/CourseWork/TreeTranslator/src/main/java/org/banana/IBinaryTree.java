package org.banana;

public interface IBinaryTree<T extends Comparable<T>> {
        boolean search(T value);
        void display();

}
