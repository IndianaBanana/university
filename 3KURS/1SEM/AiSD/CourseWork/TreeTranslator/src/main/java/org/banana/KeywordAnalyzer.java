package org.banana;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class KeywordAnalyzer {
    private final static String[] FILES = {"food/program1.txt", "food/program2.txt", "food/program3.txt", "food/program4.txt", "food/program5.txt",
            "food/program6.txt", "food/program7.txt", "food/program8.txt", "food/program9.txt", "food/program10.txt"};
    private IBinaryTree<String> tree;
    private final static Map<String, Integer> KEYWORD_FREQUENCY = new HashMap<>();

    public KeywordAnalyzer(IBinaryTree<String> tree, List<String> values) {
        this.tree = tree;
        for (String keyword : values) {
            KEYWORD_FREQUENCY.put(keyword, 0);
        }
    }

    public void setTree(IBinaryTree<String> tree) {
        this.tree = tree;
    }

    public void getKeyWordsFrequency() throws IOException {
        validateFiles();
        keyWordFrequency();
        writeFrequencyToFile();
    }

    public void findKeyWordsInFiles() {
        for (String fileName : FILES) {
            findKeyWordsInFiles(fileName);
        }
    }

    private void validateFiles() {
        for (String fileName : FILES) {
            File file = new File(fileName);
            if (!file.exists() || !file.canRead()) {
                System.err.println("File not found or cannot be read: " + fileName);
            }
        }
    }

    private void keyWordFrequency() {
        for (String fileName : FILES) {
            processFile(fileName);
        }
    }


    private void findKeyWordsInFiles(String fileName) {
        File file = new File(fileName);
        System.out.println("Processing file: " + fileName);
        try (Scanner scanner = new Scanner(file)) {
            StringBuilder content = new StringBuilder();

            while (scanner.hasNextLine()) {
                content.append(scanner.nextLine()).append(" ");
            }

            String cleanedText = cleanToken(content.toString());
            String[] words = cleanedText.split("\\s+");

            for (String word : words) {
                if (word.isEmpty()) {
                    continue;
                }
                if (tree.search(word)) {
                    System.out.println(word + ": is a keyword");
                } else {
                    System.out.println(word + ": is not a keyword");
                }

            }
        } catch (IOException e) {
            System.err.println("Error processing file: " + fileName + " - " + e.getMessage());
        }
    }

    private void processFile(String fileName) {
        File file = new File(fileName);
        try (Scanner scanner = new Scanner(file)) {
            StringBuilder content = new StringBuilder();

            while (scanner.hasNextLine()) {
                content.append(scanner.nextLine()).append(" ");
            }

            String cleanedText = cleanToken(content.toString());
            String[] words = cleanedText.split("\\s+");
            for (String word : words) {
                if (word.isEmpty()) {
                    continue;
                }
                if (tree.search(word)) {
                    KEYWORD_FREQUENCY.put(word, KEYWORD_FREQUENCY.get(word) + 1);
                }
            }

        } catch (IOException e) {
            System.err.println("Error processing file: " + fileName + " - " + e.getMessage());
        }
    }

    private String cleanToken(String token) {
        return token.replaceAll("\"\"\".*?\"\"\"", "")
                .replaceAll("\".*?\"", "")
                .replaceAll("[(){}\\[\\]=+\\-*/%;,<>!|&]", " ")
                .replaceAll("[.]", " ")
                .replaceAll("\\s+", " ");
    }

    private void writeFrequencyToFile() throws IOException {
        try (FileWriter writer = new FileWriter("keyword_frequencies.txt")) {
            for (Map.Entry<String, Integer> entry : KEYWORD_FREQUENCY.entrySet()) {
                writer.write(entry.getKey() + ": " + entry.getValue() + "\n");
            }
        }
    }
}