package org.banana;

import java.util.List;

public class BalancedBinaryTree<T extends Comparable<T>> implements IBinaryTree<T> {
    private class Node {
        T data;
        Node left, right;

        public Node(T data) {
            this.data = data;
            this.left = this.right = null;
        }
    }

    private final Node root;

    public BalancedBinaryTree(List<T> sortedList) {
        this.root = buildTree(sortedList, 0, sortedList.size() - 1);
    }

    @Override
    public void display() {
        display(root, "", true);
    }

    private void display(Node node, String prefix, boolean isTail) {
        if (node != null) {
            System.out.println(prefix + (isTail ? "└── " : "├── ") + node.data);
            display(node.right, prefix + (isTail ? "    " : "│   "), false);
            display(node.left, prefix + (isTail ? "    " : "│   "), true);
        }
    }

    @Override
    public boolean search(T value) {
        return search(root, value);
    }

    private boolean search(Node node, T value) {
        if (node == null) {
            return false;
        }

        int cmp = value.compareTo(node.data);
        if (cmp == 0) {
            return true;
        } else if (cmp < 0) {
            return search(node.left, value);
        } else {
            return search(node.right, value);
        }
    }

    private Node buildTree(List<T> list, int start, int end) {
        if (start > end) {
            return null;
        }

        int mid = start + (end - start) / 2;
        Node node = new Node(list.get(mid));

        node.left = buildTree(list, start, mid - 1);
        node.right = buildTree(list, mid + 1, end);

        return node;
    }
}
