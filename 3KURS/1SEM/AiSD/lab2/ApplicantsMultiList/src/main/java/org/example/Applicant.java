package org.example;

import java.util.Arrays;

public class Applicant {
    private static final int GENERAL = 0;
    private static final int EXCELLENT = 1;
    private static final int HONORED = 2;
    private static final int BY_CITY = 3;
    private static final int BY_DORMITORY = 4;
    private static final int MAX_MARK_SUM = 15;
    private static final int SEQUENCE_START_ID = 1;
    private static int SEQUENCE_ID = SEQUENCE_START_ID;

    private final int id;
    private final String LastName;
    private final int[] marks;
    private final boolean isHonored;
    private final String city;
    private final boolean isNeedsDormitory;
    private Applicant next;
    private Applicant prev;
    private Applicant nextExcellent;
    private Applicant prevExcellent;
    private Applicant nextHonored;
    private Applicant prevHonored;
    private Applicant nextByCity;
    private Applicant prevByCity;
    private Applicant nextByDormitory;
    private Applicant prevByDormitory;

    public Applicant(String lastName, int[] marks, boolean isHonored, String city, boolean isNeedsDormitory) {
        this.LastName = lastName;
        this.marks = marks;
        this.isHonored = isHonored;
        this.city = city;
        this.isNeedsDormitory = isNeedsDormitory;
        this.id = SEQUENCE_ID++;
    }

    public int getId() {
        return id;
    }

    public static void resetSequenceID() {
        SEQUENCE_ID = SEQUENCE_START_ID;
    }

    public int[] getMarks() {
        return marks;
    }

    public String getCity() {
        return city;
    }

    public void setNextByIndex(int index1, Applicant applicant) {
        switch (index1) {
            case GENERAL:
                next = applicant;
                return;
            case EXCELLENT:
                nextExcellent = applicant;
                return;
            case HONORED:
                nextHonored = applicant;
                return;
            case BY_CITY:
                nextByCity = applicant;
                return;
            case BY_DORMITORY:
                nextByDormitory = applicant;
                return;
            default:
        }
    }

    public void setPrevByIndex(int index1, Applicant applicant) {
        switch (index1) {
            case GENERAL:
                prev = applicant;
                return;
            case EXCELLENT:
                prevExcellent = applicant;
                return;
            case HONORED:
                prevHonored = applicant;
                return;
            case BY_CITY:
                prevByCity = applicant;
                return;
            case BY_DORMITORY:
                prevByDormitory = applicant;
                return;
            default:
        }
    }

    public Applicant getNextByIndex(int index1) {
        switch (index1) {
            case GENERAL:
                return next;
            case EXCELLENT:
                return nextExcellent;
            case HONORED:
                return nextHonored;
            case BY_CITY:
                return nextByCity;
            case BY_DORMITORY:
                return nextByDormitory;
            default:
                return null;
        }
    }

    public Applicant getPrevByIndex(int index1) {
        switch (index1) {
            case GENERAL:
                return prev;
            case EXCELLENT:
                return prevExcellent;
            case HONORED:
                return prevHonored;
            case BY_CITY:
                return prevByCity;
            case BY_DORMITORY:
                return prevByDormitory;
            default:
                return null;
        }
    }

    public boolean isExcellent() {
        return Arrays.stream(getMarks()).sum() == MAX_MARK_SUM;
    }

    public boolean isHonored() {
        return isHonored;
    }

    public boolean isFarFromUniversityCity(String universityCity) {
        return !getCity().equalsIgnoreCase(universityCity);
    }

    public boolean isNeedsDormitory() {
        return isNeedsDormitory;
    }

    @Override
    public String toString() {
        return "Applicant{" +
                "id=" + id +
                ", LastName='" + LastName + '\'' +
                ", marks=" + Arrays.toString(marks) +
                ", honors=" + isHonored +
                ", city='" + city + '\'' +
                ", dormitory=" + isNeedsDormitory +
                '}';
    }
}
