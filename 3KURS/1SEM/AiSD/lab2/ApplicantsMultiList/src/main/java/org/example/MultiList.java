package org.example;

public class MultiList {
    private static final int PTR_COUNT = 2;
    private static final int FIRST_APPLICANT = 0;
    private static final int LAST_APPLICANT = 1;
    private static final int DESCRIPTOR_COUNT = 5;
    private static final int GENERAL_LIST = 0;
    private static final int EXCELLENT_APPLICANTS = 1;
    private static final int HONORED_APPLICANTS = 2;
    private static final int FAR_FROM_UNIVERSITY_CITY_APPLICANTS = 3;
    private static final int NEEDS_DORMITORY_APPLICANTS = 4;

    private final String universityCity;
    private final Applicant[][] descriptors = new Applicant[DESCRIPTOR_COUNT][PTR_COUNT];

    public MultiList(String universityCity) {
        this.universityCity = universityCity;
    }

    public Applicant getGeneralList() {
        return descriptors[GENERAL_LIST][FIRST_APPLICANT];
    }

    public Applicant getExcellentList() {
        return descriptors[EXCELLENT_APPLICANTS][FIRST_APPLICANT];
    }

    public Applicant getHonoredList() {
        return descriptors[HONORED_APPLICANTS][FIRST_APPLICANT];
    }

    public Applicant getFarFromCityList() {
        return descriptors[FAR_FROM_UNIVERSITY_CITY_APPLICANTS][FIRST_APPLICANT];
    }

    public Applicant getDormitoryList() {
        return descriptors[NEEDS_DORMITORY_APPLICANTS][FIRST_APPLICANT];
    }

    public void addApplicant(Applicant applicant) {
        addApplicant(GENERAL_LIST, applicant);
        if (applicant.isExcellent()) {
            addApplicant(EXCELLENT_APPLICANTS, applicant);
        }
        if (applicant.isHonored()) {
            addApplicant(HONORED_APPLICANTS, applicant);
        }
        if (applicant.isFarFromUniversityCity(universityCity)) {
            addApplicant(FAR_FROM_UNIVERSITY_CITY_APPLICANTS, applicant);
        }
        if (applicant.isNeedsDormitory()) {
            addApplicant(NEEDS_DORMITORY_APPLICANTS, applicant);
        }
    }

    private void addApplicant(int sublistDescriptorIndex, Applicant applicant) {
        if (isSublistEmpty(sublistDescriptorIndex)) {
            addFront(sublistDescriptorIndex, applicant);
        } else {
            addBack(sublistDescriptorIndex, applicant);
        }
        updateDescriptorLastPtr(sublistDescriptorIndex, applicant);
    }

    private void addFront(int sublistDescriptorIndex, Applicant applicant) {
        updateDescriptorFirstPtr(sublistDescriptorIndex, applicant);
    }

    private void updateDescriptorFirstPtr(int sublistDescriptorIndex, Applicant applicant) {
        descriptors[sublistDescriptorIndex][FIRST_APPLICANT] = applicant;
    }

    private void addBack(int sublistDescriptorIndex, Applicant applicant) {
        descriptors[sublistDescriptorIndex][LAST_APPLICANT].setNextByIndex(sublistDescriptorIndex, applicant);
        applicant.setPrevByIndex(sublistDescriptorIndex, descriptors[sublistDescriptorIndex][LAST_APPLICANT]);
    }

    private void updateDescriptorLastPtr(int sublistDescriptorIndex, Applicant applicant) {
        descriptors[sublistDescriptorIndex][LAST_APPLICANT] = applicant;
    }

    public boolean removeApplicant(int id) {
        Applicant applicant = getGeneralList();
        while (applicant != null) {
            if (applicant.getId() == id) {
                removeApplicant(GENERAL_LIST, applicant);
                if (applicant.isExcellent()) {
                    removeApplicant(EXCELLENT_APPLICANTS, applicant);
                }
                if (applicant.isHonored()) {
                    removeApplicant(HONORED_APPLICANTS, applicant);
                }
                if (applicant.isFarFromUniversityCity(universityCity)) {
                    removeApplicant(FAR_FROM_UNIVERSITY_CITY_APPLICANTS, applicant);
                }
                if (applicant.isNeedsDormitory()) {
                    removeApplicant(NEEDS_DORMITORY_APPLICANTS, applicant);
                }
                return true;
            }
            applicant = applicant.getNextByIndex(0);
        }
        return false;
    }

    private void removeApplicant(int sublistDescriptorIndex, Applicant applicant) {
        if (applicant == descriptors[sublistDescriptorIndex][FIRST_APPLICANT]) {
            descriptors[sublistDescriptorIndex][FIRST_APPLICANT] = applicant.getNextByIndex(sublistDescriptorIndex);
        }
        if (applicant == descriptors[sublistDescriptorIndex][LAST_APPLICANT]) {
            descriptors[sublistDescriptorIndex][LAST_APPLICANT] = applicant.getPrevByIndex(sublistDescriptorIndex);
        }
        if (applicant.getPrevByIndex(sublistDescriptorIndex) != null) {
            applicant.getPrevByIndex(sublistDescriptorIndex).setNextByIndex(sublistDescriptorIndex, applicant.getNextByIndex(sublistDescriptorIndex));
        }
        if (applicant.getNextByIndex(sublistDescriptorIndex) != null) {
            applicant.getNextByIndex(sublistDescriptorIndex).setPrevByIndex(sublistDescriptorIndex, applicant.getPrevByIndex(sublistDescriptorIndex));
        }
    }

    public void clearList() {
        for (int i = 0; i < DESCRIPTOR_COUNT; i++) {
            clearDescriptor(i);
        }
        Applicant.resetSequenceID();
    }

    private void clearDescriptor(int i) {
        descriptors[i][FIRST_APPLICANT] = null;
        descriptors[i][LAST_APPLICANT] = null;
    }

    private boolean isSublistEmpty(int sublistDescriptorIndex) {
        return descriptors[sublistDescriptorIndex][FIRST_APPLICANT] == null;
    }

    public void printGeneralList() {
        printList(getGeneralList(), GENERAL_LIST);
    }

    public void printExcellentApplicantsList() {
        printList(getExcellentList(), EXCELLENT_APPLICANTS);
    }

    public void printHonoredApplicantsList() {
        printList(getHonoredList(), HONORED_APPLICANTS);
    }

    public void printFarFromUniversityCityApplicantsList() {
        printList(getFarFromCityList(), FAR_FROM_UNIVERSITY_CITY_APPLICANTS);
    }

    public void printNeedsDormitoryApplicantsList() {
        printList(getDormitoryList(), NEEDS_DORMITORY_APPLICANTS);
    }

    private void printList(Applicant a, int index) {
        while (a != null) {
            System.out.println(a);
            a = a.getNextByIndex(index);
        }
    }
}
