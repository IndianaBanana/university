package org.example;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Main {
    private static final String UNIVERSITY_CITY = "Orel";
    private static final int ADD_APPLICANT = 1;
    private static final int GET_GENERAL_LIST = 2;
    private static final int GET_EXCELLENT_LIST = 3;
    private static final int GET_HONORED_LIST = 4;
    private static final int GET_FAR_FROM_CITY_LIST = 5;
    private static final int GET_DORMITORY_LIST = 6;
    private static final int REMOVE_APPLICANT = 7;
    private static final int CLEAR_LIST = 8;
    private static final int EXIT = 9;
    private static final MultiList multiList = new MultiList(UNIVERSITY_CITY);

    public static void main(String[] args) {
        choiceAction();
    }

    private static void choiceAction() {
        int choice;
        do {
            printMenu();
            choice = readNumberInput();
            switch (choice) {
                case ADD_APPLICANT:
                    Applicant applicant = createApplicant();
                    if (applicant != null) {
                        multiList.addApplicant(applicant);
                    }
                    break;
                case GET_GENERAL_LIST:
                    multiList.printGeneralList();
                    break;
                case GET_EXCELLENT_LIST:
                    multiList.printExcellentApplicantsList();
                    break;
                case GET_HONORED_LIST:
                    multiList.printHonoredApplicantsList();
                    break;
                case GET_FAR_FROM_CITY_LIST:
                    multiList.printFarFromUniversityCityApplicantsList();
                    break;
                case GET_DORMITORY_LIST:
                    multiList.printNeedsDormitoryApplicantsList();
                    break;
                case REMOVE_APPLICANT:
                    removeApplicant();
                    break;
                case CLEAR_LIST:
                    multiList.clearList();
                    System.out.println("List cleared");
                    break;
                case EXIT:
                    break;
            }
        } while (choice != EXIT);
    }

    private static void removeApplicant() {
        System.out.println("Enter id of applicant to remove: ");

        int id = readNumberInput();
        if (id == 0) {
            System.out.println("Invalid id");
            return;
        }

        if (multiList.removeApplicant(id)) {
            System.out.println("Applicant removed");
        } else {
            System.out.println("Applicant not found");
        }
    }

    private static Applicant createApplicant() {
        try {
            Scanner scanner = new Scanner(System.in);
            System.out.print("Enter last name: ");
            String lastName = scanner.nextLine();
            System.out.print("Enter first mark: ");
            int firstMark = scanner.nextInt();
            System.out.print("Enter second mark: ");
            int secondMark = scanner.nextInt();
            System.out.print("Enter third mark: ");
            int thirdMark = scanner.nextInt();
            System.out.print("Is honors. True or False: ");
            boolean honors = scanner.nextBoolean();
            System.out.print("Enter city: ");
            String city = scanner.next();
            System.out.print("Does applicant need dormitory? True or False: ");
            boolean dormitory = scanner.nextBoolean();
            return new Applicant(lastName, new int[]{firstMark, secondMark, thirdMark}, honors, city, dormitory);
        } catch (InputMismatchException e) {
            System.out.println("Invalid input. Applicant not created");
            return null;
        }
    }

    private static int readNumberInput() {
        Scanner scanner = new Scanner(System.in);
        try {
            return scanner.nextInt();
        } catch (NumberFormatException e) {
            // FIXME так делать нельзя, но для лабы сойдет.
            return 0;
        }
    }

    private static void printMenu() {
        System.out.printf("%d - add applicant%n", ADD_APPLICANT);
        System.out.printf("%d - get general list%n", GET_GENERAL_LIST);
        System.out.printf("%d - get excellent list%n", GET_EXCELLENT_LIST);
        System.out.printf("%d - get honored list%n", GET_HONORED_LIST);
        System.out.printf("%d - get far from city list%n", GET_FAR_FROM_CITY_LIST);
        System.out.printf("%d - get dormitory list%n", GET_DORMITORY_LIST);
        System.out.printf("%d - remove applicant%n", REMOVE_APPLICANT);
        System.out.printf("%d - clear list%n", CLEAR_LIST);
        System.out.printf("%d - exit%n", EXIT);
        System.out.print("Enter your choice: ");
    }
}