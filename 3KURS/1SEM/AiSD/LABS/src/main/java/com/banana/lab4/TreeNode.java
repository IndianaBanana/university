package com.banana.lab4;

public class TreeNode {
    int value;
    com.banana.lab4.TreeNode left;
    com.banana.lab4.TreeNode right;

    public TreeNode(int value) {
        this.value = value;
        left = null;
        right = null;
    }

    @Override
    public String toString() {
        return "TreeNode{" + value + "}";
    }
}
