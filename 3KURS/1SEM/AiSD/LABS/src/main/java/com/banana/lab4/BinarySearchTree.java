package com.banana.lab4;

import com.banana.common.SinglyLinkedList;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


public class BinarySearchTree {
    TreeNode root;

    public BinarySearchTree() {
        root = null;
    }

    public void insert(int value) {
        root = insertRecursive(root, value);
    }

    private TreeNode insertRecursive(TreeNode node, int value) {
        if (node == null) {
            return new TreeNode(value);
        }
        if (value < node.value) {
            node.left = insertRecursive(node.left, value);
        } else if (value > node.value) {
            node.right = insertRecursive(node.right, value);
        }
        return node;
    }

    public SinglyLinkedList<TreeNode> findNodesWithSubtreeDifference() {
        SinglyLinkedList<TreeNode> list = new SinglyLinkedList<>();
        countDescendants(root, list);
        return list;
    }

    private int countDescendants(TreeNode node, SinglyLinkedList<TreeNode> list) {
        if (node == null) {
            return 0;
        }
        int leftCount = countDescendants(node.left, list);
        int rightCount = countDescendants(node.right, list);
        if (Math.abs(leftCount - rightCount) == 1) {
            list.pushBack(node);
        }
        return 1 + leftCount + rightCount;
    }

    public int getHeight() {
        if (root == null) {
            return -1;
        }

        SinglyLinkedList<TreeNode> queue = new SinglyLinkedList<>();
        queue.pushBack(root);
        int height = -1;

        while (!queue.isEmpty()) {
            int levelSize = queue.getSize();

            for (int i = 0; i < levelSize; i++) {
                TreeNode currentNode = queue.front();
                queue.remove(currentNode);

                if (currentNode.left != null) {
                    queue.pushBack(currentNode.left);
                }
                if (currentNode.right != null) {
                    queue.pushBack(currentNode.right);
                }
            }

            height++;
        }

        return height;
    }

    public TreeNode findKthElement(int k) {
        int[] count = {0};
        return findKthElement(root, k, count);
    }

    private TreeNode findKthElement(TreeNode node, int k, int[] count) {
        if (node == null) {
            return null;
        }

        TreeNode leftResult = findKthElement(node.left, k, count);
        if (leftResult != null) {
            return leftResult;
        }

        count[0]++;
        if (count[0] == k) {
            return node;
        }

        return findKthElement(node.right, k, count);
    }

    public Boolean deleteNode(int key) {
        boolean[] isDeleted = {false};
        root = deleteNode(root, key, isDeleted);
        return isDeleted[0];
    }

    private TreeNode deleteNode(TreeNode node, int key, boolean[] isDeleted) {
        if (node == null) {
            return null;
        }

        if (key < node.value) {
            node.left = deleteNode(node.left, key, isDeleted);
        } else if (key > node.value) {
            node.right = deleteNode(node.right, key, isDeleted);
        } else {
            isDeleted[0] = true;
            // Случай 1: узел имеет только правое поддерево или нет потомков
            if (node.left == null) {
                return node.right;
            }
            // Случай 2: узел имеет только левое поддерево
            else if (node.right == null) {
                return node.left;
            }
            // Случай 3: узел имеет двух потомков
            // Найдем наименьший узел в правом поддереве
            node.value = minValue(node.right);

            // Удаляем наименьший узел
            node.right = deleteNode(node.right, node.value, isDeleted);
        }

        return node;
    }

    // Берем данные из самого левого листа правого поддерва удаляемого узла
    private int minValue(TreeNode node) {
        int minValue = node.value;
        while (node.left != null) {
            minValue = node.left.value;
            node = node.left;
        }
        return minValue;
    }

    @Override
    public String toString() {
        int maxLevel = getHeight() + 1;
//        if (maxLevel > 6) {
//            return "Too many levels. To display the tree, the maximum level must be a maximum of 6.";
//        }
        return buildTreeString(Collections.singletonList(root), 1, maxLevel, new StringBuilder()).toString();
    }

    private StringBuilder buildTreeString(List<TreeNode> nodes, int level, int maxLevel, StringBuilder sb) {
        if (nodes.isEmpty() || areAllElementsNull(nodes)) {
            return sb;
        }

        int floor = maxLevel - level;
        int edgeLines = (int) Math.pow(2, (Math.max(floor - 1, 0)));
        int firstSpaces = (int) Math.pow(2, (floor)) - 1;
        int betweenSpaces = (int) Math.pow(2, (floor + 1)) - 1;

        appendSpaces(firstSpaces, sb);

        List<TreeNode> newNodes = new ArrayList<>();
        for (TreeNode node : nodes) {
            if (node != null) {
                sb.append(node.value);
                newNodes.add(node.left);
                newNodes.add(node.right);
            } else {
                sb.append(" ");
                newNodes.add(null);
                newNodes.add(null);
            }
            appendSpaces(betweenSpaces, sb);
        }
        sb.append("\n");

        for (int i = 1; i <= edgeLines; i++) {
            for (TreeNode node : nodes) {
                appendSpaces(firstSpaces - i, sb);
                if (node == null) {
                    appendSpaces(edgeLines + edgeLines + i + 1, sb);
                    continue;
                }

                if (node.left != null) {
                    sb.append("/");
                } else {
                    sb.append(" ");
                }

                appendSpaces(i + i - 1, sb);

                if (node.right != null) {
                    sb.append("\\");
                } else {
                    sb.append(" ");
                }
                appendSpaces(edgeLines + edgeLines - i, sb);
            }
            sb.append("\n");
        }

        buildTreeString(newNodes, level + 1, maxLevel, sb);
        return sb;
    }

    private void appendSpaces(int count, StringBuilder sb) {
        sb.append(" ".repeat(Math.max(0, count)));
    }

    private boolean areAllElementsNull(List<TreeNode> list) {
        for (TreeNode node : list) {
            if (node != null) {
                return false;
            }
        }
        return true;
    }
}
