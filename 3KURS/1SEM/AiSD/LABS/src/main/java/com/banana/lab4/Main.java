package com.banana.lab4;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.InputMismatchException;
import java.util.Scanner;

public class Main {

    public static final String FILE_PATH = "/Users/banana/university/3KURS/1SEM/AiSD/Labs/src/com/banana/lab4/input.txt";

    public static void main(String[] args) {
        BinarySearchTree tree = new BinarySearchTree();
        try (Scanner scanner = new Scanner(new File(FILE_PATH))) {
            while (scanner.hasNextInt()) {
                int number = scanner.nextInt();
                tree.insert(number);
            }
        } catch (FileNotFoundException e) {
            System.out.println("File not found.");
        } catch (InputMismatchException e) {
            System.out.println("File might be corrupted or something...");
        }
        char choice = '0';
        while (choice != '5') {
            System.out.println("0. Print tree");
            System.out.println("1. Find nodes with subtree difference by 1");
            System.out.println("2. Find tree's height");
            System.out.println("3. Find Kth element");
            System.out.println("4. Delete node");
            System.out.println("5. Exit");
            Scanner scanner = new Scanner(System.in);
            choice = scanner.next().charAt(0);
            switch (choice) {
                case '0':
                    System.out.println(tree);
                    break;
                case '1':
                    System.out.println(tree.findNodesWithSubtreeDifference());
                    break;
                case '2':
                    System.out.println("Height: " + tree.getHeight());
                    break;
                case '3':
                    System.out.println("Enter k: ");
                    try {
                        TreeNode node = tree.findKthElement(scanner.nextInt());
                        System.out.println(node == null ? "Node not found" : node);
                    } catch (InputMismatchException e) {
                        System.out.println("Invalid input");
                        break;
                    }
                    break;
                case '4':
                    System.out.println("Enter key: ");
                    try {
                        int key = scanner.nextInt();
                        System.out.printf(tree.deleteNode(key) ? "Node {%d} deleted" : "Node {%d} not found", key);
                    } catch (InputMismatchException e) {
                        System.out.println("Invalid input");
                        break;
                    }
                    break;
                case '5':
                    break;
                default:
                    System.out.println("Invalid choice");
            }
        }
    }

}