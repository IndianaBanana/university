package com.banana.lab5;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.category.BarRenderer;
import org.jfree.data.category.DefaultCategoryDataset;

import javax.swing.*;
import java.util.ArrayList;
import java.util.Random;

public class Main {

    private static final int KEY_LENGTH = 6;
    private static final String CHARSET = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    private static final int TABLE_SIZE = 1003;
    private static final int NUM_KEYS =100000;


    public static void main(String[] args) {
        ArrayList<String> keys = generateKeys();

        evaluateAndPlot(keys, "Additive + Division", Main::additiveMethod, Main::divisionHash);

        evaluateAndPlot(keys, "Additive + Multiplication", Main::additiveMethod, Main::multiplicationHash);

        evaluateAndPlot(keys, "XOR + Division", Main::xorMethod, Main::divisionHash);

        evaluateAndPlot(keys, "XOR + Multiplication", Main::xorMethod, Main::multiplicationHash);
    }

    private static ArrayList<String> generateKeys() {
        ArrayList<String> keys = new ArrayList<>();
        Random random = new Random();

        for (int i = 0; i < Main.NUM_KEYS; i++) {
            StringBuilder key = new StringBuilder();
            for (int j = 0; j < KEY_LENGTH; j++) {
                key.append(CHARSET.charAt(random.nextInt(CHARSET.length())));
            }
            keys.add(key.toString());
        }
        return keys;
    }

    private static int additiveMethod(String key) {
        int hash = 0;
        for (char c : key.toCharArray()) {
            hash += c;
        }
        return hash;
    }

    private static int xorMethod(String key) {
        int hash = 0;
        for (char c : key.toCharArray()) {
            hash ^= c;
        }
        return hash;
    }

    private static int divisionHash(int value, int num) {
        return Math.abs(value % num);
    }

    private static int multiplicationHash(int value, int num) {
        double A = 0.6180339887;
        return Math.abs((int) (num * ((value * A) % 1)));
    }

    private static void evaluateAndPlot(ArrayList<String> keys, String title, KeyToIntMethod keyMethod, HashFunction hashMethod) {
        int[] hashTable = new int[TABLE_SIZE];
        int num = TABLE_SIZE;
        for (String key : keys) {
            int intermediateValue = keyMethod.convert(key);
            int hash = hashMethod.hash(intermediateValue, num);
            hashTable[hash]++;
        }
        plotGraph(hashTable, title);
    }

    private static void plotGraph(int[] hashTable, String title) {
        Integer[] totalCollisions = {0};
        DefaultCategoryDataset dataset = getDataset(hashTable, totalCollisions);
        JFreeChart barChart = ChartFactory.createBarChart(
                title + " \nTotal Collisions: " + totalCollisions[0],
                "Hash Table Index",
                "Value",
                dataset,
                PlotOrientation.VERTICAL,
                true,
                true,
                false
        );

        CategoryPlot plot = barChart.getCategoryPlot();
        BarRenderer renderer = (BarRenderer) plot.getRenderer();
        renderer.setSeriesPaint(0, java.awt.Color.BLUE);
        renderer.setSeriesPaint(1, java.awt.Color.RED);

        JFrame frame = new JFrame(title);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(800, 600);
        frame.add(new ChartPanel(barChart));
        frame.setVisible(true);
    }

    private static DefaultCategoryDataset getDataset(int[] hashTable, Integer[] totalCollisions) {
        DefaultCategoryDataset dataset = new DefaultCategoryDataset();

        for (int i = 0; i < hashTable.length; i++) {
            dataset.addValue(hashTable[i], "Keys", Integer.toString(i));
        }

        for (int i = 0; i < hashTable.length; i++) {
            dataset.addValue(
                    hashTable[i] > 1 ? hashTable[i] - 1 : 0,
                    "Collisions",
                    Integer.toString(i)
            );
            if (hashTable[i] > 1) {
                totalCollisions[0] += hashTable[i] - 1;
            }
        }
        return dataset;
    }

    @FunctionalInterface
    interface KeyToIntMethod {
        int convert(String key);
    }

    @FunctionalInterface
    interface HashFunction {
        int hash(int value, int tableSize);
    }
}
