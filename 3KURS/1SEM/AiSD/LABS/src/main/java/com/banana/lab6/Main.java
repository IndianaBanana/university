package com.banana.lab6;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.category.BarRenderer;
import org.jfree.data.category.DefaultCategoryDataset;

import javax.swing.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class Main {

    private static final int KEY_LENGTH = 6;
    private static final String CHARSET = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        // Ввод параметров
        System.out.print("Введите размер хеш-таблицы: ");
        int tableSize = scanner.nextInt();

        System.out.print("Введите количество ключей: ");
        int numKeys = scanner.nextInt();

        System.out.print("Выберите метод хеширования (1 - деление, 2 - умножение): ");
        int hashMethodChoice = scanner.nextInt();

        System.out.print("Выберите метод разрешения коллизий (1 - линейное, 2 - квадратичное пробирование): ");
        int collisionMethodChoice = scanner.nextInt();

        // Генерация ключей
        ArrayList<String> keys = generateKeys(numKeys);

        // Эксперимент
        DefaultCategoryDataset dataset = new DefaultCategoryDataset();
        for (int numInsertedKeys = 10; numInsertedKeys <= numKeys; numInsertedKeys += 10) {
            double avgSearchPath = performExperiment(keys.subList(0, numInsertedKeys), tableSize, hashMethodChoice, collisionMethodChoice);
            dataset.addValue(avgSearchPath, "Средняя длина пути", String.valueOf(numInsertedKeys));
        }

        // Построение графика
        plotGraph(dataset, "Длина пути поиска");
    }

    private static ArrayList<String> generateKeys(int numKeys) {
        ArrayList<String> keys = new ArrayList<>();
        Random random = new Random();

        for (int i = 0; i < numKeys; i++) {
            StringBuilder key = new StringBuilder();
            for (int j = 0; j < KEY_LENGTH; j++) {
                key.append(CHARSET.charAt(random.nextInt(CHARSET.length())));
            }
            keys.add(key.toString());
        }
        return keys;
    }

    private static double performExperiment(
            List<String> keys,
            int tableSize,
            int hashMethodChoice,
            int collisionMethodChoice) {

        String[] hashTable = new String[tableSize];
        int totalSearchPath = 0;

        for (String key : keys) {
            int hash = xorMethod(key);
            int index = hashMethodChoice == 1
                    ? divisionHash(hash, tableSize)
                    : multiplicationHash(hash, tableSize);

            int searchPath = collisionMethodChoice == 1
                    ? linearProbing(hashTable, key, index, tableSize)
                    : quadraticProbing(hashTable, key, index, tableSize);

            totalSearchPath += searchPath;
        }

        return (double) totalSearchPath / keys.size();
    }

    private static int xorMethod(String key) {
        int hash = 0;
        for (char c : key.toCharArray()) {
            hash ^= c;
        }
        return hash;
    }

    private static int divisionHash(int value, int tableSize) {
        return Math.abs(value % tableSize);
    }

    private static int multiplicationHash(int value, int tableSize) {
        double A = 0.6180339887;
        return Math.abs((int) (tableSize * ((value * A) % 1)));
    }

    private static int linearProbing(String[] hashTable, String key, int index, int tableSize) {
        int searchPath = 0;

        while (hashTable[index] != null) {
            index = (index + 1) % tableSize;
            searchPath++;
        }

        hashTable[index] = key;
        return searchPath + 1;
    }

    private static int quadraticProbing(String[] hashTable, String key, int index, int tableSize) {
        int searchPath = 0;
        int i = 0;

        while (hashTable[index] != null) {
            i++;
            index = (index + i * i) % tableSize;
            searchPath++;
        }

        hashTable[index] = key;
        return searchPath + 1;
    }

    private static void plotGraph(DefaultCategoryDataset dataset, String title) {
        JFreeChart lineChart = ChartFactory.createLineChart(
                title,
                "Количество ключей",
                "Средняя длина пути поиска",
                dataset,
                PlotOrientation.VERTICAL,
                true,
                true,
                false
        );

        JFrame frame = new JFrame(title);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(800, 600);
        frame.add(new ChartPanel(lineChart));
        frame.setVisible(true);
    }
}
