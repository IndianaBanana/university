package com.example.ll5;

import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import java.io.File;

public class MainActivity extends AppCompatActivity implements FileManager.FileManagerListener {

    private EditText editTextId;
    private Button buttonDownload, buttonView, buttonDelete;
    private FileManager fileManager;
    private PopupManager popupManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        editTextId = findViewById(R.id.editTextId);
        buttonDownload = findViewById(R.id.buttonDownload);
        buttonView = findViewById(R.id.buttonView);
        buttonDelete = findViewById(R.id.buttonDelete);

        File downloadDirectory = new File(getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS), "NTVJournal");
        fileManager = new FileManager(downloadDirectory, this);
        fileManager.setFileManagerListener(this);
        popupManager = new PopupManager(this);

        buttonDownload.setOnClickListener(v -> {
            String id = editTextId.getText().toString();
            if (!id.isEmpty()) {
                fileManager.downloadFile("http://ntv.ifmo.ru/file/journal/" + id + ".pdf");
            } else {
                Toast.makeText(MainActivity.this, "Введите ID журнала", Toast.LENGTH_SHORT).show();
            }
        });

        buttonView.setOnClickListener(v -> fileManager.viewFile());
        buttonDelete.setOnClickListener(v -> fileManager.deleteFile());

        buttonView.setVisibility(View.GONE);
        buttonDelete.setVisibility(View.GONE);

        new Handler().postDelayed(() -> popupManager.showPopupIfNeeded(), 500);
    }

    @Override
    public void onDownloadComplete(boolean success) {
        if (success) {
            buttonView.setVisibility(View.VISIBLE);
            buttonDelete.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onFileDeleted() {
        buttonView.setVisibility(View.GONE);
        buttonDelete.setVisibility(View.GONE);
    }
}
