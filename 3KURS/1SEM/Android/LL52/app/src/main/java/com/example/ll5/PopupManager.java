package com.example.ll5;

import android.content.Context;
import android.content.SharedPreferences;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.PopupWindow;

public class PopupManager {
    private final Context context;
    private final SharedPreferences sharedPreferences;

    public PopupManager(Context context) {
        this.context = context;
        this.sharedPreferences = context.getSharedPreferences("app_prefs", Context.MODE_PRIVATE);
    }

    public void showPopupIfNeeded() {
        boolean showPopup = sharedPreferences.getBoolean("showPopup", true);
        if (showPopup) {
            showPopup();
        }
    }

    private void showPopup() {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View popupView = inflater.inflate(R.layout.popup_window, null);

        final PopupWindow popupWindow = new PopupWindow(popupView,
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT,
                true);

        popupWindow.setAnimationStyle(android.R.style.Animation_Dialog);
        popupWindow.showAtLocation(popupView, Gravity.CENTER, 0, 0);

        final CheckBox checkBox = popupView.findViewById(R.id.checkBox);
        Button buttonOk = popupView.findViewById(R.id.buttonOk);

        buttonOk.setOnClickListener(v -> {
            sharedPreferences.edit()
                    .putBoolean("showPopup", !checkBox.isChecked())
                    .apply();
            popupWindow.dismiss();
        });
    }
}

