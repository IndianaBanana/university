package com.example.ll5;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.widget.Toast;

import androidx.core.content.FileProvider;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class FileManager {
    private final File downloadDirectory;
    private final Context context;
    private String currentFileName;
    private FileManagerListener listener;

    public FileManager(File downloadDirectory, Context context) {
        this.downloadDirectory = downloadDirectory;
        this.context = context;
        if (!downloadDirectory.exists()) {
            downloadDirectory.mkdirs();
        }
    }

    public void setFileManagerListener(FileManagerListener listener) {
        this.listener = listener;
    }

    public void downloadFile(String url) {
        new DownloadTask().execute(url);
    }

    public void viewFile() {
        if (currentFileName == null) {
            Toast.makeText(context, "Файл не выбран", Toast.LENGTH_SHORT).show();
            return;
        }

        File file = new File(downloadDirectory, currentFileName);
        if (file.exists()) {
            Uri fileUri = FileProvider.getUriForFile(context, context.getPackageName() + ".fileprovider", file);
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setDataAndType(fileUri, "application/pdf");
            intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_ACTIVITY_NO_HISTORY);
            if (intent.resolveActivity(context.getPackageManager()) != null) {
                context.startActivity(intent);
            } else {
                Toast.makeText(context, "Нет приложения для открытия PDF", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(context, "Файл не найден", Toast.LENGTH_SHORT).show();
        }
    }

    public void deleteFile() {
        if (currentFileName == null) {
            Toast.makeText(context, "Файл не выбран", Toast.LENGTH_SHORT).show();
            return;
        }

        File file = new File(downloadDirectory, currentFileName);
        if (file.exists() && file.delete()) {
            Toast.makeText(context, "Файл удален", Toast.LENGTH_SHORT).show();
            if (listener != null) {
                listener.onFileDeleted();
            }
        } else {
            Toast.makeText(context, "Ошибка удаления файла", Toast.LENGTH_SHORT).show();
        }
    }

    private class DownloadTask extends AsyncTask<String, Void, Boolean> {
        @Override
        protected Boolean doInBackground(String... urls) {
            String url = urls[0];
            currentFileName = url.substring(url.lastIndexOf('/') + 1);
            File outputFile = new File(downloadDirectory, currentFileName);

            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder().url(url).build();

            try (Response response = client.newCall(request).execute()) {
                if (response.isSuccessful()) {
                    try (InputStream inputStream = response.body().byteStream();
                         FileOutputStream fileOutputStream = new FileOutputStream(outputFile)) {
                        byte[] buffer = new byte[1024];
                        int bytesRead;
                        while ((bytesRead = inputStream.read(buffer)) != -1) {
                            fileOutputStream.write(buffer, 0, bytesRead);
                        }
                        return true;
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return false;
        }

        @Override
        protected void onPostExecute(Boolean success) {
            String message = success ? "Файл успешно скачан" : "Файл не найден";
            Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
            if (listener != null) {
                listener.onDownloadComplete(success);
            }
        }
    }

    public interface FileManagerListener {
        void onDownloadComplete(boolean success);
        void onFileDeleted();
    }
}
