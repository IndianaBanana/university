package com.example.ll4;

import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    private SongDatabaseHelper dbHelper;
    private ListView listView;
    private SimpleCursorAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        try {
            dbHelper = new SongDatabaseHelper(this);
            listView = findViewById(R.id.listView);

            if (!NetworkUtils.isNetworkAvailable(this)) {
                Toast.makeText(this, "Running in offline mode", Toast.LENGTH_LONG).show();
            } else {
                startSongFetching();
            }

            displaySongs();
        } catch (Exception e) {
            Log.e("MainActivity", "Error in onCreate: " + e.getMessage(), e);
            Toast.makeText(this, "Error: " + e.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    private void startSongFetching() {
        new SongFetcher(this, dbHelper, adapter).execute();
        new android.os.Handler().postDelayed(this::startSongFetching, 20000);
    }

    private void displaySongs() {
        Cursor cursor = dbHelper.getAllSongs();
        String[] fromColumns = {SongDatabaseHelper.COLUMN_ARTIST, SongDatabaseHelper.COLUMN_TITLE, SongDatabaseHelper.COLUMN_TIMESTAMP};
        int[] toViews = {R.id.textViewArtist, R.id.textViewTitle, R.id.textViewTimestamp};
        adapter = new SimpleCursorAdapter(this, R.layout.list_item, cursor, fromColumns, toViews, 0);
        listView.setAdapter(adapter);
    }
}