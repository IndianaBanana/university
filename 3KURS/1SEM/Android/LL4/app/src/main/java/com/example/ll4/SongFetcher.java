package com.example.ll4;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.SimpleCursorAdapter;
import android.widget.Toast;
import org.json.JSONObject;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class SongFetcher extends AsyncTask<Void, Void, String> {

    private Context context;
    private SongDatabaseHelper dbHelper;
    private SimpleCursorAdapter adapter;

    public SongFetcher(Context context, SongDatabaseHelper dbHelper, SimpleCursorAdapter adapter) {
        this.context = context;
        this.dbHelper = dbHelper;
        this.adapter = adapter;
    }

    @Override
    protected String doInBackground(Void... voids) {
        try {
            URL url = new URL("http://media.ifmo.ru/api_get_current_song.php");
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("POST");
            connection.setDoOutput(true);
            connection.setInstanceFollowRedirects(true); // Автоматическая обработка перенаправлений

            String postData = "login=4707login&password=4707pass";
            OutputStream os = connection.getOutputStream();
            os.write(postData.getBytes());
            os.flush();
            os.close();

            int responseCode = connection.getResponseCode();
            if (responseCode == HttpURLConnection.HTTP_MOVED_PERM || responseCode == HttpURLConnection.HTTP_MOVED_TEMP) {
                String newUrl = connection.getHeaderField("Location");
                connection = (HttpURLConnection) new URL(newUrl).openConnection();
                connection.setRequestMethod("POST");
                connection.setDoOutput(true);
                connection.setInstanceFollowRedirects(true);

                os = connection.getOutputStream();
                os.write(postData.getBytes());
                os.flush();
                os.close();
            }

            BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            StringBuilder result = new StringBuilder();
            String line;
            while ((line = reader.readLine()) != null) {
                result.append(line);
            }
            reader.close();

            Log.d("SongFetcher", "Response: " + result);

            return result.toString();
        } catch (Exception e) {
            Log.e("SongFetcher", "Error in doInBackground: " + e.getMessage(), e);
            return "{ \"result\": \"error\", \"info\": \"" + e.getMessage() + "\" }";
        }
    }

    @Override
    protected void onPostExecute(String result) {
        try {
            if (result.isEmpty()) {
                Toast.makeText(context, "Empty response from server", Toast.LENGTH_SHORT).show();
                return;
            }

            JSONObject json = new JSONObject(result);
            if (json.getString("result").equals("success")) {
                String info = json.getString("info");
                String[] parts = info.split(" — | - "); // Разделение по тире или дефису

                if (parts.length < 2) {
                    Toast.makeText(context, "Invalid response format: " + info, Toast.LENGTH_SHORT).show();
                    return;
                }

                String artist = parts[0];
                String title = parts[1];

                if (!title.equals(dbHelper.getLastSongTitle())) {
                    dbHelper.insertSongWithNotify(artist, title, adapter);
                }
            } else {
                Toast.makeText(context, "Error: " + json.getString("info"), Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
            Log.e("SongFetcher", "Error in onPostExecute: " + e.getMessage(), e);
            Toast.makeText(context, "JSON parsing error: " + e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }
}