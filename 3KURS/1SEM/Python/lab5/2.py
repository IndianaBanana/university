import tkinter as tk
from tkinter import messagebox, filedialog
import json


class Medicine:
    def __init__(self, article, name, price, prescription_required, description):
        self.article = article
        self.name = name
        self.price = price
        self.prescription_required = prescription_required
        self.description = description

    def __str__(self):
        return (f"Article: {self.article}, Name: {self.name}, Price: {self.price}, "
                f"Prescription Required: {'Yes' if self.prescription_required else 'No'}, "
                f"Description: {self.description}")

    def to_dict(self):
        return {
            "article": self.article,
            "name": self.name,
            "price": self.price,
            "prescription_required": self.prescription_required,
            "description": self.description
        }

    @staticmethod
    def from_dict(data):
        return Medicine(
            article=data['article'],
            name=data['name'],
            price=data['price'],
            prescription_required=data['prescription_required'],
            description=data['description']
        )


class MedicineApp:
    def __init__(self, root):
        self.root = root
        self.root.title("Medicine Information System")
        self.medicines_list = []
        self.current_file = None

        self.selected_article = tk.StringVar()
        self.selected_article.set("Select Article") 

        self.create_menu()

        self.label_article = tk.Label(root, text="Article:")
        self.label_article.grid(row=0, column=0)
        self.entry_article = tk.Entry(root)
        self.entry_article.grid(row=0, column=1)

        self.label_name = tk.Label(root, text="Name:")
        self.label_name.grid(row=1, column=0)
        self.entry_name = tk.Entry(root)
        self.entry_name.grid(row=1, column=1)

        self.label_price = tk.Label(root, text="Price:")
        self.label_price.grid(row=2, column=0)
        self.entry_price = tk.Entry(root)
        self.entry_price.grid(row=2, column=1)

        self.label_description = tk.Label(root, text="Description:")
        self.label_description.grid(row=3, column=0)
        self.entry_description = tk.Entry(root)
        self.entry_description.grid(row=3, column=1)

        self.prescription_required = tk.BooleanVar()
        self.check_prescription = tk.Checkbutton(root, text="Prescription required", variable=self.prescription_required)
        self.check_prescription.grid(row=4, columnspan=2)

        self.button_add = tk.Button(root, text="Add Medicine", command=self.add_medicine)
        self.button_add.grid(row=5, column=0)

        self.dropdown_remove = tk.OptionMenu(root, self.selected_article, "Select Article", command=self.on_article_selected)
        self.dropdown_remove.grid(row=5, column=1)

        self.button_remove = tk.Button(root, text="Remove Selected Medicine", command=self.remove_selected_medicine, state="disabled")
        self.button_remove.grid(row=5, column=2)

        self.button_display_all = tk.Button(root, text="Display All Medicines", command=self.display_all_medicines)
        self.button_display_all.grid(row=6, column=0)

        self.button_display_non_prescription = tk.Button(root, text="Display Non-Prescription Medicines", command=self.display_non_prescription_medicines)
        self.button_display_non_prescription.grid(row=6, column=1)

        self.medicine_listbox = tk.Listbox(root, width=80, height=10)
        self.medicine_listbox.grid(row=7, column=0, columnspan=3)

    def create_menu(self):
        menubar = tk.Menu(self.root)

        file_menu = tk.Menu(menubar, tearoff=0)
        file_menu.add_command(label="Create", command=self.clear_form)
        file_menu.add_command(label="Open", command=self.open_file)
        file_menu.add_command(label="Save", command=self.save_file)
        file_menu.add_command(label="Save as...", command=self.save_file_as)
        file_menu.add_separator()
        file_menu.add_command(label="Quit", command=self.root.quit)

        menubar.add_cascade(label="File", menu=file_menu)

        help_menu = tk.Menu(menubar, tearoff=0)
        help_menu.add_command(label="About", command=self.show_about_info)
        menubar.add_cascade(label="Help", menu=help_menu)

        self.root.config(menu=menubar)

    def clear_form(self):
        """Clear all input fields to create a new entry."""
        self.current_file = None
        self.medicines_list = []
        self.update_dropdown()
        self.display_all_medicines()
        self.entry_article.delete(0, tk.END)
        self.entry_name.delete(0, tk.END)
        self.entry_price.delete(0, tk.END)
        self.entry_description.delete(0, tk.END)
        self.prescription_required.set(False)

    def open_file(self):
        """Open a file and load medicines from it."""
        file_path = filedialog.askopenfilename(
            title="Open File",
            filetypes=(("JSON files", "*.json"), ("All files", "*.*"))
        )
        if file_path:
            try:
                with open(file_path, 'r') as file:
                    data = json.load(file)
                    self.medicines_list = [Medicine.from_dict(med) for med in data]
                    self.display_all_medicines()
                    self.update_dropdown()
                    self.current_file = file_path
                    messagebox.showinfo("Success", "Data loaded successfully!")
            except Exception as e:
                messagebox.showerror("Error", f"Failed to open file: {e}")

    def save_file(self):
        """Save current list to file (if opened) or prompt for 'Save As...'."""
        if self.current_file:
            self.save_to_file(self.current_file)
        else:
            self.save_file_as()

    def save_file_as(self):
        """Save current list to a new file."""
        file_path = filedialog.asksaveasfilename(
            title="Save File",
            defaultextension=".json",
            filetypes=(("JSON files", "*.json"), ("All files", "*.*"))
        )
        if file_path:
            self.save_to_file(file_path)

    def save_to_file(self, file_path):
        """Save medicines list to the specified file."""
        try:
            with open(file_path, 'w') as file:
                json.dump([med.to_dict() for med in self.medicines_list], file, indent=4)
                self.current_file = file_path
                messagebox.showinfo("Success", "Data saved successfully!")
        except Exception as e:
            messagebox.showerror("Error", f"Failed to save file: {e}")

    def add_medicine(self):
        article = self.entry_article.get()

        if any(medicine.article == article for medicine in self.medicines_list):
            messagebox.showerror("Duplicate Article", f"Medicine with article '{article}' already exists.")
            return

        name = self.entry_name.get()
        try:
            price = float(self.entry_price.get())
        except ValueError:
            messagebox.showerror("Input Error", "Invalid price format. Please enter a valid number.")
            return
        description = self.entry_description.get()
        prescription_required = self.prescription_required.get()

        if not article or not name or not description:
            messagebox.showerror("Input Error", "Please fill in all fields.")
            return

        medicine = Medicine(article, name, price, prescription_required, description)
        self.medicines_list.append(medicine)
        self.update_dropdown()
        messagebox.showinfo("Success", "Medicine added successfully!")

    def update_dropdown(self):
        """Update the dropdown menu with the current list of articles."""
        menu = self.dropdown_remove["menu"]
        menu.delete(0, "end")
        menu.add_command(label="Select Article", command=lambda: self.on_article_selected("Select Article"))
        for medicine in self.medicines_list:
            menu.add_command(label=medicine.article, command=lambda value=medicine.article: self.on_article_selected(value))
        self.selected_article.set("Select Article")
        self.button_remove.config(state="disabled")

    def on_article_selected(self, value):
        """Handle the selection of an article from the dropdown."""
        self.selected_article.set(value)
        if value == "Select Article":
            self.button_remove.config(state="disabled")
        else:
            self.button_remove.config(state="normal")

    def remove_selected_medicine(self):
        """Remove the selected medicine based on the dropdown value."""
        selected_article = self.selected_article.get()
        for medicine in self.medicines_list:
            if medicine.article == selected_article:
                self.medicines_list.remove(medicine)
                self.update_dropdown()
                self.display_all_medicines()
                messagebox.showinfo("Success", f"Medicine '{medicine.name}' removed.")
                return
        messagebox.showerror("Not Found", f"No medicine found with article '{selected_article}'.")

    def display_all_medicines(self):
        """Display all medicines in the Listbox."""
        self.medicine_listbox.delete(0, tk.END)
        if not self.medicines_list:
            self.medicine_listbox.insert(tk.END, "No medicines available.")
        else:
            for medicine in self.medicines_list:
                self.medicine_listbox.insert(tk.END, str(medicine))

    def display_non_prescription_medicines(self):
        """Display only non-prescription medicines in the Listbox."""
        self.medicine_listbox.delete(0, tk.END)
        non_prescription_medicines = [med for med in self.medicines_list if not med.prescription_required]
        if not non_prescription_medicines:
            self.medicine_listbox.insert(tk.END, "No non-prescription medicines available.")
        else:
            for medicine in non_prescription_medicines:
                self.medicine_listbox.insert(tk.END, str(medicine))

    def show_about_info(self):
        """Show information about the developer."""
        messagebox.showinfo("About", "Created by Kuzhelev Alexander for 5th lab. Group - 22PG")



def main():
    root = tk.Tk()
    app = MedicineApp(root)
    root.mainloop()


if __name__ == "__main__":
    main()
