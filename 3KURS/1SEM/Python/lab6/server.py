import socket
import threading

def evaluate_expression(expression):
    try:
        
        octal_parts = expression.split('+')
        decimal_parts = []
        for part in octal_parts:
            sub_parts = part.split('-')
            decimal_sub_parts = [int(x, 8) for x in sub_parts]
            decimal_parts.append(
                decimal_sub_parts[0] - sum(decimal_sub_parts[1:])
            )
        decimal_result = sum(decimal_parts)
        
        octal_result = oct(decimal_result)[2:]  
        return octal_result
    except Exception as e:
        return f"Error: {str(e)}"

def handle_client(client_socket):
    try:
        while True:
            expression = client_socket.recv(1024).decode('utf-8')
            if not expression:
                break
            print(f"Received expression: {expression}")
            result = evaluate_expression(expression)
            client_socket.send(result.encode('utf-8'))
    except ConnectionResetError:
        print("Client disconnected.")
    finally:
        client_socket.close()

def start_server():
    server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_socket.bind(('0.0.0.0', 9999))
    server_socket.listen()
    print("Server is running on port 9999...")

    try:
        while True:
            client_socket, addr = server_socket.accept()
            print(f"Connection established with {addr}")
            client_handler = threading.Thread(target=handle_client, args=(client_socket,))
            client_handler.start()
    except KeyboardInterrupt:
        print("Server shutting down...")
    finally:
        server_socket.close()

if __name__ == "__main__":
    start_server()
