import socket
import tkinter as tk
from tkinter import messagebox

def send_expression(event=None):
    expression = entry.get()
    if not expression:
        messagebox.showerror("Ошибка", "Введите выражение!")
        return

    try:
        client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        client_socket.connect(('127.0.0.1', 9999))
        client_socket.send(expression.encode('utf-8'))
        result = client_socket.recv(1024).decode('utf-8')
        client_socket.close()
        result_label.config(text=f"Результат: {result}")
        entry.delete(0, tk.END)
        entry.focus()
    except Exception as e:
        messagebox.showerror("Ошибка", f"Не удалось подключиться к серверу: {str(e)}")

root = tk.Tk()
root.title("Калькулятор выражений")

root.geometry("700x300")
root.resizable(False, False)

frame = tk.Frame(root, padx=20, pady=20)
frame.pack(expand=True)

entry_label = tk.Label(frame, text="Введите выражение:", font=("Arial", 16))
entry_label.grid(row=0, column=0, padx=5, pady=10)

entry = tk.Entry(frame, width=30, font=("Arial", 16))
entry.grid(row=0, column=1, padx=5, pady=10)
entry.bind("<Return>", send_expression) 
entry.focus()

send_button = tk.Button(frame, text="Вычислить", font=("Arial", 16), command=send_expression)
send_button.grid(row=1, column=0, columnspan=2, pady=20)

result_label = tk.Label(frame, text="Результат: ", font=("Arial", 16))
result_label.grid(row=2, column=0, columnspan=2, pady=10)

root.mainloop()
