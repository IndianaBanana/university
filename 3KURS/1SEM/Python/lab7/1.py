import sqlite3
import tkinter as tk
from tkinter import ttk, messagebox


def init_db():
    conn = sqlite3.connect("medicines.db")
    cursor = conn.cursor()
    
    
    cursor.execute('''CREATE TABLE IF NOT EXISTS categories (
                        id INTEGER PRIMARY KEY AUTOINCREMENT,
                        name TEXT UNIQUE NOT NULL
                      )''')

    cursor.execute('''CREATE TABLE IF NOT EXISTS medicines (
                        id INTEGER PRIMARY KEY AUTOINCREMENT,
                        name TEXT NOT NULL,
                        category_id INTEGER NOT NULL,
                        price REAL NOT NULL,
                        FOREIGN KEY (category_id) REFERENCES categories (id)
                      )''')

    conn.commit()
    return conn


def seed_data(conn):
    cursor = conn.cursor()
    cursor.execute("INSERT OR IGNORE INTO categories (name) VALUES ('Antibiotics'), ('Vitamins')")
    conn.commit()


def get_categories():
    cursor = conn.cursor()
    cursor.execute("SELECT id, name FROM categories")
    return cursor.fetchall()


def add_medicine():
    name = name_entry.get()
    category = category_combobox.get()
    price = price_entry.get()

    if not name or not category or not price:
        messagebox.showerror("Error", "Please fill in all fields.")
        return

    try:
        price = float(price)
    except ValueError:
        messagebox.showerror("Error", "Price must be a number.")
        return

    category_id = category_map[category]
    cursor = conn.cursor()
    cursor.execute("INSERT INTO medicines (name, category_id, price) VALUES (?, ?, ?)", (name, category_id, price))
    conn.commit()
    messagebox.showinfo("Success", "Medicine added successfully!")
    refresh_table()


def delete_medicine():
    selected_item = table.selection()
    if not selected_item:
        messagebox.showerror("Error", "Please select a medicine to delete.")
        return

    item = table.item(selected_item)
    medicine_id = item["values"][0]

    cursor = conn.cursor()
    cursor.execute("DELETE FROM medicines WHERE id = ?", (medicine_id,))
    conn.commit()
    messagebox.showinfo("Успех", "Лекарство удалено успешно!")
    refresh_table()


def refresh_table():
    for row in table.get_children():
        table.delete(row)

    cursor = conn.cursor()
    cursor.execute('''SELECT medicines.id, medicines.name, categories.name, medicines.price
                      FROM medicines
                      JOIN categories ON medicines.category_id = categories.id''')

    for row in cursor.fetchall():
        table.insert("", tk.END, values=row)


root = tk.Tk()
root.title("Medicine Information System")


frame = tk.Frame(root, padx=10, pady=10)
frame.pack()

name_label = tk.Label(frame, text="Medicine name:")
name_label.grid(row=0, column=0, sticky="w")
name_entry = tk.Entry(frame, width=30)
name_entry.grid(row=0, column=1)

category_label = tk.Label(frame, text="Category:")
category_label.grid(row=1, column=0, sticky="w")
category_combobox = ttk.Combobox(frame, width=27, state="readonly")
category_combobox.grid(row=1, column=1)

price_label = tk.Label(frame, text="Price:")
price_label.grid(row=2, column=0, sticky="w")
price_entry = tk.Entry(frame, width=30)
price_entry.grid(row=2, column=1)

add_button = tk.Button(frame, text="Add Medicine", command=add_medicine)
add_button.grid(row=3, column=0, columnspan=2, pady=10)

columns = ("id", "Name", "Category", "Price")
display_columns = ("Name", "Category", "Price")
table = ttk.Treeview(root, columns=columns, show="headings", height=10, displaycolumns=display_columns)
for col in columns:
    table.heading(col, text=col)
    table.column(col, anchor="center")
table.pack(padx=10, pady=10)

delete_button = tk.Button(root, text="Delete selected Medicine", command=delete_medicine)
delete_button.pack(pady=10)

conn = init_db()
seed_data(conn)

categories = get_categories()
category_map = {name: id for id, name in categories}
category_combobox["values"] = [name for _, name in categories]

refresh_table()

root.mainloop()

conn.close()
