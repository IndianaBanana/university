# gui.py

import tkinter as tk
from tkinter import messagebox
from model import Medicine

class MedicineManagerGUI:
    def __init__(self, root, add_medicine_callback, remove_medicine_callback, display_medicines_callback, display_without_prescription_callback):
        self.root = root
        self.root.title("Medicine Manager")

        self.add_medicine_callback = add_medicine_callback
        self.remove_medicine_callback = remove_medicine_callback
        self.display_medicines_callback = display_medicines_callback
        self.display_without_prescription_callback = display_without_prescription_callback

        self.create_widgets()
        self.current_medicines_list = []

    def create_widgets(self):
        # Labels and Entries
        tk.Label(self.root, text="Article:").grid(row=0, column=0, padx=10, pady=5)
        self.article_entry = tk.Entry(self.root)
        self.article_entry.grid(row=0, column=1)

        tk.Label(self.root, text="Name:").grid(row=1, column=0, padx=10, pady=5)
        self.name_entry = tk.Entry(self.root)
        self.name_entry.grid(row=1, column=1)

        tk.Label(self.root, text="Price:").grid(row=2, column=0, padx=10, pady=5)
        self.price_entry = tk.Entry(self.root)
        self.price_entry.grid(row=2, column=1)

        tk.Label(self.root, text="Prescription Required:").grid(row=3, column=0, padx=10, pady=5)
        self.prescription_var = tk.BooleanVar()
        self.prescription_checkbox = tk.Checkbutton(self.root, variable=self.prescription_var)
        self.prescription_checkbox.grid(row=3, column=1)

        tk.Label(self.root, text="Description:").grid(row=4, column=0, padx=10, pady=5)
        self.description_entry = tk.Entry(self.root)
        self.description_entry.grid(row=4, column=1)

        # Buttons
        tk.Button(self.root, text="Add Medicine", command=self.add_medicine).grid(row=5, column=0, padx=10, pady=5)
        tk.Button(self.root, text="Remove Medicine", command=self.remove_medicine).grid(row=5, column=1, padx=10, pady=5)
        tk.Button(self.root, text="Display Medicines", command=self.display_medicines).grid(row=6, column=0, padx=10, pady=5)
        tk.Button(self.root, text="Display Without Prescription", command=self.display_without_prescription).grid(row=6, column=1, padx=10, pady=5)
        tk.Button(self.root, text="Clear Fields", command=self.clear_fields).grid(row=7, column=0, padx=10, pady=5)

        # Listbox for displaying medicines
        self.medicines_listbox = tk.Listbox(self.root, width=50)
        self.medicines_listbox.grid(row=8, column=0, columnspan=2, padx=10, pady=5)

    def add_medicine(self):
        article = self.article_entry.get()
        name = self.name_entry.get()
        try:
            price = float(self.price_entry.get())
        except ValueError:
            messagebox.showerror("Input Error", "Please enter a valid price.")
            return
        prescription_required = self.prescription_var.get()
        description = self.description_entry.get()

        if article and name and description:
            medicine = Medicine(article, name, price, prescription_required, description)
            self.add_medicine_callback(medicine)
            messagebox.showinfo("Success", f"Medicine '{name}' added!")
            self.clear_fields()
        else:
            messagebox.showerror("Input Error", "Please fill all fields except price.")

    def remove_medicine(self):
        selected_index = self.medicines_listbox.curselection()
        if selected_index:
            # Remove the selected medicine from the main list
            selected_medicine = self.current_medicines_list[selected_index[0]]
            index_to_remove = self.current_medicines_list.index(selected_medicine)
            self.remove_medicine_callback(index_to_remove)
            self.display_medicines()
        else:
            messagebox.showerror("Selection Error", "Please select a medicine to remove.")

    def display_medicines(self):
        self.medicines_listbox.delete(0, tk.END)
        self.current_medicines_list = self.display_medicines_callback()
        for medicine in self.current_medicines_list:
            self.medicines_listbox.insert(tk.END, str(medicine))

    def display_without_prescription(self):
        self.medicines_listbox.delete(0, tk.END)
        self.current_medicines_list = self.display_without_prescription_callback()
        if self.current_medicines_list:
            for medicine in self.current_medicines_list:
                self.medicines_listbox.insert(tk.END, str(medicine))
        else:
            messagebox.showinfo("Result", "No medicines available without a prescription.")

    def clear_fields(self):
        self.article_entry.delete(0, tk.END)
        self.name_entry.delete(0, tk.END)
        self.price_entry.delete(0, tk.END)
        self.prescription_var.set(False)
        self.description_entry.delete(0, tk.END)
        self.medicines_listbox.delete(0, tk.END)
