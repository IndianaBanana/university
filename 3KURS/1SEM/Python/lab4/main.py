# main.py

import tkinter as tk
from gui import MedicineManagerGUI
from app import MedicineManagerApp

if __name__ == "__main__":
    root = tk.Tk()
    app_logic = MedicineManagerApp()
    
    gui = MedicineManagerGUI(
        root,
        add_medicine_callback=app_logic.add_medicine,
        remove_medicine_callback=app_logic.remove_medicine,
        display_medicines_callback=app_logic.display_medicines,
        display_without_prescription_callback=app_logic.display_without_prescription
    )
    
    root.mainloop()
