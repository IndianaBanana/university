# app.py

from model import Medicine

class MedicineManagerApp:
    def __init__(self):
        self.medicines_list = []

    def add_medicine(self, medicine):
        self.medicines_list.append(medicine)

    def remove_medicine(self, index):
        if 0 <= index < len(self.medicines_list):
            removed_medicine = self.medicines_list.pop(index)
            return removed_medicine
        return None

    def display_medicines(self):
        return self.medicines_list

    def display_without_prescription(self):
        return [medicine for medicine in self.medicines_list if not medicine.prescription_required]
