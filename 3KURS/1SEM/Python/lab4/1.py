import tkinter as tk
from tkinter import messagebox


class Medicine:
    def __init__(self, article, name, price, prescription_required, description):
        self.article = article
        self.name = name
        self.price = price
        self.prescription_required = prescription_required
        self.description = description

    def __str__(self):
        return (f"Article: {self.article}, Name: {self.name}, Price: {self.price}, "
                f"Prescription Required: {'Yes' if self.prescription_required else 'No'}, "
                f"Description: {self.description}")


class MedicineApp:
    def __init__(self, root):
        self.root = root
        self.root.title("Medicine Information System")

        self.medicines_list = []

        # Labels and Entry fields for input
        self.label_article = tk.Label(root, text="Article:")
        self.label_article.grid(row=0, column=0)
        self.entry_article = tk.Entry(root)
        self.entry_article.grid(row=0, column=1)

        self.label_name = tk.Label(root, text="Name:")
        self.label_name.grid(row=1, column=0)
        self.entry_name = tk.Entry(root)
        self.entry_name.grid(row=1, column=1)

        self.label_price = tk.Label(root, text="Price:")
        self.label_price.grid(row=2, column=0)
        self.entry_price = tk.Entry(root)
        self.entry_price.grid(row=2, column=1)

        self.label_description = tk.Label(root, text="Description:")
        self.label_description.grid(row=3, column=0)
        self.entry_description = tk.Entry(root)
        self.entry_description.grid(row=3, column=1)

        # Checkbox for prescription requirement
        self.prescription_required = tk.BooleanVar()
        self.check_prescription = tk.Checkbutton(root, text="Prescription required", variable=self.prescription_required)
        self.check_prescription.grid(row=4, columnspan=2)

        # Buttons
        self.button_add = tk.Button(root, text="Add Medicine", command=self.add_medicine)
        self.button_add.grid(row=5, column=0)

        # Dropdown menu for removing medicine
        self.selected_article = tk.StringVar(root)
        self.selected_article.set("-") 
        self.dropdown_remove = tk.OptionMenu(root, self.selected_article, "-")
        self.dropdown_remove.grid(row=5, column=1)
        
        self.button_remove = tk.Button(root, text="Remove Medicine", command=self.remove_medicine)
        self.button_remove.grid(row=5, column=2)

        # Button to display all medicines
        self.button_display_all = tk.Button(root, text="Display All Medicines", command=self.display_all_medicines)
        self.button_display_all.grid(row=6, column=0)

        # Button to display only non-prescription medicines
        self.button_display_prescription = tk.Button(root, text="Display Non Prescription Medicines", command=self.display_non_prescription_medicines)
        self.button_display_prescription.grid(row=6, column=1)

        # Listbox for displaying results
        self.medicine_listbox = tk.Listbox(root, width=80, height=10)
        self.medicine_listbox.grid(row=7, column=0, columnspan=3)

    def add_medicine(self):
        try:
            article = int(self.entry_article.get())
        except ValueError:
            messagebox.showerror("Input Error", "Invalid article format. Please enter a valid number.")
            return

        if any(medicine.article == article for medicine in self.medicines_list):
            messagebox.showerror("Duplicate Article", f"Medicine with article '{article}' already exists.")
            return

        name = self.entry_name.get()
        try:
            price = float(self.entry_price.get())
        except ValueError:
            messagebox.showerror("Input Error", "Invalid price format. Please enter a valid number.")
            return
        description = self.entry_description.get()
        prescription_required = self.prescription_required.get()

        if not article or not name or not description:
            messagebox.showerror("Input Error", "Please fill in all fields.")
            return

        medicine = Medicine(article, name, price, prescription_required, description)
        self.medicines_list.append(medicine)
        
        # Update dropdown menu options
        self.update_dropdown()
        messagebox.showinfo("Success", "Medicine added successfully!")

    def remove_medicine(self):
        try:
            article = int(self.selected_article.get())
        except ValueError:
            messagebox.showerror("Input Error", "Invalid article format. Please enter a valid number.")
            return
        for medicine in self.medicines_list:
            if medicine.article == article:
                self.medicines_list.remove(medicine)
                self.update_dropdown()
                self.selected_article.set("-")  # Reset selection after deletion
                messagebox.showinfo("Success", f"Medicine '{medicine.name}' removed.")
                return
        messagebox.showerror("Not Found", f"No medicine found with article '{article}'.")

    def display_all_medicines(self):
        # Clear the Listbox
        self.medicine_listbox.delete(0, tk.END)

        # Display all medicines
        if not self.medicines_list:
            self.medicine_listbox.insert(tk.END, "No medicines available.")
        else:
            for medicine in self.medicines_list:
                self.medicine_listbox.insert(tk.END, str(medicine))

    def display_non_prescription_medicines(self):
        # Clear the Listbox
        self.medicine_listbox.delete(0, tk.END)

        # Display only non-prescription medicines
        non_prescription_medicines = [med for med in self.medicines_list if not med.prescription_required]

        if not non_prescription_medicines:
            self.medicine_listbox.insert(tk.END, "No non-prescription medicines available.")
        else:
            for medicine in non_prescription_medicines:
                self.medicine_listbox.insert(tk.END, str(medicine))

    def update_dropdown(self):
        # Clear existing options
        menu = self.dropdown_remove["menu"]
        menu.delete(0, "end")

        # Add placeholder option
        menu.add_command(label="-", command=lambda: self.selected_article.set("-"))

        # Add new options to the dropdown
        for medicine in self.medicines_list:
            menu.add_command(label=medicine.article, command=lambda value=medicine.article: self.selected_article.set(value))


def main():
    root = tk.Tk()
    app = MedicineApp(root)
    root.mainloop()


if __name__ == "__main__":
    main()
