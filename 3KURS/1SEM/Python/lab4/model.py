# model.py

class Medicine:
    def __init__(self, article, name, price, prescription_required, description):
        self.article = article
        self.name = name
        self.price = price
        self.prescription_required = prescription_required
        self.description = description

    def __str__(self):
        return (f"{{ Article: {self.article}, Name: {self.name}, Price: {self.price}, "
                f"Prescription Required: {'Yes' if self.prescription_required else 'No'}, "
                f"Description: {self.description} }}")
