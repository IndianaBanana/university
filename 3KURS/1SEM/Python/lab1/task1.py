import math
try:
    x = float(input("Введите значение x: "))
    y = float(input("Введите значение y: "))
    n = float(input("Введите значение n: "))

    result = (math.sin(x**n + y**(1/n)) + (math.exp(x**4)/math.cos(y))**(1/3))**(1/5)

    print("Результат: ", result)
except ValueError:
    print("Ошибка во входных данных. Должны быть введены числа")
except ZeroDivisionError:
    print("Произошло деление на ноль")