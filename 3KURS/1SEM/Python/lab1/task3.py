import math

def calculate_area(choice):
    if choice == 'R':  # Площадь прямоугольного треугольника
        try:
            base = float(input("Введите основание треугольника: "))
            height = float(input("Введите высоту треугольника: "))
            area = 0.5 * base * height
            print(f"Площадь треугольника: {area}")
        except ValueError:
            print("Ошибка: введите числовые значения.")
    
    elif choice == 'T':  # Площадь трапеции
        try:
            a = float(input("Введите длину первого основания: "))
            b = float(input("Введите длину второго основания: "))
            height = float(input("Введите высоту трапеции: "))
            area = 0.5 * (a + b) * height
            print(f"Площадь трапеции: {area}")
        except ValueError:
            print("Ошибка: введите числовые значения.")
    
    elif choice == 'S':  # Площадь квадрата
        try:
            side = float(input("Введите длину стороны квадрата: "))
            area = side ** 2
            print(f"Площадь квадрата: {area}")
        except ValueError:
            print("Ошибка: введите числовое значение.")
while True:
    print("\nМеню программы:")
    print("R: Площадь прямоугольного треугольника")
    print("T: Площадь трапеции")
    print("S: Площадь квадрата")
    print("E: Выход")
    
    choice = input("Выберите действие: ").strip().upper()
    
    if choice == 'E':
        print("Выход из программы.")
        break
    
    elif choice in ['R', 'T', 'S']:
        calculate_area(choice)
    
    else:
        print("Некорректный ввод. Попробуйте еще раз.")