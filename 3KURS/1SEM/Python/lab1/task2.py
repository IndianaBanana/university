lst = [1, False, 2.3, "python", 4.0, "Hello World!"]
def convert_type(value):
    try:
        return int(value)
    except ValueError:
        pass
    try:
        return float(value)
    except ValueError:
        pass
    
    if value.lower() == "true":
        return True
    elif value.lower() == "false":
        return False
    return value

while True:
    print("\n1) Показать список.")
    print("2) Добавить элемент в конец списка.")
    print("3) Удалить элемент из списка.")
    print("4) Создать кортеж из элементов на нечетных позициях.")
    print("5) Найти произведение всех вещественных элементов.")
    print("6) Создать строку из элементов списка и посчитать арифметические операторы.")
    print("7) Операции с множествами (пересечение).")
    print("8) Преобразовать список в словарь с ключом меньше 3.")
    print("9) Выход.")
    
    choice = input("Выберите действие: ").strip()
    
    if choice == '1':
        print("Список:", lst)
    
    elif choice == '2':
        element = convert_type(input("Введите новый элемент: "))
        lst.append(element)
        print(f"Элемент '{element}' добавлен.")
    
    elif choice == '3':
        try:
            index = int(input("Введите индекс элемента для удаления: "))
            lst.pop(index)
            print("Элемент удален.")
        except ValueError:
            print("Ошибка: Введено некорректное значение.")
        except IndexError:
            print("Ошибка: Позиция за пределами списка.")
            
    elif choice == '4':
        odd_tuple = tuple(lst[i] for i in range(len(lst)) if i % 2 != 0)
        print("Кортеж с элементами на нечетных позициях:", odd_tuple)
    
    elif choice == '5':
        product = 1
        found = False
        for elem in lst:
            if isinstance(elem, (float)):
                product *= elem
                found = True
        if found:
            print("Произведение вещественных элементов:", product)
        else:
            print("Нет вещественных элементов в списке.")
    
    elif choice == '6':
        string_rep = ''.join(str(elem) for elem in lst)
        count = string_rep.count('+') + string_rep.count('-') + string_rep.count('*') + string_rep.count('/')
        print(f"Строка из элементов: {string_rep}")
        print(f"Количество арифметических операторов: {count}")
    
    elif choice == '7':
        M1 = set((input("Введите множество M1 через пробел: ").split()))
        M2 = set(map(str, lst))
        print("Пересечение множеств M1 и M2:", M1 & M2)
    
    elif choice == '8':
        dictionary = {i: lst[i] for i in range(len(lst))}
        for k, v in dictionary.items():
            if k < 3:
                print(f"Ключ: {k}, Значение: {v}")
    
    elif choice == '9':
        print("Выход из программы.")
        break
    
    else:
        print("Некорректный ввод. Попробуйте еще раз.")