import math

# Задача 1: Вычисление выражения
def calculate_expression(x, y, n):
    try:
        # Вычисление выражения
        result = (math.sin(x**n + y**(1/n)) + (math.exp(x**4)/math.cos(y))**(1/3))**(1/5)
        return result
    except ValueError as e:
        return f"Ошибка: {e}"  
    except ZeroDivisionError:
        return "Ошибка: Деление на ноль."

def task_1():
    while True:
        try:
            # Ввод данных с клавиатуры
            x = float(input("Введите значение x: "))
            y = float(input("Введите значение y: "))
            n = int(input("Введите значение n: "))
            
            # Вычисление выражения
            result = calculate_expression(x, y, n)
            print(f"Результат: {result}")
        except ValueError:
            print("Ошибка: введите корректные числовые значения.")
        
        # Повтор работы программы
        repeat = input("Хотите продолжить? (да/нет): ").strip().lower()
        if repeat != 'да':
            break


# Задача 2: Работа со списком
def task_2():
    lst = [1, "kok", 2.3, "kek", 4, "zhizha"]

    while True:
        print("\n1) Показать список.")
        print("2) Добавить элемент в конец списка.")
        print("3) Удалить элемент из списка.")
        print("4) Создать кортеж из элементов на нечетных позициях.")
        print("5) Найти произведение всех вещественных элементов.")
        print("6) Создать строку из элементов списка и посчитать арифметические операторы.")
        print("7) Операции с множествами.")
        print("8) Преобразовать список в словарь.")
        print("9) Выход.")
        
        choice = input("Выберите действие: ").strip()
        
        if choice == '1':
            print("Список:", lst)
        
        elif choice == '2':
            element = input("Введите новый элемент: ")
            lst.append(element)
            print(f"Элемент '{element}' добавлен.")
        
        elif choice == '3':
            try:
                index = int(input("Введите индекс элемента для удаления: "))
                lst.pop(index)
                print("Элемент удален.")
            except ValueError:
                print("Ошибка: Введено некорректное значение.")
            except IndexError:
                print("Ошибка: Позиция за пределами списка.")
                
        elif choice == '4':
            odd_tuple = tuple(lst[i] for i in range(len(lst)) if i % 2 != 0)
            print("Кортеж с элементами на нечетных позициях:", odd_tuple)
        
        elif choice == '5':
            product = 1
            found = False
            for elem in lst:
                if isinstance(elem, (float)):
                    product *= elem
                    found = True
            if found:
                print("Произведение вещественных элементов:", product)
            else:
                print("Нет вещественных элементов в списке.")
        
        elif choice == '6':
            string_rep = ''.join(str(elem) for elem in lst)
            count = string_rep.count('+') + string_rep.count('-') + string_rep.count('*') + string_rep.count('/')
            print(f"Строка из элементов: {string_rep}")
            print(f"Количество арифметических операторов: {count}")
        
        elif choice == '7':
            M1 = set(map(input("Введите множество M1 через пробел: ").split()))
            M2 = set(map(str, lst))
            print("Пересечение множеств M1 и M2:", M1 & M2)
        
        elif choice == '8':
            dictionary = {i: lst[i] for i in range(len(lst))}
            for k, v in dictionary.items():
                if k < 3:
                    print(f"Ключ: {k}, Значение: {v}")
        
        elif choice == '9':
            print("Выход из программы.")
            break
        
        else:
            print("Некорректный ввод. Попробуйте еще раз.")


# Задача 3: Площадь фигур
def calculate_area(choice):
    if choice == 'R':  # Площадь прямоугольного треугольника
        try:
            base = float(input("Введите основание треугольника: "))
            height = float(input("Введите высоту треугольника: "))
            area = 0.5 * base * height
            print(f"Площадь треугольника: {area}")
        except ValueError:
            print("Ошибка: введите числовые значения.")
    
    elif choice == 'T':  # Площадь трапеции
        try:
            a = float(input("Введите длину первого основания: "))
            b = float(input("Введите длину второго основания: "))
            height = float(input("Введите высоту трапеции: "))
            area = 0.5 * (a + b) * height
            print(f"Площадь трапеции: {area}")
        except ValueError:
            print("Ошибка: введите числовые значения.")
    
    elif choice == 'S':  # Площадь квадрата
        try:
            side = float(input("Введите длину стороны квадрата: "))
            area = side ** 2
            print(f"Площадь квадрата: {area}")
        except ValueError:
            print("Ошибка: введите числовое значение.")

def task_3():
    while True:
        print("\nМеню программы:")
        print("R: Площадь прямоугольного треугольника")
        print("T: Площадь трапеции")
        print("S: Площадь квадрата")
        print("E: Выход")
        
        choice = input("Выберите действие: ").strip().upper()
        
        if choice == 'E':
            print("Выход из программы.")
            break
        
        elif choice in ['R', 'T', 'S']:
            calculate_area(choice)
        
        else:
            print("Некорректный ввод. Попробуйте еще раз.")


# Main: Меню для выбора задачи
def main():
    while True:
        print("\nВыберите задачу:")
        print("1: Вычисление выражения (Задача 1)")
        print("2: Работа со списком (Задача 2)")
        print("3: Площадь фигур (Задача 3)")
        print("4: Выход")

        choice = input("Введите номер задачи: ").strip()

        if choice == '1':
            task_1()
        elif choice == '2':
            task_2()
        elif choice == '3':
            task_3()
        elif choice == '4':
            print("Завершение программы.")
            break
        else:
            print("Некорректный ввод. Попробуйте снова.")

if __name__ == "__main__":
    main()
