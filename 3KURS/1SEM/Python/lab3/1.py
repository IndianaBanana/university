class Medicine:
    def __init__(self, article, name, price, prescription_required, description):
        self.article = article
        self.name = name
        self.price = price
        self.prescription_required = prescription_required
        self.description = description

    def __str__(self):
        return (f"{{ Article: {self.article}, Name: {self.name}, Price: {self.price}, "
                f"Prescription Required: {'Yes' if self.prescription_required else 'No'}, "
                f"Description: {self.description} }}")


def main():
    medicines_list = []

    def is_article_unique(article):
        return all(medicine.article != article for medicine in medicines_list)

    while True:
        print("\nMenu:")
        print("1) Add medicine information")
        print("2) Remove medicine information by article")
        print("3) Display information about all medicines")
        print("4) Search for medicines sold without a prescription")
        print("5) Exit")

        choice = input("Select a menu option (1-5): ")

        if choice == '1':
            article = input("Enter article: ")

            # Check for unique article
            while not is_article_unique(article):
                print(f"Article '{article}' already exists. Please enter a unique article.")
                article = input("Enter article: ")

            name = input("Enter name: ")
            price = float(input("Enter price: "))
            prescription_required = input("Prescription required (y/n): ").strip().lower() == 'y'
            description = input("Enter description: ")

            medicine = Medicine(article, name, price, prescription_required, description)
            medicines_list.append(medicine)
            print("Medicine added!")

        elif choice == '2':
            if medicines_list:
                article_to_delete = input("Enter the article of the medicine to remove: ")
                for medicine in medicines_list:
                    if medicine.article == article_to_delete:
                        medicines_list.remove(medicine)
                        print(f"Medicine '{medicine.name}' with article '{medicine.article}' removed.")
                        break
                else:
                    print(f"No medicine found with article '{article_to_delete}'.")
            else:
                print("The list of medicines is empty.")

        elif choice == '3':
            if medicines_list:
                print("\nInformation about all medicines:")
                for medicine in medicines_list:
                    print(medicine)
            else:
                print("The list of medicines is empty.")

        elif choice == '4':
            print("\nMedicines without a prescription:")
            found = False
            for medicine in medicines_list:
                if not medicine.prescription_required:
                    print(medicine)
                    found = True
            if not found:
                print("No medicines without a prescription.")

        elif choice == '5':
            print("Exiting the program.")
            break

        else:
            print("Invalid choice. Please select a menu option from 1 to 5.")


if __name__ == "__main__":
    main()
