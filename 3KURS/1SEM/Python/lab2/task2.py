from itertools import takewhile

# Функции для вычисления площадей
def area_right_triangle(a, b):
    return 0.5 * a * b

def area_trapezoid(a, b, h):
    return 0.5 * (a + b) * h

def area_square(a):
    return a ** 2

figure_areas = {
    'R': lambda params: area_right_triangle(*params[:2]),
    'T': lambda params: area_trapezoid(*params[:3]),
    'S': lambda params: area_square(params[0])
}

def process_areas(symbols, values):
    valid_pairs = takewhile(lambda pair: pair[0] != 'E', zip(symbols, values))
    
    results = map(
        lambda sym, params: figure_areas[sym](params) if sym in figure_areas else None,
        *zip(*valid_pairs)
    )
    
    return list(filter(None, results))

L = [ ['R', 'S', 'T', 'T', 'E'], [3, 4], [2], [3, 4, 5], [3, 4, 5], [] ]

print(process_areas(L[0], L[1:]))