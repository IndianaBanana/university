%option noyywrap

%{
#include "parser.h"
#include <stdio.h>
#include <string.h>

void yyerror(const char *s);
%}

%%

"var" { return TOKEN_VAR; }
"begin" { return TOKEN_BEGIN; }
"end" { return TOKEN_END; }
"read" { return TOKEN_READ; }
"write" { return TOKEN_WRITE; }
"round" { return TOKEN_ROUND; }
"if" { return TOKEN_IF; }
"then" { return TOKEN_THEN; }
"else" { return TOKEN_ELSE; }
"and" { return TOKEN_AND; }
"or" { return TOKEN_OR; }

[0-9]+\.[0-9]+ { return TOKEN_REAL; }
[0-9]+ { return TOKEN_INTEGER; }
[a-zA-Z_][a-zA-Z0-9_]* { return TOKEN_IDENTIFIER; }
\"[^\"]*\" { return TOKEN_STRING; }

":=" { return TOKEN_ASSIGN; }
":" { return TOKEN_COLON; }
";" { return TOKEN_SEMICOLON; }
"=" { return TOKEN_EQUAL; }
"<" { return TOKEN_LESS; }
">" { return TOKEN_GREATER; }
"(" { return TOKEN_LPAREN; }
")" { return TOKEN_RPAREN; }
"+" { return TOKEN_PLUS; }
"-" { return TOKEN_MINUS; }
"*" { return TOKEN_MUL; }
"/" { return TOKEN_DIV; }

[ \t\r\n]+ {}

. { printf("Unknown character: %s\n", yytext); return TOKEN_INVALID; }

%%

