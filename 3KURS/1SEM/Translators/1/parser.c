
%option noyywrap

%{
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "ast.h"
#include "parser.h"
// #include <stdio.h>
// #include <stdlib.h>
// #include <string.h>
// #include "parser.h"
// #include <stdio.h>
// #include <string.h>

void yyerror(const char *s);
%}

%%

"var" { return TOKEN_VAR; }
"begin" { return TOKEN_BEGIN; }
"end" { return TOKEN_END; }
"read" { return TOKEN_READ; }
"write" { return TOKEN_WRITE; }
"round" { return TOKEN_ROUND; }
"if" { return TOKEN_IF; }
"then" { return TOKEN_THEN; }
"else" { return TOKEN_ELSE; }
"and" { return TOKEN_AND; }
"or" { return TOKEN_OR; }

[0-9]+\.[0-9]+ { return TOKEN_REAL; }
[0-9]+ { return TOKEN_INTEGER; }
[a-zA-Z_][a-zA-Z0-9_]* { return TOKEN_IDENTIFIER; }
\"[^\"]*\" { return TOKEN_STRING; }

":=" { return TOKEN_ASSIGN; }
":" { return TOKEN_COLON; }
";" { return TOKEN_SEMICOLON; }
"=" { return TOKEN_EQUAL; }
"<" { return TOKEN_LESS; }
">" { return TOKEN_GREATER; }
"(" { return TOKEN_LPAREN; }
")" { return TOKEN_RPAREN; }
"+" { return TOKEN_PLUS; }
"-" { return TOKEN_MINUS; }
"*" { return TOKEN_MUL; }
"/" { return TOKEN_DIV; }

[ \t\r\n]+ {}

. { printf("Unknown character: %s\n", yytext); return TOKEN_INVALID; }

%%


void printIndentation(int indent)
{
    for (int i = 0; i < indent; ++i)
    {
        printf("  ");
        printf("  ");
    }
}


void printProgram(Program *program, int indent);
void printDeclarations(Declarations *declarations, int indent);
void printCompoundStatement(CompoundStatement *compound, int indent);
void printStatement(Statement *statement, int indent);
void printSimpleStatement(SimpleStatement *simple, int indent);
void printArithmeticExpression(ArithmeticExpression *expression, int indent);
void printArithmeticTerm(ArithmeticTerm *term, int indent);
void printArithmeticOperation(ArithmeticOperation *operation, int indent);
void printConditionalStatement(ConditionalStatement *conditional, int indent);
void printConditionalExpression(ConditionalExpression *expression, int indent);
void printRelationalExpression(RelationalExpression *expression, int indent);
void printLogicalExpression(LogicalExpression *expression, int indent);
void printRelationalTerm(RelationalTerm *term, int indent);


void printProgram(Program *program, int indent)
{
    printIndentation(indent);
    printf("Program:\n");


    printIndentation(indent + 1);
    printf("Declarations:\n");
    printDeclarations(program->declarations, indent + 2);


    printIndentation(indent + 1);
    printf("Body:\n");
    printCompoundStatement(program->body, indent + 2);
}


void printDeclarations(Declarations *declarations, int indent)
{
    if (declarations == NULL)
    {
        return;
    }
    for (unsigned int i = 0; i < declarations->size; ++i)
    {
        printIndentation(indent);
        printf("Declaration: variable = %s, type = %s\n",
               declarations->declarations[i].variable, declarations->declarations[i].type);
    }
}


void printCompoundStatement(CompoundStatement *compound, int indent)
{
    for (unsigned int i = 0; i < compound->size; ++i)
    {
        printStatement(&compound->statements[i], indent);
    }
}


void printStatement(Statement *statement, int indent)
{
    printIndentation(indent);
    printf("Statement:\n");

    if (statement->type == StatementType_SIMPLE)
    {
        printSimpleStatement(statement->statement.simple, indent + 1);
    }
    else if (statement->type == StatementType_COMPOUND)
    {
        printCompoundStatement(statement->statement.compound, indent + 1);
    }
}


void printSimpleStatement(SimpleStatement *simple, int indent)
{
    printIndentation(indent);

    switch (simple->type)
    {
    case SimpleStatementType_ASSIGNMENT:
        printf("Assignment Statement:\n");
        printIndentation(indent + 1);
        printf("Variable: %s\n", simple->statement.assignment->variable);
        printArithmeticExpression(simple->statement.assignment->expression, indent + 1);
        break;

    case SimpleStatementType_READ:
        printf("Read Statement: variable = %s\n", simple->statement.read->variable);
        break;

    case SimpleStatementType_WRITE:
        printf("Write Statement:\n");
        printIndentation(indent + 1);
        if (simple->statement.write->argument->type == WriteArgumentType_IDENTIFIER)
        {
            printf("Identifier: %s\n", simple->statement.write->argument->argument.identifier);
        }
        else
        {
            printf("String: \"%s\"\n", simple->statement.write->argument->argument.string);
        }
        break;

    case SimpleStatementType_CONDITIONAL:
        printConditionalStatement(simple->statement.conditional, indent + 1);
        break;
    }
}


void printArithmeticExpression(ArithmeticExpression *expression, int indent)
{
    printIndentation(indent);
    printf("Arithmetic Expression:\n");

    if (expression->type == ArithmeticExpressionType_TERM)
    {
        printArithmeticTerm(expression->expression.term, indent + 1);
    }
    else if (expression->type == ArithmeticExpressionType_OPERATION)
    {
        printArithmeticOperation(expression->expression.operation, indent + 1);
    }
}


void printArithmeticTerm(ArithmeticTerm *term, int indent)
{
    printIndentation(indent);

    switch (term->type)
    {
    case ArithmeticTermType_IDENTIFIER:
        printf("Identifier: %s\n", term->term.identifier);
        break;
    case ArithmeticTermType_INTEGER:
        printf("Integer: %d\n", term->term.integer);
        break;
    case ArithmeticTermType_REAL:
        printf("Real: %lf\n", term->term.real);
        break;
    case ArithmeticTermType_ROUND:
        printf("Round Expression:\n");
        printArithmeticExpression(term->term.round->expression, indent + 1);
        break;
    }
}


void printArithmeticOperation(ArithmeticOperation *operation, int indent)
{
    printIndentation(indent);

    switch (operation->type)
    {
    case ArithmeticOperationType_ADDITION:
        printf("Addition Operation:\n");
        break;
    case ArithmeticOperationType_SUBTRACTION:
        printf("Subtraction Operation:\n");
        break;
    case ArithmeticOperationType_MULTIPLICATION:
        printf("Multiplication Operation:\n");
        break;
    case ArithmeticOperationType_DIVISION:
        printf("Division Operation:\n");
        break;
    }

    printIndentation(indent + 1);
    printf("Left Operand:\n");
    printArithmeticExpression(operation->left, indent + 2);

    printIndentation(indent + 1);
    printf("Right Operand:\n");
    printArithmeticExpression(operation->right, indent + 2);
}


void printConditionalStatement(ConditionalStatement *conditional, int indent) {
    printf("Conditional Statement:\n");
    

    printIndentation(indent);
    printf("Condition:\n");
    printConditionalExpression(conditional->condition, indent + 1);


    printIndentation(indent + 1);
    printf("Then Branch:\n");
    printStatement(conditional->then_branch, indent + 1);


    if (conditional->else_branch != NULL) {
        printIndentation(indent + 1);
        printf("Else Branch:\n");
        printStatement(conditional->else_branch, indent + 1);
    }
}


void printConditionalExpression(ConditionalExpression *expression, int indent) {
    if (expression->type == ConditionalExpressionType_RELATIONAL) {
        printRelationalExpression(expression->expression.relational, indent);
    }
    else if (expression->type == ConditionalExpressionType_LOGICAL)
    {
        printLogicalExpression(expression->expression.logical, indent);
    }
}


void printRelationalExpression(RelationalExpression *expression, int indent) {
    printIndentation(indent);
    printf("Relational Expression:\n");


    printIndentation(indent + 1);
    printf("Left Term:\n");
    printRelationalTerm(expression->left, indent + 2);


    printIndentation(indent + 1);
    printf("Operator: ");
    switch (expression->op)
    {
    case RelationalOperatorType_EQUAL:
        printf("==\n");
        break;
    case RelationalOperatorType_LESS:
        printf("<\n");
        break;
    case RelationalOperatorType_GREATER:
        printf(">\n");
        break;
    }


    printIndentation(indent + 1);
    printf("Right Term:\n");
    printRelationalTerm(expression->right, indent + 2);
}


void printLogicalExpression(LogicalExpression *expression, int indent) {
    printIndentation(indent);
    printf("Logical Expression:\n");


    printIndentation(indent + 1);
    printf("Left Condition:\n");
    printConditionalExpression(expression->left, indent + 2);


    printIndentation(indent + 1);
    printf("Operator: ");
    switch (expression->op)
    {
    case LogicalOperatorType_AND:
        printf("AND\n");
        break;
    case LogicalOperatorType_OR:
        printf("OR\n");
        break;
    }


    printIndentation(indent + 1);
    printf("Right Condition:\n");
    printConditionalExpression(expression->right, indent + 2);
}


void printRelationalTerm(RelationalTerm *term, int indent) {
    printIndentation(indent);

    switch (term->type)
    {
    case RelationalTermType_IDENTIFIER:
        printf("Identifier: %s\n", term->term.identifier);
        break;
    case RelationalTermType_INTEGER:
        printf("Integer: %d\n", term->term.integer);
        break;
    case RelationalTermType_REAL:
        printf("Real: %lf\n", term->term.real);
        break;
    }
}

extern int yylex();
extern char *yytext;
int currentToken;

void nextToken();
void error(const char *msg);
Program *program();
Declarations *declarations_part();
Declaration *declaration();
CompoundStatement *compound_statement();
Statement *statement();
SimpleStatement *simple_statement();
AssignmentStatement *assignment_statement();
ArithmeticExpression *arithmetic_expression();
RoundExpression *round_expression();
ArithmeticExpression *arithmetic_priority_1_term();
ArithmeticExpression *arithmetic_priority_2_term();
ReadStatement *read_statement();
WriteStatement *write_statement();
ConditionalStatement *conditional_statement();
ConditionalExpression *conditional_expression();
RelationalExpression *relational_expression();
RelationalTerm *relational_term();
LogicalExpression *logical_expression();
WriteArgument *write_argument();

const char *tokenToString(int token)
{
    switch (token)
    {
    case TOKEN_VAR:
        return "VAR";
    case TOKEN_BEGIN:
        return "BEGIN";
    case TOKEN_END:
        return "TERMINAL_EOF";
    case TOKEN_IDENTIFIER:
        return "TERMINAL_IDENTIFIER";
    case TOKEN_INTEGER:
        return "TERMINAL_KEYWORD_INTEGER";
    case TOKEN_REAL:
        return "REAL";
    case TOKEN_SEMICOLON:
        return "SEMICOLON";
    case TOKEN_COLON:
        return "COLON";
    case TOKEN_ASSIGN:
        return "ASSIGN";
    case TOKEN_PLUS:
        return "PLUS";
    case TOKEN_MINUS:
        return "MIN";
    case TOKEN_MUL:
        return "MUL";
    case TOKEN_DIV:
        return "DIV";
    case TOKEN_LPAREN:
        return "TERMINAL_LEFT_PARENTHESIS";
    case TOKEN_RPAREN:
        return "TERMINAL_RIGHT_PARENTHESIS";
    case TOKEN_READ:
        return "READ";
    case TOKEN_WRITE:
        return "WRITE";
    case TOKEN_ROUND:
        return "ROUND";
    case TOKEN_IF:
        return "IF";
    case TOKEN_THEN:
        return "THEN";
    case TOKEN_ELSE:
        return "ELSE";
    case TOKEN_EQUAL:
        return "EQUAL";
    case TOKEN_LESS:
        return "LESS";
    case TOKEN_GREATER:
        return "GREATER";
    case TOKEN_AND:
        return "AND";
    case TOKEN_OR:
        return "OR";
    case TOKEN_STRING:
        return "STRING";
    case TOKEN_EOF:
        return "EOF";
    case TOKEN_INVALID:
        return "INVALID";
    default:
        return "UNKNOWN";
    }
}

void error_unexpected(int expected)
{
    printf("Error: Unexpected token '%s', expected '%s'\n", yytext, tokenToString(expected));
    exit(1);
}

void match(int expected)
{
    if (currentToken == expected)
    {
        printf("%s, ", tokenToString(currentToken));
        nextToken();
    }
    else
    {
        error_unexpected(expected);
    }
}

void nextToken()
{
    currentToken = yylex();
}

void error(const char *msg)
{
    printf("Error: %s at token '%s'\n", msg, yytext);
    exit(1);
}

Program *program()
{
    Program *prog = malloc(sizeof(Program));
    prog->declarations = NULL;
    if (currentToken == TOKEN_VAR)
    {
        prog->declarations = declarations_part();
    }
    prog->body = compound_statement();
    return prog;
}

Declarations *declarations_part()
{
    Declarations *decls = malloc(sizeof(Declarations));
    decls->size = 0;
    decls->declarations = malloc(sizeof(Declaration) * 100);

    match(TOKEN_VAR);
    decls->declarations[decls->size++] = *declaration();
    match(TOKEN_SEMICOLON);
    while (currentToken == TOKEN_IDENTIFIER)
    {
        decls->declarations[decls->size++] = *declaration();
        match(TOKEN_SEMICOLON);
    }

    return decls;
}

Declaration *declaration()
{
    Declaration *decl = malloc(sizeof(Declaration));
    decl->variable = strdup(yytext);
    match(TOKEN_IDENTIFIER);
    match(TOKEN_COLON);
    decl->type = strdup(yytext);
    match(TOKEN_IDENTIFIER);
    return decl;
}

CompoundStatement *compound_statement()
{
    CompoundStatement *compound = malloc(sizeof(CompoundStatement));
    compound->size = 0;
    compound->statements = malloc(sizeof(Statement) * 100);

    match(TOKEN_BEGIN);
    compound->statements[compound->size++] = *statement();
    match(TOKEN_SEMICOLON);
    while (currentToken == TOKEN_IDENTIFIER || currentToken == TOKEN_READ ||
           currentToken == TOKEN_WRITE || currentToken == TOKEN_IF)
    {
        compound->statements[compound->size++] = *statement();
        match(TOKEN_SEMICOLON);
    }
    match(TOKEN_END);

    return compound;
}

Statement *statement()
{
    Statement *stmt = malloc(sizeof(Statement));

    if (currentToken == TOKEN_BEGIN)
    {
        stmt->type = StatementType_COMPOUND;
        stmt->statement.compound = compound_statement();
    }
    else
    {
        stmt->type = StatementType_SIMPLE;
        stmt->statement.simple = simple_statement();
    }

    return stmt;
}

SimpleStatement *simple_statement()
{
    SimpleStatement *simpleStmt = malloc(sizeof(SimpleStatement));

    if (currentToken == TOKEN_IDENTIFIER)
    {
        simpleStmt->type = SimpleStatementType_ASSIGNMENT;
        simpleStmt->statement.assignment = assignment_statement();
    }
    else if (currentToken == TOKEN_READ)
    {
        simpleStmt->type = SimpleStatementType_READ;
        simpleStmt->statement.read = read_statement();
    }
    else if (currentToken == TOKEN_WRITE)
    {
        simpleStmt->type = SimpleStatementType_WRITE;
        simpleStmt->statement.write = write_statement();
    }
    else if (currentToken == TOKEN_IF)
    {
        simpleStmt->type = SimpleStatementType_CONDITIONAL;
        simpleStmt->statement.conditional = conditional_statement();
    }
    else
    {
        error("Expected simple statement");
    }

    return simpleStmt;
}

AssignmentStatement *assignment_statement()
{
    AssignmentStatement *assign = malloc(sizeof(AssignmentStatement));
    assign->variable = strdup(yytext);
    match(TOKEN_IDENTIFIER);
    match(TOKEN_ASSIGN);
    assign->expression = arithmetic_expression();
    return assign;
}

RoundExpression *round_expression()
{
    RoundExpression *round = malloc(sizeof(RoundExpression));
    match(TOKEN_ROUND);
    match(TOKEN_LPAREN);
    round->expression = arithmetic_expression();
    match(TOKEN_RPAREN);
    return round;
}

ArithmeticExpression *arithmetic_expression()
{
    ArithmeticExpression *left = arithmetic_priority_1_term();
    while (currentToken == TOKEN_PLUS || currentToken == TOKEN_MINUS)
    {
        ArithmeticExpression *new_left = malloc(sizeof(ArithmeticExpression));
        new_left->type = ArithmeticExpressionType_OPERATION;

        ArithmeticOperation *op = malloc(sizeof(ArithmeticOperation));
        if (currentToken == TOKEN_PLUS)
        {
            op->type = ArithmeticOperationType_ADDITION;
            match(TOKEN_PLUS);
        }
        else if (currentToken == TOKEN_MINUS)
        {
            op->type = ArithmeticOperationType_SUBTRACTION;
            match(TOKEN_MINUS);
        }
        op->left = left;
        op->right = arithmetic_priority_1_term();
        new_left->expression.operation = op;
        left = new_left;
    }
    return left;
}

ArithmeticExpression *arithmetic_priority_1_term()
{
    ArithmeticExpression *left = arithmetic_priority_2_term();
    while (currentToken == TOKEN_MUL || currentToken == TOKEN_DIV)
    {
        ArithmeticExpression *new_left = malloc(sizeof(ArithmeticExpression));
        new_left->type = ArithmeticExpressionType_OPERATION;

        ArithmeticOperation *op = malloc(sizeof(ArithmeticOperation));
        if (currentToken == TOKEN_MUL)
        {
            op->type = ArithmeticOperationType_MULTIPLICATION;
            match(TOKEN_MUL);
        }
        else if (currentToken == TOKEN_DIV)
        {
            op->type = ArithmeticOperationType_DIVISION;
            match(TOKEN_DIV);
        }
        op->left = left;
        op->right = arithmetic_priority_2_term();
        new_left->expression.operation = op;
        left = new_left;
    }
    return left;
}

ArithmeticExpression *arithmetic_priority_2_term()
{
    if (currentToken == TOKEN_LPAREN)
    {
        match(TOKEN_LPAREN);
        ArithmeticExpression *expr = arithmetic_expression();
        match(TOKEN_RPAREN);
        return expr;
    }
    ArithmeticTerm *term = malloc(sizeof(ArithmeticTerm));
    if (currentToken == TOKEN_IDENTIFIER)
    {
        term->type = ArithmeticTermType_IDENTIFIER;
        term->term.identifier = strdup(yytext);
        match(TOKEN_IDENTIFIER);
    }
    else if (currentToken == TOKEN_INTEGER)
    {
        term->type = ArithmeticTermType_INTEGER;
        term->term.integer = atoi(yytext);
        match(TOKEN_INTEGER);
    }
    else if (currentToken == TOKEN_REAL)
    {
        term->type = ArithmeticTermType_REAL;
        term->term.real = atof(yytext);
        match(TOKEN_REAL);
    }
    else if (currentToken == TOKEN_ROUND)
    {
        term->type = ArithmeticTermType_ROUND;
        term->term.round = round_expression();
    }
    else
    {
        error("Expected term");
    }
    ArithmeticExpression *expr = malloc(sizeof(ArithmeticExpression));
    expr->type = ArithmeticExpressionType_TERM;
    expr->expression.term = term;
    return expr;
}

ReadStatement *read_statement()
{
    ReadStatement *readStmt = malloc(sizeof(ReadStatement));
    match(TOKEN_READ);
    match(TOKEN_LPAREN);
    readStmt->variable = strdup(yytext);
    match(TOKEN_IDENTIFIER);
    match(TOKEN_RPAREN);
    return readStmt;
}

WriteStatement *write_statement()
{
    WriteStatement *writeStmt = malloc(sizeof(WriteStatement));
    match(TOKEN_WRITE);
    match(TOKEN_LPAREN);
    writeStmt->argument = write_argument();
    match(TOKEN_RPAREN);
    return writeStmt;
}

WriteArgument *write_argument()
{
    WriteArgument *arg = malloc(sizeof(WriteArgument));

    if (currentToken == TOKEN_IDENTIFIER)
    {
        arg->type = WriteArgumentType_IDENTIFIER;
        arg->argument.identifier = strdup(yytext);
        match(TOKEN_IDENTIFIER);
    }
    else if (currentToken == TOKEN_STRING)
    {
        arg->type = WriteArgumentType_STRING;
        arg->argument.string = strdup(yytext);
        arg->argument.string[strlen(arg->argument.string) - 1] = '\0';
        arg->argument.string++;
        match(TOKEN_STRING);
    }
    else
    {
        error("Expected write argument");
    }

    return arg;
}

ConditionalStatement *conditional_statement()
{
    ConditionalStatement *condStmt = malloc(sizeof(ConditionalStatement));

    match(TOKEN_IF);
    condStmt->condition = conditional_expression();
    match(TOKEN_THEN);
    condStmt->then_branch = statement();
    if (currentToken == TOKEN_ELSE)
    {
        match(TOKEN_ELSE);
        condStmt->else_branch = statement();
    }
    else
    {
        condStmt->else_branch = NULL;
    }

    return condStmt;
}

ConditionalExpression *conditional_expression()
{
    ConditionalExpression *expr = malloc(sizeof(ConditionalExpression));

    if (currentToken == TOKEN_IDENTIFIER || currentToken == TOKEN_INTEGER || currentToken == TOKEN_REAL)
    {
        expr->type = ConditionalExpressionType_RELATIONAL;
        expr->expression.relational = relational_expression();
    }
    else
    {
        expr->type = ConditionalExpressionType_LOGICAL;
        expr->expression.logical = logical_expression();
    }
    return expr;
}

RelationalExpression *relational_expression()
{
    RelationalExpression *relExpr = malloc(sizeof(RelationalExpression));
    relExpr->left = relational_term();
    if (currentToken == TOKEN_EQUAL)
    {
        relExpr->op = RelationalOperatorType_EQUAL;
        match(TOKEN_EQUAL);
    }
    else if (currentToken == TOKEN_LESS)
    {
        relExpr->op = RelationalOperatorType_LESS;
        match(TOKEN_LESS);
    }
    else if (currentToken == TOKEN_GREATER)
    {
        relExpr->op = RelationalOperatorType_GREATER;
        match(TOKEN_GREATER);
    }
    relExpr->right = relational_term();
    return relExpr;
}

RelationalTerm *relational_term()
{
    RelationalTerm *relTerm = malloc(sizeof(RelationalTerm));
    if (currentToken == TOKEN_IDENTIFIER)
    {
        relTerm->type = RelationalTermType_IDENTIFIER;
        relTerm->term.identifier = strdup(yytext);
        match(TOKEN_IDENTIFIER);
    }
    else if (currentToken == TOKEN_INTEGER)
    {
        relTerm->type = RelationalTermType_INTEGER;
        relTerm->term.integer = atoi(yytext);
        match(TOKEN_INTEGER);
    }
    else if (currentToken == TOKEN_REAL)
    {
        relTerm->type = RelationalTermType_REAL;
        relTerm->term.real = atof(yytext);
        match(TOKEN_REAL);
    }
    else
    {
        error("Expected relational term");
    }
    return relTerm;
}

LogicalExpression *logical_expression()
{
    match(TOKEN_LPAREN);
    LogicalExpression *logExpr = malloc(sizeof(LogicalExpression));
    logExpr->left = conditional_expression();
    match(TOKEN_RPAREN);
    if (currentToken == TOKEN_AND)
    {
        logExpr->op = LogicalOperatorType_AND;
        match(TOKEN_AND);
    }
    else if (currentToken == TOKEN_OR)
    {
        logExpr->op = LogicalOperatorType_OR;
        match(TOKEN_OR);
    }
    match(TOKEN_LPAREN);
    logExpr->right = conditional_expression();
    match(TOKEN_RPAREN);
    return logExpr;
}

int main()
{
    yyin = fopen("example.txt", "r");
    nextToken();
    Program *ast = program();
    printf("Parsing successful.\n\n");
    printProgram(ast, 0);
    return 0;
}
