#ifndef AST_H
#define AST_H

typedef struct Program Program;
typedef struct Declarations Declarations;
typedef struct Declaration Declaration;
typedef struct CompoundStatement CompoundStatement;
typedef struct Statement Statement;
typedef struct SimpleStatement SimpleStatement;
typedef struct AssignmentStatement AssignmentStatement;
typedef struct ArithmeticExpression ArithmeticExpression;
typedef struct RoundExpression RoundExpression;
typedef struct ArithmeticTerm ArithmeticTerm;
typedef struct ArithmeticOperation ArithmeticOperation;
typedef struct ReadStatement ReadStatement;
typedef struct RelationalTerm RelationalTerm;
typedef struct WriteStatement WriteStatement;
typedef struct WriteArgument WriteArgument;
typedef struct ConditionalStatement ConditionalStatement;
typedef struct ConditionalExpression ConditionalExpression;
typedef struct RelationalExpression RelationalExpression;
typedef struct LogicalExpression LogicalExpression;
typedef enum RelationalOperator RelationalOperator;
typedef enum LogicalOperator LogicalOperator;
typedef enum SimpleArithmeticExpressionType SimpleArithmeticExpressionType;
typedef enum ArithmeticExpressionType ArithmeticExpressionType;
typedef enum ArithmeticTermType ArithmeticTermType;
typedef enum ArithmeticOperationType ArithmeticOperationType;
typedef enum SimpleStatementType SimpleStatementType;
typedef enum StatementType StatementType;
typedef enum ConditionalExpressionType ConditionalExpressionType;
typedef enum WriteArgumentType WriteArgumentType;
typedef enum RelationalTermType RelationalTermType;
typedef char *Identifier;

typedef struct Program
{
    Declarations *declarations;
    CompoundStatement *body;
} Program;

struct Declarations
{
    unsigned int size;
    Declaration *declarations;
};

struct Declaration
{
    Identifier variable;
    Identifier type;
};

struct CompoundStatement
{
    unsigned int size;
    Statement *statements;
};

enum StatementType
{
    StatementType_SIMPLE,
    StatementType_COMPOUND
};

struct Statement
{
    StatementType type;
    union
    {
        SimpleStatement *simple;
        CompoundStatement *compound;
    } statement;
};

enum SimpleStatementType
{
    SimpleStatementType_ASSIGNMENT,
    SimpleStatementType_READ,
    SimpleStatementType_WRITE,
    SimpleStatementType_CONDITIONAL
};

struct SimpleStatement
{
    SimpleStatementType type;
    union
    {
        AssignmentStatement *assignment;
        ReadStatement *read;
        WriteStatement *write;
        ConditionalStatement *conditional;
    } statement;
};

struct AssignmentStatement
{
    Identifier variable;
    ArithmeticExpression *expression;
};

struct RoundExpression
{
    ArithmeticExpression *expression;
};

enum ArithmeticExpressionType
{
    ArithmeticExpressionType_TERM,
    ArithmeticExpressionType_OPERATION
};

struct ArithmeticExpression
{
    ArithmeticExpressionType type;
    union
    {
        ArithmeticTerm *term;
        ArithmeticOperation *operation;
    } expression;
};

enum ArithmeticTermType
{
    ArithmeticTermType_IDENTIFIER,
    ArithmeticTermType_INTEGER,
    ArithmeticTermType_REAL,
    ArithmeticTermType_ROUND,
};

struct ArithmeticTerm
{
    ArithmeticTermType type;
    union
    {
        Identifier identifier;
        int integer;
        double real;
        RoundExpression *round;
    } term;
};

enum ArithmeticOperationType
{
    ArithmeticOperationType_ADDITION,
    ArithmeticOperationType_SUBTRACTION,
    ArithmeticOperationType_MULTIPLICATION,
    ArithmeticOperationType_DIVISION
};

struct ArithmeticOperation
{
    ArithmeticOperationType type;
    ArithmeticExpression *left;
    ArithmeticExpression *right;
};

struct ReadStatement
{
    Identifier variable;
};

struct WriteStatement
{
    WriteArgument *argument;
};

enum WriteArgumentType
{
    WriteArgumentType_IDENTIFIER,
    WriteArgumentType_STRING
};

struct WriteArgument
{
    WriteArgumentType type;
    union
    {
        Identifier identifier;
        char *string;
    } argument;
};

struct ConditionalStatement
{
    ConditionalExpression *condition;
    Statement *then_branch;
    Statement *else_branch;
};

enum ConditionalExpressionType
{
    ConditionalExpressionType_RELATIONAL,
    ConditionalExpressionType_LOGICAL
};

struct ConditionalExpression
{
    ConditionalExpressionType type;
    union
    {
        RelationalExpression *relational;
        LogicalExpression *logical;
    } expression;
};

enum RelationalOperator
{
    RelationalOperatorType_EQUAL,
    RelationalOperatorType_LESS,
    RelationalOperatorType_GREATER
};

enum RelationalTermType
{
    RelationalTermType_IDENTIFIER,
    RelationalTermType_INTEGER,
    RelationalTermType_REAL
};

struct RelationalTerm
{
    RelationalTermType type;
    union
    {
        Identifier identifier;
        int integer;
        double real;
    } term;
};

struct RelationalExpression
{
    RelationalTerm *left;
    RelationalTerm *right;
    RelationalOperator op;
};

enum LogicalOperator
{
    LogicalOperatorType_AND,
    LogicalOperatorType_OR
};

struct LogicalExpression
{
    ConditionalExpression *left;
    ConditionalExpression *right;
    LogicalOperator op;
};

#endif
