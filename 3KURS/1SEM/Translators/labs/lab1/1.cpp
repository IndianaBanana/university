#include <iostream>
#include <string>
#include <algorithm>
#include <map>

using namespace std;

string lex_val="";
string entry;

enum class input_signal {
    digit = 0,
    minus,
    point,
    unknown,
    endf,
};

using transitionTable = std::map<std::tuple<int8_t, input_signal>, int8_t>;

transitionTable transition_table{  
      {{0, input_signal::minus}, 1},
      {{0, input_signal::point}, 3},
      {{0, input_signal::digit}, 2},
      {{1, input_signal::point}, 3},
      {{1, input_signal::digit}, 2},
      {{2, input_signal::digit}, 2},
      {{2, input_signal::point}, 4},
      {{3, input_signal::digit}, 4},
      {{4, input_signal::digit}, 4}      
    };

input_signal recognize() {

    input_signal result;
 
    switch (entry[0])
    {
    case '.':
    {
        result = input_signal::point;
    }
    break;
    case '-':
    {
        result = input_signal::minus;
    }
    break;
    case '0':
    case '1':
    case '2':
    case '3':
    case '4':
    case '5':
    case '6':
    case '7':
    case '8':
    case '9':
    {
        result = input_signal::digit;
    }
    break;
    case '\0':
        result = input_signal::endf;
        break;
    default:
        result = input_signal::unknown;
        break;
    }

    lex_val += entry[0];
    entry.erase(0, 1);

    return result;
}

string state_machine() {

    int8_t cur_state=0;
    input_signal cur_input = input_signal::unknown;

    while (cur_input != input_signal::endf)
    {
        cur_input = recognize();
        if (cur_input == input_signal::unknown)
        {            
            return "������";
        }
        else {
            if (cur_input != input_signal::endf)
            {
                cur_state = transition_table.at({ cur_state, cur_input });
            }
        }
    }
    if (cur_state != 4) {
        return "������";
    }
    return "letter";
}

void main() {
	
    setlocale(LC_ALL, "Russian");

    cout << "�������: ";
	cin >> entry;

    string rezult = state_machine();
    if (rezult == "letter") {       
        cout << "����� " << lex_val;
    }
    else {
        cout << "����������� ������";
    }	
}