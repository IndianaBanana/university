%option noyywrap

%{
    #include <iostream>
    #include <cstdint>
    #include <deque>

    enum Token {
        TOKEN_ERROR = -2, TOKEN_END_OF_FILE = -1, TOKEN_TYPE_INTEGER,
        TOKEN_TYPE_FLOAT, TOKEN_IDENTIFICATOR, TOKEN_INTEGRAL,
        TOKEN_DECIMAL, TOKEN_ASSIGNMENT, TOKEN_MATH_OP,
        TOKEN_KWRD_VAR, TOKEN_KWRD_IF, TOKEN_KWRD_THEN,
        TOKEN_KWRD_ELSE, TOKEN_KWRD_BEGIN, TOKEN_KWRD_END, TOKEN_LOGIC_OP,
        TOKEN_LOGIC_CMP, TOKEN_CMD_ROUND, TOKEN_CMD_READ,
        TOKEN_CMD_WRITE, TOKEN_COLON, TOKEN_LEFT_PARENTHESIS,
        TOKEN_RIGHT_PARENTHESIS, TOKEN_STRING
    };

    class Node {
        std::string name_;
        std::deque<Node*> next_;
    public:
        Node(std::string name) { name_ = name; }
        void setName(std::string name) { name_ = name; }
        friend class Tree;
    };

    class Tree {
        Node* head_;
    public:
        void print(Node* node, uint8_t level);
        void print();
        Node* addChild(Node* nodeParent, Node* node);
        void setHead(Node* node);
    };

    void Tree::setHead(Node* node) { head_ = node; }
    void Tree::print() { print(head_, 0); }

    void Tree::print(Node* node, uint8_t level) {
        if (node) {
            for (int i = 0; i < level; ++i) {
                std::cout << "\t";
            }
            std::cout << node->name_ << std::endl;
            for (Node* nodes : node->next_) {
                print(nodes, level + 1);
            }
        }
    }

    Node* Tree::addChild(Node* nodeParent, Node* node) {
        if (node) {
            nodeParent->next_.push_back(node);
            return nodeParent;
        }
        return 0;
    }

    char yylval[256];
    int tokk;
    Node* node;
    Tree tree;

%}

%%

"var" {memcpy(yylval, yytext, strlen(yytext) + 1); return TOKEN_KWRD_VAR; }
"if" {memcpy(yylval, yytext, strlen(yytext) + 1); return TOKEN_KWRD_IF; }
"then" {memcpy(yylval, yytext, strlen(yytext) + 1); return TOKEN_KWRD_THEN; }
"else" {memcpy(yylval, yytext, strlen(yytext) + 1); return TOKEN_KWRD_ELSE; }
"begin" {memcpy(yylval, yytext, strlen(yytext) + 1); return TOKEN_KWRD_BEGIN; }
"end" {memcpy(yylval, yytext, strlen(yytext) + 1); return TOKEN_KWRD_END; }
"or"|"and" {memcpy(yylval, yytext, strlen(yytext) + 1); return TOKEN_LOGIC_OP; }
"round" {memcpy(yylval, yytext, strlen(yytext) + 1); return TOKEN_CMD_ROUND; }
"read" {memcpy(yylval, yytext, strlen(yytext) + 1); return TOKEN_CMD_READ; }
"write" {memcpy(yylval, yytext, strlen(yytext) + 1); return TOKEN_CMD_WRITE; }
"integer" {memcpy(yylval, yytext, strlen(yytext) + 1); return TOKEN_TYPE_INTEGER; }
"float" {memcpy(yylval, yytext, strlen(yytext) + 1); return TOKEN_TYPE_FLOAT; }
[a-zA-z][a-zA-z|0-9]* { memcpy(yylval, yytext, strlen(yytext) + 1); return TOKEN_IDENTIFICATOR; }
[0-9]+ {memcpy(yylval, yytext, strlen(yytext) + 1); return TOKEN_INTEGRAL; }
[0-9]+"."[0-9]+ {memcpy(yylval, yytext, strlen(yytext) + 1); return TOKEN_DECIMAL; }
":=" {memcpy(yylval, yytext, strlen(yytext) + 1); return TOKEN_ASSIGNMENT; }
"-"|"+"|"*"|"/" {memcpy(yylval, yytext, strlen(yytext) + 1); return TOKEN_MATH_OP; }
"<"|">"|"=" {memcpy(yylval, yytext, strlen(yytext) + 1); return TOKEN_LOGIC_CMP; }
":" {memcpy(yylval, yytext, strlen(yytext) + 1); return TOKEN_COLON; }
"(" {memcpy(yylval, yytext, strlen(yytext) + 1); return TOKEN_LEFT_PARENTHESIS; }
")" {memcpy(yylval, yytext, strlen(yytext) + 1); return TOKEN_RIGHT_PARENTHESIS; }
"'".*"'" {memcpy(yylval, yytext, strlen(yytext) + 1); return TOKEN_STRING; }

[ \t\n\r ]

<<EOF>> { return TOKEN_END_OF_FILE; }
. { return TOKEN_ERROR; }

%%

Node* is_statement(Node* node);

Node* is_T() {
    if (tokk == TOKEN_IDENTIFICATOR || tokk == TOKEN_INTEGRAL || tokk == TOKEN_DECIMAL) {
        return new Node(std::string{yylval});
    }
    return 0;
}

bool is_param(Node* node) {
    Node* t;
    Node* cmp = new Node("");
    t = is_T(); 
    if (tree.addChild(cmp, t)) {
        tokk = yylex();
        if (tokk == TOKEN_LOGIC_CMP) {
            cmp->setName(std::string{yylval});
            tree.addChild(node, cmp);
            tokk = yylex();
            if (tree.addChild(cmp, is_T())) {
                return true;
            }
        }
        return false;
    }

    if (tokk != TOKEN_LEFT_PARENTHESIS) return false;
    tokk = yylex();

    cmp = new Node("");
    is_param(cmp);
    tokk = yylex();
    
    if (tokk == TOKEN_RIGHT_PARENTHESIS) {
        tokk = yylex();
        if (tokk == TOKEN_LOGIC_OP) {
            cmp->setName(std::string{yylval});
            tokk = yylex();
            if (tokk != TOKEN_LEFT_PARENTHESIS) return false;
            tokk = yylex();
            is_param(cmp);
            tokk = yylex();
            if (tokk == TOKEN_RIGHT_PARENTHESIS) {
                tree.addChild(node, cmp);
                return true;
            }
        }
    }
    return false;
}

Node* is_condition(Node* node) {
    if (tokk != TOKEN_KWRD_IF) return 0;
    tokk = yylex();
    if (tokk != TOKEN_LEFT_PARENTHESIS) return 0;
    tokk = yylex();
    if (!is_param(node)) return 0;
    tokk = yylex();
    if (tokk != TOKEN_RIGHT_PARENTHESIS) return 0;
    tokk = yylex();
    if (tokk != TOKEN_KWRD_THEN) return 0;
    tokk = yylex();
    if (tokk != TOKEN_KWRD_BEGIN) return 0;
    tokk = yylex();
    if (!tree.addChild(node, is_statement(new Node("STATEMENT_THEN")))) return 0;

    if (tokk != TOKEN_KWRD_END) return 0;
    tokk = yylex();
    if (tokk != TOKEN_KWRD_ELSE) {
        return node;
    }
    tokk = yylex();
    if (tokk != TOKEN_KWRD_BEGIN) return 0;
    tokk = yylex();
    if (!tree.addChild(node, is_statement(new Node("STATEMENT_ELSE")))) return 0;
    if (tokk == TOKEN_KWRD_END) {
        tokk = yylex();
        return node;
    }
    return 0;
}

Node* is_write(Node* node) {
    if (tokk != TOKEN_CMD_WRITE) return 0;
    tokk = yylex();
    if (tokk == TOKEN_LEFT_PARENTHESIS) {
        tokk = yylex();
        if (tokk == TOKEN_IDENTIFICATOR || tokk == TOKEN_STRING) {
            tree.addChild(node, new Node(std::string{yylval}));
            tokk = yylex();
            if (tokk == TOKEN_RIGHT_PARENTHESIS) {
                tokk = yylex();
                return node;
            }
        }
    }
    return 0;
}

Node* is_read(Node* node) {
    if (tokk != TOKEN_CMD_READ) return 0;
    tokk = yylex();
    if (tokk == TOKEN_LEFT_PARENTHESIS) {
        tokk = yylex();
        if (tokk == TOKEN_IDENTIFICATOR) {
            tree.addChild(node, new Node(std::string{yylval}));
            tokk = yylex();
            if (tokk == TOKEN_RIGHT_PARENTHESIS) {
                tokk = yylex();
                return node;
            }
        }
    }
    return 0;
}

Node* is_expr() {
    Node* op = new Node("");
    Node* t = is_T();
    if (t) {
        tokk = yylex();
        tree.addChild(op, t);
        if (tokk == TOKEN_MATH_OP) {
            op->setName(std::string{yylval});
            tokk = yylex();
            t = is_expr();
            if (t) {
                tree.addChild(op, t);
                return op;
            }
        }
        else if (tokk != TOKEN_END_OF_FILE && tokk != TOKEN_ERROR) {
            return t;
        }
    }
    else {
        Node* round = new Node("round");
        if (tokk == TOKEN_CMD_ROUND) {
            tokk = yylex();
            if (tokk == TOKEN_LEFT_PARENTHESIS) {
                tokk = yylex();
                t = is_T();
                if (t) {
                    tree.addChild(round, t);
                    tokk = yylex();
                    if (tokk == TOKEN_RIGHT_PARENTHESIS) {
                        tokk = yylex();
                        return round;
                    }
                }
            }
        }
    }
    return 0;
}

Node* is_assignment(Node* node) {
    if (tokk != TOKEN_IDENTIFICATOR) return 0;
    tree.addChild(node, new Node(std::string{yylval}));
    tokk = yylex();
    if (tokk != TOKEN_ASSIGNMENT) return 0;
    tokk = yylex();
    return tree.addChild(node, is_expr());
}

Node* is_statement(Node* node) {
    if (tokk == TOKEN_KWRD_END) { return node; }
    if (tokk == TOKEN_ERROR || tokk == TOKEN_END_OF_FILE) { return 0; }

    Node* nodeStat = is_assignment(new Node("ASSIGMENT"));
    if (nodeStat) {
        return tree.addChild(node, nodeStat);
    }
    nodeStat = is_read(new Node("READ"));
    if (nodeStat) {
        return tree.addChild(node, nodeStat);
    }
    nodeStat = is_write(new Node("WRITE"));
    if (nodeStat) {
        return tree.addChild(node, nodeStat);
    }
    nodeStat = is_condition(new Node("CONDITION"));
    if (nodeStat) {
        return tree.addChild(node, nodeStat);
    }
    return 0;
}

Node* is_command_section(Node* node) {
    if (tokk != TOKEN_KWRD_BEGIN) return 0;
    tokk = yylex();
    do {
        node = is_statement(node);
    } while (node && tokk != TOKEN_KWRD_END && tokk != TOKEN_ERROR && tokk != TOKEN_END_OF_FILE);

    if (tokk == TOKEN_KWRD_END) {
        return node;
    }
    return 0;
}

Node* is_type() {
    if (tokk == TOKEN_TYPE_INTEGER || tokk == TOKEN_TYPE_FLOAT) {
        return new Node(std::string{yylval});
    }
    return 0;
}

Node* is_decl(Node* node) {
    while (tokk == TOKEN_IDENTIFICATOR) {
        Node* id = new Node(std::string{yylval});
        tokk = yylex();
        if (tokk == TOKEN_COLON) {
            Node* colon = new Node(std::string{yylval});
            tree.addChild(node, colon);
            tree.addChild(colon, id);
            tokk = yylex();
            if (!tree.addChild(colon, is_type())) return 0;
        }
        else return 0;
        tokk = yylex();
    } 
    if (tokk == TOKEN_END_OF_FILE || tokk == TOKEN_ERROR) return 0;
    else return node;
}

Node* is_decl_section(Node* node) {
    if (tokk == TOKEN_KWRD_VAR) {   
        tokk = yylex();
        return is_decl(node);
    }
    return 0;
}

bool is_goal(Node* node) {
    tree.setHead(node);
    tokk = yylex();
    if (!is_decl_section(node)) { return false; }
    if (!is_command_section(node)) { return false; }
    return true;
}

int main() {
    yyin = fopen("input.in", "r");

    int res = is_goal(new Node("Abstract tree"));
    if (res) std::cout << "Success.\n\n";
    else std::cout << "Fail.\n\n";
    tree.print();
}