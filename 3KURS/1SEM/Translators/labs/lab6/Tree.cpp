#include "Tree.h"

#include "Token.h"

Tree::Tree() {

}

Tree::~Tree() {

}

void Tree::print() {
    if (head) {
        print(head, 0); // Начинаем с корневого узла на нулевом уровне
    } else {
        std::cout << "Tree is empty." << std::endl;
    }
}

void Tree::print(Node* node, uint8_t level) {
    if (node) {
        // Добавим красивые отступы для уровня вложенности
        for (int i = 0; i < level; ++i) {
            std::cout << "  |"; // Два пробела для каждого уровня вложенности
        }
        
        // Печатаем имя текущего узла
        std::cout << " " << node->name << std::endl;

        // Рекурсивно печатаем дочерние узлы
        for (auto child : node->next) {
            print(child, level + 1);
        }
    }
}

Node* Tree::add(std::string std) {
    Node* node = new Node(std);
    return node;
}

std::string Tree::getName(Node* node) {
    return node->name;
}

void Tree::setHead(Node* node) {
    this->head = node;
}

Node* Tree::add(Node* nodeParent, Node* node) {
    if (node)
    {
        nodeParent->next.push_back(node);
        return nodeParent;
    }
    return 0;
}