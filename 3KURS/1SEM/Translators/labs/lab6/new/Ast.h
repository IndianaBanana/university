#pragma once
#ifndef AST_H
#define AST_H

#include <iostream>
#include <string>
#include <deque>
#include <stack>
#include <cctype>
#include <memory>

using namespace std;

enum MathOperation
{
    ASSIGNMENT = 0,
    ADDITION = 1,
    SUBTRACTION = 1,
    MULTIPLICATION = 2,
    DIVISION = 2
};

enum LogicOperation
{
    LOGIC_OR = 0,
    LOGIC_AND = 1
};

class Node
{
protected:
    deque<Node *> children;

public:
    virtual void print();
};

class MathNode : public Node
{
private:
    string value;
    shared_ptr<MathNode> left;
    shared_ptr<MathNode> right;

public:
    MathNode(string val) : value(val), left(nullptr), right(nullptr) {}
};

#endif
