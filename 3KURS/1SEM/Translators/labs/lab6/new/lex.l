%option noyywrap

%{
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include "Token.h"
%}

%%

"var"             { return KEYWORD_VAR; }
"if"              { return KEYWORD_IF; }
"then"            { return KEYWORD_THEN; }
"else"            { return KEYWORD_ELSE; }
"begin"           { return KEYWORD_BEGIN; }
"end"             { return KEYWORD_END; }
"or"|"and"        { return LOGIC_OPERATION; }
"round"           { return COMMAND_ROUND; }
"read"            { return COMMAND_READ; }
"write"           { return COMMAND_WRITE; }
"integer"         { return TYPE_INTEGER; }
"float"           { return TYPE_FLOAT; }
[a-zA-Z][a-zA-Z0-9]* { return ID; }
[0-9]+            { return INTEGER; }
[0-9]+"."[0-9]+   { return DECIMAL; }
":="              { return ASSIGNMENT; }
"-"|"+"|"*"|"/"   { return MATH_OPERATION; }
"<"|">"|"="       { return LOGIC_COMPARAISON; }
":"               { return COLON; }
"("               { return LEFT_BRACKETS; }
")"               { return RIGHT_BRACKETS; }
"'".*?"'"          { return STRING; }
[ \t\n\r]+        {}
<<EOF>>           { return END_OF_FILE; }
.                 { return ERR; }

%%