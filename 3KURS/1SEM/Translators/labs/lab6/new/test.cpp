#include <iostream>
#include <stack>
#include <cctype>
#include <memory>
#include <string>

using namespace std;
// Узел дерева
struct Node {
    char value;
    shared_ptr<Node> left, right;

    Node(char val) : value(val), left(nullptr), right(nullptr) {}
};

// Приоритет операторов
int precedence(char op) {
    if (op == '=') return 1;
    if (op == '+' || op == '-') return 2;
    if (op == '*' || op == '/') return 3;
    return 0;
}

// Проверка, является ли символ оператором
bool isOperator(char c) {
    return c == '+' || c == '-' || c == '*' || c == '/' || c == '=';
}

// Построение дерева
shared_ptr<Node> buildTree(const string& expr) {
    stack<shared_ptr<Node> > operands;
    stack<char> operators;

    for (char c : expr) {
        if (isspace(c))
            continue;

        if (isalnum(c)) {
            printf("operand %c\n", c);
            operands.push(make_shared<Node>(c));
        } else if (isOperator(c)) {
            printf("operand %c\n", c);
            while (!operators.empty() && precedence(operators.top()) >= precedence(c)) {
                char op = operators.top();
                operators.pop();

                auto right = operands.top();
                printf("right %c\n", right->value);
                operands.pop();
                auto left = operands.top();
                printf("left %c\n", left->value);
                operands.pop();
                auto opNode = make_shared<Node>(op);
                opNode->left = left;
                opNode->right = right;
                operands.push(opNode);
            }
            operators.push(c);
        }
    }

    while (!operators.empty()) {
        char op = operators.top();
        operators.pop();

        auto right = operands.top();
        operands.pop();
        auto left = operands.top();
        operands.pop();

        auto opNode = make_shared<Node>(op);
        opNode->left = left;
        opNode->right = right;
        operands.push(opNode);
    }

    return operands.top();
}

// Обход дерева в префиксной форме
void printPrefix(const shared_ptr<Node>& root) {
    if (!root) return;
    cout << root->value << " ";
    printPrefix(root->left);
    printPrefix(root->right);
}

void printTree(const shared_ptr<Node>& root, int depth = 0) {
    if (!root) return;

    // Отступы для текущего узла
    for (int i = 0; i < depth; ++i)
        cout << "   ";

    cout << root->value << endl;

    // Рекурсивный вывод поддеревьев
    printTree(root->left, depth + 1);
    printTree(root->right, depth + 1);
}
// Пример использования
int main() {
    string expression = "b=4*3+b-c/d";
    auto tree = buildTree(expression);

    cout << "Префиксная форма: ";
    printPrefix(tree);
    cout << endl;
printTree(tree);
    return 0;
}