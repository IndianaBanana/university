#include <iostream>
#include <vector>
#include <memory>
#include <string>
#include <cstdio>
#include <FlexLexer.h>
extern "C" {
#include <stdlib.h>
#include <string.h>
#include "Token.h"
}
// using namespace std;
using namespace std;
// Базовый класс для узлов AST
class Node {
public:
    std::string type;                      // Тип узла (например, "Assignment", "Condition")
    std::string value;                     // Значение узла (например, ":=", ">")
    std::vector<std::shared_ptr<Node>> children; // Дочерние узлы

    Node(std::string t, std::string v = "") : type(std::move(t)), value(std::move(v)) {}

    void addChild(std::shared_ptr<Node> child) {
        children.push_back(std::move(child));
    }

    // Рекурсивный вывод дерева
    void print(int depth = 0) const {
        for (int i = 0; i < depth; ++i) std::cout << "   |";
        std::cout << type;
        if (!value.empty()) std::cout << " (" << value << ")";
        std::cout << "\n";
        for (const auto& child : children) {
            child->print(depth + 1);
        }
    }
};

// Переменная текущего токена
int cur_token;

// Лексическая функция
extern int yylex();
extern FILE* yyin;
extern char* yytext;

std::shared_ptr<Node> is_expr(stack<shared_ptr<Node>> operands, stack<string> operators);
std::shared_ptr<Node> is_T();
std::shared_ptr<Node> is_parameter();
std::shared_ptr<Node> is_condition();
std::shared_ptr<Node> is_assignment();
std::shared_ptr<Node> is_statement(std::shared_ptr<Node> parent = nullptr);
std::shared_ptr<Node> is_decl();
std::shared_ptr<Node> is_decl_section();
std::shared_ptr<Node> is_command_section();
std::shared_ptr<Node> is_goal();
std::shared_ptr<Node> is_read();
std::shared_ptr<Node> is_write();

// Обработка T (терминалов: идентификаторов и чисел)
std::shared_ptr<Node> is_T() {
    if (cur_token == ID || cur_token == INTEGER || cur_token == DECIMAL) {
        auto node = std::make_shared<Node>(cur_token == ID ? "Identifier" : "Literal", yytext);
        // cur_token = yylex();
        return node;
    }
    return nullptr;
}
int precedence(string op) {
    if (op.compare(":=") == 0) return 1;
    if (op.compare("+") == 0 || op.compare("-") == 0) return 2;
    if (op.compare("*") == 0 || op.compare("/") == 0) return 3;
    return 0;
}

// Обработка выражений
std::shared_ptr<Node> is_expr(stack<shared_ptr<Node>> operands, stack<string> operators) {
    auto operand = is_T(); // Первый операнд
    // if (!left) throw std::runtime_error("Syntax error in is_expr");
    // cur_token = yylex();
    // printf("operators.top = %s, operand = %s\n", operators.top().c_str(), operands.top()->value.c_str());
    // printf("cur_token = %d\n", cur_token);

    if (operand) {
        operands.push(operand);
        cur_token = yylex();
        return is_expr(operands, operators);
    }
    else if (cur_token == COMMAND_READ) {
        auto readNode = std::make_shared<Node>(string{ yytext });
        cur_token = yylex();
        if (cur_token != LEFT_BRACKETS) throw std::runtime_error("Syntax error in is_expr, LEFT_BRACKETS");
        cur_token = yylex();
        auto op = is_T();
        if (!op) throw std::runtime_error("Syntax error in is_expr, T");
        readNode->addChild(std::move(op));
        cur_token = yylex();
        if (cur_token != RIGHT_BRACKETS) throw std::runtime_error("Syntax error in is_expr, RIGHT_BRACKETS");
        cur_token = yylex();
        return is_expr(operands, operators);
    }
    else if (cur_token == MATH_OPERATION) {
        while (!operators.empty() && precedence(operators.top()) >= precedence(string{ yytext })) {
            string op = operators.top();
            operators.pop();
            auto right = operands.top();
            operands.pop();
            auto left = operands.top();
            operands.pop();
            auto opNode = std::make_shared<Node>("MathOperation", op);
            opNode->addChild(std::move(left));
            opNode->addChild(std::move(right));
            operands.push(std::move(opNode));
        }
        operators.push(string{ yytext });
        cur_token = yylex();
        return is_expr(operands, operators);
    }
    while (!operators.empty()) {
        string op = operators.top();
        operators.pop();

        auto right = operands.top();
        operands.pop();
        auto left = operands.top();
        operands.pop();

        auto opNode = make_shared<Node>(op);
        opNode->addChild(std::move(left));
        opNode->addChild(std::move(right));
        operands.push(opNode);
    }
    return operands.top();
}

// Обработка параметров в скобках
std::shared_ptr<Node> is_parameter() {
    auto term = is_T();
    if (term) {
    // printf("term = %s\n", term->value.c_str());
        cur_token = yylex();
        if (cur_token != LOGIC_COMPARAISON) throw std::runtime_error("Syntax error in is_parameter, LOGIC_COMPARAISON");
        auto logCompare = std::make_shared<Node>("LogicComparison", string{ yytext });
        logCompare->addChild(std::move(term));

        cur_token = yylex();
        term = is_T();
        if (!term) throw std::runtime_error("Syntax error in is_parameter, T");
        logCompare->addChild(std::move(term));
        return logCompare;
    }
    if (cur_token != LEFT_BRACKETS) throw std::runtime_error("Syntax error in is_parameter, LEFT_BRACKETS");
    cur_token = yylex();
    auto param = is_parameter();
    // printf("param = %s\n", param->value.c_str());
    // printf("cur_token = %d\n", cur_token);
    if (!param) throw std::runtime_error("Syntax error in is_parameter, parameter");
    cur_token = yylex();
    if (cur_token != RIGHT_BRACKETS) throw std::runtime_error("Syntax error in is_parameter, RIGHT_BRACKETS");
    cur_token = yylex();
    // printf("cur_token = %d\n", cur_token);
    if (cur_token != LOGIC_OPERATION) throw std::runtime_error("Syntax error in is_parameter, LOGIC_OPERATION");
    auto logOp = std::make_shared<Node>("LogicOperation", string{ yytext });
    logOp->addChild(std::move(param));
    cur_token = yylex();
    if (cur_token != LEFT_BRACKETS) throw std::runtime_error("Syntax error in is_parameter, LEFT_BRACKETS2");
    cur_token = yylex();
    param = is_parameter();
    if (!param) throw std::runtime_error("Syntax error in is_parameter, parameter2");
    logOp->addChild(std::move(param));
    cur_token = yylex();
    if (cur_token != RIGHT_BRACKETS) throw std::runtime_error("Syntax error in is_parameter, RIGHT_BRACKETS2");
    return logOp;
}

// Обработка условий
std::shared_ptr<Node> is_condition() {
    if (cur_token != KEYWORD_IF) return nullptr;
    auto node = std::make_shared<Node>("IF BRANCH");

    cur_token = yylex();
    if (cur_token != LEFT_BRACKETS) throw std::runtime_error("Syntax error in is_condition, LEFT_BRACKETS");

    cur_token = yylex();
    auto param = is_parameter();
    if (!param) throw std::runtime_error("Syntax error in is_condition, parameter");
    node->addChild(std::move(param));
    
    cur_token = yylex();
    if (cur_token != RIGHT_BRACKETS) throw std::runtime_error("Syntax error in is_condition, RIGHT_BRACKETS");

    cur_token = yylex();
    if (cur_token != KEYWORD_THEN) throw std::runtime_error("Syntax error in is_condition, THEN");
    auto then_node = std::make_shared<Node>("THEN_BRANCH");

    cur_token = yylex();
    if (cur_token != KEYWORD_BEGIN) throw std::runtime_error("Syntax error in is_condition, BEGIN");

    cur_token = yylex();
    auto then_block = is_statement();
    if (!then_block) throw std::runtime_error("Syntax error in is_condition, then_block");

    then_node->addChild(std::move(then_block));
    cur_token = yylex();
    if (cur_token != KEYWORD_END) throw std::runtime_error("Syntax error in is_condition, END");
    node->addChild(std::move(then_node));
    cur_token = yylex();
    if (cur_token == KEYWORD_ELSE) {
        cur_token = yylex();
        if (cur_token != KEYWORD_BEGIN) throw std::runtime_error("Syntax error in is_condition, BEGIN IN ELSE");
        auto else_node = std::make_shared<Node>("ELSE_BRANCH");
        cur_token = yylex();
        auto else_block = is_statement();
        if (!else_block) throw std::runtime_error("Syntax error in is_condition, else_block");
        else_node->addChild(std::move(else_block));
        cur_token = yylex();
        if (cur_token != KEYWORD_END) throw std::runtime_error("Syntax error in is_condition, END IN ELSE");
        node->addChild(std::move(else_node));
        cur_token = yylex();
    }

    return node;
}

// Обработка присваивания
std::shared_ptr<Node> is_assignment() {
    if (cur_token != ID) return nullptr;

    auto node = std::make_shared<Node>("Assignment");
    auto id_node = std::make_shared<Node>("Identifier", yytext);

    cur_token = yylex();
    if (cur_token != ASSIGNMENT) return nullptr;

    auto assign_op = std::make_shared<Node>(string{ yytext });
    // assign_op->addChild(std::move(id_node));
    // node->addChild(std::move(assign_op));

    stack<shared_ptr<Node>> operands;
    stack<string> operators;
    operands.push(id_node);
    operators.push(string{ yytext });
    cur_token = yylex();
    auto expr = is_expr(operands, operators);
    if (!expr) return nullptr;

    node->addChild(std::move(expr));
    return node;
}

shared_ptr<Node> is_read() {
    if (cur_token != COMMAND_READ) return nullptr;

    auto node = std::make_shared<Node>("READ");
    cur_token = yylex();
    if (cur_token != LEFT_BRACKETS) throw std::runtime_error("Syntax error in is_read, LEFT_BRACKETS");
    cur_token = yylex();
    if (cur_token != ID) throw std::runtime_error("Syntax error in is_read, ID");
    auto id_node = std::make_shared<Node>("Identifier", yytext);
    node->addChild(std::move(id_node));
    cur_token = yylex();
    if (cur_token != RIGHT_BRACKETS) throw std::runtime_error("Syntax error in is_read, RIGHT_BRACKETS");
    return node;
}

shared_ptr<Node> is_write() {
    if (cur_token != COMMAND_WRITE) return nullptr;

    auto node = std::make_shared<Node>("WRITE");
    cur_token = yylex();
    if (cur_token != LEFT_BRACKETS) throw std::runtime_error("Syntax error in is_write, LEFT_BRACKETS");
    cur_token = yylex();
    if (cur_token != ID && cur_token != STRING) throw std::runtime_error("Syntax error in is_write, ID or STRING");
    auto id_node = make_shared<Node>(cur_token == ID ? "Identifier" : "String", yytext);
    if (!id_node) return nullptr;
    node->addChild(std::move(id_node));
    cur_token = yylex();
    if (cur_token != RIGHT_BRACKETS) throw std::runtime_error("Syntax error in is_write, RIGHT_BRACKETS");
    return node;
}

// Обработка отдельных команд
std::shared_ptr<Node> is_statement(std::shared_ptr<Node> parent) {
    if (cur_token == KEYWORD_END) return make_shared<Node>("");

    if (cur_token == ERR || cur_token == END_OF_FILE) throw std::runtime_error("Syntax error in is_statement");

    auto assignment = is_assignment();
    if (assignment) return assignment;

    auto read = is_read();
    if (read) return read;

    auto write = is_write();
    if (write) return write;

    auto condition = is_condition();
    if (condition) return condition;

    return nullptr;
}

// Обработка секции команд
std::shared_ptr<Node> is_command_section() {
    if (cur_token != KEYWORD_BEGIN) throw std::runtime_error("Expected 'begin' in command section");
    auto node = std::make_shared<Node>("CommandSection");

    cur_token = yylex();
    do {
        auto stmt = is_statement();
        if (!stmt) break;
        node->addChild(std::move(stmt));
    } while (node && cur_token != KEYWORD_END && cur_token != ERR && cur_token != END_OF_FILE);

    // printf("cur_token = %d\n", cur_token);
    if (cur_token != KEYWORD_END)  throw std::runtime_error("Expected 'end' in command section");
    // cur_token = yylex();
    return node;
}

// Обработка объявлений
std::shared_ptr<Node> is_decl() {
    auto node = std::make_shared<Node>("Declaration");

    while (cur_token == ID) {
        auto var_node = std::make_shared<Node>("Variable", yytext);
        cur_token = yylex();
        if (cur_token != COLON) throw std::runtime_error("Syntax error in is_decl, COLON");
        cur_token = yylex();
        if (cur_token == TYPE_INTEGER || cur_token == TYPE_FLOAT) {
            var_node->addChild(std::make_shared<Node>("Type", yytext));
        }
        else throw std::runtime_error("Syntax error in is_decl, TYPE_INTEGER or TYPE_FLOAT");
        node->addChild(std::move(var_node));
        cur_token = yylex();
    }
    if (cur_token == ERR || cur_token == END_OF_FILE) throw std::runtime_error("Syntax error in is_decl");
    return node;
}

// Обработка секции объявлений
std::shared_ptr<Node> is_decl_section() {
    if (cur_token != KEYWORD_VAR) throw std::runtime_error("Syntax error in is_decl_section!");
    auto node = std::make_shared<Node>("DeclarationSection");

    cur_token = yylex();
    auto decl = is_decl();
    if (!decl) throw std::runtime_error("Syntax error in is_decl!");

    node->addChild(std::move(decl));
    return node;
}

// Главная функция анализа
std::shared_ptr<Node> is_goal() {
    auto root = std::make_shared<Node>("GOAL");

    cur_token = yylex();
    auto decl_section = is_decl_section();

    root->addChild(std::move(decl_section));

    auto command_section = is_command_section();
    if (!command_section) throw std::runtime_error("Syntax error in is_goal!");

    root->addChild(std::move(command_section));
    return root;
}

int main() {
    yyin = fopen("input.in", "r");
    if (!yyin) {
        std::cerr << "Error: Cannot open file\n";
        return 1;
    }

    auto ast = is_goal();
    if (ast) {
        std::cout << "Abstract Syntax Tree:\n";
        ast->print();
    }
    else {
        std::cerr << "Syntax error\n";
    }

    return 0;
}