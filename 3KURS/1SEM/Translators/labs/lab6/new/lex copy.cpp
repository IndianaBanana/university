%option noyywrap

%{
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include "Token.h"
int cur_token;
%}

%%

"var" { return KEYWORD_VAR; }
"if" { return KEYWORD_IF; }
"then" { return KEYWORD_THEN; }
"else" { return KEYWORD_ELSE; }
"begin" { return KEYWORD_BEGIN; }
"end" { return KEYWORD_END; }
"or"|"and" { return LOGIC_OPERATION; }
"round" { return COMMAND_ROUND; }
"read" { return COMMAND_READ; }
"write" { return COMMAND_WRITE; }
"integer" { return TYPE_INTEGER; }
"float" { return TYPE_FLOAT; }
[a-zA-z][a-zA-z|0-9]* { return ID; }
[0-9]+ { return INTEGER; }
[0-9]+"."[0-9]+ { return DECIMAL; }
":=" { return ASSIGNMENT; }
"-"|"+"|"*"|"/" { return MATH_OPERATION; }
"<"|">"|"=" { return LOGIC_COMPARAISON; }
":" { return COLON; }
"(" { return LEFT_BRACKETS; }
")" { return RIGHT_BRACKETS; }
"'".*"'" { return STRING; }

[ \t\n\r ]

<<EOF>> { return END_OF_FILE; }
. { return ERR; }

%%

bool is_statement();

bool is_T()
{
    return cur_token == ID || cur_token == INTEGER || cur_token == DECIMAL;
}


bool is_parameter()
{
    if (is_T())
    {
        cur_token = yylex();

        if (cur_token == LOGIC_COMPARAISON)
        {
            cur_token = yylex();
            return is_T();
        }
    }

    if (cur_token != LEFT_BRACKETS) return false;

    cur_token = yylex();

    if (is_parameter())
    {
        cur_token = yylex();
        if (cur_token == RIGHT_BRACKETS)
        {
            cur_token = yylex();
            if (cur_token == LOGIC_OPERATION)
            {
                cur_token = yylex();
                if (cur_token != LEFT_BRACKETS) return false;

                cur_token = yylex();
                if (is_parameter())
                {
                    cur_token = yylex();

                    return cur_token == RIGHT_BRACKETS;
                }
            }
        }
    }
    return false;
}

bool is_condition()
{
    if (cur_token != KEYWORD_IF) return false;

    cur_token = yylex();
    if (cur_token != LEFT_BRACKETS) return false;

    cur_token = yylex();
    if (!is_parameter()) return false;

    cur_token = yylex();
    if (cur_token != RIGHT_BRACKETS) return false;

    cur_token = yylex();
    if (cur_token != KEYWORD_THEN) return false;

    cur_token = yylex();
    if (cur_token != KEYWORD_BEGIN) return false;

    cur_token = yylex();
    if (!is_statement()) return false;

    if (cur_token != KEYWORD_END) return false;

    cur_token = yylex();
    if (cur_token != KEYWORD_ELSE)
    {
        
        return true;
    }
    cur_token = yylex();
    if (cur_token != KEYWORD_BEGIN) return false;

    cur_token = yylex();
    if (!is_statement()) return false;

    return cur_token == KEYWORD_END;
}

bool is_write()
{
    if (cur_token != COMMAND_WRITE) return false;

    cur_token = yylex();
    if (cur_token == LEFT_BRACKETS)
    {
        cur_token = yylex();
        if (cur_token == ID || cur_token == STRING)
        {
            cur_token = yylex();
            if (cur_token == RIGHT_BRACKETS)
            {
                cur_token = yylex();
                return true;
            }
        }
    }
    return false;
}

bool is_read()
{
    if (cur_token != COMMAND_READ) return false;

    cur_token = yylex();
    if (cur_token == LEFT_BRACKETS)
    {
        cur_token = yylex();
        if (cur_token == ID)
        {
            cur_token = yylex();
            if (cur_token == RIGHT_BRACKETS)
            {
                cur_token = yylex();
                return true;
            }
           
        }
    }
    return false;
}

bool is_expr() 
{
    if (is_T())
    {
       
        cur_token = yylex();

        if (cur_token == MATH_OPERATION)
        {
            cur_token = yylex();
           
            return is_expr(); 
        }
        else if (cur_token != END_OF_FILE && cur_token != ERR)
        {           
            
            return true;
        }
    }
    else
    {       
        if (cur_token == COMMAND_ROUND)
        {
            cur_token = yylex();
            if (cur_token == LEFT_BRACKETS)
            {
                cur_token = yylex();
                if (is_T())
                {
                    cur_token = yylex();
                    if (cur_token == RIGHT_BRACKETS)
                    {
                        cur_token = yylex();
                        return true;
                    }
                }
            }
        }
    }

    return false;
}

bool is_assignment()
{
    if (cur_token != ID) return false;

    cur_token = yylex();
    if (cur_token != ASSIGNMENT) return false;

    cur_token = yylex();

    return is_expr();
}

bool is_statement()
{
    if (cur_token == KEYWORD_END) return true;

    if (cur_token == ERR || cur_token == END_OF_FILE) return false;

    return is_assignment() || is_read() || is_write() || is_condition();
}

bool is_command_section()
{
    if (cur_token != KEYWORD_BEGIN) return false;
    bool correct = false;
    
    cur_token = yylex();
    do
    {
        correct = is_statement();
    } while (correct && cur_token != KEYWORD_END && cur_token != ERR && cur_token != END_OF_FILE);

    return correct;
}

bool is_type()
{
    return cur_token == TYPE_INTEGER || cur_token == TYPE_FLOAT;
}

bool is_decl()
{
    while (cur_token == ID)
    {
        cur_token = yylex();
        if (cur_token == COLON)
        {
            cur_token = yylex();
            if (!is_type()) return false;
        }
        else return false;
        cur_token = yylex();
    }
    if (cur_token == END_OF_FILE || cur_token == ERR) return false;
    else return true;
}

bool is_decl_section()
{
    if (cur_token == KEYWORD_VAR)
    {
        cur_token = yylex();
        return is_decl();
    }

    return false;
}

bool is_goal()
{
    cur_token = yylex();
    return is_decl_section() && is_command_section();
}

main()
{
    yyin = fopen("input.in", "r");
    if (is_goal()) printf("yes\n");
    else printf("no\n");
    return 0;
}