#pragma once
#ifndef TREE_H
#define TREE_H

#include "Node.h"

#include <iostream>

class Tree {
private:
	Node* head;
	void print(Node* node, uint8_t level);
public:
	Tree();
	~Tree();
	void print();

	Node* add(std::string std);

	Node* add(Node* nodeParent, Node* node);

	std::string getName(Node* node);


	Node* add(uint8_t num, uint8_t condition);

	void setHead(Node* node);

	friend class Node;


};

#endif // TREE_H