%option noyywrap

%{
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include "Token.h"
#include "Tree.h"
int cur_token;
char yylval[256];
Tree* tree;

%}

%%

"var" {memcpy(yylval, yytext, strlen(yytext) + 1); return KEYWORD_VAR; }
"if" {memcpy(yylval, yytext, strlen(yytext) + 1); return KEYWORD_IF; }
"then" {memcpy(yylval, yytext, strlen(yytext) + 1); return KEYWORD_THEN; }
"else" {memcpy(yylval, yytext, strlen(yytext) + 1); return KEYWORD_ELSE; }
"begin" {memcpy(yylval, yytext, strlen(yytext) + 1); return KEYWORD_BEGIN; }
"end" {memcpy(yylval, yytext, strlen(yytext) + 1); return KEYWORD_END; }
"or"|"and" {memcpy(yylval, yytext, strlen(yytext) + 1); return LOGIC_OPERATION; }
"round" {memcpy(yylval, yytext, strlen(yytext) + 1); return COMMAND_ROUND; }
"read" {memcpy(yylval, yytext, strlen(yytext) + 1); return COMMAND_READ; }
"write" {memcpy(yylval, yytext, strlen(yytext) + 1); return COMMAND_WRITE; }
"integer" {memcpy(yylval, yytext, strlen(yytext) + 1); return TYPE_INTEGER; }
"float" {memcpy(yylval, yytext, strlen(yytext) + 1); return TYPE_FLOAT; }
[a-zA-z][a-zA-z|0-9]* {memcpy(yylval, yytext, strlen(yytext) + 1); return ID; }
[0-9]+ {memcpy(yylval, yytext, strlen(yytext) + 1); return INTEGER; }
[0-9]+"."[0-9]+ {memcpy(yylval, yytext, strlen(yytext) + 1); return DECIMAL; }
":=" {memcpy(yylval, yytext, strlen(yytext) + 1); return ASSIGNMENT; }
"-"|"+"|"*"|"/" {memcpy(yylval, yytext, strlen(yytext) + 1); return MATH_OPERATION; }
"<"|">"|"=" {memcpy(yylval, yytext, strlen(yytext) + 1); return LOGIC_COMPARAISON; }
":" {memcpy(yylval, yytext, strlen(yytext) + 1); return COLON; }
"(" {memcpy(yylval, yytext, strlen(yytext) + 1); return LEFT_BRACKETS; }
")" {memcpy(yylval, yytext, strlen(yytext) + 1); return RIGHT_BRACKETS; }
"'".*"'" {memcpy(yylval, yytext, strlen(yytext) + 1); return STRING; }

[ \t\n\r ]

<<EOF>> { return END_OF_FILE; }
. { return ERR; }

%%
// #include <stdbool.h>
// #include <stdlib.h>
// #include <string.h>
// #include "Token.h"
// #include "Tree.h"
// int cur_token;
// char yylval[256];
// Tree tree;

Node *is_statement(Node* node);

Node *is_T(Node *node)
{
    return cur_token == ID || cur_token == INTEGER || cur_token == DECIMAL
               ? tree->add(node, new Node(std::string{yylval}))
               : 0;
}

Node *is_param(Node *node)
{
    if (tree->add(node, is_T(new Node("T"))))
    {
        cur_token = yylex();

        if (cur_token == LOGIC_COMPARAISON)
        {
            tree->add(node, new Node(std::string{yylval}));
            cur_token = yylex();
            return tree->add(node, is_T(new Node("T")));
        }
    }

    if (cur_token != LEFT_BRACKETS) return 0;

    // tree->add(node, new Node(std::string{yylval}));
    cur_token = yylex();

    if (tree->add(node, is_param(new Node("PARAM"))))
    {
        cur_token = yylex();
        if (cur_token == RIGHT_BRACKETS)
        {
            // tree->add(node, new Node(std::string{yylval}));
            cur_token = yylex();
            if (cur_token == LOGIC_OPERATION)
            {
                tree->add(node, new Node(std::string{yylval}));
                cur_token = yylex();
                if (cur_token != LEFT_BRACKETS) return 0;
                // tree->add(node, new Node(std::string{yylval}));
                cur_token = yylex();
                if (tree->add(node, is_param(new Node("PARAM"))))
                {
                    cur_token = yylex();
                    if (cur_token == RIGHT_BRACKETS)
                    {
                        return node;
                    }
                }
            }
        }
    }
    return 0;
}

Node *is_condition(Node *node)
{
    if (cur_token != KEYWORD_IF) return 0;
    tree->add(node, new Node("IF BRANCH"));

    cur_token = yylex();
    if (cur_token != LEFT_BRACKETS) return 0;
    // tree->add(node, new Node(std::string{yylval}));

    cur_token = yylex();
    if (!tree->add(node, is_param(new Node("PARAM")))) return 0;
    cur_token = yylex();
    if (cur_token != RIGHT_BRACKETS) return 0;
    // tree->add(node, new Node(std::string{yylval}));

    cur_token = yylex();
    if (cur_token != KEYWORD_THEN) return 0;
    tree->add(node, new Node("THEN BRANCH"));

    cur_token = yylex();
    if (cur_token != KEYWORD_BEGIN) return 0;
    // tree->add(node, new Node(std::string{yylval}));

    cur_token = yylex();
    if (!tree->add(node, is_statement(new Node("STATEMENT")))) return 0;
    while (node && cur_token != KEYWORD_END && cur_token != ERR && cur_token != END_OF_FILE)
    {
        tree->add(node, is_statement(new Node("STATEMENT")));
    }
    
    if (cur_token != KEYWORD_END) return 0;
    // tree->add(node, new Node(std::string{yylval}));

    cur_token = yylex();
    if (cur_token != KEYWORD_ELSE) return node;
    tree->add(node, new Node("ELSE BRANCH"));

    cur_token = yylex();
    if (cur_token != KEYWORD_BEGIN) return 0;
    // tree->add(node, new Node(std::string{yylval}));

    cur_token = yylex();
    if (!tree->add(node, is_statement(new Node("STATEMENT")))) return 0;
    while (node && cur_token != KEYWORD_END && cur_token != ERR && cur_token != END_OF_FILE)
    {
        tree->add(node, is_statement(new Node("STATEMENT")));
    }
    if (cur_token == KEYWORD_END)
    {
        // tree->add(node, new Node(std::string{yylval}));
        cur_token = yylex();
        return node;
    }
    return 0;
}

Node *is_write(Node *node)
{
    if (cur_token != COMMAND_WRITE)
    {
        return 0;
    }
    // tree->add(node, new Node(std::string{yylval}));
    cur_token = yylex();
    if (cur_token == LEFT_BRACKETS)
    {
        // tree->add(node, new Node(std::string{yylval}));
        cur_token = yylex();
        if (cur_token == ID || cur_token == STRING)
        {
            tree->add(node, new Node(std::string{yylval}));
            cur_token = yylex();
            if (cur_token == RIGHT_BRACKETS)
            {
                // tree->add(node, new Node(std::string{yylval}));
                cur_token = yylex();
                return node;
            }
        }
    }
    return 0;
}

Node *is_read(Node *node)
{
    if (cur_token != COMMAND_READ)
        return 0;
    // tree->add(node, new Node(std::string{yylval}));
    cur_token = yylex();
    if (cur_token == LEFT_BRACKETS)
    {
        // tree->add(node, new Node(std::string{yylval}));
        cur_token = yylex();
        if (cur_token == ID)
        {
            tree->add(node, new Node(std::string{yylval}));
            cur_token = yylex();
            if (cur_token == RIGHT_BRACKETS)
            {
                // tree->add(node, new Node(std::string{yylval}));
                cur_token = yylex();
                return node;
            }
        }
    }
    return 0;
}

Node *is_expr(Node *node)
{
    if (tree->add(node, is_T(new Node("T"))))
    {
        cur_token = yylex();

        if (cur_token == MATH_OPERATION)
        {
            tree->add(node, new Node(std::string{yylval}));
            cur_token = yylex();

            return tree->add(node, is_expr(new Node("EXPR")));
        }
        else if (cur_token != END_OF_FILE && cur_token != ERR)
        {
            return node;
        }
    }
    else
    {
        if (cur_token == COMMAND_ROUND)
        {
            tree->add(node, new Node(std::string{yylval}));
            cur_token = yylex();
            if (cur_token == LEFT_BRACKETS)
            {
                // tree->add(node, new Node(std::string{yylval}));
                cur_token = yylex();
                if (tree->add(node, is_T(new Node("T"))))
                {
                    cur_token = yylex();
                    if (cur_token == RIGHT_BRACKETS)
                    {
                        // tree->add(node, new Node(std::string{yylval}));
                        cur_token = yylex();
                        return node;
                    }
                }
            }
        }
    }

    return 0;
}

Node *is_assignment(Node *node)
{
    if (cur_token != ID)
        return 0;
    tree->add(node, new Node(std::string{yylval}));
    cur_token = yylex();
    if (cur_token != ASSIGNMENT)
        return 0;
    // tree->add(node, new Node(std::string{yylval}));
    cur_token = yylex();

    return tree->add(node, is_expr(new Node("EXPR")));
}

Node *is_statement(Node *node)
{
    if (cur_token == KEYWORD_END)
        return node;

    if (cur_token == ERR || cur_token == END_OF_FILE)
        return 0;

    Node *tmp = is_assignment(new Node("ASSIGNMENT"));
    if (tmp)
    {  
        printf("cur_token = %d\n", cur_token);
        return tree->add(node, tmp);
    }
    tmp = is_read(new Node("READ"));
    if (tmp)
    {
        return tree->add(node, tmp);
    }
    tmp = is_write(new Node("WRITE"));
    if (tmp)
    {
        return tree->add(node, tmp);
    }
    tmp = is_condition(new Node("CONDITION"));
    if (tmp)
    {
        return tree->add(node, tmp);
    }

    return 0;
}

Node *is_command_section(Node *node)
{
    if (cur_token != KEYWORD_BEGIN)
        return 0;
    // tree->add(node, new Node(std::string{yylval}));

    cur_token = yylex();
    do
    {
        tree->add(node, is_statement(new Node("STATEMENT")));
    } while (node && cur_token != KEYWORD_END && cur_token != ERR && cur_token != END_OF_FILE);
    if (cur_token == KEYWORD_END)
    {
        return node;
    }
    return 0;
}

Node *is_type(Node *node)
{
    return cur_token == TYPE_INTEGER || cur_token == TYPE_FLOAT
               ? tree->add(node, new Node(std::string{yylval}))
               : 0;
}

Node *is_decl(Node *node)
{
    while (cur_token == ID)
    {
        tree->add(node, new Node(std::string{yylval}));
        cur_token = yylex();
        if (cur_token == COLON)
        {
            // tree->add(node, new Node(std::string{yylval}));
            cur_token = yylex();
            if (!tree->add(node, is_type(new Node("TYPE"))))
                return 0;
        }
        else
            return 0;
        cur_token = yylex();
    }
    if (cur_token == END_OF_FILE || cur_token == ERR)
        return 0;
    else
        return node;
}

Node *is_decl_section(Node *node)
{
    if (cur_token == KEYWORD_VAR)
    {
        // tree->add(node, new Node(std::string{yylval}));
        cur_token = yylex();

        return tree->add(node, is_decl(new Node("DECL")));
    }

    return 0;
}

Node *is_goal(Node *node)
{
    cur_token = yylex();

    if (!tree->add(node, is_decl_section(new Node("DECL_SECTION"))))
    {
        return 0;
    }

    if (!tree->add(node, is_command_section(new Node("COMMAND_SECTION"))))
    {
        return 0;
    }
    return node;
}

int main()
{
    tree = new Tree();
    yyin = fopen("input.in", "r");
    Node *node = new Node("GOAL");
    node = is_goal(node);
    tree->setHead(node);
    if (node != 0)
        std::cout << "yes\n";
    else
        printf("no");

    tree->print();

    return EXIT_SUCCESS;
}