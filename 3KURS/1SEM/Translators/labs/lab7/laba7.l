%option noyywrap

%{
#include <iostream>
#include <deque>
#include <cstdint>
#include <stack>

enum Token
{
    TOKEN_T_SELECT=0,
    TOKEN_T_ID,
    TOKEN_T_COMMA,
    TOKEN_T_FROM,
    TOKEN_T_WHERE,
    TOKEN_T_NUM,
    TOKEN_T_LESS,
    TOKEN_T_MORE,
    TOKEN_T_EQUALLY,
    TOKEN_T_AND,
    TOKEN_T_OR,
    TOKEN_END_OF_FILE,
    TOKEN_ERROR,

    TOKEN_GOAL=0,
    TOKEN_KEY,
    TOKEN_KEY_,
    TOKEN_FROM,
    TOKEN_WHERE,
    TOKEN_PARAM,
    TOKEN_F,
    TOKEN_F_,

    TOKEN_EPS
};

uint8_t action[27][12] = {
    {1,0,0,0,0,0,0,0,0,0,0,0},
    {0,4,0,0,0,0,0,0,0,0,0,0},
    {0,0,0,6,0,0,0,0,0,0,0,0},
    {0,0,8,103,0,0,0,0,0,0,0,0},
    {0,0,107,107,107,0,107,107,107,0,0,107},
    {0,0,0,0,10,0,0,0,0,0,0,200},
    {0,4,0,0,0,0,0,0,0,0,0,0},
    {0,0,0,102,0,0,0,0,0,0,0,0},
    {0,4,0,0,0,0,0,0,0,0,0,0},
    {0,0,0,0,0,0,0,0,0,0,0,200},
    {0,4,0,0,0,0,0,0,0,0,0,0},
    {0,0,0,0,104,0,0,0,0,0,0,104},
    {0,0,8,103,0,0,0,0,0,0,0,0},
    {0,0,0,0,0,0,0,0,0,17,16,105},
    {0,0,0,0,0,0,18,19,20,0,0,0},
    {0,0,0,103,0,0,0,0,0,0,0,0},
    {0,4,0,0,0,0,0,0,0,0,0,0},
    {0,4,0,0,0,0,0,0,0,0,0,0},
    {0,0,0,0,0,26,0,0,0,0,0,0},
    {0,0,0,0,0,26,0,0,0,0,0,0},
    {0,0,0,0,0,26,0,0,0,0,0,0},
    {0,0,0,0,0,0,0,0,0,106,16,106},
    {0,0,0,0,0,0,0,0,0,17,106,106},
    {0,0,0,0,0,0,0,0,0,106,106,106},
    {0,0,0,0,0,0,0,0,0,106,106,106},
    {0,0,0,0,0,0,0,0,0,106,106,106},
    {0,0,0,0,0,0,0,0,0,108,108,108}
};

uint8_t goto1[27][7] = {
    {0,0,0,0,0,0,0},
    {2,0,0,0,0,3,0},
    {0,0,5,0,0,0,0},
    {0,7,0,0,0,0,0},
    {0,0,0,0,0,0,0},
    {0,0,0,9,0,0,0},
    {0,0,0,0,0,11,0},
    {0,0,0,0,0,0,0},
    {0,0,0,0,0,12,0},
    {0,0,0,0,0,0,0},
    {0,0,0,0,13,14,0},
    {0,0,0,0,0,0,0},
    {0,15,0,0,0,0,0},
    {0,0,0,0,0,0,0},
    {0,0,0,0,0,0,0},
    {0,0,0,0,0,0,0},
    {0,0,0,0,21,14,0},
    {0,0,0,0,22,14,0},
    {0,0,0,0,0,0,23},
    {0,0,0,0,0,0,24},
    {0,0,0,0,0,0,25},
    {0,0,0,0,0,0,0},
    {0,0,0,0,0,0,0},
    {0,0,0,0,0,0,0},
    {0,0,0,0,0,0,0},
    {0,0,0,0,0,0,0},
    {0,0,0,0,0,0,0}
};

class Node {
	std::string name_;
	std::deque<Node*> next_;

public:
	Node(std::string name) { name_ = name; }
    std::string getName() { return name_; }
    void setName(std::string name) { name_ = name; }
    void add(Node* n) { this->next_.push_back(n); }

	friend class Tree;
};

class Tree {
public:
	void print(Node* node, uint8_t level);
	Node* add(std::string std);
	void addConnect(Node* nodePatern, Node* node);
};

void Tree::print(Node* node, uint8_t level) {
    if (node) {
        for (int i = 0; i < level; ++i) {
            std::cout << std::string("    ");
        }
        std::cout << node->name_ << std::endl;
        for (auto nodes : node->next_) {
            print(nodes, level + 1);
        }
    }
}

Node* Tree::add(std::string std) {
	Node* node = new Node(std);
	return node;
}

void Tree::addConnect(Node* nodePatern, Node* node) {
	nodePatern->next_.push_front(node);
}

Tree tree;
char yylval[256];

%}

%%

"SELECT" {memcpy(yylval, yytext, strlen(yytext) + 1); return TOKEN_T_SELECT; }
"," {memcpy(yylval, yytext, strlen(yytext) + 1); return TOKEN_T_COMMA; }
"FROM" {memcpy(yylval, yytext, strlen(yytext) + 1); return TOKEN_T_FROM; }
"WHERE" {memcpy(yylval, yytext, strlen(yytext) + 1); return TOKEN_T_WHERE; }
"<" {memcpy(yylval, yytext, strlen(yytext) + 1); return TOKEN_T_LESS; }
">" {memcpy(yylval, yytext, strlen(yytext) + 1); return TOKEN_T_MORE; }
"=" {memcpy(yylval, yytext, strlen(yytext) + 1); return TOKEN_T_EQUALLY; }
"and" {memcpy(yylval, yytext, strlen(yytext) + 1); return TOKEN_T_AND; }
"or" {memcpy(yylval, yytext, strlen(yytext) + 1); return TOKEN_T_OR; }
[a-zA-z][a-zA-z|0-9]* { memcpy(yylval, yytext, strlen(yytext) + 1); return TOKEN_T_ID; }
[0-9]+ {memcpy(yylval, yytext, strlen(yytext) + 1); return TOKEN_T_NUM; }
";" {memcpy(yylval, yytext, strlen(yytext) + 1); return TOKEN_END_OF_FILE; }
[ \t\n\r ]

<<EOF>> { return TOKEN_END_OF_FILE; }
. { return TOKEN_ERROR; }

%%

uint8_t transitionAction(uint8_t condition, uint8_t term) {
    return action[condition][term];
}

uint8_t transitionGoto(uint8_t condition, uint8_t neterm) {
    return goto1[condition][neterm];
}

uint8_t lenghtP(uint8_t neterm, uint8_t condition) {
    switch (neterm)
    {      
    case TOKEN_KEY:
        return 2;
        break;
    case TOKEN_KEY_:
        if (condition!=15) {
            return 0;
        }
        else {
            return 3;
        }
        break;
    case TOKEN_FROM:
        return 2;
        break;
    case TOKEN_WHERE:
        return 2;
        break;
    case TOKEN_PARAM:
        return 3;
        break;
    case TOKEN_F:
        return 1;
        break;
    case TOKEN_F_:
        return 1;
        break;
    default:
        return 0;
        break;
    }        
}

Node* createParamCmp(std::stack<Node*>& stack) {
    Node* cmp = new Node("");
    Node* top = stack.top();
    tree.addConnect(cmp, top);
    stack.pop();
    top = stack.top();
    cmp->setName(top->getName());
    stack.pop();
    top = stack.top();
    tree.addConnect(cmp, top);
    stack.pop();
    return cmp;
}

// void printStack(std::stack<Node*> stack) {
//     uint8_t j = stack.size();
//     for (uint8_t i = 0; i < j; i++) {
//         Node* n = stack.top();
//         std::cout << n->getName() << "\n";
//         stack.pop();                    
//     }
// }

Node* createParamAndOr(std::stack<Node*>& stack) {
    Node* param = new Node("");
    Node* top = stack.top();
    if (top->getName() == "and" or top->getName() == "or") {
        param->setName(top->getName());
        stack.pop();
    }
    
    Node* cmp = createParamCmp(stack);
    top = stack.top();
    tree.addConnect(param, cmp);
    
    if (top->getName() != "WHERE") {
        param->setName(top->getName());
        stack.pop();
        top = stack.top();

        cmp = createParamCmp(stack);
        tree.addConnect(param, cmp);
        top = stack.top();

        if (top->getName() == "WHERE") {
            return param;
        }

        tree.addConnect(param, createParamAndOr(stack));
        return param;
    }
    return param;
}

// Node* createParamAndOr(std::stack<Node*>& stack) {
//     Node* param = new Node("");
//     Node* cmp = createParamCmp(stack);
//     Node* top = stack.top();
//     //std::cout << top->getName() << '\n';
//     if (top->getName() != "WHERE") {
//         tree.addConnect(param, cmp);
//         param->setName(top->getName());
//         stack.pop();
//         cmp = createParamCmp(stack);
//         tree.addConnect(param, cmp);
//         stack.pop();
//         return param;
//     }
//     delete param;
//     //stack.pop();
//     return cmp;
// }

Node* createWhere(std::stack<Node*>& stack) {
    Node* where = new Node("WHERE");
    tree.addConnect(where, createParamAndOr(stack));
    stack.pop();
    return where;
}

Node* createFrom(std::stack<Node*>& stack) {
    Node* from = new Node("FROM");
    Node* top = stack.top();
    tree.addConnect(from, top);
    stack.pop();
    stack.pop();
    return from;
}

Node* createSelect(std::stack<Node*>& stack) {
    Node* select = new Node("SELECT");
    Node* top = stack.top();
    while (top->getName() != "SELECT") {
        if (top->getName() != ",") {
            tree.addConnect(select, top);
        }
        stack.pop();
        top = stack.top();
    }
    return select;
}

int main() {
    uint8_t lexema = 0;
    uint8_t token = 0;
    uint8_t condition = 0; 

    yyin = fopen("input.in", "r");

    std::stack<Node*> stack_attributes;
    std::stack<uint8_t> stack;

    stack.push(TOKEN_END_OF_FILE);
    stack.push(TOKEN_GOAL);

    token = yylex();
    do {
        lexema = transitionAction(stack.top(), token);
        if (token != 12 && lexema != 0) {
            if (lexema < 100) {
                stack_attributes.push(tree.add(std::string{ yylval }));
                stack.push(token);
                stack.push(lexema);
                token = yylex();               
            }
            else if (lexema > 100 && lexema != 200) {
                uint8_t j = lenghtP(lexema % 100 - 1, stack.top());
                
                for (uint8_t i = 0; i < j; i++) {
                    Node* n = stack_attributes.top();
                    stack_attributes.push(n);
                    stack_attributes.pop();
                    stack.pop();
                    stack.pop();
                }
                condition = transitionGoto(stack.top(), (lexema % 100) - 2);
                stack.push(lexema % 100 - 1);
                stack.push(condition);
            } 
            else if (lexema == 200) {
                std::cout << "Success.\n";
                Node* node = tree.add("Abstract Tree");

                Node* where = createWhere(stack_attributes);
                Node* from = createFrom(stack_attributes);
                Node* select = createSelect(stack_attributes);
                tree.addConnect(node, where);
                tree.addConnect(node, from);
                tree.addConnect(node, select);
                tree.print(node, 0);
                break;
            }
            else {
                std::cout << "Fail.";
                break;
            }            
        }
        else {
            std::cout << "Fail.";
            break;
        }  
    } while (true);
}