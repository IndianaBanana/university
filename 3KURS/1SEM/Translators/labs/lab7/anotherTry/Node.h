#pragma once
#ifndef NODE_H
#define NODE_H

#include <iostream>

#include <deque>

class Node {
protected:
	std::string name;

	std::deque<Node*> next;

public:
	std::string getName();
	Node(std::string name);
	~Node();

	friend class Tree;

};


#endif // NODE_H