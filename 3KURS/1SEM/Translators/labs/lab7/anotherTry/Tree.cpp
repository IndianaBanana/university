#include "Tree.h"

#include "Token.h"


Tree::Tree() {

}

Tree::~Tree() {

}

void Tree::print() {
    if (head) {
        print(head, 0); // Начинаем с корневого узла на нулевом уровне
    } else {
        std::cout << "Tree is empty." << std::endl;
    }
}

void Tree::print(Node* node, uint8_t level) {
    if (node) {
        // Добавим красивые отступы для уровня вложенности
        for (int i = 0; i < level; ++i) {
            std::cout << "  |"; // Два пробела для каждого уровня вложенности
        }
        
        // Печатаем имя текущего узла
        std::cout << " " << node->name << std::endl;

        // Рекурсивно печатаем дочерние узлы
        for (auto child : node->next) {
            print(child, level + 1);
        }
    }
}

Node* Tree::add(std::string std) {
    Node* node = new Node(std);
    return node;
}

std::string Tree::getName(Node* node) {
    return node->name;
}

void Tree::setHead(Node* node) {
    this->head = node;
}

void Tree::addConnect(Node* nodeParent, Node* node) {
        nodeParent->next.push_front(node);
}

Node* Tree::add(uint8_t num, uint8_t condition) {
    switch (num)
    {      
    case TOKEN_KEY:
        return new Node("KEY");
        break;
    case TOKEN_KEY_:
        if (condition!=14) {
            return nullptr;
        }
        else {
            return new Node("KEY_");
        }
        break;
    case TOKEN_FROM:
        return new Node("FROM");
        break;
    case TOKEN_WHERE:
        return new Node("WHERE");
        break;
    case TOKEN_PARAM:
        return new Node("PARAM");
        break;
    default:
        return 0;
        break;
    }        
}
