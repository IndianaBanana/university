#pragma once
#ifndef VARS_H
#define VARS_H

enum VARS
{
    SHIFT1 = 1,
    SHIFT3 = 3,
    SHIFT5 = 5,
    SHIFT7 = 7,
    SHIFT9 = 9,
    SHIFT10 = 10,
    SHIFT11 = 11,
    SHIFT13 = 13,
    SHIFT15 = 15,
    SHIFT16 = 16,
    SHIFT17 = 17,
    SHIFT18 = 18,
    SHIFT19 = 19,
    SHIFT22 = 22,
    SHIFT23 = 23,
    SHIFT24 = 24,

    REDUCE1 = 101,
    REDUCE2 = 102,
    REDUCE3 = 103,
    REDUCE4 = 104,
    REDUCE5 = 105,
    REDUCE6 = 106,

    ACCEPT = 200
};

enum GOTOVARS
{
    S2 = 2,
    S4 = 4,
    S6 = 6,
    S8 = 8,
    S12 = 12,
    S14 = 14,
    S20 = 20,
    S21 = 21
};

#endif
