
#include <iostream>
#include <stdlib.h>
#include <string>
#include <stack>
#include "Token.h"
#include <FlexLexer.h>
#include "Vars.h"
#include "Token.h"
#include <memory>
#include "Node.h"

using namespace std;
extern int yylex();
extern FILE* yyin;
extern char* yytext;


enum CUR {
    SELECTION,
    FROM,
    WHERE
};
CUR cur = SELECTION;

const int actionTable[25][12] = {
    // select,    id,      ','       from,    where,   num,     <,       >,       =,       and,     or,      eof
    { SHIFT1,     0,       0,       0,       0,       0,       0,       0,       0,       0,       0,       0       }, // State 0
    { 0,          SHIFT3,  0,       0,       0,       0,       0,       0,       0,       0,       0,       0       }, // State 1
    { 0,          0,       0,       SHIFT5,  0,       0,       0,       0,       0,       0,       0,       0       }, // State 2
    { 0,          0,       SHIFT7,  REDUCE3, 0,       0,       0,       0,       0,       0,       0,       0       }, // State 3
    { 0,          0,       0,       0,       SHIFT9,  0,       0,       0,       0,       0,       0,       ACCEPT  }, // State 4
    { 0,          SHIFT10, 0,       0,       0,       0,       0,       0,       0,       0,       0,       0       }, // State 5
    { 0,          0,       0,       REDUCE2, 0,       0,       0,       0,       0,       0,       0,       0       }, // State 6
    { 0,          SHIFT11, 0,       0,       0,       0,       0,       0,       0,       0,       0,       0       }, // State 7
    { 0,          0,       0,       0,       0,       0,       0,       0,       0,       0,       0,       ACCEPT  }, // State 8
    { 0,          SHIFT13, 0,       0,       0,       0,       0,       0,       0,       0,       0,       0       }, // State 9
    { 0,          0,       0,       0,       REDUCE4, 0,       0,       0,       0,       0,       0,       REDUCE4 }, // State 10
    { 0,          0,       SHIFT7,  REDUCE3, 0,       0,       0,       0,       0,       0,       0,       REDUCE3 }, // State 11
    { 0,          0,       0,       0,       0,       0,       0,       0,       0,       SHIFT15, SHIFT16, REDUCE5 }, // State 12
    { 0,          0,       0,       0,       0,       0,       SHIFT17, SHIFT18, SHIFT19, 0,       0,       0       }, // State 13
    { 0,          0,       0,       REDUCE3, 0,       0,       0,       0,       0,       0,       0,       0       }, // State 14
    { 0,          SHIFT13, 0,       0,       0,       0,       0,       0,       0,       0,       0,       0       }, // State 15
    { 0,          SHIFT13, 0,       0,       0,       0,       0,       0,       0,       0,       0,       0       }, // State 16
    { 0,          0,       0,       0,       0,       SHIFT22, 0,       0,       0,       0,       0,       0       }, // State 17
    { 0,          0,       0,       0,       0,       SHIFT23, 0,       0,       0,       0,       0,       0       }, // State 18
    { 0,          0,       0,       0,       0,       SHIFT24, 0,       0,       0,       0,       0,       0       }, // State 19
    { 0,          0,       0,       0,       0,       0,       0,       0,       0,       SHIFT15, SHIFT16, REDUCE6 }, // State 20
    { 0,          0,       0,       0,       0,       0,       0,       0,       0,       SHIFT15, SHIFT16, REDUCE6 }, // State 21
    { 0,          0,       0,       0,       0,       0,       0,       0,       0,       REDUCE6, REDUCE6, REDUCE6 }, // State 22
    { 0,          0,       0,       0,       0,       0,       0,       0,       0,       REDUCE6, REDUCE6, REDUCE6 }, // State 23
    { 0,          0,       0,       0,       0,       0,       0,       0,       0,       REDUCE6, REDUCE6, REDUCE6 }  // State 24
};

const int gotoTable[25][5] = {
    //  KEY,   KEY`, FROM, WHERE,PARAM
        {0,    0,    0,    0,    0  },// State 0
        {S2,   0,    0,    0,    0  },// State 1
        {0,    0,    S4,   0,    0  },// State 2
        {0,    S6,   0,    0,    0  },// State 3
        {0,    0,    0,    S8,   0  },// State 4
        {0,    0,    0,    0,    0  },// State 5
        {0,    0,    0,    0,    0  },// State 6
        {0,    0,    0,    0,    0  },// State 7
        {0,    0,    0,    0,    0  },// State 8
        {0,    0,    0,    0,    S12},// State 9
        {0,    0,    0,    0,    0  },// State 10
        {0,    S14,  0,    0,    0  },// State 11
        {0,    0,    0,    0,    0  },// State 12
        {0,    0,    0,    0,    0  },// State 13
        {0,    0,    0,    0,    0  },// State 14
        {0,    0,    0,    0,    S20},// State 15
        {0,    0,    0,    0,    S21},// State 16
        {0,    0,    0,    0,    0  },// State 17
        {0,    0,    0,    0,    0  },// State 18
        {0,    0,    0,    0,    0  },// State 19
        {0,    0,    0,    0,    0  },// State 20
        {0,    0,    0,    0,    0  },// State 21
        {0,    0,    0,    0,    0  },// State 22
        {0,    0,    0,    0,    0  },// State 23
        {0,    0,    0,    0,    0  } // State 24
};

uint8_t transitionAction(uint8_t condition, uint8_t term) {
    return actionTable[condition][term];
}

uint8_t transitionGoto(uint8_t condition, uint8_t reduce) {
    return gotoTable[condition][reduce];
}

uint8_t getLength(uint8_t rule, uint8_t condition) {

    switch (rule) {
    case TOKEN_KEY: return 2;
    case TOKEN_KEY_: return condition != 14 ? 0 : 3;
    case TOKEN_FROM: return 2;
    case TOKEN_WHERE: return 2;
    case TOKEN_PARAM: return 3;
    default: throw std::runtime_error("Unknown rule");
    }
}

std::deque<shared_ptr<Node>> select_stack;
std::deque<shared_ptr<Node>> from_stack;
std::deque<shared_ptr<Node>> where_stack;
void push(string s) {
    switch (cur) {
    case SELECTION: select_stack.push_front(make_shared<Node>(s));
        break;
    case FROM: from_stack.push_front(make_shared<Node>(s));
        break;
    case WHERE: where_stack.push_front(make_shared<Node>(s));
        break;
    default: throw std::runtime_error("ERROR");
    }
}

void nextCur(uint8_t rule) {
    switch (rule) {
    case TOKEN_KEY: cur = FROM;
        break;
    case TOKEN_KEY_: cur = FROM;
        break;
    case TOKEN_FROM: cur = WHERE;
        break;
    case TOKEN_WHERE: cur = WHERE;
        break;
    case TOKEN_PARAM: cur = WHERE;
        break;
    default: throw std::runtime_error("Unknown rule");
    }
}

shared_ptr<Node> selectNode() {
    shared_ptr<Node> node = make_shared<Node>("SELECTION");
    select_stack.pop_back();
    if (select_stack.size() == 1) {
        node->addChild(select_stack.back());
        return node;
    }
    auto commaNode = make_shared<Node>(",");
    while (!select_stack.empty()) {
        if (select_stack.back()->value == ",") {
            select_stack.pop_back();
            continue;
        }
        commaNode->addChild(select_stack.back());
        select_stack.pop_back();
    }
    node->addChild(commaNode);
    return node;
}

shared_ptr<Node> fromNode() {
    shared_ptr<Node> node = make_shared<Node>("FROM");
    node->addChild(from_stack.front());
    return node;
}
int precedence(string s) {
    return s.compare("and") == 0 ? 1 : 0;
}
shared_ptr<Node> whereNode() {
    if (where_stack.empty()) {
        return nullptr;
    }
    stack<shared_ptr<Node>> operands;
    stack<string> operators;
    where_stack.pop_back();
    while (!where_stack.empty()) {
        if (where_stack.back()->value.compare("and") != 0 && where_stack.back()->value.compare("or") != 0) {
            auto idNode = where_stack.back();
            where_stack.pop_back();
            auto logicalNode = where_stack.back();
            logicalNode->addChild(idNode);
            where_stack.pop_back();
            logicalNode->addChild(where_stack.back());
            where_stack.pop_back();
            operands.push(logicalNode);
            continue;
        }  
            string c = where_stack.back()->value;
            where_stack.pop_back();
            while (!operators.empty() && precedence(operators.top()) >= precedence(c)) {
                string op = operators.top();
                operators.pop();
                auto right = operands.top();
                operands.pop();
                auto left = operands.top();
                operands.pop();
                auto opNode = make_shared<Node>(op);
                opNode->addChild(left);
                opNode->addChild(right);
                operands.push(opNode);
            }
            operators.push(c);
        

    }
    while (!operators.empty()) {
        string op = operators.top();
        operators.pop();

        auto right = operands.top();
        operands.pop();
        auto left = operands.top();
        operands.pop();

        auto opNode = make_shared<Node>(op);
        opNode->addChild(left);
        opNode->addChild(right);
        operands.push(opNode);
    }
    shared_ptr<Node> node = make_shared<Node>("WHERE");
    node->addChild(operands.top());
    return node;
}

int main(int argc, char** argv) {
    yyin = fopen("input.in", "r");
    uint8_t lexema = 0;
    uint8_t token = 0;
    uint8_t condition = 0;
    shared_ptr<Node> mainNode = make_shared<Node>("GOAL");
    std::stack<uint8_t> stack;


    stack.push(TOKEN_END_OF_FILE);
    stack.push(TOKEN_GOAL);

    token = yylex();

    do {
        lexema = transitionAction(stack.top(), token);
        if (token == TOKEN_ERROR || lexema == 0) {
            std::cout << "Error";
            break;
        }
        if (lexema < 100) {
            stack.push(token);
            stack.push(lexema);
            push(string{ yytext });
            token = yylex();
        }
        else if (lexema > 100 && lexema != 200) {
            uint8_t rule = lexema % 100 - 1;
            nextCur(rule);
            uint8_t j = getLength(rule, stack.top());

            for (uint8_t i = 0; i < j; i++)
            {
                stack.pop();
                stack.pop();
            }
            condition = transitionGoto(stack.top(), rule - 1);
            stack.push(rule);
            stack.push(condition);

        }
        else  if (lexema == ACCEPT) {
            std::cout << "Succes";
            auto select = selectNode();
            select->addChild(fromNode());
            select->addChild(whereNode());
            mainNode->addChild(select);
            mainNode->print();
            break;
        }
        else {
            std::cout << "Error";
            break;
        }
    } while (true);

}