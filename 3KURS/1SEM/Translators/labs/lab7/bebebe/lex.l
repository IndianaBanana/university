%option noyywrap

%{
    #include <iostream>
    #include <stdlib.h>
    #include <string.h>
    #include <string>
    #include <stack>
    #include "Token.h"

%}

%%
"select" {return TOKEN_TERM_SELECT; }
"," {return TOKEN_TERM_COMMA; }
"from" {return TOKEN_TERM_FROM; }
"where" {return TOKEN_TERM_WHERE; }
"<" {return TOKEN_TERM_LESS; }
">" {return TOKEN_TERM_MORE; }
"=" {return TOKEN_TERM_EQUALLY; }
"and" {return TOKEN_TERM_AND; }
"or" {return TOKEN_TERM_OR; }
[a-zA-z][a-zA-z|0-9]* { return TOKEN_TERM_ID; }
[0-9]+ { return TOKEN_TERM_NUM; }
";" {return TOKEN_END_OF_FILE; }
[ \t\n\r ]

<<EOF>> { return TOKEN_END_OF_FILE; }
. { return TOKEN_ERROR; }

%%