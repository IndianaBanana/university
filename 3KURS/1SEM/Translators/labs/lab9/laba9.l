%option noyywrap

%{
    #include <stdlib.h>
    #include <stdbool.h>
    #include <string.h>
    #include <cstdint>

    #include <iostream>
    #include <string>
    #include <stack>
    #include <vector>
    #include <set>
    #include <map>

    #define MAX_STR_LENGTH 256

namespace {
	using std::vector;
	using std::set;
	using std::map;
	using std::stack;
	using std::string;
	using std::cout;

	class Node {
		std::string name_;
		std::deque<Node*> next_;
	public:
		Node(std::string name) { name_ = name; }
		void setName(std::string name) { name_ = name; }
		Node* getLastChild() { return next_.back(); }
		friend class Tree;
	};

	Node* goal;

	class Tree {
		Node* head_;
	public:
		void print(Node* node, uint8_t level);
		void print();
		Node* addChild(Node* nodeParent, Node* node);
		void setHead(Node* node);
	} tree;

	void Tree::setHead(Node* node) { head_ = node; }
	void Tree::print() { print(head_, 0); }

	void Tree::print(Node* node, uint8_t level) {
		if (node) {
			for (int i = 0; i < level; ++i) {
				std::cout << "  |";
			}
			std::cout << node->name_ << std::endl;
			for (Node* nodes : node->next_) {
				print(nodes, level + 1);
			}
		}
	}

	Node* Tree::addChild(Node* nodeParent, Node* node) {
		if (node) {
			nodeParent->next_.push_back(node);
			return nodeParent;
		}
		return 0;
	}

	enum Token {
		TOKEN_T_CREATE, //0
		TOKEN_T_TABLE, //1
		TOKEN_T_PRIMARY, //2
		TOKEN_T_FOREIGN, //3
		TOKEN_T_KEY, //4
		TOKEN_T_REFERENCES, //5
		TOKEN_T_NULL, //6
		TOKEN_T_NOT, //7
		TOKEN_T_INTEGER, //8
		TOKEN_T_CHAR, //9
		TOKEN_T_LEFT_PARENTHESIS, //10
		TOKEN_T_RIGHT_PARENTHESIS, //11
		TOKEN_T_COMMA, //12
		TOKEN_END_STATEMENT, //13
		TOKEN_T_ID, //14
		TOKEN_T_NUM, //15
		TOKEN_END_OF_FILE, //16
		TOKEN_ERROR, //17

		TOKEN_GOAL, //18
		TOKEN_FIELD, //19
		TOKEN_FIELD_, //20
		TOKEN_TYPE, //21
		TOKEN_NULLABLE, //22
		TOKEN_PRIMARY_KEY, //23
		TOKEN_FOREIGN_KEY, //24
		TOKEN_FOREIGN_KEY_, //25
		TOKEN_KEY, //26
		TOKEN_KEY_, //27
		TOKEN_EPS //28
	};

	Node* curr_node;
	Node* foreign_node;
	Node* null_node;

	void terminal(std::string s) { tree.addChild(curr_node, new Node(s)); }
	void references_terminal(std::string s) { Node* n = new Node("REFERENCES"); tree.addChild(foreign_node, n); curr_node = n; }
	void NOTterminal(std::string s) { Node* n = new Node("NOT"); tree.addChild(curr_node, n); curr_node = n; }

	void n_field(std::string s) { Node* n = new Node("FIELD"); tree.addChild(goal, n); curr_node = n; }
	void n_field_(std::string s) { Node* n = new Node("FIELD"); tree.addChild(goal, n); curr_node = n; }
	void n_type(std::string s) { Node* n = new Node("TYPE"); tree.addChild(curr_node, n); curr_node = n; }
	void n_nullable(std::string s) { Node* n = new Node("NULLABLE"); tree.addChild(curr_node, n); curr_node = n; }
	void n_primary(std::string s) { Node* n = new Node("PRIMARY"); tree.addChild(goal, n); curr_node = n; }
	void n_foreign(std::string s) { Node* n = new Node("FOREIGN"); tree.addChild(goal, n); curr_node = n; foreign_node = n; }
	void n_foreign_(std::string s) { Node* n = new Node("FOREIGN"); tree.addChild(goal, n); curr_node = n; foreign_node = n; }
	void n_key(std::string s) { Node* n = new Node(","); tree.addChild(curr_node, n); curr_node = n; }
	void n_eps(std::string s) { Node* n = new Node("EPS"); tree.addChild(curr_node, n); curr_node = n; }

	void (*SemanticRules[])(std::string) {
		NULL, //0
		NULL, //1
		NULL, //2
		NULL, //3
		NULL, //4
		references_terminal, //5
		terminal, //6
		NOTterminal, //7
		terminal, //8
		terminal, //9
		NULL, //10
		NULL, //11
		NULL, //12
		terminal, //13
		terminal, //14
		terminal, //15
		terminal, //16
		terminal, //17

		NULL, //18
		n_field, //19
		n_field_, //20
		n_type, //21
		n_nullable, //22
		n_primary, //23
		n_foreign, //24
		n_foreign_, //25
		n_key, //26
		NULL, //27
		n_eps, //28
	};


	set<Token> const TERMINALS {
		TOKEN_T_CREATE,
		TOKEN_T_TABLE,
		TOKEN_T_PRIMARY,
		TOKEN_T_FOREIGN,
		TOKEN_T_KEY,
		TOKEN_T_REFERENCES,
		TOKEN_T_NULL,
		TOKEN_T_NOT,
		TOKEN_T_INTEGER,
		TOKEN_T_CHAR,
		TOKEN_T_ID,
		TOKEN_T_NUM,
		TOKEN_T_LEFT_PARENTHESIS,
		TOKEN_T_RIGHT_PARENTHESIS,
		TOKEN_T_COMMA
	};

	set<Token> const NON_TERMINALS {
		TOKEN_GOAL,
		TOKEN_FIELD,
		TOKEN_FIELD_,
		TOKEN_TYPE,
		TOKEN_NULLABLE,
		TOKEN_PRIMARY_KEY,
		TOKEN_FOREIGN_KEY,
		TOKEN_FOREIGN_KEY_,
		TOKEN_KEY,
		TOKEN_KEY_
	};
	// non_terminal - extern
	// terminal - intern
	map<Token, map<Token, vector<Token>>> const TRANSMITION_TABLE {
		{ TOKEN_GOAL,
			{
				{TOKEN_T_CREATE,            {TOKEN_T_CREATE, TOKEN_T_TABLE, TOKEN_T_ID, TOKEN_T_LEFT_PARENTHESIS, TOKEN_FIELD, TOKEN_PRIMARY_KEY, TOKEN_FOREIGN_KEY}},
				{TOKEN_T_TABLE,             {}},
				{TOKEN_T_PRIMARY,           {}},
				{TOKEN_T_FOREIGN,           {}},
				{TOKEN_T_KEY,               {}},
				{TOKEN_T_REFERENCES,        {}},
				{TOKEN_T_NULL,              {}},
				{TOKEN_T_NOT,               {}},
				{TOKEN_T_INTEGER,           {}},
				{TOKEN_T_CHAR,              {}},
				{TOKEN_T_ID,                {}},
				{TOKEN_T_NUM,               {}},
				{TOKEN_T_LEFT_PARENTHESIS,  {}},
				{TOKEN_T_RIGHT_PARENTHESIS, {}},
				{TOKEN_T_COMMA,             {}}
			}
		},
		{ TOKEN_FIELD,
			{
				{TOKEN_T_CREATE,            {}},
				{TOKEN_T_TABLE,             {}},
				{TOKEN_T_PRIMARY,           {}},
				{TOKEN_T_FOREIGN,           {}},
				{TOKEN_T_KEY,               {}},
				{TOKEN_T_REFERENCES,        {}},
				{TOKEN_T_NULL,              {}},
				{TOKEN_T_NOT,               {}},
				{TOKEN_T_INTEGER,           {}},
				{TOKEN_T_CHAR,              {}},
				{TOKEN_T_ID,                {TOKEN_T_ID, TOKEN_TYPE, TOKEN_NULLABLE, TOKEN_T_COMMA, TOKEN_FIELD_}},
				{TOKEN_T_NUM,               {}},
				{TOKEN_T_LEFT_PARENTHESIS,  {}},
				{TOKEN_T_RIGHT_PARENTHESIS, {}},
				{TOKEN_T_COMMA,             {}}
			}
		},
		{ TOKEN_FIELD_,
			{
				{TOKEN_T_CREATE,            {}},
				{TOKEN_T_TABLE,             {}},
				{TOKEN_T_PRIMARY,           {TOKEN_EPS}},
				{TOKEN_T_FOREIGN,           {}},
				{TOKEN_T_KEY,               {}},
				{TOKEN_T_REFERENCES,        {}},
				{TOKEN_T_NULL,              {}},
				{TOKEN_T_NOT,               {}},
				{TOKEN_T_INTEGER,           {}},
				{TOKEN_T_CHAR,              {}},
				{TOKEN_T_ID,                {TOKEN_T_ID, TOKEN_TYPE, TOKEN_NULLABLE, TOKEN_T_COMMA, TOKEN_FIELD_}},
				{TOKEN_T_NUM,               {}},
				{TOKEN_T_LEFT_PARENTHESIS,  {}},
				{TOKEN_T_RIGHT_PARENTHESIS, {}},
				{TOKEN_T_COMMA,             {}}
			}
		},
		{ TOKEN_TYPE,
			{
				{TOKEN_T_CREATE,            {}},
				{TOKEN_T_TABLE,             {}},
				{TOKEN_T_PRIMARY,           {}},
				{TOKEN_T_FOREIGN,           {}},
				{TOKEN_T_KEY,               {}},
				{TOKEN_T_REFERENCES,        {}},
				{TOKEN_T_NULL,              {}},
				{TOKEN_T_NOT,               {}},
				{TOKEN_T_INTEGER,           {TOKEN_T_INTEGER}},
				{TOKEN_T_CHAR,              {TOKEN_T_CHAR, TOKEN_T_LEFT_PARENTHESIS, TOKEN_T_NUM, TOKEN_T_RIGHT_PARENTHESIS}},
				{TOKEN_T_ID,                {}},
				{TOKEN_T_NUM,               {}},
				{TOKEN_T_LEFT_PARENTHESIS,  {}},
				{TOKEN_T_RIGHT_PARENTHESIS, {}},
				{TOKEN_T_COMMA,             {}}
			}
		},
		{ TOKEN_NULLABLE,
			{
				{TOKEN_T_CREATE,            {}},
				{TOKEN_T_TABLE,             {}},
				{TOKEN_T_PRIMARY,           {}},
				{TOKEN_T_FOREIGN,           {}},
				{TOKEN_T_KEY,               {}},
				{TOKEN_T_REFERENCES,        {}},
				{TOKEN_T_NULL,              {TOKEN_T_NULL}},
				{TOKEN_T_NOT,               {TOKEN_T_NOT, TOKEN_T_NULL}},
				{TOKEN_T_INTEGER,           {}},
				{TOKEN_T_CHAR,              {}},
				{TOKEN_T_ID,                {}},
				{TOKEN_T_NUM,               {}},
				{TOKEN_T_LEFT_PARENTHESIS,  {}},
				{TOKEN_T_RIGHT_PARENTHESIS, {}},
				{TOKEN_T_COMMA,             {}}
			}
		},
		{ TOKEN_PRIMARY_KEY,
			{
				{TOKEN_T_CREATE,            {}},
				{TOKEN_T_TABLE,             {}},
				{TOKEN_T_PRIMARY,           {TOKEN_T_PRIMARY, TOKEN_T_KEY, TOKEN_T_LEFT_PARENTHESIS, TOKEN_KEY, TOKEN_T_RIGHT_PARENTHESIS}},
				{TOKEN_T_FOREIGN,           {}},
				{TOKEN_T_KEY,               {}},
				{TOKEN_T_REFERENCES,        {}},
				{TOKEN_T_NULL,              {}},
				{TOKEN_T_NOT,               {}},
				{TOKEN_T_INTEGER,           {}},
				{TOKEN_T_CHAR,              {}},
				{TOKEN_T_ID,                {}},
				{TOKEN_T_NUM,               {}},
				{TOKEN_T_LEFT_PARENTHESIS,  {}},
				{TOKEN_T_RIGHT_PARENTHESIS, {}},
				{TOKEN_T_COMMA,             {}}
			}
		},
		{ TOKEN_FOREIGN_KEY,
			{
				{TOKEN_T_CREATE,            {}},
				{TOKEN_T_TABLE,             {}},
				{TOKEN_T_PRIMARY,           {}},
				{TOKEN_T_FOREIGN,           {}},
				{TOKEN_T_KEY,               {}},
				{TOKEN_T_REFERENCES,        {}},
				{TOKEN_T_NULL,              {}},
				{TOKEN_T_NOT,               {}},
				{TOKEN_T_INTEGER,           {}},
				{TOKEN_T_CHAR,              {}},
				{TOKEN_T_ID,                {}},
				{TOKEN_T_NUM,               {}},
				{TOKEN_T_LEFT_PARENTHESIS,  {}},
				{TOKEN_T_RIGHT_PARENTHESIS, {}},
				{TOKEN_T_COMMA,             {TOKEN_T_COMMA, TOKEN_T_FOREIGN, TOKEN_T_KEY, TOKEN_T_LEFT_PARENTHESIS,
				TOKEN_KEY, TOKEN_T_RIGHT_PARENTHESIS, TOKEN_T_REFERENCES, TOKEN_T_ID, TOKEN_T_LEFT_PARENTHESIS, TOKEN_KEY, TOKEN_T_RIGHT_PARENTHESIS, TOKEN_FOREIGN_KEY_}}
			}
		},
		{ TOKEN_FOREIGN_KEY_,
			{
				{TOKEN_T_CREATE,            {}},
				{TOKEN_T_TABLE,             {}},
				{TOKEN_T_PRIMARY,           {}},
				{TOKEN_T_FOREIGN,           {}},
				{TOKEN_T_KEY,               {}},
				{TOKEN_T_REFERENCES,        {}},
				{TOKEN_T_NULL,              {}},
				{TOKEN_T_NOT,               {}},
				{TOKEN_T_INTEGER,           {}},
				{TOKEN_T_CHAR,              {}},
				{TOKEN_T_ID,                {}},
				{TOKEN_T_NUM,               {}},
				{TOKEN_T_LEFT_PARENTHESIS,  {}},
				{TOKEN_T_RIGHT_PARENTHESIS, {TOKEN_EPS}},
				{TOKEN_T_COMMA,             {TOKEN_T_COMMA, TOKEN_T_FOREIGN, TOKEN_T_KEY, TOKEN_T_LEFT_PARENTHESIS,
				TOKEN_KEY, TOKEN_T_RIGHT_PARENTHESIS, TOKEN_T_REFERENCES, TOKEN_T_ID, TOKEN_T_LEFT_PARENTHESIS, TOKEN_KEY, TOKEN_T_RIGHT_PARENTHESIS, TOKEN_FOREIGN_KEY_}}
			}
		},
		{ TOKEN_KEY,
			{
				{TOKEN_T_CREATE,            {}},
				{TOKEN_T_TABLE,             {}},
				{TOKEN_T_PRIMARY,           {}},
				{TOKEN_T_FOREIGN,           {}},
				{TOKEN_T_KEY,               {}},
				{TOKEN_T_REFERENCES,        {}},
				{TOKEN_T_NULL,              {}},
				{TOKEN_T_NOT,               {}},
				{TOKEN_T_INTEGER,           {}},
				{TOKEN_T_CHAR,              {}},
				{TOKEN_T_ID,                {TOKEN_T_ID, TOKEN_KEY_}},
				{TOKEN_T_NUM,               {}},
				{TOKEN_T_LEFT_PARENTHESIS,  {}},
				{TOKEN_T_RIGHT_PARENTHESIS, {}},
				{TOKEN_T_COMMA,             {}}
			}
		},
		{ TOKEN_KEY_,
			{
				{TOKEN_T_CREATE,            {}},
				{TOKEN_T_TABLE,             {}},
				{TOKEN_T_PRIMARY,           {}},
				{TOKEN_T_FOREIGN,           {}},
				{TOKEN_T_KEY,               {}},
				{TOKEN_T_REFERENCES,        {}},
				{TOKEN_T_NULL,              {}},
				{TOKEN_T_NOT,               {}},
				{TOKEN_T_INTEGER,           {}},
				{TOKEN_T_CHAR,              {}},
				{TOKEN_T_ID,                {}},
				{TOKEN_T_NUM,               {}},
				{TOKEN_T_LEFT_PARENTHESIS,  {}},
				{TOKEN_T_RIGHT_PARENTHESIS, {TOKEN_EPS}},
				{TOKEN_T_COMMA,             {TOKEN_T_COMMA, TOKEN_T_ID, TOKEN_KEY_}}
			}
		},
	};

	inline bool is_terminal(Token tok) {
		return TERMINALS.find(tok) != TERMINALS.cend();
	}

	bool analyze(vector<Token> const& tokens, vector<string> const& values) {
		std::stack<Token> stack;
		std::stack<Node*> nodeStack;

		stack.push(TOKEN_END_OF_FILE);
		stack.push(TOKEN_GOAL);

		nodeStack.push(new Node("TOKEN_END_OF_FILE"));
		goal = new Node("CREATE");
		curr_node = goal;
		tree.setHead(goal);
		nodeStack.push(goal);

		auto cur_token{ tokens.cbegin() };
		auto cur_value{ values.cbegin() };

		void (*f)(std::string);
		do {
			f = SemanticRules[(int)stack.top()];
			
			if (is_terminal(stack.top())) {
				if (f != NULL) f(*cur_value);

				if (*cur_token == stack.top()) {
					stack.pop();
					nodeStack.pop();
					++cur_token;
					++cur_value;
				}
				else return false;
			}
			else {
				vector<Token> const& transmition{ TRANSMITION_TABLE.at(stack.top()).at(*cur_token) };
				if (transmition.size() > 0) {
					stack.pop();
					nodeStack.pop();

					if (transmition.size() == 1U && transmition.at(0U) == TOKEN_EPS) continue;

					if (f != NULL) f(*cur_value);

					for (auto it{ transmition.crbegin() }; it != transmition.crend(); ++it) {
						stack.push(*it);
					}
				}
				else return false;
			}
		} while (stack.top() != TOKEN_END_OF_FILE);
		return (tokens.back() == TOKEN_T_RIGHT_PARENTHESIS);
	}
}

%}


%%
"create" {return TOKEN_T_CREATE; }
"table" {return TOKEN_T_TABLE; }
"primary" {return TOKEN_T_PRIMARY; }
"foreign" {return TOKEN_T_FOREIGN; }
"key" {return TOKEN_T_KEY; }
"references" {return TOKEN_T_REFERENCES; }
"null" {return TOKEN_T_NULL; }
"not" {return TOKEN_T_NOT; }
"integer" {return TOKEN_T_INTEGER; }
"char" {return TOKEN_T_CHAR; }
"(" {return TOKEN_T_LEFT_PARENTHESIS; }
")" {return TOKEN_T_RIGHT_PARENTHESIS; }
"," {return TOKEN_T_COMMA; }
";" {return TOKEN_END_STATEMENT; }
[a-zA-z][a-zA-z|0-9]* { return TOKEN_T_ID; }
[0-9]+ { return TOKEN_T_NUM; }

[ \t\n\r ]

<<EOF>> { return TOKEN_END_OF_FILE; }
. { return TOKEN_ERROR; }

%%

int main() {
	yyin = fopen("input.in", "r");

	vector<Token> tokens;
	vector<string> values;
	do {
		Token tmp_token{ static_cast<Token>(yylex()) };
		string tmp_value = yytext;

		if (tmp_token == TOKEN_END_STATEMENT) break;

		tokens.push_back(tmp_token);
		values.push_back(tmp_value);
	} while (tokens.back() != TOKEN_ERROR && tokens.back() != TOKEN_END_OF_FILE);

	if (tokens.back() == TOKEN_ERROR) std::cout << "Fail.\n";
	else {
		if (analyze(tokens, values)) {
			tree.print();
			std::cout << "Success.\n";
		}
		else {
			std::cout << "Fail.\n";
			return -1;
		}
	}
}