#pragma once
#ifndef NODE_H
#define NODE_H

#include <iostream>
#include <vector>
#include <set>
#include <map>
#include <string>
#include <memory>
#include <deque>

#include "Token.h"
#include "Table.h"

using namespace std;



class Node {
public:             
    std::string value;                  
    std::deque<std::shared_ptr<Node>> children; 

    Node(std::string v) : value(std::move(v)) {}

    void addChild(std::shared_ptr<Node> child) {
        children.push_back(std::move(child));
    }

    void print(int depth = 0) const {
        for (int i = 0; i < depth; ++i) std::cout << "   |";
        std::cout << value;
        std::cout << "\n";
        for (const auto& child : children) {
            if (!child) continue;
            child->print(depth + 1);
        }
    }
};
#endif