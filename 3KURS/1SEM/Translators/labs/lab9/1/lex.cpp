%option noyywrap

%{
    #include <stdlib.h>
    #include <stdbool.h>
    #include <string.h>

    #include <iostream>
    #include <string>
    #include <stack>
    #include <vector>
    #include <set>
    #include <map>
	#include <memory>

	#include "Token.h"
	#include "Table.h"
	#include "Node.h"

    #define MAX_STR_LENGTH 512

	using namespace std;
	int yylex();
	uint8_t token;
	char yylval[MAX_STR_LENGTH];

	void next_token()
	{
		token = yylex();
		if (token == TOKEN_ERROR) throw runtime_error("Lexical error");
	}

	bool is_terminal(Token tok)
	{
		return TERMINALS.find(tok) != TERMINALS.cend();
	}

	bool analyze()
	{
		stack<Token> stack;

		stack.push(TOKEN_END_OF_FILE);
		stack.push(TOKEN_GOAL);

		next_token();
		if (token == TOKEN_END_OF_FILE) return true;

		do
		{
			if (is_terminal(stack.top()))
			{
				if (token == stack.top())
				{
					stack.pop();
				// printf("%d ", token);
					next_token();
				}
				else return false;
			}
			else
			{
				// printf("%d ", stack.top());
				// printf("%d ", token);
				vector<Token> const& transmission = TRANSMISSION_TABLE.at(stack.top()).at((Token) token);
				if (transmission.size() > 0)
				{
					// if (stack.top() == 27 || stack.top() == 25) printf("\t");
					// if (token == 12) printf("\t");
					stack.pop();
					if (transmission.size() == 1U && transmission.at(0U) == TOKEN_EPS) continue;
					for (int i = transmission.size() - 1; i >= 0; --i) {
						stack.push(transmission.at(i));
					}
				}
				else return false;
			}
		} while (stack.top() != TOKEN_END_OF_FILE);
		return (token == TOKEN_T_RIGHT_PARENTHESIS);
	}
	int refer = 0;
	char* sp = "\t";

%}


%%
"create" {memcpy(yylval, yytext, strlen(yytext) + 1);printf("%s\n", yylval); return TOKEN_T_CREATE; }

"table" {memcpy(yylval, yytext, strlen(yytext) + 1); printf("\t%s\n\t", yylval); return TOKEN_T_TABLE; }

"primary" {memcpy(yylval, yytext, strlen(yytext) + 1); printf("\t%s ", yylval); return TOKEN_T_PRIMARY; }

"foreign" {memcpy(yylval, yytext, strlen(yytext) + 1); printf("\t%s ", yylval); return TOKEN_T_FOREIGN; }

"key" {memcpy(yylval, yytext, strlen(yytext) + 1); printf("%s\n\t\t", yylval); sp = "\t\t"; return TOKEN_T_KEY; }

"references" {memcpy(yylval, yytext, strlen(yytext) + 1); printf("\t\t%s\n\t\t\t", yylval); refer=1; return TOKEN_T_REFERENCES; }

"null" {memcpy(yylval, yytext, strlen(yytext) + 1); printf("\t\t%s\n\t\t", yylval);  return TOKEN_T_NULL; }

"not" {memcpy(yylval, yytext, strlen(yytext) + 1); printf("\t\t%s\n\t\t\t", yylval); return TOKEN_T_NOT; }

"integer" {memcpy(yylval, yytext, strlen(yytext) + 1); printf("\t\t%s\n\t\t", yylval); return TOKEN_T_INTEGER; }

"char" {memcpy(yylval, yytext, strlen(yytext) + 1); printf("\t\t%s\n\t\t\t", yylval); return TOKEN_T_CHAR; }

"(" {memcpy(yylval, yytext, strlen(yytext) + 1); if (refer){refer=0;sp="\t\t\t\t";}  return TOKEN_T_LEFT_PARENTHESIS; }

")" {memcpy(yylval, yytext, strlen(yytext) + 1); return TOKEN_T_RIGHT_PARENTHESIS; }

"," {memcpy(yylval, yytext, strlen(yytext) + 1); return TOKEN_T_COMMA; }

";" {memcpy(yylval, yytext, strlen(yytext) + 1); return TOKEN_END_STATEMENT; }

[a-zA-z_][a-zA-z_|0-9]* { memcpy(yylval, yytext, strlen(yytext) + 1); printf("%s%s\n\t\t", sp, yylval); return TOKEN_T_ID; }

[0-9]+ { memcpy(yylval, yytext, strlen(yytext) + 1); printf("\t\t%s\n\t\t ", yylval); return TOKEN_T_NUM; }

[ \t\n\r ]

<<EOF>> { return TOKEN_END_OF_FILE; }
. { return TOKEN_ERROR; }

%%

int main()
{	

	yyin = fopen("input.in", "r");

	std::cout << (analyze() ? "\nsuccess" : "\nfail");

	return EXIT_SUCCESS;
}