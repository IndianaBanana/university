#pragma once
#ifndef TABLE_H
#define TABLE_H

#include <iostream>
#include <vector>
#include <set>
#include <map>
#include "Token.h"

std::map<Token, std::map<Token, std::vector<Token>>> const TRANSMISSION_TABLE
{
    { TOKEN_GOAL,
        {
            {TOKEN_T_CREATE, {TOKEN_T_CREATE, TOKEN_T_TABLE, TOKEN_T_ID, TOKEN_T_LEFT_PARENTHESIS, TOKEN_FIELD, TOKEN_PRIMARY_KEY, TOKEN_FOREIGN_KEY}},
        }
    },
    { TOKEN_FIELD,
        {
            {TOKEN_T_ID,     {TOKEN_T_ID, TOKEN_TYPE, TOKEN_NULLABLE, TOKEN_T_COMMA, TOKEN_FIELD_}},
        }
    },
    { TOKEN_FIELD_,
        {
            {TOKEN_T_PRIMARY,{TOKEN_EPS}},
            {TOKEN_T_ID,     {TOKEN_T_ID, TOKEN_TYPE, TOKEN_NULLABLE, TOKEN_T_COMMA, TOKEN_FIELD_}},
        }
    },
    { TOKEN_TYPE,
        {
            {TOKEN_T_INTEGER,{TOKEN_T_INTEGER}},
            {TOKEN_T_CHAR,   {TOKEN_T_CHAR, TOKEN_T_LEFT_PARENTHESIS, TOKEN_T_NUM, TOKEN_T_RIGHT_PARENTHESIS}},
        }
    },
    { TOKEN_NULLABLE,
        {
            {TOKEN_T_NULL,   {TOKEN_T_NULL}},
            {TOKEN_T_NOT,    {TOKEN_T_NOT, TOKEN_T_NULL}},
        }
    },
    { TOKEN_PRIMARY_KEY,
        {
            {TOKEN_T_PRIMARY,{TOKEN_T_PRIMARY, TOKEN_T_KEY, TOKEN_T_LEFT_PARENTHESIS, TOKEN_KEY, TOKEN_T_RIGHT_PARENTHESIS}},
        }
    },
    { TOKEN_FOREIGN_KEY,
        {
            {TOKEN_T_COMMA,  {TOKEN_T_COMMA, TOKEN_T_FOREIGN, TOKEN_T_KEY, TOKEN_T_LEFT_PARENTHESIS, TOKEN_KEY, TOKEN_T_RIGHT_PARENTHESIS, TOKEN_T_REFERENCES, TOKEN_T_ID, TOKEN_T_LEFT_PARENTHESIS, TOKEN_KEY, TOKEN_T_RIGHT_PARENTHESIS, TOKEN_FOREIGN_KEY_}}
        }
    },
    { TOKEN_FOREIGN_KEY_,
        {
            {TOKEN_T_RIGHT_PARENTHESIS, {TOKEN_EPS}},
            {TOKEN_T_COMMA,  {TOKEN_T_COMMA, TOKEN_T_FOREIGN, TOKEN_T_KEY, TOKEN_T_LEFT_PARENTHESIS, TOKEN_KEY, TOKEN_T_RIGHT_PARENTHESIS, TOKEN_T_REFERENCES, TOKEN_T_ID, TOKEN_T_LEFT_PARENTHESIS, TOKEN_KEY, TOKEN_T_RIGHT_PARENTHESIS, TOKEN_FOREIGN_KEY_}}
        }
    },
    { TOKEN_KEY,
        {
            {TOKEN_T_ID,     {TOKEN_T_ID, TOKEN_KEY_}},
        }
    },
    { TOKEN_KEY_,
        {
            {TOKEN_T_RIGHT_PARENTHESIS, {TOKEN_EPS}},
            {TOKEN_T_COMMA,  {TOKEN_T_COMMA, TOKEN_T_ID, TOKEN_KEY_}}
        }
    },
};

#endif