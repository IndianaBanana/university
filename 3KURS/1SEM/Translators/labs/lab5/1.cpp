%option noyywrap

%{
    #include <iostream>
    #include <stdlib.h>
    // #include <stdbool.h>
    #include <string.h>
    #include <string>
    #include <stack>
    #include "Token.h"

    enum VARS
    {
        SHIFT1=1,
        SHIFT3=3,
        SHIFT5=5,
        SHIFT7=7,
        SHIFT9=9,
        SHIFT10=10,
        SHIFT11=11,
        SHIFT13=13,
        SHIFT15=15,
        SHIFT16=16,
        SHIFT17=17,
        SHIFT18=18,
        SHIFT19=19,
        SHIFT22=22,
        SHIFT23=23,
        SHIFT24=24,

        REDUCE1=101,
        REDUCE2=102,
        REDUCE3=103,
        REDUCE4=104,
        REDUCE5=105,
        REDUCE6=106,

        ACCEPT = 200
    };
    enum GOTOVARS
    {
        S2 = 2,
        S4 = 4,
        S6 = 6,
        S8 = 8,
        S12 = 12,
        S14 = 14,
        S20 = 20,
        S21 = 21
    };


%}

%%
"select" {return TOKEN_TERM_SELECT; }

"," {return TOKEN_TERM_COMMA; }
"from" {return TOKEN_TERM_FROM; }
"where" {return TOKEN_TERM_WHERE; }

"<" {return TOKEN_TERM_LESS; }
">" {return TOKEN_TERM_MORE; }
"=" {return TOKEN_TERM_EQUALLY; }
"and" {return TOKEN_TERM_AND; }
"or" {return TOKEN_TERM_OR; }
[a-zA-z][a-zA-z|0-9]* { return TOKEN_TERM_ID; }
[0-9]+ { return TOKEN_TERM_NUM; }
";" {return TOKEN_END_OF_FILE; }
[ \t\n\r ]

<<EOF>> { return TOKEN_END_OF_FILE; }
. { return TOKEN_ERROR; }

%%
const int actionTable[25][12] = {
    // select,    id,      ','       from,    where,   num,     <,       >,       =,       and,     or,      eof
    { SHIFT1,     0,       0,       0,       0,       0,       0,       0,       0,       0,       0,       0       }, // State 0
    { 0,          SHIFT3,  0,       0,       0,       0,       0,       0,       0,       0,       0,       0       }, // State 1
    { 0,          0,       0,       SHIFT5,  0,       0,       0,       0,       0,       0,       0,       0       }, // State 2
    { 0,          0,       SHIFT7,  REDUCE3, 0,       0,       0,       0,       0,       0,       0,       0       }, // State 3
    { 0,          0,       0,       0,       SHIFT9,  0,       0,       0,       0,       0,       0,       ACCEPT  }, // State 4
    { 0,          SHIFT10, 0,       0,       0,       0,       0,       0,       0,       0,       0,       0       }, // State 5
    { 0,          0,       0,       REDUCE2, 0,       0,       0,       0,       0,       0,       0,       0       }, // State 6
    { 0,          SHIFT11, 0,       0,       0,       0,       0,       0,       0,       0,       0,       0       }, // State 7
    { 0,          0,       0,       0,       0,       0,       0,       0,       0,       0,       0,       ACCEPT  }, // State 8
    { 0,          SHIFT13, 0,       0,       0,       0,       0,       0,       0,       0,       0,       0       }, // State 9
    { 0,          0,       0,       0,       REDUCE4, 0,       0,       0,       0,       0,       0,       REDUCE4 }, // State 10
    { 0,          0,       SHIFT7,  REDUCE3, 0,       0,       0,       0,       0,       0,       0,       REDUCE3 }, // State 11
    { 0,          0,       0,       0,       0,       0,       0,       0,       0,       SHIFT15, SHIFT16, REDUCE5 }, // State 12
    { 0,          0,       0,       0,       0,       0,       SHIFT17, SHIFT18, SHIFT19, 0,       0,       0       }, // State 13
    { 0,          0,       0,       REDUCE3, 0,       0,       0,       0,       0,       0,       0,       0       }, // State 14
    { 0,          SHIFT13, 0,       0,       0,       0,       0,       0,       0,       0,       0,       0       }, // State 15
    { 0,          SHIFT13, 0,       0,       0,       0,       0,       0,       0,       0,       0,       0       }, // State 16
    { 0,          0,       0,       0,       0,       SHIFT22, 0,       0,       0,       0,       0,       0       }, // State 17
    { 0,          0,       0,       0,       0,       SHIFT23, 0,       0,       0,       0,       0,       0       }, // State 18
    { 0,          0,       0,       0,       0,       SHIFT24, 0,       0,       0,       0,       0,       0       }, // State 19
    { 0,          0,       0,       0,       0,       0,       0,       0,       0,       SHIFT15, SHIFT16, REDUCE6 }, // State 20
    { 0,          0,       0,       0,       0,       0,       0,       0,       0,       SHIFT15, SHIFT16, REDUCE6 }, // State 21
    { 0,          0,       0,       0,       0,       0,       0,       0,       0,       REDUCE6, REDUCE6, REDUCE6 }, // State 22
    { 0,          0,       0,       0,       0,       0,       0,       0,       0,       REDUCE6, REDUCE6, REDUCE6 }, // State 23
    { 0,          0,       0,       0,       0,       0,       0,       0,       0,       REDUCE6, REDUCE6, REDUCE6 }  // State 24
};

const int gotoTable[25][5] = {
//  KEY,   KEY`, FROM, WHERE,PARAM
    {0,    0,    0,    0,    0  },// State 0
    {S2,   0,    0,    0,    0  },// State 1
    {0,    0,    S4,   0,    0  },// State 2
    {0,    S6,   0,    0,    0  },// State 3
    {0,    0,    0,    S8,   0  },// State 4
    {0,    0,    0,    0,    0  },// State 5
    {0,    0,    0,    0,    0  },// State 6
    {0,    0,    0,    0,    0  },// State 7
    {0,    0,    0,    0,    0  },// State 8
    {0,    0,    0,    0,    S12},// State 9
    {0,    0,    0,    0,    0  },// State 10
    {0,    S14,  0,    0,    0  },// State 11
    {0,    0,    0,    0,    0  },// State 12
    {0,    0,    0,    0,    0  },// State 13
    {0,    0,    0,    0,    0  },// State 14
    {0,    0,    0,    0,    S20},// State 15
    {0,    0,    0,    0,    S21},// State 16
    {0,    0,    0,    0,    0  },// State 17
    {0,    0,    0,    0,    0  },// State 18
    {0,    0,    0,    0,    0  },// State 19
    {0,    0,    0,    0,    0  },// State 20
    {0,    0,    0,    0,    0  },// State 21
    {0,    0,    0,    0,    0  },// State 22
    {0,    0,    0,    0,    0  },// State 23
    {0,    0,    0,    0,    0  } // State 24
};

uint8_t transitionAction(uint8_t condition, uint8_t term) {
    return actionTable[condition][term];
}

uint8_t transitionGoto(uint8_t condition, uint8_t neterm) {
    return gotoTable[condition][neterm];
}

uint8_t getLength(uint8_t neterm, uint8_t condition) {

    switch (neterm)
    {      
    case TOKEN_KEY:
        return 2;
        break;
    case TOKEN_KEY_:
        if (condition!=14) {
            return 0;
        }
        else {
            return 3;
        }
        break;
    case TOKEN_FROM:
        return 2;
        break;
    case TOKEN_WHERE:
        return 2;
        break;
    case TOKEN_PARAM:
        return 3;
        break;
    default:
        return 0;
        break;
    }        
}


int main(int argc, char **argv ) {
    yyin = fopen("input.in", "r");
    uint8_t lexema = 0;
    uint8_t token = 0;
    uint8_t condition = 0;

    std::stack<uint8_t> stack;

    stack.push(TOKEN_END_OF_FILE);
    stack.push(TOKEN_GOAL);

    token = yylex();

    do {
        lexema = transitionAction(stack.top(), token);
        if (token == TOKEN_ERROR || lexema == 0){
            std::cout << "Error";
            break;
        }
        if (lexema < 100) {
            stack.push(token);
            stack.push(lexema);
            token = yylex();               
        }else if (lexema > 100 && lexema != 200) {
            uint8_t j = getLength(lexema % 100 - 1, stack.top());
            for (uint8_t i = 0; i < j; i++)
            {
                stack.pop();
                stack.pop();
            }                
            condition = transitionGoto(stack.top(), (lexema % 100) - 2);
            stack.push(lexema % 100 - 1);
            stack.push(condition);
        } else  if (lexema == ACCEPT) {
            std::cout << "Succes";
            break;
        }
        else {
            std::cout << "Error";
            break;
        }            
    } while (true);
    
}