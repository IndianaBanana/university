%{
    #include <stdio.h>
    #include <stdlib.h>
    #include <string.h>

    int yylex();
    int yyerror(char *s);

    int check_table_and_field(const char *table_name, const char *field_name);
    int check_table(const char *table_name);

    int push();
    const char* pop();
    char* request_table;
    int fields_stack_pointer = 0;
    const char* fields_stack[256];
%}

%union{
    int num;
    char* str;
}

%token SELECT FROM WHERE DELM AND OR GREATER LESS EQUAL 
%token <str> ID
%token <num> NUM

%type <str> CD CDS SCTN SC FLDS

%% 

S : SC {
    while(fields_stack_pointer > 0){
        const char* field = pop();
        if(!check_table_and_field(request_table, field)){
            printf("Table %s dont contain the field %s", request_table, field);
            return -1;
        }
    }
    printf("PROJ %s\n", $<str>1);
    printf("SUCCESS"); 
};

SC : SELECT FLDS SCTN {
    char SCBuffer[256];
    sprintf(SCBuffer, "%s %s", $<str>2, $<str>3);
    $$ = strdup(SCBuffer);
};

// ТАБЛИЦА
SCTN : FROM ID {
    if(!check_table($<str>2)){
        printf("ERROR: Table dont exists in database");
        return -1;
    }
    request_table = $<str>2;

    char SCTNBuffer[256];
    sprintf(SCTNBuffer, "(%s)", $<str>2);
    $$ = strdup(SCTNBuffer);
};
SCTN : FROM ID WHERE CDS {
    if(!check_table($<str>2)){
        printf("ERROR: Table dont exists in database");
        return -1;
    }
    request_table = $<str>2;

    char SCTNBuffer[256];
    sprintf(SCTNBuffer, "(SEL %s (%s))", $<str>4, $<str>2);
    $$ = strdup(SCTNBuffer);
};

// ПОЛЯ
FLDS : ID {
    char FLDSBuffer[256];
    sprintf(FLDSBuffer, "%s", $<str>1);
    $$ = strdup(FLDSBuffer);
    push($<str>1);
};
FLDS : ID DELM FLDS {
    char FLDSBuffer[256];
    sprintf(FLDSBuffer, "%s, %s", $<str>1, $<str>3);
    $$ = strdup(FLDSBuffer);
    push($<str>1);
};

// ЧАСТИ УСЛОВИЙ
CDS : CD {$$ = $<str>1; };
CDS : CD AND CDS {
    char CDSABuffer[256];
    sprintf(CDSABuffer, "%s AND %s", $<str>1, $<str>3);
    $$ = strdup(CDSABuffer);
};
CDS : CD OR CDS {
    char CDSOBuffer[256];
    sprintf(CDSOBuffer, "%s OR %s", $<str>1, $<str>3);
    $$ = strdup(CDSOBuffer);
};

// УСЛОИВИЯ
CD : ID GREATER NUM { 
    push($<str>1); 
    char CDGBuffer[256];
    sprintf(CDGBuffer, "%s > %d", $<str>1, $<num>3);
    $$ = strdup(CDGBuffer);
};
CD : ID LESS NUM {
    push($<str>1); 
    char CDLBuffer[256];
    sprintf(CDLBuffer, "%s < %d", $<str>1, $<num>3);
    $$ = strdup(CDLBuffer);
};
CD : ID EQUAL NUM {
    push($<str>1); 
    char CDEBuffer[256];
    sprintf(CDEBuffer, "%s = %d", $<str>1, $<num>3);
    $$ = strdup(CDEBuffer);
};

%%

int push(const char* field){
    fields_stack[fields_stack_pointer] = field;
    fields_stack_pointer++;
}

const char* pop(){
    return fields_stack[--fields_stack_pointer];
}