%option noyywrap
%{
    #include <stdio.h>
    #include <stdlib.h>
    #include <string.h>

    #define MAX_TABLES 100
    #define MAX_FIELDS 100
    #define MAX_NAME_LENGTH 100

    struct Table{
        char name[MAX_NAME_LENGTH];
        char fields[MAX_FIELDS][MAX_NAME_LENGTH];
        int field_count;
    };

    struct Table tables[MAX_TABLES];
    int table_count = 0;

    #include "lab.tab.h"
    int yyparse();
%}

digit [0-9]
letter [a-zA-Z]

%%
"SELECT" {return SELECT;}
"FROM" {return FROM;}
"WHERE" {return WHERE;}
, {return DELM;}
"and" {return AND;}
"or" {return OR;}
">" {return GREATER;}
"<" {return LESS;}
"=" {return EQUAL;}
{letter}({letter}|{digit})* {yylval.str = strdup(yytext); return ID;}
-?{digit}+ {yylval.num = atoi(yytext); return NUM;}
" "|"\n" {}
. {printf("Unknown lexem");}
%%

void read_file(const char *filename) {
    FILE *file = fopen(filename, "r");
    if (!file) {
        exit(EXIT_FAILURE);
    }

    char line[MAX_NAME_LENGTH * 2];
    while (fgets(line, sizeof(line), file)) {
        char *table_name = strtok(line, " ");
        char *field_name = strtok(NULL, " \n");

        if (!table_name || !field_name) {
            continue;
        }

        int found = 0;
        for (int i = 0; i < table_count; i++) {
            if (strcmp(tables[i].name, table_name) == 0) {
                strcpy(tables[i].fields[tables[i].field_count], field_name);
                tables[i].field_count++;
                found = 1;
                break;
            }
        }

        if (!found) {
            if (table_count >= MAX_TABLES) {
                exit(EXIT_FAILURE);
            }

            strcpy(tables[table_count].name, table_name);
            strcpy(tables[table_count].fields[0], field_name);
            tables[table_count].field_count = 1;
            table_count++;
        }
    }

    fclose(file);
}

int check_table(char *table_name){
    for (int i = 0; i < table_count; i++) 
        if (strcmp(tables[i].name, table_name) == 0) 
            return 1;
    return 0;
}

int check_table_and_field(const char *table_name, const char *field_name) {
    for (int i = 0; i < table_count; i++) {
        if (strcmp(tables[i].name, table_name) == 0) {
            for (int j = 0; j < tables[i].field_count; j++) {
                if (strcmp(tables[i].fields[j], field_name) == 0) {
                    return 1; 
                }
            }
        }
    }
    return 0;
}

int main(int argc, char **argv)
{
    yyin=fopen("./file.txt","r");
    const char *filename = "tables.txt";
    read_file(filename);
    yyparse();
}

int yyerror(char *s)
{
  fprintf(stderr, "error: %s\n", s);
}