%option noyywrap
%{
    #include <stdio.h>
    #include <stdlib.h>
    #include <string.h>

    #include "parser.tab.h"
    int yyparse();

%}

digit [0-9]
letter [a-zA-Z_]

%%
"SELECT" {return SELECT;}
"FROM" {return FROM;}
"WHERE" {return WHERE;}
, {return COMMA;}
"and" {return AND;}
"or" {return OR;}
">" {return GREATER;}
"<" {return LESS;}
"=" {return EQUAL;}
"<>" {return NOT_EQUAL;}
{letter}({letter}|{digit})* {yylval.str = strdup(yytext); return ID;}
\'{letter}({letter}|{digit})*\' {yylval.str = strdup(yytext); return STRING;}
-?{digit}+ {yylval.num = atoi(yytext); return NUM;}
[ \t\n\r ]
. {printf("Unknown lexem: %s\n", yytext);}
%%
