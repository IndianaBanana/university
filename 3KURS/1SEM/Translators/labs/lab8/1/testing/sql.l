%{
    #include <string>
    #include <vector>
    #include <iostream>
    #include "sql.tab.h"
    #include <stdio.h>
%}

%option noyywrap

SELECT      "select"
FROM        "from"
WHERE       "where"
AND         "and"
OR          "or"
EQ          "="
NEQ         "<>"
LT          "<"
GT          ">"
ID          [a-zA-Z_][a-zA-Z0-9_]*
NUM         -?[0-9]+

%%

{SELECT}    { return SELECT; }
{FROM}      { return FROM; }
{WHERE}     { return WHERE; }
{AND}       { return AND; }
{OR}        { return OR; }
{EQ}        { return EQ; }
{NEQ}       { return NEQ; }
{LT}        { return LT; }
{GT}        { return GT; }
{ID}        { yylval.str = strdup(yytext); return ID; }
{NUM}       { yylval.num = atoi(yytext); return NUM; }

[ \t\n\r]   { /* ignore whitespace */ }
.           { /* ignore unrecognized characters */ }

%%