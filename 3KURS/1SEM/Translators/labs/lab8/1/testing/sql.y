%{
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <string>
#include <cstring>
#include <cstdlib>
#include <cassert>
#include <map>

struct Schema {
    std::vector<std::string> tables;
    std::map<std::string, std::vector<std::string>> fields;
};

Schema db_schema;

void yyerror(const char* s) {
    std::cerr << "Error: " << s << std::endl;
}

void load_schema(const char* schema_file) {
    std::ifstream file(schema_file);
    if (!file) {
        std::cerr << "Error opening schema file" << std::endl;
        exit(1);
    }

    std::string line;
    while (std::getline(file, line)) {
        std::istringstream ss(line);
        std::string table_name, fields;

        std::getline(ss, table_name, ':');
        std::getline(ss, fields);

        db_schema.tables.push_back(table_name);

        std::vector<std::string> field_list;
        std::istringstream field_stream(fields);
        std::string field;
        while (std::getline(field_stream, field, ',')) {
            field_list.push_back(field);
        }
        db_schema.fields[table_name] = field_list;
    }
}

bool table_exists(const std::string& table_name) {
    return std::find(db_schema.tables.begin(), db_schema.tables.end(), table_name) != db_schema.tables.end();
}

bool field_exists(const std::string& table_name, const std::string& field_name) {
    auto it = db_schema.fields.find(table_name);
    if (it == db_schema.fields.end()) {
        return false;
    }
    const auto& field_list = it->second;
    return std::find(field_list.begin(), field_list.end(), field_name) != field_list.end();
}

void check_table_and_fields(const std::string& table_name, const std::vector<std::string>& fields) {
    if (!table_exists(table_name)) {
        std::cerr << "Error: Table " << table_name << " does not exist." << std::endl;
        exit(1);
    }

    for (const auto& field : fields) {
        if (!field_exists(table_name, field)) {
            std::cerr << "Error: Field " << field << " does not exist in table " << table_name << "." << std::endl;
            exit(1);
        }
    }
}

struct SelectQuery {
    std::string table_name;
    std::vector<std::string> fields;
    std::string where_clause;
};

extern "C" {
    int yylex();
    FILE* yyin;
}

#include "sql.tab.h"
%}

%union {
    int num;
    char* str;
    char** str_list;
}

%token SELECT FROM WHERE AND OR EQ NEQ LT GT
%token <str> ID
%token <num> NUM

%type <str_list> field_list
%type <str> table_name where_clause condition

%%

query:
    SELECT field_list FROM table_name where_clause {
        std::cout << "Executing query on table " << $3 << " with fields: ";
        char** fields = $2;
        for (int i = 0; fields[i] != nullptr; ++i) {
            std::cout << fields[i] << " ";
        }
        std::cout << std::endl;

        std::vector<std::string> field_vec;
        for (int i = 0; fields[i] != nullptr; ++i) {
            field_vec.push_back(fields[i]);
        }
        check_table_and_fields($3, field_vec);

        if ($4 == nullptr) {
            std::cout << "No WHERE clause." << std::endl;
        } else {
            std::cout << "WHERE: " << $4 << std::endl;
        }
    }
    ;

field_list:
    ID { 
        $$ = (char**)malloc(2 * sizeof(char*));
        $$[0] = $1;
        $$[1] = nullptr;
    }
    | field_list ',' ID {
        int i = 0;
        while ($1[i] != nullptr) i++;
        $$ = (char**)realloc($1, (i + 2) * sizeof(char*));
        $$[i] = $3;
        $$[i + 1] = nullptr;
    }
    ;

table_name:
    ID { $$ = $1; }
    ;

where_clause:
    WHERE condition { $$ = $2; }
    | /* empty */ { $$ = nullptr; }
    ;

condition:
    ID EQ NUM {
        $$ = (char*)malloc(256);
        snprintf($$, 256, "%s = %d", $1, $3);
    }
    | ID NEQ NUM {
        $$ = (char*)malloc(256);
        snprintf($$, 256, "%s <> %d", $1, $3);
    }
    | ID LT NUM {
        $$ = (char*)malloc(256);
        snprintf($$, 256, "%s < %d", $1, $3);
    }
    | ID GT NUM {
        $$ = (char*)malloc(256);
        snprintf($$, 256, "%s > %d", $1, $3);
    }
    ;

%%

int main(int argc, char** argv) {
    if (argc < 3) {
        std::cerr << "Usage: " << argv[0] << " <schema_file> <query_file>" << std::endl;
        return 1;
    }

    load_schema(argv[1]);

    yyin = fopen(argv[2], "r");
    if (!yyin) {
        std::cerr << "Error opening query file" << std::endl;
        return 1;
    }

    yyparse();
    fclose(yyin);

    return 0;
}