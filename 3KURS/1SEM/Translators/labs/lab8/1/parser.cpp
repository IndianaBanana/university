#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <cstring>
#include <cstdlib>
#include <unordered_map>

#define MAX_NAME_LENGTH 100

struct Table {
    std::string name;
    std::vector<std::string> fields;

    void addField(const std::string& field) {
        fields.push_back(field);
    }

    bool hasField(const std::string& field) const {
        return std::find(fields.begin(), fields.end(), field) != fields.end();
    }
};

std::vector<Table> tables;

bool checkTable(const std::string& tableName) {
    for (const auto& table : tables) {
        if (table.name == tableName) {
            return true;
        }
    }
    return false;
}

bool checkTableAndField(const std::string& tableName, const std::string& fieldName) {
    for (const auto& table : tables) {
        if (table.name == tableName) {
            return table.hasField(fieldName);
        }
    }
    return false;
}

void readFile(const std::string& filename) {
    std::ifstream file(filename);
    if (!file) {
        std::cerr << "Error: Unable to open file " << filename << std::endl;
        exit(EXIT_FAILURE);
    }

    std::string line;
    while (std::getline(file, line)) {
        size_t spacePos = line.find(' ');
        if (spacePos == std::string::npos) {
            continue;
        }

        std::string tableName = line.substr(0, spacePos);
        std::string fieldName = line.substr(spacePos + 1);

        bool found = false;
        for (auto& table : tables) {
            if (table.name == tableName) {
                table.addField(fieldName);
                found = true;
                break;
            }
        }

        if (!found) {
            Table newTable{tableName};
            newTable.addField(fieldName);
            tables.push_back(newTable);
        }
    }

    file.close();
}

int yyerror(const char* s) {
    std::cerr << "Error: " << s << std::endl;
    return 1;
}

extern int yyparse();
extern FILE* yyin;

int main(int argc, char** argv) {
    const std::string filename = "tables.txt";
    readFile(filename);

    yyin = fopen("./file.txt", "r");
    if (!yyin) {
        std::cerr << "Error: Unable to open file.txt" << std::endl;
        return EXIT_FAILURE;
    }

    yyparse();
    fclose(yyin);

    return 0;
}