%{
    #include <stdio.h>
    #include <stdlib.h>
    #include <string.h>

    #define MAX_TABLES 100
    #define MAX_FIELDS 100
    #define MAX_NAME_LENGTH 100
    #define BUFFER_SIZE 512

    int yylex();
    int yyerror(char *s);
    FILE *yyin;

    struct Table{
        char name[MAX_NAME_LENGTH];
        char fields[MAX_FIELDS][MAX_NAME_LENGTH];
        int field_count;
    };
    
    struct Table tables[MAX_TABLES];
    int table_count = 0;
    char* request_table;
    
    const char* fields_stack[256];
    int fields_stack_pointer = 0;
   
    char BUFFER[BUFFER_SIZE];

    void push(char* field){
        fields_stack[fields_stack_pointer++] = field;
    }

    const char* pop(){
        return fields_stack[--fields_stack_pointer];
    }

    int check_table(const char *table_name){
        for (int i = 0; i < table_count; i++) 
            if (strcmp(tables[i].name, table_name) == 0) 
                return 1;
        return 0;
    }

    int check_fields(const char *table_name, const char *field_name) {
        for (int i = 0; i < table_count; i++) {
            if (strcmp(tables[i].name, table_name) == 0) {
                for (int j = 0; j < tables[i].field_count; j++) {
                    if (strcmp(tables[i].fields[j], field_name) == 0) {
                        return 1; 
                    }
                }
            }
        }
        return 0;
    }
    
%}

%union{
    int num;
    char* str;
}

%token SELECT FROM WHERE COMMA AND OR GREATER LESS EQUAL NOT_EQUAL
%token <str> ID STRING
%token <num> NUM

%type <str> CONDITION PARAM TABLE_SECTION SELECT_SECTION KEY_SECTION T

%% 

S : 
SELECT_SECTION {
    while(fields_stack_pointer > 0){
        const char* field = pop();

        if(check_fields(request_table, field) == 0){
            printf("Table %s doesn't contain the field %s\n", request_table, field);
            return -1;
        }
    }
    printf("\nRESULT: π %s\n\n", $<str>1);
};

SELECT_SECTION : SELECT KEY_SECTION TABLE_SECTION {
    sprintf(BUFFER, "%s %s", $<str>2, $<str>3);
    $$ = strdup(BUFFER);
};

KEY_SECTION : 
ID {
    sprintf(BUFFER, "%s", $<str>1);
    $$ = strdup(BUFFER);
    push($<str>1);
}
| ID COMMA KEY_SECTION {
    sprintf(BUFFER, "(%s, %s)", $<str>1, $<str>3);
    $$ = strdup(BUFFER);
    push($<str>1);
};

TABLE_SECTION : 
FROM ID {
    if(check_table($<str>2) == 0){
        printf("ERROR: Table doesn`t exist in database");
        return -1;
    }
    request_table = $<str>2;

    sprintf(BUFFER, "(%s)", $<str>2);
    $$ = strdup(BUFFER);
}
| FROM ID WHERE PARAM {
    if(check_table($<str>2) == 0){
        printf("ERROR: Table doesn`t exist in database");
        return -1;
    }
    request_table = $<str>2;

    sprintf(BUFFER, "(σ (%s) (%s))", $<str>4, $<str>2);
    $$ = strdup(BUFFER);
};


PARAM : 
CONDITION {$$ = $<str>1; }
| CONDITION AND PARAM {
    sprintf(BUFFER, "%s ∧ %s", $<str>1, $<str>3);
    $$ = strdup(BUFFER);
}
| CONDITION OR PARAM {
    sprintf(BUFFER, "%s ∨ %s", $<str>1, $<str>3);
    $$ = strdup(BUFFER);
};

CONDITION : 
ID GREATER T { 
    push($<str>1); 
    sprintf(BUFFER, "%s > %s", $<str>1, $<str>3);
    $$ = strdup(BUFFER);
}
| ID LESS T {
    push($<str>1); 
    sprintf(BUFFER, "%s < %s", $<str>1, $<str>3);
    $$ = strdup(BUFFER);
}
| ID NOT_EQUAL T {
    push($<str>1); 
    sprintf(BUFFER, "%s <> %s", $<str>1, $<str>3);
    $$ = strdup(BUFFER);
}
| ID EQUAL T {
    push($<str>1); 
    sprintf(BUFFER, "%s = %s", $<str>1, $<str>3);
    $$ = strdup(BUFFER);
};

T:
STRING {
    sprintf(BUFFER, "%s", $<str>1);
    $$ = strdup(BUFFER);}
| NUM {
    sprintf(BUFFER, "%d", $<num>1);
    $$ = strdup(BUFFER);
};

%%

void read_file(const char *filename) {
    FILE *file = fopen(filename, "r");
    if (!file) {
        exit(EXIT_FAILURE);
    }

    char line[512];
    while (fgets(line, sizeof(line), file)) {
        char *table_name = strtok(line, " ");
        char *field_name = strtok(NULL, " \n\r");
        if (!table_name || !field_name) {
            continue;
        }

        int found = 0;
        for (int i = 0; i < table_count; i++) {
            if (strcmp(tables[i].name, table_name) == 0) {
                strcpy(tables[i].fields[tables[i].field_count], field_name);
                tables[i].field_count++;
                found = 1;
                break;
            }
        }

        if (!found) {
            if (table_count >= MAX_TABLES) {
                exit(EXIT_FAILURE);
            }

            strcpy(tables[table_count].name, table_name);
            strcpy(tables[table_count].fields[0], field_name);
            tables[table_count].field_count = 1;
            table_count++;
        }
    }

    fclose(file);
}



int main(int argc, char **argv)
{
    yyin=fopen("file.txt","r");
    const char *filename = "tables.txt";
    read_file(filename);
    yyparse();
}

int yyerror(char *s)
{
  fprintf(stderr, "error: %s\n", s);
  return -1;
}