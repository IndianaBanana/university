%{
    #include <stdio.h>
    #include <stdlib.h>
    #include <string.h>

    int yylex();
    int yyerror(char *s);

    int check_table_and_field(const char *table_name, const char *field_name);
    int check_table(const char *table_name);

    int push();
    const char* pop();
    char* request_table;
    int fields_stack_pointer = 0;
    const char* fields_stack[256];
%}

%union{
    int num;
    char* str;
}

%token SELECT FROM WHERE COMMA AND OR GREATER LESS EQUAL 
%token <str> ID
%token <num> NUM

%type <str> CONDITION PARAM TABLE_SECTION SELECT_SECTION KEY_SECTION

%% 

S : 
SELECT_SECTION {
    while(fields_stack_pointer > 0){
        const char* field = pop();
        if(!check_table_and_field(request_table, field)){
            printf("Table %s dont contain the field %s\n", request_table, field);
            return -1;
        }
    }
    printf("PROJ %s\n", $<str>1);
    printf("SUCCESS"); 
};

SELECT_SECTION : SELECT KEY_SECTION TABLE_SECTION {
    char SCBuffer[256];
    sprintf(SCBuffer, "%s %s", $<str>2, $<str>3);
    $$ = strdup(SCBuffer);
};

// ТАБЛИЦА
TABLE_SECTION : 
FROM ID {
    if(!check_table($<str>2)){
        printf("ERROR: Table dont exists in database");
        return -1;
    }
    request_table = $<str>2;

    char SCTNBuffer[256];
    sprintf(SCTNBuffer, "(%s)", $<str>2);
    $$ = strdup(SCTNBuffer);
}
| FROM ID WHERE PARAM {
    if(!check_table($<str>2)){
        printf("ERROR: Table dont exists in database");
        return -1;
    }
    request_table = $<str>2;

    char SCTNBuffer[256];
    sprintf(SCTNBuffer, "(SEL %s (%s))", $<str>4, $<str>2);
    $$ = strdup(SCTNBuffer);
};

// ПОЛЯ
KEY_SECTION : 
ID {
    char FLDSBuffer[256];
    sprintf(FLDSBuffer, "%s", $<str>1);
    $$ = strdup(FLDSBuffer);
    push($<str>1);
}
| ID COMMA KEY_SECTION {
    char FLDSBuffer[256];
    sprintf(FLDSBuffer, "%s, %s", $<str>1, $<str>3);
    $$ = strdup(FLDSBuffer);
    push($<str>1);
};

// ЧАСТИ УСЛОВИЙ
PARAM : 
CONDITION {$$ = $<str>1; }
| CONDITION AND PARAM {
    char CDSABuffer[256];
    sprintf(CDSABuffer, "%s AND %s", $<str>1, $<str>3);
    $$ = strdup(CDSABuffer);
}
| CONDITION OR PARAM {
    char CDSOBuffer[256];
    sprintf(CDSOBuffer, "%s OR %s", $<str>1, $<str>3);
    $$ = strdup(CDSOBuffer);
};

// УСЛОИВИЯ
CONDITION : 
ID GREATER NUM { 
    push($<str>1); 
    char CDGBuffer[256];
    sprintf(CDGBuffer, "%s > %d", $<str>1, $<num>3);
    $$ = strdup(CDGBuffer);
}
| ID LESS NUM {
    push($<str>1); 
    char CDLBuffer[256];
    sprintf(CDLBuffer, "%s < %d", $<str>1, $<num>3);
    $$ = strdup(CDLBuffer);
}
| ID EQUAL NUM {
    push($<str>1); 
    char CDEBuffer[256];
    sprintf(CDEBuffer, "%s = %d", $<str>1, $<num>3);
    $$ = strdup(CDEBuffer);
};

%%

int push(const char* field){
    fields_stack[fields_stack_pointer] = field;
    fields_stack_pointer++;
}

const char* pop(){
    return fields_stack[--fields_stack_pointer];
}