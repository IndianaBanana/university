%option noyywrap

%{
    #include <stdlib.h>
    #include <stdbool.h>
    #include <string.h>

    #include <iostream>
    #include <string>
    #include <stack>
    #include <vector>
    #include <set>
    #include <map>

    #define MAX_STR_LENGTH 256

    enum Token
    {
        TOKEN_T_CREATE,
        TOKEN_T_TABLE,
        TOKEN_T_PRIMARY,
        TOKEN_T_FOREIGN,
        TOKEN_T_KEY,
        TOKEN_T_REFERENCES,
        TOKEN_T_NULL,
        TOKEN_T_NOT,
        TOKEN_T_INTEGER,
        TOKEN_T_CHAR,
        TOKEN_T_LEFT_PARENTHESIS,
        TOKEN_T_RIGHT_PARENTHESIS,
        TOKEN_T_COMMA,
        TOKEN_END_STATEMENT,
        TOKEN_T_ID,
        TOKEN_T_NUM,
        TOKEN_END_OF_FILE,
        TOKEN_ERROR,

        TOKEN_GOAL,
        TOKEN_FIELD,
        TOKEN_FIELD_,
        TOKEN_TYPE,
        TOKEN_NULLABLE,
        TOKEN_PRIMARY_KEY,
        TOKEN_FOREIGN_KEY,
        TOKEN_FOREIGN_KEY_,
        TOKEN_KEY,
        TOKEN_KEY_,
        TOKEN_EPS
    };

    namespace {
		using std::vector;
		using std::set;
		using std::map;
		using std::stack;
		using std::string;
		using std::cout;


		set<Token> const TERMINALS
		{
			TOKEN_T_CREATE,
			TOKEN_T_TABLE,
			TOKEN_T_PRIMARY,
			TOKEN_T_FOREIGN,
			TOKEN_T_KEY,
			TOKEN_T_REFERENCES,
			TOKEN_T_NULL,
			TOKEN_T_NOT,
			TOKEN_T_INTEGER,
			TOKEN_T_CHAR,
			TOKEN_T_ID,
			TOKEN_T_NUM,
			TOKEN_T_LEFT_PARENTHESIS,
			TOKEN_T_RIGHT_PARENTHESIS,
			TOKEN_T_COMMA
		};
		set<Token> const NON_TERMINALS
		{
			TOKEN_GOAL,
			TOKEN_FIELD,
			TOKEN_FIELD_,
			TOKEN_TYPE,
			TOKEN_NULLABLE,
			TOKEN_PRIMARY_KEY,
			TOKEN_FOREIGN_KEY,
			TOKEN_FOREIGN_KEY_,
			TOKEN_KEY,
			TOKEN_KEY_
		};
		map<Token, map<Token, vector<Token>>> const TRANSMISSION_TABLE
		{
			{ TOKEN_GOAL,
				{
					{TOKEN_T_CREATE,            {TOKEN_T_CREATE, TOKEN_T_TABLE, TOKEN_T_ID, TOKEN_T_LEFT_PARENTHESIS, TOKEN_FIELD, TOKEN_PRIMARY_KEY, TOKEN_FOREIGN_KEY}},
				}
			},
			{ TOKEN_FIELD,
				{
					{TOKEN_T_ID,                {TOKEN_T_ID, TOKEN_TYPE, TOKEN_NULLABLE, TOKEN_T_COMMA, TOKEN_FIELD_}},
				}
			},
			{ TOKEN_FIELD_,
				{
					{TOKEN_T_PRIMARY,           {TOKEN_EPS}},
					{TOKEN_T_ID,                {TOKEN_T_ID, TOKEN_TYPE, TOKEN_NULLABLE, TOKEN_T_COMMA, TOKEN_FIELD_}},
				}
			},
			{ TOKEN_TYPE,
				{
					{TOKEN_T_INTEGER,           {TOKEN_T_INTEGER}},
					{TOKEN_T_CHAR,              {TOKEN_T_CHAR, TOKEN_T_LEFT_PARENTHESIS, TOKEN_T_NUM, TOKEN_T_RIGHT_PARENTHESIS}},
				}
			},
			{ TOKEN_NULLABLE,
				{
					{TOKEN_T_NULL,              {TOKEN_T_NULL}},
					{TOKEN_T_NOT,               {TOKEN_T_NOT, TOKEN_T_NULL}},
				}
			},
			{ TOKEN_PRIMARY_KEY,
				{
					{TOKEN_T_PRIMARY,           {TOKEN_T_PRIMARY, TOKEN_T_KEY, TOKEN_T_LEFT_PARENTHESIS, TOKEN_KEY, TOKEN_T_RIGHT_PARENTHESIS}},
				}
			},
			{ TOKEN_FOREIGN_KEY,
				{
					{TOKEN_T_COMMA,             {TOKEN_T_COMMA, TOKEN_T_FOREIGN, TOKEN_T_KEY, TOKEN_T_LEFT_PARENTHESIS,
					TOKEN_KEY, TOKEN_T_RIGHT_PARENTHESIS, TOKEN_T_REFERENCES, TOKEN_T_ID, TOKEN_T_LEFT_PARENTHESIS, TOKEN_KEY, TOKEN_T_RIGHT_PARENTHESIS, TOKEN_FOREIGN_KEY_}}
				}
			},
			{ TOKEN_FOREIGN_KEY_,
				{
					{TOKEN_T_RIGHT_PARENTHESIS, {TOKEN_EPS}},
					{TOKEN_T_COMMA,             {TOKEN_T_COMMA, TOKEN_T_FOREIGN, TOKEN_T_KEY, TOKEN_T_LEFT_PARENTHESIS,
					TOKEN_KEY, TOKEN_T_RIGHT_PARENTHESIS, TOKEN_T_REFERENCES, TOKEN_T_ID, TOKEN_T_LEFT_PARENTHESIS, TOKEN_KEY, TOKEN_T_RIGHT_PARENTHESIS, TOKEN_FOREIGN_KEY_}}
				}
			},
			{ TOKEN_KEY,
				{
					{TOKEN_T_ID,                {TOKEN_T_ID, TOKEN_KEY_}},
				}
			},
			{ TOKEN_KEY_,
				{
					{TOKEN_T_RIGHT_PARENTHESIS, {TOKEN_EPS}},
					{TOKEN_T_COMMA,             {TOKEN_T_COMMA, TOKEN_T_ID, TOKEN_KEY_}}
				}
			},
		};

		bool is_terminal(Token tok)
		{
			return TERMINALS.find(tok) != TERMINALS.cend();
		}

		bool analyze(vector<Token> const& tokens)
		{
			stack<Token> stack;

			stack.push(TOKEN_END_OF_FILE);
			stack.push(TOKEN_GOAL);

			vector<Token>::const_iterator cur_token = tokens.cbegin();

			do
			{
				if (is_terminal(stack.top()))
				{
					if (*cur_token == stack.top())
					{
						stack.pop();
						++cur_token;
					}
					else return false;
				}
				else
				{
					vector<Token> const& transmission = TRANSMISSION_TABLE.at(stack.top()).at(*cur_token);
					if (transmission.size() > 0)
					{
						stack.pop();
						if (transmission.size() == 1U && transmission.at(0U) == TOKEN_EPS) continue;
						for (int i = transmission.size() - 1; i >= 0; --i) {
							stack.push(transmission.at(i));
						}
					}
					else return false;
				}
			} while (stack.top() != TOKEN_END_OF_FILE);
			return (tokens.back() == TOKEN_T_RIGHT_PARENTHESIS);
		}
	}

%}


%%
"create" {return TOKEN_T_CREATE; }
"table" {return TOKEN_T_TABLE; }
"primary" {return TOKEN_T_PRIMARY; }
"foreign" {return TOKEN_T_FOREIGN; }
"key" {return TOKEN_T_KEY; }
"references" {return TOKEN_T_REFERENCES; }
"null" {return TOKEN_T_NULL; }
"not" {return TOKEN_T_NOT; }
"integer" {return TOKEN_T_INTEGER; }
"char" {return TOKEN_T_CHAR; }
"(" {return TOKEN_T_LEFT_PARENTHESIS; }
")" {return TOKEN_T_RIGHT_PARENTHESIS; }
"," {return TOKEN_T_COMMA; }
";" {return TOKEN_END_STATEMENT; }
[a-zA-z][a-zA-z|0-9]* { return TOKEN_T_ID; }
[0-9]+ { return TOKEN_T_NUM; }

[ \t\n\r ]

<<EOF>> { return TOKEN_END_OF_FILE; }
. { return TOKEN_ERROR; }

%%

int main()
{
	yyin = fopen("input.in", "r");

	vector<Token> tokens;
	do
	{
		Token tmp_token = static_cast<Token>(yylex());
		if (tmp_token == TOKEN_END_STATEMENT) break;

		tokens.push_back(tmp_token);
	} while (tokens.back() != TOKEN_ERROR && tokens.back() != TOKEN_END_OF_FILE);

	if (tokens.back() == TOKEN_ERROR) std::cout << "fail";
	else std::cout << (analyze(tokens) ? "success" : "fail");

	return EXIT_SUCCESS;
}