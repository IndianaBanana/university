import pandas as pd
import scipy.stats as stats
import matplotlib.pyplot as plt

# Путь к файлу Excel
file_path = 'practice/MSt_2024.xlsx'

# Имя листа в Excel файле, откуда вы хотите взять данные
sheet_name = 'Задние 1 для х - норм распр'

# Загружаем лист Excel в DataFrame
df = pd.read_excel(file_path, sheet_name=sheet_name)
print(df.columns)

# Выбираем диапазон данных, например, столбец A до столбца C и строки 1 до 10
# Предполагается, что данные находятся в столбце 'A'
range_data = df['x']

# Создаем QQ-plot
stats.probplot(range_data, dist="norm", plot=plt)
plt.title('QQ-plot для данных из Excel')
plt.show()