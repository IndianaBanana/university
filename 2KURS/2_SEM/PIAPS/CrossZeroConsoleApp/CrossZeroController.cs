﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CrossZeroConsoleApp
{
    class CrossZeroController
    {
        private ICrossZeroModel m;
        private ICrossZeroView v;
        public CrossZeroController(ICrossZeroModel m, ICrossZeroView v)
        {
            this.m = m;
            this.v = v;
        }

        public void run()
        {
            v.show();
            do
            {
                v.showmessage("Ход игрока " + (m.getPlayer() == Player.CROSS ? "крестик" : "нолик"));
                do
                {
                    int[] move = v.getmove();
                    if (move.Length >= 2 && m.move(move[0], move[1]))
                        break;
                    else
                        v.showmessage("Неверный ход. Попробуйте ещё раз");
                } while (true);
                v.show();
                if (m.iswin())
                {
                    v.showmessage("Выйграл " + (m.getPlayer() == Player.CROSS ? "крестик" : "нолик"));
                    break;
                }
                else
                {
                    if (m.canmove())
                    {
                        m.nextplayer();
                    }
                    else
                    {
                        v.showmessage("Ничья ");
                        break;
                    }
                }
            } while (true);
        }
    }
}
