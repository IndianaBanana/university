﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CrossZeroConsoleApp
{
    interface ICrossZeroModel
    {
        public int[,] getPole();
        public Player getPlayer();
        public bool move(int i, int j);
        public bool iswin();
        public bool canmove();
        public void nextplayer();
        public int getSize();
    }
}
