﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CrossZeroConsoleApp
{
    interface ICrossZeroView
    {
        public void show();
        public int[] getmove();
        public void showmessage(String msg);
    }
}
