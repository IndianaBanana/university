﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CrossZeroConsoleApp
{
    enum Player {CROSS, ZERO};
    
    class CrossZeroModel : ICrossZeroModel
    {
        private int[,] pole;
        private Player player = Player.CROSS;
        private int size = 3;

        public CrossZeroModel(int size = 3)
        {
            this.size = size;
            pole = new int[size, size];
            for (int i = 0; i<size; i++)
                for (int j = 0; j<size; j++)
                {
                    pole[i, j] = -1;
                }
        }

        public int[,] getPole()
        {
            return pole;
        }

        public Player getPlayer()
        {
            return player;
        }

        public bool move(int i, int j)
        {
            if (i>=0 && i<size && j>=0 && j<size && pole[i,j]<0)
            {
                pole[i, j] = player == Player.CROSS ? 1 : 0;
                return true;
            }
            return false;
        }

        public bool iswin()
        {
            if (player == Player.CROSS && ((pole[0, 0] == 1 && pole[0, 1] == 1 && pole[0, 2] == 1) ||
                (pole[1, 0] == 1 && pole[1, 1] == 1 && pole[1, 2] == 1) ||
                (pole[2, 0] == 1 && pole[2, 1] == 1 && pole[2, 2] == 1) ||
                (pole[0, 0] == 1 && pole[1, 0] == 1 && pole[2, 0] == 1) ||
                (pole[0, 1] == 1 && pole[1, 1] == 1 && pole[2, 1] == 1) ||
                (pole[0, 2] == 1 && pole[1, 2] == 1 && pole[2, 2] == 1) ||
                (pole[0, 0] == 1 && pole[1, 1] == 1 && pole[2, 2] == 1) ||
                (pole[0, 2] == 1 && pole[1, 1] == 1 && pole[2, 0] == 1)) ||
                player == Player.ZERO && ((pole[0, 0] == 0 && pole[0, 1] == 0 && pole[0, 2] == 0) ||
                (pole[1, 0] == 0 && pole[1, 1] == 0 && pole[1, 2] == 0) ||
                (pole[2, 0] == 0 && pole[2, 1] == 0 && pole[2, 2] == 0) ||
                (pole[0, 0] == 0 && pole[1, 0] == 0 && pole[2, 0] == 0) ||
                (pole[0, 1] == 0 && pole[1, 1] == 0 && pole[2, 1] == 0) ||
                (pole[0, 2] == 0 && pole[1, 2] == 0 && pole[2, 2] == 0) ||
                (pole[0, 0] == 0 && pole[1, 1] == 0 && pole[2, 2] == 0) ||
                (pole[0, 2] == 0 && pole[1, 1] == 0 && pole[2, 0] == 0)))
                return true;
            else
                return false;
        }

        public bool canmove()
        {
            for (int i = 0; i < size; i++)
                for (int j = 0; j < size; j++)
                {
                    if (pole[i, j] == -1)
                        return true;
                }
            return false;
        }

        public void nextplayer()
        {
            player = player == Player.CROSS ? Player.ZERO : Player.CROSS;
        }

        public int getSize()
        {
            return size;
        }
    }
}
