﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CrossZeroConsoleApp
{
    class CrossZeroView : ICrossZeroView
    {
        ICrossZeroModel m;
        public CrossZeroView(ICrossZeroModel m)
        {
            this.m = m;
        }

        public void show()
        {
            for(int i=0; i<m.getSize(); i++)
            {
                
                for(int j = 0; j<m.getSize(); j++)
                {
                    switch (m.getPole()[i, j])
                    {
                        case 0:
                            Console.Write("|0");
                            break;
                        case 1:
                            Console.Write("|X");
                            break;
                        default:
                            Console.Write("| ");
                            break;
                    }
                }
                Console.WriteLine("|");
            }
        }

        public int[] getmove()
        {
            string str = Console.ReadLine();
            return Array.ConvertAll(str.Split(' '), s => int.Parse(s));
        }

        public void showmessage(String msg)
        {
            Console.WriteLine(msg);
        }
    }
}
