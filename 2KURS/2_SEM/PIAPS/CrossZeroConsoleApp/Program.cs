﻿using System;

namespace CrossZeroConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            ICrossZeroModel m = new CrossZeroModel(3);
            ICrossZeroView v = new CrossZeroView(m);
            CrossZeroController c = new CrossZeroController(m, v);
            c.run();
        }
    }
}
