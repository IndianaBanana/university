﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.Threading.Tasks;
using Nwc.XmlRpc;

namespace XMLRPCServer
{
    class Server
    {

        const int PORT = 5050;
        private static XmlRpcServer server;

        //static public void WriteEntry(String msg, EventLogEntryType type)
        //{
        //    if (type != EventLogEntryType.Information) // ignore debug msgs
        //        Console.WriteLine("{0}: {1}", type, msg);
        //}
        static void Main(string[] args)
        {
            //Logger.Delegate = new Logger.LoggerDelegate(WriteEntry);

            server = new XmlRpcServer(PORT);
            server.Add("sample", new Server());
            Console.WriteLine("Web Server Running on port {0} ... Press ^C to Stop...", PORT);
            server.Start();
        }


        public String MatrixShenanigans(String arg)
        {
            int [,] matrix = ConvertStringTo2DIntArray(arg);
            int n = matrix.GetLength(0);
            Console.WriteLine(server);

            int mainDiagMin = int.MaxValue;
            int secondaryDiagMin = int.MaxValue;

            for (int i = 0; i < n; i++)
            {
                if (matrix[i, i] < mainDiagMin)
                {
                    mainDiagMin = matrix[i, i];
                }

                if (matrix[i, n - i - 1] < secondaryDiagMin)
                {
                    secondaryDiagMin = matrix[i, n - i - 1];
                }
            }

            bool useMainDiagonal = mainDiagMin <= secondaryDiagMin;
            int minElement = useMainDiagonal ? mainDiagMin : secondaryDiagMin;

            StringBuilder sb = new StringBuilder();
            sb.AppendLine("Given Matrix:");
            sb.AppendLine(PrintMatrix(matrix));

            if (useMainDiagonal)
            {
                for (int i = 0; i < n; i++)
                {
                    matrix[i, i] = 0;
                }
            }
            else
            {
                for (int i = 0; i < n; i++)
                {
                    matrix[i, n - i - 1] = 0;
                }
            }

            if (useMainDiagonal)
            {
                for (int i = 1; i < n; i++)
                {
                    for (int j = 0; j < i; j++)
                    {
                        matrix[i, j] *= matrix[i, j];
                    }
                }
            }
            else
            {
                for (int i = 1; i < n; i++)
                {
                    for (int j = n - 1; j > (n - i - 1); j--)
                    {
                        matrix[i, j] *= matrix[i, j];
                    }
                }
            }

            sb.AppendLine($"Min element: {minElement}");
            sb.AppendLine("Changed Matrix:");
            sb.AppendLine(PrintMatrix(matrix));
            return sb.ToString();
        }

        private string PrintMatrix(int[,] matrix)
        {
            int n = matrix.GetLength(0);
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < n; j++)
                {
                    sb.Append(matrix[i, j] + "\t");
                }
                sb.AppendLine();
            }
            return sb.ToString();
        }

        private int[,] ConvertStringTo2DIntArray(string str)
        {
            string[] elements = str.Split(',');
            int size = (int)Math.Sqrt(elements.Length);
            int[,] matrix = new int[size, size];
            int yaCHMO = 0;
            for (int i = 0; i < size; i++)
            {
                for (int j = 0; j < size; j++, yaCHMO++)
                {
                    matrix[i, j] = int.Parse(elements[yaCHMO].Trim());
                }
            }

            return matrix;
        }
    }
}


