﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace MathComLibrary
{
    [Guid("A95AE47A-E1B7-418C-9880-F0D8CB2A658D")]
    [InterfaceType(ComInterfaceType.InterfaceIsIDispatch)]
    public interface IMathOperations
    {
        double MaxNum(double a, double b);
        double MinNum(double a, double b);
        double AverageNum(double a, double b, double c);
    }

    [Guid("78963C1B-C9BC-400D-80B5-F8FE7A6D2B78")]
    [InterfaceType(ComInterfaceType.InterfaceIsIDispatch)]
    public interface IMyEvents
    {

    }
}
