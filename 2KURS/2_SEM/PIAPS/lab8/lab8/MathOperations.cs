﻿using System.Runtime.InteropServices;

namespace MathComLibrary
{
    [Guid("BF2B2804-062C-4205-A060-9C391D3DE0B7")]
    [ClassInterface(ClassInterfaceType.None)]
    [ProgId("MathComLibrary.MathOperations")]
    public class MathOperations : IMathOperations
    {
        public double AverageNum(double a, double b, double c)
        {
            return (a==b && b==c) ? 0: (a + b + c) / 3;
        }

        public double MaxNum(double a, double b)
        {
            return a >= b ? a : b;
        }

        public double MinNum(double a, double b)
        {
            return a <= b ? a : b;
        }
    }
}
