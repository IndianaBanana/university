﻿namespace ClientLab3
{
    partial class ClentChat
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            connectButton = new Button();
            sendButton = new Button();
            usernameBox = new TextBox();
            messageBox = new TextBox();
            portBox = new TextBox();
            hostBox = new TextBox();
            chatBox = new ListBox();
            userOnlineBox = new ListBox();
            SuspendLayout();
            // 
            // connectButton
            // 
            connectButton.Cursor = Cursors.Hand;
            connectButton.Enabled = false;
            connectButton.Font = new Font("Segoe UI", 14.25F, FontStyle.Regular, GraphicsUnit.Point, 204);
            connectButton.Location = new Point(389, 12);
            connectButton.Name = "connectButton";
            connectButton.Size = new Size(144, 42);
            connectButton.TabIndex = 4;
            connectButton.Text = "Connect";
            connectButton.UseVisualStyleBackColor = true;
            connectButton.Click += connectButton_Click;
            // 
            // sendButton
            // 
            sendButton.Cursor = Cursors.Hand;
            sendButton.Enabled = false;
            sendButton.Font = new Font("Segoe UI", 14.25F, FontStyle.Regular, GraphicsUnit.Point, 204);
            sendButton.Location = new Point(664, 573);
            sendButton.Name = "sendButton";
            sendButton.Size = new Size(237, 85);
            sendButton.TabIndex = 6;
            sendButton.Text = "Send";
            sendButton.UseVisualStyleBackColor = true;
            sendButton.Click += sendButton_Click;
            // 
            // usernameBox
            // 
            usernameBox.Enabled = false;
            usernameBox.Font = new Font("Segoe UI", 12F);
            usernameBox.Location = new Point(194, 21);
            usernameBox.MaxLength = 15;
            usernameBox.Name = "usernameBox";
            usernameBox.PlaceholderText = "Username";
            usernameBox.Size = new Size(189, 29);
            usernameBox.TabIndex = 3;
            usernameBox.TextChanged += usernameBox_TextChanged;
            // 
            // messageBox
            // 
            messageBox.Enabled = false;
            messageBox.Font = new Font("Segoe UI", 12F);
            messageBox.Location = new Point(12, 573);
            messageBox.MaxLength = 3000;
            messageBox.Multiline = true;
            messageBox.Name = "messageBox";
            messageBox.Size = new Size(617, 85);
            messageBox.TabIndex = 5;
            messageBox.TextChanged += messageBox_TextChanged;
            // 
            // portBox
            // 
            portBox.CharacterCasing = CharacterCasing.Upper;
            portBox.Enabled = false;
            portBox.Font = new Font("Segoe UI", 12F);
            portBox.Location = new Point(124, 21);
            portBox.MaxLength = 4;
            portBox.Name = "portBox";
            portBox.PlaceholderText = "Port";
            portBox.Size = new Size(64, 29);
            portBox.TabIndex = 2;
            portBox.Text = "1234";
            portBox.TextChanged += portBox_TextChanged;
            // 
            // hostBox
            // 
            hostBox.CharacterCasing = CharacterCasing.Upper;
            hostBox.Enabled = false;
            hostBox.Font = new Font("Segoe UI", 12F);
            hostBox.Location = new Point(12, 21);
            hostBox.MaxLength = 15;
            hostBox.Name = "hostBox";
            hostBox.PlaceholderText = "Host";
            hostBox.Size = new Size(106, 29);
            hostBox.TabIndex = 1;
            hostBox.Text = "127.0.0.1";
            hostBox.TextChanged += hostBox_TextChanged;
            // 
            // chatBox
            // 
            chatBox.Font = new Font("Segoe UI", 12F, FontStyle.Regular, GraphicsUnit.Point, 204);
            chatBox.FormattingEnabled = true;
            chatBox.HorizontalScrollbar = true;
            chatBox.ItemHeight = 21;
            chatBox.Location = new Point(12, 56);
            chatBox.Name = "chatBox";
            chatBox.Size = new Size(617, 487);
            chatBox.TabIndex = 0;
            chatBox.SelectedIndexChanged += chatBox_SelectedIndexChanged;
            // 
            // userOnlineBox
            // 
            userOnlineBox.Font = new Font("Segoe UI", 12F, FontStyle.Regular, GraphicsUnit.Point, 204);
            userOnlineBox.FormattingEnabled = true;
            userOnlineBox.HorizontalScrollbar = true;
            userOnlineBox.ItemHeight = 21;
            userOnlineBox.Location = new Point(664, 56);
            userOnlineBox.Name = "userOnlineBox";
            userOnlineBox.Size = new Size(238, 487);
            userOnlineBox.TabIndex = 7;
            userOnlineBox.SelectedIndexChanged += userOnlineBox_SelectedIndexChanged;
            // 
            // ClentChat
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(953, 670);
            Controls.Add(userOnlineBox);
            Controls.Add(hostBox);
            Controls.Add(portBox);
            Controls.Add(messageBox);
            Controls.Add(usernameBox);
            Controls.Add(chatBox);
            Controls.Add(sendButton);
            Controls.Add(connectButton);
            Name = "ClentChat";
            Text = "Form1";
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private Button connectButton;
        private Button sendButton;
        private TextBox usernameBox;
        private TextBox messageBox;
        private TextBox portBox;
        private TextBox hostBox;
        private ListBox chatBox;
        private ListBox userOnlineBox;
    }
}
