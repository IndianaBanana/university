﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static ClientLab3.IView;

namespace ClientLab3
{
    public interface IModel
    {

        public delegate void UpdateChat(string message);
        public delegate void UpdateUsers(string users);
        void SetDelegates(UpdateChat updateChat, UpdateUsers updateUsers);

        void Start(string host, int port, string username);
        void Stop();
        void SendData(string data);
        void ReceiveData();
        bool IsRunnig();
    }
}
