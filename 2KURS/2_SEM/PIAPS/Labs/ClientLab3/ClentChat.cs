using static System.Windows.Forms.VisualStyles.VisualStyleElement.StartPanel;

namespace ClientLab3
{
    public partial class ClentChat : Form, IView
    {
        IView.ExitProgram exit;
        IView.Connect connect;
        IView.SendMessage sendMessage;
        private IModel client;
        public ClentChat(IModel model)
        {
            InitializeComponent();
            this.FormClosing += clientChat_FormClosing;
            client = model;
            /*client.SetView(this);*/
        }
        public void SetDelegates(IView.ExitProgram exit, IView.SendMessage sendMessage, IView.Connect connect)
        {
            this.connect = connect;
            this.exit = exit;
            this.sendMessage = sendMessage;
        }

        public void Start()
        {
            usernameBox.Enabled = true;
            hostBox.Enabled = true;
            portBox.Enabled = true;
            connectButton.Enabled = true;
        }
        public void updateChat(string message)
        {
            chatBox.Items.Add(message);

        }

        public void addUsers(string usernames)
        {
            List<string> users = usernames.Split(';').ToList();
            userOnlineBox.Items.Clear();
            for(int i = 1; i < users.Count; i++)
            {
                userOnlineBox.Items.Add(users[i]);
            }
        }
        public void removeUser(string username)
        {
            userOnlineBox.Items.Remove(username);
        }

        private void clientChat_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (client.IsRunnig())
            {
                exit();
            }
        }

        private void connectButton_Click(object sender, EventArgs e)
        {
            string host = hostBox.Text;
            string port = portBox.Text;
            string username = usernameBox.Text;
            if (!string.IsNullOrEmpty(host) &&
                !string.IsNullOrEmpty(port) &&
                !string.IsNullOrEmpty(username) &&
                string.Equals(connectButton.Text, "Connect"))
            {
                connectButton.Text = "Disconnect";
                usernameBox.Enabled = false;
                hostBox.Enabled = false;
                portBox.Enabled = false;
                messageBox.Enabled = true;
                sendButton.Enabled = true;
                connect(host, int.Parse(port), username);

            }
            else if (connectButton.Text.Equals("Disconnect"))
            {
                exit();
                usernameBox.Enabled = true;
                hostBox.Enabled = true;
                portBox.Enabled = true;
                messageBox.Enabled = false;
                sendButton.Enabled = false;
                connectButton.Text = "Connect";
            }

        }

        private void chatBox_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void sendButton_Click(object sender, EventArgs e)
        {
            sendMessage(messageBox.Text);
            messageBox.Clear();
        }

        private void usernameBox_TextChanged(object sender, EventArgs e)
        {

        }

        private void hostBox_TextChanged(object sender, EventArgs e)
        {

        }

        private void portBox_TextChanged(object sender, EventArgs e)
        {

        }

        private void messageBox_TextChanged(object sender, EventArgs e)
        {

        }

        private void ClentChat_Load(object sender, EventArgs e)
        {

        }

        private void userOnlineBox_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

    }
}
