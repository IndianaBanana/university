using System.Windows.Forms;

namespace ClientLab3
{
    public class Program
    {
        
        /// <summary>
        ///  The main entry point for the application.
        /// </summary>
        [STAThread]
        
        public static void Main()
        {
            IModel model = new Client();
            ClentChat client = new ClentChat(model);
            IView view = client;
            Controller controller = new Controller(model, view);
            Thread t = new Thread(delegate () { controller.Run();});
            t.Start();
            Application.Run(client);

        }

        
    }
}