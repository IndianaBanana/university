﻿using System;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace ClientLab3
{
    public class Client : IModel
    {
        IModel.UpdateChat updateChat;
        IModel.UpdateUsers updateUsers;

        private TcpClient client;
        private NetworkStream? stream;
        private bool running = false;
        private Thread receiveThread;

       

        public void Start(string host, int port, string username)
        {
            receiveThread = new Thread(new ThreadStart(ReceiveData));
            client = new TcpClient();
            running = true;
            client.Connect(host, port);
            stream = client.GetStream();
            SendData(username); // Отправка имени пользователя на сервер
            receiveThread.Start();
            /*Thread.Sleep(1000);*/

        }

        public void Stop()
        {
            running = false;
            /*receiveThread.Suspend();*/
            client.Close();
            client.Dispose();
        }

        public void SendData(string message)
        {
            byte[] data = Encoding.ASCII.GetBytes(message);
            stream.Write(data, 0, data.Length);
        }

        public void ReceiveData()
        {
            byte[] buffer = new byte[4096];
            int bytesRead;

            while (running)
            {
                bytesRead = 0;

                try
                {
                    bytesRead = stream.Read(buffer, 0, buffer.Length);
                }
                catch
                {
                    break;
                }

                if (bytesRead == 0)
                {
                    break;
                }

                string data = Encoding.ASCII.GetString(buffer, 0, bytesRead);
                List<string> onineUsers = data.Split(';').ToList();
                if (onineUsers.Count > 1)
                {
                    if (string.Equals(onineUsers[0], "ServerIsSendingInfoAboutOnlineUsers"))
                    {
                        updateUsers(data);
                        continue;
                    }
                }
                /*observer.update();*/
                updateChat(data);
                /*Console.WriteLine(data);*/
            }

            Stop();
        }
        public bool IsRunnig()
        {
            return running;
        }

        public void SetDelegates(IModel.UpdateChat updateChat, IModel.UpdateUsers updateUsers)
        {
            this.updateChat = updateChat;
            this.updateUsers = updateUsers;
        }
    }
}