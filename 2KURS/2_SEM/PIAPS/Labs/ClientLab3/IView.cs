﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClientLab3
{
    public interface IView
    {
        public delegate void ExitProgram();
        public delegate void SendMessage(string message);
        public delegate void Connect(string host, int port, string username);

        void updateChat(string message);
        void addUsers(string usernames);
        void removeUser(string username);

        void SetDelegates(ExitProgram exit, SendMessage sendMessage, Connect connect);
        /*void connectToServer(string host, int port, string username);
        void disconnectFromServer();*/
        void Start();
    }

 
}
