﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClientLab3
{
    public class Controller
    {
        private IModel model;
        private IView view;

        public Controller(IModel model, IView view)
        {
            this.model = model;
            this.view = view;
        }

        public void Run()
        {
            model.SetDelegates(UpdateChat, UpdateUsers);
            view.SetDelegates(CloseConnection, SendMessage, Connect);
            view.Start();
            /*do
            {

            }while(true);*/
        }

        public void CloseConnection()
        {
            model.Stop();
        }
        public void SendMessage(string message)
        {
            model.SendData(message);
        }
        public void Connect(string host, int port, string username)
        {
            model.Start(host, port, username);
        }

        public void UpdateChat(string message)
        {
            view.updateChat(message);
        }

        public void UpdateUsers(string data) { 
            view.addUsers(data);
        }
    }
}
