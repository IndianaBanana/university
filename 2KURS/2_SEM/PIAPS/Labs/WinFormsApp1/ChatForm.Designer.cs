﻿namespace WinFormsApp1
{
    partial class ChatForm
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.chatBox = new System.Windows.Forms.TextBox();
            this.userNameBox = new System.Windows.Forms.TextBox();
            this.connectButton = new System.Windows.Forms.Button();
            this.enterUserNameLabel = new System.Windows.Forms.Label();
            this.sendButton = new System.Windows.Forms.Button();
            this.messageBox = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // chatBox
            // 
            this.chatBox.Location = new System.Drawing.Point(21, 73);
            this.chatBox.Multiline = true;
            this.chatBox.Name = "chatBox";
            this.chatBox.Size = new System.Drawing.Size(713, 554);
            this.chatBox.TabIndex = 0;
            this.chatBox.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // userNameBox
            // 
            this.userNameBox.Location = new System.Drawing.Point(21, 30);
            this.userNameBox.Name = "userNameBox";
            this.userNameBox.Size = new System.Drawing.Size(165, 23);
            this.userNameBox.TabIndex = 1;
            this.userNameBox.TextChanged += new System.EventHandler(this.userNameBox_TextChanged);
            // 
            // connectButton
            // 
            this.connectButton.Location = new System.Drawing.Point(212, 30);
            this.connectButton.Name = "connectButton";
            this.connectButton.Size = new System.Drawing.Size(115, 23);
            this.connectButton.TabIndex = 2;
            this.connectButton.Text = "Connect";
            this.connectButton.UseVisualStyleBackColor = true;
            this.connectButton.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // enterUserNameLabel
            // 
            this.enterUserNameLabel.AutoSize = true;
            this.enterUserNameLabel.Location = new System.Drawing.Point(21, 12);
            this.enterUserNameLabel.Name = "enterUserNameLabel";
            this.enterUserNameLabel.Size = new System.Drawing.Size(90, 15);
            this.enterUserNameLabel.TabIndex = 3;
            this.enterUserNameLabel.Text = "Ener your name";
            this.enterUserNameLabel.Click += new System.EventHandler(this.label1_Click);
            // 
            // sendButton
            // 
            this.sendButton.Location = new System.Drawing.Point(664, 648);
            this.sendButton.Name = "sendButton";
            this.sendButton.Size = new System.Drawing.Size(70, 67);
            this.sendButton.TabIndex = 4;
            this.sendButton.Text = "Send";
            this.sendButton.UseVisualStyleBackColor = true;
            this.sendButton.Click += new System.EventHandler(this.button2_Click);
            // 
            // messageBox
            // 
            this.messageBox.Location = new System.Drawing.Point(21, 648);
            this.messageBox.Multiline = true;
            this.messageBox.Name = "messageBox";
            this.messageBox.Size = new System.Drawing.Size(637, 67);
            this.messageBox.TabIndex = 5;
            this.messageBox.TextChanged += new System.EventHandler(this.textBox3_TextChanged);
            // 
            // ChatForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(756, 727);
            this.Controls.Add(this.messageBox);
            this.Controls.Add(this.sendButton);
            this.Controls.Add(this.enterUserNameLabel);
            this.Controls.Add(this.connectButton);
            this.Controls.Add(this.userNameBox);
            this.Controls.Add(this.chatBox);
            this.Name = "ChatForm";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private TextBox chatBox;
        private TextBox userNameBox;
        private Button connectButton;
        private Label enterUserNameLabel;
        private Button sendButton;
        private TextBox messageBox;
    }
}