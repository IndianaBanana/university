﻿using System;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace lab3_client
{
    class ChatClient
    {
        private TcpClient client;
        private NetworkStream stream;
        private bool running;

        public ChatClient(string server, int port)
        {
            client = new TcpClient();
            client.Connect(server, port);
            stream = client.GetStream();
            running = true;
        }

        public void Start()
        {
            Thread receiveThread = new Thread(new ThreadStart(ReceiveData));
            string username = null;
            while (username == null)
            {
                Console.Write("Enter your username: ");
                username = Console.ReadLine();
            }
            SendData(username); // Отправка имени пользователя на сервер
            Thread.Sleep(2000);
            receiveThread.Start();

            while (running)
            {
                string message = Console.ReadLine();
                SendData(message);
            }
        }

        public void Stop()
        {
            running = false;
            client.Close();
        }

        private void SendData(string message)
        {
            byte[] data = Encoding.ASCII.GetBytes(message);
            stream.Write(data, 0, data.Length);
        }

        private void ReceiveData()
        {
            byte[] buffer = new byte[1024];
            int bytesRead;
            try
            {
                bytesRead = stream.Read(buffer, 0, buffer.Length);
            }
            catch
            {
            }

            while (running)
            {
                bytesRead = 0;

                try
                {
                    bytesRead = stream.Read(buffer, 0, buffer.Length);
                }
                catch
                {
                    break;
                }

                if (bytesRead == 0)
                {
                    break;
                }

                string data = Encoding.ASCII.GetString(buffer, 0, bytesRead);
                Console.WriteLine(data);
            }

            Stop();
        }
    }
}