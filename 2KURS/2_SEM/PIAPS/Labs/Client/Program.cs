﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace lab3_client
{
    class Program
    {
        static void Main(string[] args)
        {
            ChatClient client = new ChatClient("127.0.0.1", 1234);
            client.Start();
        }
    }
}