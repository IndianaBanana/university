﻿using System;
using CookComputing.XmlRpc;

public class Server : XmlRpcListenerService
{
    [XmlRpcMethod("processMatrix")]
    public int[][] ProcessMatrix(int[][] matrix)
    {
        int minDiagonal = int.MaxValue;
        int minDiagonalIndex = -1;

        // Находим диагональ с минимальным элементом
        for (int i = 0; i < matrix.Length; i++)
        {
            if (matrix[i][i] < minDiagonal)
            {
                minDiagonal = matrix[i][i];
                minDiagonalIndex = i;
            }
        }

        // Заменяем элементы диагонали на 0
        for (int i = 0; i < matrix.Length; i++)
        {
            matrix[i][i] = 0;
        }

        // Возводим в квадрат элементы ниже главной диагонали
        for (int i = 0; i < matrix.Length; i++)
        {
            for (int j = 0; j < i; j++)
            {
                matrix[i][j] = matrix[i][j] * matrix[i][j];
            }
        }

        return matrix;
    }

    public static void Main(string[] args)
    {
        XmlRpcListener listener = new XmlRpcListener(8080);
        listener.Start();
        Console.WriteLine("Server is running...");
        Console.ReadLine();
        listener.Stop();
    }
}