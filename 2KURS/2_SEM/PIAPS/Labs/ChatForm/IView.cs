﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChatForm
{
    internal interface IView
    {
        void stopCLient();
        void updateChat(string message);
        void addUser(string username);
        void removeUser(string username);
        void connect(string host, int port);
        void disconnect();
    }
}
