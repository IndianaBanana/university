﻿using chatForm;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ChatForm
{
    public partial class ViewChat : Form 
    {
        private ChatClient client = null;
        public ViewChat()
        {
            InitializeComponent();
            this.FormClosing += ViewChat_FormClosing;
        }

        private void ViewChat_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (client != null){
                client.Stop();
            }
        }

        public void updateChat(string message)
        {

            chatList.Items.Add(message);
        }

        private void connButton_Click(object sender, EventArgs e)
        {
            if (usernameTextBox.Text != "" && string.Equals(connButton.Text, "Connect")) {
                connButton.Text = "Disconnect";
                usernameTextBox.Enabled = false;
                sendBox.Enabled = true;
                sendButton.Enabled = true;
                client = new ChatClient("127.0.0.1", 1234, this);
                client.Start(usernameTextBox.Text);
            }
            else if (connButton.Text.Equals("Disconnect"))
            {
                client.Stop();
                usernameTextBox.Enabled = true;
                sendBox.Enabled = false;
                sendButton.Enabled = false;
                connButton.Text = "Connect";
            }

        }

        private void usernameTextBox_TextChanged(object sender, EventArgs e)
        {

        }

        private void chatList_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void sendButton_Click(object sender, EventArgs e)
        {
            client.SendData(sendBox.Text);
            sendBox.Clear();
        }

        private void sendBox_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
