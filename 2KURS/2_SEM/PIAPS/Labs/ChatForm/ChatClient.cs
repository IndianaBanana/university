﻿using ChatForm;
using System;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace chatForm
{
    class ChatClient
    {
        private TcpClient client;
        private NetworkStream stream;
        private bool running;
        private ViewChat viewChat;

        public ChatClient(string server, int port, ViewChat viewChat)
        {
            client = new TcpClient();
            client.Connect(server, port);
            stream = client.GetStream();
            this.viewChat = viewChat;
            running = true;
        }

        public void Start(string username)
        {
            Thread receiveThread = new Thread(new ThreadStart(ReceiveData));

            SendData(username); // Отправка имени пользователя на сервер
            receiveThread.Start();
            Thread.Sleep(1000);

        }

        public void Stop()
        {
            running = false;
            client.Close();
        }

        public void SendData(string message)
        {
            byte[] data = Encoding.ASCII.GetBytes(message);
            stream.Write(data, 0, data.Length);
        }

        private void ReceiveData()
        {
            byte[] buffer = new byte[1024];
            int bytesRead;
            try
            {
                bytesRead = stream.Read(buffer, 0, buffer.Length);
            }
            catch
            {
            }

            while (running)
            {
                bytesRead = 0;

                try
                {
                    bytesRead = stream.Read(buffer, 0, buffer.Length);
                }
                catch
                {
                    break;
                }

                if (bytesRead == 0)
                {
                    break;
                }

                string data = Encoding.ASCII.GetString(buffer, 0, bytesRead);
                viewChat.updateChat(data);
                /*Console.WriteLine(data);*/
            }

            Stop();
        }
    }
}