﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab2_Server
{
    class View
    {
        private Controller controller;
        public View (Controller controller)
        {
            this.controller = controller;
            
        }
        public int enterServerPort()
        {
            Console.WriteLine("Enter the server`s port: ");
            return Convert.ToInt32(Console.ReadLine());
        }
        public void writeToConsole(string data)
        {
            Console.WriteLine(data);
        }

    }
}
