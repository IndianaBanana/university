﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace lab2_Server
{
    class Controller
    {
        private Server server;
        private View view;

        public Controller()
        {
            view = new View(this);
            server = new Server(1234, this);
            server.Start();
        }

        public void sendDataToView(string data)
        {
            view.writeToConsole(data);
        }
        public void stopServer()
        {
            server.Stop();
        }

    }
}
