﻿/*using System;
using System.Collections.Generic;*/
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Timers;
using System.Xml.Linq;
/*using System.Threading;*/

namespace lab2_Server{
    class Server
    {
        private TcpListener server;
        private List<TcpClient> clients;
        private Dictionary<TcpClient, string> users;
        private static System.Timers.Timer aTimer;
        private Controller controller;
        private bool running;

        public Server(int port, Controller controller)
        {
            server = new TcpListener(IPAddress.Any, port);
            clients = new List<TcpClient>();
            users = new Dictionary<TcpClient, string>();
            running = false;
            this.controller = controller;
        }

        public void Start()
        {
            server.Start();
            running = true;
            controller.sendDataToView("Server started. Waiting for connections...");
            aTimer = new System.Timers.Timer(7000);
            aTimer.Elapsed += SendUsersOnlineInfo;
            aTimer.AutoReset = true; // Повторный запуск таймера после каждого срабатывания.
            aTimer.Enabled = true;
            while (running)
            {
                TcpClient client = server.AcceptTcpClient();
                clients.Add(client);
                Thread thread = new Thread(new ThreadStart(() => HandleClient(client)));
                thread.Start();
            }
        }

        public void Stop()
        {
            running = false;
            server.Stop();
            controller.sendDataToView($"\nServer is stopped");
            aTimer.Stop();
            aTimer.Dispose();
        }

        private void HandleClient(TcpClient client)
        {
            NetworkStream stream = client.GetStream();

            byte[] buffer = new byte[1024];
            int bytesRead = 0;
            while (bytesRead == 0)
            {
                try
                {
                    bytesRead = stream.Read(buffer, 0, buffer.Length);
                }
                catch
                {
                    controller.sendDataToView("Error\n");
                    bytesRead = 0;
                }
            }
            string username = Encoding.ASCII.GetString(buffer, 0, bytesRead);
            controller.sendDataToView($"{username} connected to server");
            users.Add(client, username);

            Broadcast($"server: {username} has joined the chat.\n");

            while (running)
            {
                bytesRead = 0;

                try
                {
                    bytesRead = stream.Read(buffer, 0, buffer.Length);
                }
                catch
                {
                    break;
                }

                if (bytesRead == 0)
                {
                    break;
                }

                string data = Encoding.ASCII.GetString(buffer, 0, bytesRead);
                Broadcast($"{username}: {data}");
            }
            controller.sendDataToView($"{username} disconnected from server");
            users.Remove(client);
            Broadcast($"server: {username} disconnected from chat\n");
            client.Close();
            clients.Remove(client);
        }

        private void SendUsersOnlineInfo(Object? source, ElapsedEventArgs e)
        {
            List<string> usernamesList = users.Values.ToList();
            string onlineUsers = "ServerIsSendingInfoAboutOnlineUsers;" + string.Join(";", usernamesList);
            byte[] data = Encoding.ASCII.GetBytes(onlineUsers);
            if (running){
                foreach (TcpClient client in clients)
                {
                    try
                    {
                        client.GetStream().Write(data, 0, data.Length);
                    }
                    catch { continue; }
                }
            }

        }

        private void Broadcast(string message)
        {
            byte[] data = Encoding.ASCII.GetBytes(message);

            foreach (TcpClient client in clients)
            {
                try
                {
                    client.GetStream().Write(data, 0, data.Length);
                }
                catch { continue; }
            }
        }
    }
}