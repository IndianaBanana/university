﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1
{
    public static class CommandLineArguments
    {
        public static void PrintArguments(string[] args)
        {
            Console.WriteLine("Command line arguments:");
            foreach (string arg in args)
            {
                Console.WriteLine(arg);
            }
        }
    }
}

