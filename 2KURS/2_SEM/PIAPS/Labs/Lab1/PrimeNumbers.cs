﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1
{
    public static class PrimeNumbers
    {
        public static void PrintPrimeNumbers()
        {
            Console.Write("Enter limit number: ");
            int limit = Convert.ToInt32(Console.ReadLine());

            bool[] sieve = new bool[limit + 1];
            for (int i = 2; i <= limit; i++)
            {
                sieve[i] = true;
            }

            for (int i = 2; i * i <= limit; i++)
            {
                if (sieve[i])
                {
                    for (int j = i * i; j <= limit; j += i)
                    {
                        sieve[j] = false;
                    }
                }
            }

            for (int i = 2; i <= limit; i++)
            {
                if (sieve[i])
                {
                    Console.Write($"{i} ");
                }
            }
            Console.WriteLine();
        }
    }
}
