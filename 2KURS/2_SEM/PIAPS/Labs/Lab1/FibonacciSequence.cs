﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1
{
    public static class FibonacciSequence
    {
        public static void PrintFibonacciSequence()
        {
            Console.Write("Enter limit number: ");
            int limit = Convert.ToInt32(Console.ReadLine());

            int a = 0;
            int b = 1;
            Console.Write($"{a} {b} ");
            while (b < limit)
            {
                int temp = a;
                a = b;
                b = temp + b;
                Console.Write($"{b} ");
            }
            Console.WriteLine();
        }
    }
}
