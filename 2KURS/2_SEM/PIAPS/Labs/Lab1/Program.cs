﻿using Lab1;
using System;

namespace Lab1
{
    class Program
    {
        static void consoleClear()
        {
            Console.Write("Press any buton to continue...");
            Console.ReadKey();
            Console.Clear();
        }
        static void Main(string[] args)
        {
            while (true)
            {
                Console.WriteLine("Choose from list: ");
                Console.WriteLine("1. Print CLI args");
                Console.WriteLine("2. Print leap years");
                Console.WriteLine("3. Print finonacci sequence");
                Console.WriteLine("4. Calculate factorial");
                Console.WriteLine("5. Print prime numbers (Sieve of Eratosthenes)");
                Console.WriteLine("0. Exit");
                Console.Write(">>> ");
                int choice = Convert.ToInt32(Console.ReadLine());

                if (choice == 0) 
                {
                    Console.WriteLine("Exit");
                    break; 
                }

                switch (choice)
                {
                    case 1:
                        CommandLineArguments.PrintArguments(args);
                        consoleClear();
                        break;
                    case 2:
                        Console.WriteLine("Enter the start and the end");
                        LeapYears.PrintLeapYears(Convert.ToInt32(Console.ReadLine()), Convert.ToInt32(Console.ReadLine()));
                        consoleClear();
                        break;
                    case 3:
                        FibonacciSequence.PrintFibonacciSequence();
                        consoleClear();
                        break;
                    case 4:
                        Console.WriteLine(Factorial.CalculateFactorial());
                        consoleClear();
                        break;
                    case 5:
                        PrimeNumbers.PrintPrimeNumbers();
                        consoleClear();
                        break;
                }
            }
        }
    }
}
