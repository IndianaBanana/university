﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1
{
    public static class Factorial
    {
        public static int CalculateFactorial()
        {
            Console.Write("Enter number: ");
            int number = Convert.ToInt32(Console.ReadLine());

            int result = 1;
            for (int i = 2; i <= number; i++)
            {
                result *= i;
            }
            return result;
        }
    }
}
