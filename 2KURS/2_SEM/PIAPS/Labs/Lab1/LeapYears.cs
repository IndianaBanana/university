﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1
{
    public static class LeapYears
    {
        public static void PrintLeapYears(int start, int end)
        {
            for (int year = start; year <= end; year++)
            {
                bool isLeapYear = DateTime.IsLeapYear(year);
                Console.WriteLine($"{year} - {(isLeapYear ? "Leap year" : "Not leap year")}");
            }
        }
    }
}
