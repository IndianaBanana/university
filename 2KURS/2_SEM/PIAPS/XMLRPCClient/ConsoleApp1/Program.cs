﻿using System;
using System.Text;

class Program
{
    static void Main()
    {
        int[,] matrix = {
            { 3, 2, 1, 4 },
            { 4, 6, 5, 1 },
            { 7, 8, 9, 3 },
            { 2, 5, 7, 8 }
        };

        string result = ProcessMatrix(matrix);
        Console.WriteLine(result);
    }

    static string ProcessMatrix(int[,] matrix)
    {
        int n = matrix.GetLength(0); // размерность матрицы
        int mainDiagMin = int.MaxValue;
        int secondaryDiagMin = int.MaxValue;
        int mainDiagIndex = -1;
        int secondaryDiagIndex = -1;

        // Находим минимальные элементы на обеих диагоналях
        for (int i = 0; i < n; i++)
        {
            if (matrix[i, i] < mainDiagMin)
            {
                mainDiagMin = matrix[i, i];
                mainDiagIndex = i;
            }

            if (matrix[i, n - i - 1] < secondaryDiagMin)
            {
                secondaryDiagMin = matrix[i, n - i - 1];
                secondaryDiagIndex = i;
            }
        }

        // Определяем диагональ с минимальным элементом
        bool useMainDiagonal = mainDiagMin <= secondaryDiagMin;
        int minElement = useMainDiagonal ? mainDiagMin : secondaryDiagMin;

        StringBuilder sb = new StringBuilder();
        sb.AppendLine("Исходная матрица:");
        sb.AppendLine(PrintMatrix(matrix));

        // Обнуление элементов на диагонали с минимальным элементом
        if (useMainDiagonal)
        {
            for (int i = 0; i < n; i++)
            {
                matrix[i, i] = 0;
            }
        }
        else
        {
            for (int i = 0; i < n; i++)
            {
                matrix[i, n - i - 1] = 0;
            }
        }

        // Возведение в квадрат элементов под минимальной диагональю
        if (useMainDiagonal)
        {
            for (int i = 1; i < n; i++)
            {
                for (int j = 0; j < i; j++)
                {
                    matrix[i, j] *= matrix[i, j];
                }
            }
        }
        else
        {
            for (int i = 1; i < n; i++)
            {
                for (int j = n - 1; j > (n - i - 1); j--)
                {
                    matrix[i, j] *= matrix[i, j];
                }
            }
        }

        sb.AppendLine($"Минимальный элемент на диагонали: {minElement}");
        sb.AppendLine("Измененная матрица:");
        sb.AppendLine(PrintMatrix(matrix));
        return sb.ToString();
    }

    static string PrintMatrix(int[,] matrix)
    {
        int n = matrix.GetLength(0);
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < n; i++)
        {
            for (int j = 0; j < n; j++)
            {
                sb.Append(matrix[i, j] + "\t");
            }
            sb.AppendLine();
        }
        return sb.ToString();
    }
}
