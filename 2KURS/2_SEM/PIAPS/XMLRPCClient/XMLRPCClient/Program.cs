﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nwc.XmlRpc;
using System.Diagnostics;

namespace XMLRPCClient
{
    class Program
    {
        
        static ServersideObject obj;
        
        static void Main(string[] args)
        {
            int[,] matrix = {
                 {3, 2, 1, 4 } ,
                 {4, 6, 5, 1 } ,
                 {7, 8, 9, 3 } ,
                 {2, 5, 7, 8 }
            };
            obj = new ServersideObject("http://127.0.0.1:5050");
            String str = obj.MatrixShenanigans(matrix);
            Console.WriteLine("MatrixShenanigans: " + str);

            Console.ReadLine();
        }
    }
}
