﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nwc.XmlRpc;

namespace XMLRPCClient
{
    class ServersideObject
    {
        XmlRpcRequest client;
        String host = "http://127.0.0.1:5050";

        public ServersideObject()
        {
             client = new XmlRpcRequest();
        }
        public ServersideObject(String host)
        {
            this.host = host;
            client = new XmlRpcRequest();
        }

        public String MatrixShenanigans(int[,] matrix)
        {
            XmlRpcResponse response;
            String arg = PrintMatrix(matrix);
            client.MethodName = "sample.MatrixShenanigans";
            client.Params.Clear();
            client.Params.Add(arg);
            Console.WriteLine(client);
            response = client.Send(host);
            if (response.IsFault)
            {
                Console.WriteLine("Fault {0}: {1}", response.FaultCode, response.FaultString);
                return null;
            }
            return response.Value.ToString();
        }

        private string PrintMatrix(int[,] matrix)
        {
            int n = matrix.GetLength(0);
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < n; j++)
                {
                    if (i == j && i == n - 1)
                    {
                        sb.Append(matrix[i, j]);
                        continue;
                    }
                    sb.Append(matrix[i, j] + ",");
                }
                sb.AppendLine();
            }
            return sb.ToString();
        }
    }
}
