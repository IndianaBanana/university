﻿using Microsoft.Office.Interop.Word;
namespace L7
{
    class Program
    {
        static public void Main()
        {
            

            string LabWorkNum = GetStringInput("Laboratory work num: ");
            string theme = GetStringInput("Laboratory work theme: ");
            string fullName = GetStringInput("FIO: ");
            string teacherFullName = GetStringInput("Teacher`s FIO: ");
            List<string> list = [LabWorkNum, theme, fullName, teacherFullName, DateTime.Now.Year.ToString()];
            try
            {
                string path = Environment.CurrentDirectory + "\\Template.docx";
                Console.WriteLine(path);
                string newPath = Environment.CurrentDirectory + $"\\{GetStringInput("Document name: ")}.docx";
                FileInfo fileInf = new FileInfo(path);
                if (fileInf.Exists)
                    fileInf.CopyTo(newPath, true);

                ReplacePlaceholders(newPath, list);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return;
            }

        }
        static public string GetStringInput(string msg)
        {
            Console.Write(msg);
            return Console.ReadLine();
        }

        static void ReplacePlaceholders(string filePath, List<string> values)
        {
           
            Application wordApp = new Application();
            
            Document doc = null;
            try
            {
                doc = wordApp.Documents.Open(filePath);

                string placeholder = ":placeholder";
                int valueIndex = 0;

                foreach (Microsoft.Office.Interop.Word.Range range in doc.StoryRanges)
                {
                    Find find = range.Find;
                    find.Text = placeholder;
                    find.Replacement.Text = "";

                    while (find.Execute(Replace: WdReplace.wdReplaceNone))
                    {
                        if (valueIndex < values.Count)
                        {
                            range.Text = values[valueIndex];
                            valueIndex++;
                        }
                        else
                        {
                            break;
                        }
                        range.Collapse(WdCollapseDirection.wdCollapseEnd);
                    }
                }

                doc.Save();
            }
            catch (Exception ex)
            {
                Console.WriteLine($"An error occurred: {ex.Message}");
            }
            finally
            {
                if (doc != null)
                {
                    doc.Close();
                }
                wordApp.Quit();
            }
        }
    }
}