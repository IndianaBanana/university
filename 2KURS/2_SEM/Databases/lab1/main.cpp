// Написать программу, которая работает с организованным в виде
// кучи файлом, хранящем информацию об отношении «студент».
// В программе должны быть реализованы следующие функции:
// -  добавление информации о студент;
// -  изменение информации о студенте;
// -  удаление информации о студенте;
// -  осуществление поиска информации о студенте.
// Отношение  студент  должно  содержать  следующие  атрибуты:
// номер  зачетки  (тип  integer),    фамилия  (тип  string(30)),  имя  (тип
// string(20)), отчество (тип string(30)), номер группы (тип integer).
// Для  организации  хранения  информации  о  записи  в  файле
// необходимо использовать тип Zap.
// Type
// Zap = record
// Id_zachet, id_gr: integer;
// Surname, Name: string (20);
// Patronymic: string(30);
// End;
// Блок файла должен включать 5 записей.
// Type
// Block = record
// Zap_block: array[1..5] of zap;
// End;
// Для хранения схемы отношения в файле должен использоваться
// нулевой блок.
// Программа должна работать с любым файлом, организованным
// по данной схеме.

#include <iostream>
#include <windows.h>
#include <cstring>
#include <string>
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <io.h>
#include <cstdlib>
#include <sys/stat.h>
#include <errno.h>
#include <sys/types.h>
using namespace std;

struct Student
{
    int id = -1;
    char firstName[30];
    char lastName[20];
    char patronymic[30];
    int group;
};

bool isStudentExist(Student student, FILE *file);
int searchEmptyPosition(int &blockPos, FILE *file, Student student, bool search);
Student enterStudent(int id = -1);
void addStudent();
void insertData(FILE *file, int blockPos, int studentPos, Student student);

Student block[5];
Student emptyBlock[5];

void addEmptyBlock(FILE *file)
{
    fseek(file, 0, SEEK_END);
    fwrite(&emptyBlock, sizeof(emptyBlock), 1, file);
}

Student enterStudent(int id)
{

    Student student;
    if (id != -1)
    {
        student.id = id;
        cout << "-----------------------------" << endl;
        cout << "Enter new student data (group, firstName, lastName, patronymic): " << endl;
    }
    else
    {
        cout << "Enter student data (id, group, firstName, lastName, patronymic): " << endl;
        cin >> student.id;
    }
    cin >> student.group >> student.firstName >> student.lastName >> student.patronymic;
    return student;
}

void addStudent()
{
    int blockPos = 0;
    Student student = enterStudent();
    FILE *file = fopen("students.txt", "r+b");
    if (file == NULL)
    {
        cout << "Error opening file" << endl;
        return;
    }
    int studentPos = searchEmptyPosition(blockPos, file, student, false);
    cout << "stPos " << studentPos << endl;
    cout << "blPos" << blockPos << endl;
    if (studentPos == -2)
        return;
    if (studentPos == -1)
    {
        studentPos = 0;
        addEmptyBlock(file);
        insertData(file, blockPos, studentPos, student);
    }
    else
    {
        insertData(file, blockPos, studentPos, student);
    }
    fclose(file);
}

void insertData(FILE *file, int blockPos, int studentPos, Student student)
{
    fseek(file, sizeof(block) * blockPos + sizeof(Student) * studentPos, SEEK_SET);
    fwrite(&student, sizeof(student), 1, file);
}

void updateStudent()
{
    int blockPos = 0, studentPos;
    Student student;
    cout << "Enter student id" << endl;
    FILE *file = fopen("students.txt", "r+b");
    cin >> student.id;
    studentPos = searchEmptyPosition(blockPos, file, student, true);
    if (studentPos == -1)
        printf("Student doesn`t exist :^(\n");
    else
    {
        student = enterStudent(student.id);
        cout << "stPos " << studentPos << endl;
        cout << "blPos" << blockPos << endl;
        insertData(file, blockPos, studentPos, student);
    }
    fclose(file);
}

void deleteStudent()
{
    int blockPos = 0, studentPos;
    Student student;
    cout << "Enter student id" << endl;
    FILE *file = fopen("students.txt", "r+b");
    cin >> student.id;
    studentPos = searchEmptyPosition(blockPos, file, student, true);
    if (studentPos == -1)
    {
        printf("Student doesn`t exist :^(\n");
        fclose(file);
        return;
    }
    else
    {

        Student lastBlock[5];
        fseek(file, -sizeof(block), SEEK_END);
        long blockQuantity = ftell(file);
        blockQuantity = blockQuantity / sizeof(block) + 1;
        fread(&lastBlock, sizeof(lastBlock), 1, file);
        if (lastBlock[0].id != -1 && lastBlock[1].id == -1 && lastBlock[2].id == -1 && lastBlock[3].id == -1 && lastBlock[4].id == -1)
        {
            cout << lastBlock[0].id << endl;
            insertData(file, blockPos, studentPos, lastBlock[0]);
            fclose(file);
            int fd = open("students.txt", O_RDWR | O_CREAT, S_IREAD | S_IWRITE);
            if (fd == -1)
            {
                cout << "can't open file in delete user\n";
                return;
            }
            if (_chsize(fd, blockQuantity * sizeof(block) - 440) == -1)
            {
                close(fd);
                return;
            }
            close(fd);
            return;
        }
        else
        {
            for (int i = 4; i >= 0; i--)
            {
                if (lastBlock[i].id != -1)
                {
                    insertData(file, blockPos, studentPos, lastBlock[i]);
                    lastBlock[i].id = -1;
                    insertData(file, blockQuantity - 1, i, lastBlock[i]);
                    break;
                }
            }
        }
        fclose(file);
    }
}
int searchEmptyPosition(int &blockPos, FILE *file, Student student, bool search = false)
{
    fseek(file, 0, SEEK_END);
    long blockQuantity = ftell(file);
    blockQuantity = blockQuantity / sizeof(block);
    fseek(file, 0, SEEK_SET);
    for (int k = 0; k < blockQuantity; k++)
    {
        fread(&block, sizeof(block), 1, file);
        for (int i = 0; i < 5; i++)
        {
            if (block[i].id == student.id)
            {
                if (search)
                {
                    printf("Student found!!!\n------------------------\nid: %d\ngroup id: %d\nfirst name: %s\nlast name: %s\npatronymic: %s\n", block[i].id, block[i].group, block[i].firstName, block[i].lastName, block[i].patronymic);
                    // cout << i << " " << blockPos << endl;
                    return i;
                }
                printf("Student already exists!\n");
                return -2;
            }
            if (block[i].id == -1 && !search)
            {
                fseek(file, 0, SEEK_SET);
                return i;
            }
        }
        blockPos++;
    }
    return -1;
}

int main()
{
    int choice;
    FILE *file;
    int blockPos = 0;
    Student student;
    cout << "1 - Open old file\n2 - Create new file" << endl;
    cin >> choice;
    switch (choice)
    {
    case 1:
        file = fopen("students.txt", "r+b");
        if (!file)
            return ERROR_OPEN_FAILED;
        fclose(file);
        break;
    case 2:
        file = fopen("students.txt", "w+");
        if (!file)
            return ERROR_OPEN_FAILED;
        fclose(file);

    default:
        break;
    }
    system("cls");
    while (choice != 0)
    {
        cout << endl
             << "1. Add student" << endl
             << "2. Search student" << endl
             << "3. Update student" << endl
             << "4. Delete student" << endl
             << "0. Exit" << endl
             << "------------------" << endl
             << "Enter your choice: ";
        cin >> choice;
        switch (choice)
        {
        case 1:
            addStudent();
            break;
        case 2:
            file = fopen("students.txt", "r+b");
            cout << "Enter student id ";
            cin >> student.id;
            if (searchEmptyPosition(blockPos, file, student, true) == -1)
                printf("Student doesn`t exist :^(\n");
            fclose(file);
            blockPos=0;
            break;
        case 3:
            updateStudent();
            break;
        case 4:
            deleteStudent();
            break;
        case 0:
            return 0;
            break;
        default:
            cout << "Error" << endl;
            return 0;
        }
        printf("\n");
        // system("pause");
        // system("cls");
    }
    return 0;
}