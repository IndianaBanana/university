drop database if exists busstop;

create database if not exists busstop;

use busstop;

create table if not exists way (
num_way int not null,
title_way varchar(40) not null,
primary key (num_way)
);

create table if not exists station_way (
num_way int not null,
num_st int not null,
primary key (num_way, num_st)
);

create table if not exists station (
num_st int not null,
title_st varchar(40) not null,
distance int not null,
primary key (num_st)
);

create table if not exists bus (
num_bus int not null,
model varchar(20) not null,
count_places tinyint not null,
primary key (num_bus)
);

create table if not exists rays (
num_way int not null,
time_hour tinyint  not null,
time_min tinyint  not null,
num_bus int not null,
primary key (num_way, time_hour, time_min)
);

create table if not exists ticket (
place tinyint not null,
num_way int not null,
time_hour tinyint  not null,
time_min tinyint  not null,
num_st int,
primary key (place, num_way, time_hour, time_min)
);

alter table station_way 
add foreign key (num_way) references way (num_way),
add foreign key (num_st) references station (num_st);

alter table rays 
add foreign key (num_way) references way (num_way),
add foreign key (num_bus) references bus (num_bus);



alter table ticket
add foreign key (num_way, time_hour, time_min) references rays (num_way, time_hour, time_min),
add foreign key (num_st) references station (num_st);