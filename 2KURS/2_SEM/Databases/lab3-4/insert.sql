insert into way values 
(1, "Way 1"),
(2, "Way 2"),
 (3, "Way 3"),
(4, "Way 4"),
(5, "Way 5");

 insert into bus values
 (1, "Pazik", 20),
 (2, "Pazik", 20),
(3, "Pazik", 20),
(4, "Pazik", 20),
(5, "Pazik", 20);

insert into station values
(1, "Station 1", 10),
(2, "Station 2", 20),
(3, "Station 3", 30),
(4, "Station 4", 40),
(5, "Station 5", 50);

insert into rays values 
(1, 11, 11, 1), 
(2, 12, 12, 2), 
(3, 13, 13, 3), 
(4, 14, 14, 4), 
(5, 15, 15, 5);

insert into ticket values 
(1, 1, 11, 11, null), 
(2, 1, 11, 11, null), 
(3, 1, 11, 11, null),
(1, 2, 12, 12, null), 
(1, 3, 13, 13, null), 
(1, 4, 14, 14, null), 
(1, 5, 15, 15, null);
