CREATE INDEX country_id ON cities(countryID);

Explain SELECT * FROM cities c where c.cityID=300267;

CREATE index city_id on cities(cityID);


INSERT INTO `cities` (`cityID`, `cityName`, `stateID`, `countryID`, `latitude`, `longitude`) VALUES
(400000, 'qqqqqqqq', 48, 'USA', 37.42, -122.06),
(400001, 'eeeeeeeee', 48, 'FRA', 39.93, 116.39),
(400002, 'wwwwwwww', 48, 'USA', 41.36, -93.43),
(400003, 'rrrrrrrrrr', 48, 'USA', 40.61, -73.79);

Explain SELECT * FROM cities where countryID="chn" and stateID="48";

drop index country_id on cities;

CREATE INDEX state_id ON cities(stateID);
Explain SELECT * FROM cities where countryID="chn" and stateID="48";

drop index state_id on cities;

CREATE INDEX state_and_country_id ON cities(stateID, countryID);
Explain SELECT * FROM cities where countryID="chn" and stateID="48";
Explain SELECT * FROM cities where stateID="48";
show index from cities;
