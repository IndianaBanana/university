/*//! Написать программу, которая работает с хэшированным фалом
хранящем информацию об отношении «студент».
В программе должны быть реализованы следующие функции:
1 добавление информации о студент;
2 изменение информации о студенте;
3 удаление информации о студенте;
4 осуществление поиска информации о студенте.
Отношение студент должно содержать следующие атрибуты:
номер зачетки (тип integer), фамилия (тип string(30)), имя (тип
string(20)), отчество (тип string(30)), номер группы (тип integer).
Атрибут «номер зачетки» выступает в роли первичного ключа.
В качестве хеш-функции необходимо использовать остаток от деления первичного ключа на 4.
Для организации хранения информации записи в файле необходимо использовать тип Zap.
    Type
        Zap = record
        Id_zachet, id_gr: integer;
        Surname, Name: string (20);
        Patronymic: string(30);
    End;
Каждый блок – это запись из массива записей и указателя на следующий блок.
Блок файла должен включать 5 записей.
    Type
        Block = record
        Zap_block: array[1..5] of zap;
        Nextb:integer;
    End;
Для хранения схемы отношения в файле должен использоваться нулевой блок.
Информация о каталоге бакетов также должна размещаться в нулевом блоке.
    Type
        Block0 = record
        Relation_scheme: string(255);
        27
        Catalog: array[0..4]of record firstBlockPos,lastBlockPos:integer;
        End;
    End;
Переменная firstBlockPos – номер первого блока в бакете, переменная lastBlockPos – номер последнего блока в бакете.
В пределах каждого бакета, блоки записываются как в файле в виде кучи.
Программа должна работать с любым файлом, организованным по данной схеме.
 */

#include <iostream>
#include <windows.h>
#include <cstring>
#include <string>
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <io.h>
#include <cstdlib>
#include <sys/stat.h>
#include <errno.h>
#include <sys/types.h>
#include <algorithm>
#include <fstream>
using namespace std;

struct Student // 88 bytes
{
    int id = -1;
    char firstName[30];
    char lastName[20];
    char patronymic[30];
    int group;
};
struct Block
{ // 448 bytes
    Student student[5];
    int next = -1, prev = -1;
};
struct ZeroBlock
{ // 288 bytes
    const char relationScheme[255] = "id: i; surname: s30; name: s20; patronymic: s30; group: i";
    struct
    {
        int firstBlockPos = -1; // number of first block in bucket
        int lastBlockPos = -1;  // number of last block in bucket
    } catalog[4];
};
struct Position
{
    int block;
    int student;
};

Position searchStudentPosition(FILE *file, Student student, int bucket, bool &flag);
Student enterStudent();
void addStudent(FILE *file);
void updateStudent(FILE *file);
void deleteStudent(FILE *file);

Block block;
Block emptyBlock;
ZeroBlock zeroBlock;

Student enterStudent()
{
    Student student;
    cout << "Enter student data (id, group, firstName, lastName, patronymic): " << endl;
    cin >> student.id >> student.group >> student.firstName >> student.lastName >> student.patronymic;
    return student;
}

int hashFunc(int id)
{
    return id % 4;
}

Position searchStudentPosition(FILE *file, Student student, int bucket, bool &flag)
{
    int blockPos = -1, prevBlockPos = -1, studentPos = 0;
    fseek(file, 0, SEEK_SET);
    fread(&zeroBlock, sizeof(ZeroBlock), 1, file);
    blockPos = zeroBlock.catalog[bucket].firstBlockPos;
    cout << "bucket: " << bucket << endl;
    while (blockPos != -1)
    {
        studentPos = 0;
        fseek(file, sizeof(ZeroBlock) + sizeof(Block) * blockPos, SEEK_SET);
        fread(&block, sizeof(Block), 1, file);
        for (Student blockStudent : block.student)
        {
            cout << "student id: " << blockStudent.id << endl;
            if (blockStudent.id == -1)
            { // if we got a -1 id in the block, we can stop searching.
                printf("Student with such id doesn`t exist\n");
                flag = false;                  //? if we doing everything except adding a new student we don't want flag to be false.
                return {blockPos, studentPos}; // returns block's position and student's empty position, so we can add new student;
            }
            if (blockStudent.id == student.id)
            {
                cout
                    << "Student found!" << endl
                    << "------------DATA------------" << endl
                    << "id: " << blockStudent.id << endl
                    << "surname: " << blockStudent.lastName << endl
                    << "name: " << blockStudent.firstName << endl
                    << "patronymic: " << blockStudent.patronymic << endl
                    << "group: " << blockStudent.group << endl
                    << "-----------------------------" << endl;
                flag = true; //* if we adding a new student we don't want flag to be true.
                return {blockPos, studentPos};
            }
            studentPos++;
        }
        prevBlockPos = blockPos;
        blockPos = block.next;
        if (blockPos == zeroBlock.catalog[bucket].firstBlockPos)
            break;
    }
    flag = false;
    cout << "Student not found!" << endl; // We search every block, every student and didn't find it.
                                          // case 1: all the blocks are full of other students but not the searched one.
                                          // we return the last block in the bucket position and position of student > 4
                                          // case 2: there is no blocks in the bucket. we return -1 in the block's position.
    return {prevBlockPos, studentPos};
}

void addStudent(FILE *file)
{
    Student student = enterStudent();
    int bucket = hashFunc(student.id);
    bool flag;
    Block tmpBlock;
    Position position = searchStudentPosition(file, student, bucket, flag);
    if (flag)
        return;
    fseek(file, 0, SEEK_END);
    int blocksCount = (ftell(file) - sizeof(ZeroBlock)) / sizeof(Block);
    if (position.block == -1)
    { // if there is NO blocks in the bucket we create one.
        cout << "The block doesn't exist. Creating new block..." << endl;
        // fwrite(&emptyBlock, sizeof(Block), 1, file);
        zeroBlock.catalog[bucket].firstBlockPos = blocksCount;
        zeroBlock.catalog[bucket].lastBlockPos = blocksCount;
        tmpBlock.student[0] = student;
        tmpBlock.next = blocksCount;
        tmpBlock.prev = blocksCount;
        fseek(file, 0, SEEK_SET);
        fwrite(&zeroBlock, sizeof(ZeroBlock), 1, file);
        fseek(file, 0, SEEK_END);
        fwrite(&tmpBlock, sizeof(Block), 1, file);
        return;
    }
    else if (position.block == zeroBlock.catalog[bucket].lastBlockPos && position.student > 4)
    { // there are already one or more blocks in the bucket but no place for the new student
        tmpBlock.student[0] = student;

        tmpBlock.next = block.next;
        tmpBlock.prev = position.block;

        block.next = blocksCount;
        zeroBlock.catalog[bucket].lastBlockPos = blocksCount;
        fseek(file, 0, SEEK_SET);
        fwrite(&zeroBlock, sizeof(ZeroBlock), 1, file); // rewrite ZeroBlock

        fseek(file, sizeof(ZeroBlock) + sizeof(Block) * position.block, SEEK_SET);
        fwrite(&block, sizeof(Block), 1, file); // rewrite last block in the bucket

        fseek(file, 0, SEEK_END);
        fwrite(&tmpBlock, sizeof(Block), 1, file); // adding the new block to the end of the file

        fseek(file, sizeof(ZeroBlock) + sizeof(Block) * zeroBlock.catalog[bucket].firstBlockPos + 444, SEEK_SET);
        fwrite(&blocksCount, sizeof(int), 1, file); // rewrite prevBlock in the first block in the bucket
        return;
    }
    else
    { // if we found empty place in the block for the new student
        block.student[position.student] = student;
        fseek(file, sizeof(ZeroBlock) + sizeof(Block) * position.block, SEEK_SET);
        fwrite(&block, sizeof(Block), 1, file);
        return;
    }
}

void updateStudent(FILE *file)
{
    Student student = enterStudent();
    bool flag;
    int bucket = hashFunc(student.id);
    Position position = searchStudentPosition(file, student, bucket, flag);
    if (!flag)
        return;
    fseek(file, sizeof(ZeroBlock) + sizeof(Block) * position.block + sizeof(Student) * position.student, SEEK_SET);
    fwrite(&student, sizeof(Student), 1, file);
}

void deleteStudent(FILE *file)
{
    Student student;
    int fileSize;
    bool flag;
    int bucket, tmp;
    cout << "Enter student id: ";
    cin >> student.id;
    bucket = hashFunc(student.id);
    Position position = searchStudentPosition(file, student, bucket, flag);
    if (!flag)
        return;
    Block lastBlock;
    fseek(file, sizeof(ZeroBlock) + sizeof(Block) * zeroBlock.catalog[bucket].lastBlockPos, SEEK_SET); // get last block in the bucket
    fread(&lastBlock, sizeof(Block), 1, file);
    if (lastBlock.student[0].id != -1 && lastBlock.student[1].id == -1 && lastBlock.student[2].id == -1 &&
        lastBlock.student[3].id == -1 && lastBlock.student[4].id == -1)
    { // if in the last block of the bucket only one student left
        fseek(file, sizeof(ZeroBlock) + sizeof(Block) * position.block + sizeof(Student) * position.student, SEEK_SET);
        fwrite(&lastBlock.student[0], sizeof(Student), 1, file); // rewrite deleted student with the one from the last block in the bucket

        // the last block in the bucket is empty now. so we need to correctly delete it from file using chsize().
        fseek(file, -sizeof(Block), SEEK_END);
        cout << ftell(file) << endl;
        int lastBlockInFilePos = (ftell(file) - sizeof(ZeroBlock)) / sizeof(Block); // position of the last block in the file
        // we need to put the last block in the file to the position of the bucket (no matter if the last block in the end of the file or not)
        // so we need to rewrite ZeroBlock catalog of two buckets also we need to rewrite two prevBlock.next in the two buckets.
        Block lastBlockInFile, deletedBlock;
        fseek(file, sizeof(ZeroBlock) + sizeof(Block) * lastBlockInFilePos, SEEK_SET); // got the last block in the file
        fread(&lastBlockInFile, sizeof(Block), 1, file);
        cout << "lastBlockInFilePos: " << lastBlockInFilePos << endl;
        cout << "lastBlockInFile student id: " << lastBlockInFile.student[0].id << endl;
        cout << "lastBlockInFile prev: " << lastBlockInFile.prev << endl;
        cout << "lastBlockInFile next: " << lastBlockInFile.next << endl;
        cout << "lastBlock next: " << lastBlock.next << endl;
        cout << "lastBlock prev: " << lastBlock.prev << endl;
        cout << "bucket first: " << zeroBlock.catalog[bucket].firstBlockPos << endl;
        cout << "bucket last: " << zeroBlock.catalog[bucket].lastBlockPos << endl;

        fseek(file, sizeof(ZeroBlock) + sizeof(Block) * lastBlock.prev + 440, SEEK_SET); // got the address of "nextBlock address" of the prev block of the last blok in the bucket
        fwrite(&lastBlock.next, sizeof(int), 1, file);                                   // rewrite the "nextBlock address" of the prev block of the last blok in the bucket
        fseek(file, sizeof(ZeroBlock) + sizeof(Block) * lastBlock.next + 444, SEEK_SET); // got the address of "prevBlock address" of the next block in the bucket
        fwrite(&lastBlock.prev, sizeof(int), 1, file);                                   // rewrite the "prevBlock address" of the next block in the bucket

        fseek(file, sizeof(ZeroBlock) + sizeof(Block) * zeroBlock.catalog[bucket].lastBlockPos, SEEK_SET);
        fwrite(&lastBlockInFile, sizeof(Block), 1, file); // move the last block in the file to the position of the last block in the bucket

        tmp = zeroBlock.catalog[bucket].lastBlockPos;
        if (lastBlockInFilePos == lastBlockInFile.prev && lastBlockInFilePos == lastBlockInFile.next)
        {
            cout << "lastBlockInFilePos == lastBlockInFile.prev && lastBlockInFilePos == lastBlockInFile.next" << endl; // if the last block in the file is the only one in the bucket
            fseek(file, sizeof(ZeroBlock) + sizeof(Block) * zeroBlock.catalog[bucket].lastBlockPos + 440, SEEK_SET);    // rewrite prev and next of the last block in the file
            fwrite(&tmp, sizeof(int), 1, file);
            fwrite(&tmp, sizeof(int), 1, file);
        }
        else if (lastBlockInFilePos != zeroBlock.catalog[bucket].lastBlockPos)
        {
            fseek(file, sizeof(ZeroBlock) + sizeof(Block) * lastBlockInFile.prev + 440, SEEK_SET); // got the address of "nextBlock address" of the prev block of the last block in the file
            fwrite(&tmp, sizeof(int), 1, file);                                                    // rewrite "nextBlock address" in the prev block of the last block in the file
            fseek(file, sizeof(ZeroBlock) + sizeof(Block) * lastBlockInFile.next + 444, SEEK_SET); // got the address of "prevBlock address" of the first block in the bucket of the last block in the file
            fwrite(&tmp, sizeof(int), 1, file);                                                    // rewrite "prevBlock address" in the first block in the bucket of the last block in the file
        }

        // zeroBlock.catalog[bucket].lastBlockPos = lastBlock.prev; // update the lastBlockPos in the catalog of the bucket of the deleted student
        for (int i = 0; i < 4; i++)
        { // update the firstBlockPos in the catalog of the bucket of the last block in file
            if (i == hashFunc(lastBlockInFile.student[0].id))
            {
                cout << "bucket = " << i << endl;
                if (i != bucket)
                {
                    if (zeroBlock.catalog[i].firstBlockPos == zeroBlock.catalog[i].lastBlockPos)
                    { // if there is only one block in the bucket of the last block in file
                        cout << "there is only one block in the bucket of the last block in file" << endl;
                        zeroBlock.catalog[i].firstBlockPos = zeroBlock.catalog[bucket].lastBlockPos; // update the firstBlockPos in the catalog of the bucket of the last block in file
                        zeroBlock.catalog[i].lastBlockPos = zeroBlock.catalog[bucket].lastBlockPos;  // update the lastBlockPos in the catalog of the bucket of the last block in file
                    }
                    else if (lastBlockInFilePos == zeroBlock.catalog[i].firstBlockPos)
                    {
                        zeroBlock.catalog[i].firstBlockPos = zeroBlock.catalog[bucket].lastBlockPos; // update the firstBlockPos in the catalog of the bucket of the last block in file
                    }
                    else if (lastBlockInFilePos == zeroBlock.catalog[i].lastBlockPos)
                    {
                        zeroBlock.catalog[i].lastBlockPos = zeroBlock.catalog[bucket].lastBlockPos; // update the lastBlockPos in the catalog of the bucket of the last block in file
                    }
                    if (zeroBlock.catalog[bucket].lastBlockPos == zeroBlock.catalog[bucket].firstBlockPos)
                    { // if there is only one block in the bucket
                        cout << "there is only one block in the bucket" << endl;
                        zeroBlock.catalog[bucket].firstBlockPos = -1;
                        zeroBlock.catalog[bucket].lastBlockPos = -1;
                        break;
                    }
                    zeroBlock.catalog[bucket].lastBlockPos = lastBlock.prev;
                }
                else
                {
                    cout << "bucket = " << "i" << endl;
                    if (zeroBlock.catalog[bucket].lastBlockPos == zeroBlock.catalog[bucket].firstBlockPos)
                    { // if there is only one block in the bucket
                        cout << "there is only one block in the bucket" << endl;
                        zeroBlock.catalog[bucket].firstBlockPos = -1;
                        zeroBlock.catalog[bucket].lastBlockPos = -1;
                        break;
                    }
                    if (lastBlockInFilePos == zeroBlock.catalog[i].firstBlockPos)
                    {
                        cout << "lastBlockInFilePos == zeroBlock.catalog[i].firstBlockPos" << endl;
                        zeroBlock.catalog[i].firstBlockPos = zeroBlock.catalog[bucket].lastBlockPos; // update the firstBlockPos in the catalog of the bucket of the last block in file
                    }
                    else
                    {
                        // cout << "there is more than one block in the bucket" << endl;
                        zeroBlock.catalog[bucket].lastBlockPos = lastBlock.prev;
                    }
                }
                break;
            }
        }

        fseek(file, 0, SEEK_SET);
        fwrite(&zeroBlock, sizeof(ZeroBlock), 1, file);
        fseek(file, -sizeof(Block), SEEK_END);
        fileSize = ftell(file);
        cout << "fileSize = " << fileSize << endl;
        fclose(file);
        // fclose(file);
        int fd = open("students.txt", O_RDWR | O_CREAT, S_IREAD | S_IWRITE);
        if (fd == -1)
        {
            cout << "can't open file in delete user\n";
            return;
        }
        if (_chsize(fd, fileSize) == -1)
        {
            cout << "can't change file size in delete user\n";
            close(fd);
            return;
        }
        close(fd);
        file = fopen("students.txt", "r+b");
        fseek(file, 0, SEEK_SET);
        fread(&zeroBlock, sizeof(ZeroBlock), 1, file);
        cout << "ZeroBlock first: " << zeroBlock.catalog[bucket].firstBlockPos << endl;
        cout << "ZeroBlock last: " << zeroBlock.catalog[bucket].lastBlockPos << endl;
        fseek(file, sizeof(ZeroBlock) + sizeof(Block) * zeroBlock.catalog[bucket].lastBlockPos, SEEK_SET);
        fread(&lastBlock, sizeof(Block), 1, file);
        cout << "Last block: " << lastBlock.student[0].id << endl;
        cout << "lastBlock next: " << lastBlock.next << endl;
        cout << "lastBlock prev: " << lastBlock.prev << endl;
        fseek(file, sizeof(ZeroBlock) + sizeof(Block) * (lastBlockInFilePos - 1), SEEK_SET);
        fread(&lastBlockInFile, sizeof(Block), 1, file);
        cout << "Last block in file student id: " << lastBlockInFile.student[0].id << endl;
        cout << "first block in bucket " << zeroBlock.catalog[hashFunc(lastBlockInFile.student[0].id)].firstBlockPos << endl;
        cout << "last block in bucket " << zeroBlock.catalog[hashFunc(lastBlockInFile.student[0].id)].lastBlockPos << endl;
        cout << "lastBlockInFile next: " << lastBlockInFile.next << endl;
        cout << "lastBlockInFile prev: " << lastBlockInFile.prev << endl;
        fseek(file, sizeof(ZeroBlock) + sizeof(Block) * lastBlockInFile.prev, SEEK_SET);
        fread(&lastBlockInFile, sizeof(Block), 1, file);
        cout << "Last block in file student id: " << lastBlockInFile.student[0].id << endl;
        cout << "first block in bucket " << zeroBlock.catalog[hashFunc(lastBlockInFile.student[0].id)].firstBlockPos << endl;
        cout << "last block in bucket " << zeroBlock.catalog[hashFunc(lastBlockInFile.student[0].id)].lastBlockPos << endl;
        cout << "lastBlockInFile next: " << lastBlockInFile.next << endl;
        cout << "lastBlockInFile prev: " << lastBlockInFile.prev << endl;
        fseek(file, sizeof(ZeroBlock) + sizeof(Block) * lastBlockInFile.prev, SEEK_SET);
        fread(&lastBlockInFile, sizeof(Block), 1, file);
        cout << "Last block in file student id: " << lastBlockInFile.student[0].id << endl;
        cout << "first block in bucket " << zeroBlock.catalog[hashFunc(lastBlockInFile.student[0].id)].firstBlockPos << endl;
        cout << "last block in bucket " << zeroBlock.catalog[hashFunc(lastBlockInFile.student[0].id)].lastBlockPos << endl;
        cout << "lastBlockInFile next: " << lastBlockInFile.next << endl;
        cout << "lastBlockInFile prev: " << lastBlockInFile.prev << endl;

        fclose(file);

        return;
    }
    else
    { // the last block is not empty after cut and paste last student from it
        cout << "The last block is not empty after cut and paste last student from it" << endl;
        for (int i = 4; i >= 0; i--)
        {
            if (lastBlock.student[i].id != -1)
            {
                fseek(file, sizeof(ZeroBlock) + sizeof(Block) * position.block + sizeof(Student) * position.student, SEEK_SET);
                fwrite(&lastBlock.student[i], sizeof(Student), 1, file); // rewrite deleted student with the one from the last block in the bucket
                lastBlock.student[i].id = -1;
                fseek(file, sizeof(ZeroBlock) + sizeof(Block) * zeroBlock.catalog[bucket].lastBlockPos + sizeof(Student) * i, SEEK_SET);
                fwrite(&lastBlock.student[i], sizeof(Student), 1, file);
                break;
            }
        }
    }
    fclose(file);
}

int main()
{
    int choice;
    FILE *file;
    int blockPos = 0;
    Student student;
    cout << "1 - Open old file\n2 - Create new file" << endl;
    cin >> choice;
    switch (choice)
    {
    case 1:
        file = fopen("students.txt", "r+b");
        if (!file)
            return -1;
        fclose(file);
        break;
    case 2:
        file = fopen("students.txt", "w+");
        if (!file)
            return -1;
        fwrite(&zeroBlock, sizeof(zeroBlock), 1, file);
        fclose(file);
        break;

    default:
        break;
    }
    // system("cls");
    while (choice != 0)
    {
        file = fopen("students.txt", "r+b");
        fseek(file, 0, SEEK_END);
        cout
            << endl
            << "--------------------------" << endl
            << "File size: " << ftell(file) << endl
            << "--------------------------" << endl
            << endl;
        fclose(file);
        cout
            << "1. Add student" << endl
            << "2. Search student" << endl
            << "3. Update student" << endl
            << "4. Delete student" << endl
            << "0. Exit" << endl
            << "------------------" << endl
            << "Enter your choice: ";
        cin >> choice;
        switch (choice)
        {
        case 1:
        {
            file = fopen("students.txt", "r+b");
            addStudent(file);
            fclose(file);
            break;
        }
        case 2:
        {
            Student student;
            bool flag;
            file = fopen("students.txt", "r+b");
            cout << "Enter student id ";
            cin >> student.id;
            searchStudentPosition(file, student, hashFunc(student.id), flag);
            fclose(file);
            break;
        }
        case 3:
        {
            file = fopen("students.txt", "r+b");
            updateStudent(file);
            fclose(file);
            break;
        }
        case 4:
        {
            file = fopen("students.txt", "r+b");
            deleteStudent(file);
            fclose(file);
            break;
        }
        case 0:
        {
            // fstream f1;
            // fstream f2;
            // string ch;
            // f1.open("students3.txt", ios::in | ios::binary);
            // f2.open("students.txt", ios::out | ios::binary);
            // while (!f1.eof())
            // {
            //     getline(f1, ch);
            //     if (f1.eof())
            //     {
            //         f2 << ch;
            //         break;
            //     }
            //     f2 << ch << endl;
            // }
            // f1.close();
            // f2.close();
            return 0;
        }
        default:
            cout << "Error" << endl;
            break;
        }
        printf("\n");
        // system("pause");
        // system("cls");
    }
    return 0;
}