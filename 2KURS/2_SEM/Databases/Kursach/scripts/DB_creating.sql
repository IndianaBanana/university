-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema workouts
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema workouts
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `workouts` DEFAULT CHARACTER SET utf8 ;
USE `workouts` ;

-- -----------------------------------------------------
-- Table `workouts`.`load_unit`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `workouts`.`load_unit` (
  `id_load_unit` TINYINT NOT NULL,
  `name` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id_load_unit`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `workouts`.`amount_unit`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `workouts`.`amount_unit` (
  `id_amount_unit` TINYINT NOT NULL,
  `name` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id_amount_unit`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `workouts`.`exercise`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `workouts`.`exercise` (
  `id_exercise` SMALLINT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NOT NULL,
  `id_load_unit` TINYINT NOT NULL,
  `id_amount_unit` TINYINT NOT NULL,
  PRIMARY KEY (`id_exercise`),
  INDEX `fk_exercise_load_unit1_idx` (`id_load_unit` ASC) VISIBLE,
  INDEX `fk_exercise_amount_unit1_idx` (`id_amount_unit` ASC) VISIBLE,
  CONSTRAINT `fk_exercise_load_unit1`
    FOREIGN KEY (`id_load_unit`)
    REFERENCES `workouts`.`load_unit` (`id_load_unit`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_exercise_amount_unit1`
    FOREIGN KEY (`id_amount_unit`)
    REFERENCES `workouts`.`amount_unit` (`id_amount_unit`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `workouts`.`muscle_group`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `workouts`.`muscle_group` (
  `id_muscle_group` TINYINT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id_muscle_group`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `workouts`.`muscle`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `workouts`.`muscle` (
  `id_muscle` TINYINT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NOT NULL,
  `id_muscle_group` TINYINT NOT NULL,
  PRIMARY KEY (`id_muscle`),
  INDEX `fk_мышца_группа_мышц_idx` (`id_muscle_group` ASC) VISIBLE,
  CONSTRAINT `fk_мышца_группа_мышц`
    FOREIGN KEY (`id_muscle_group`)
    REFERENCES `workouts`.`muscle_group` (`id_muscle_group`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `workouts`.`workout`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `workouts`.`workout` (
  `id_workout` SMALLINT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id_workout`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `workouts`.`muscle_in_exercise`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `workouts`.`muscle_in_exercise` (
  `id_exercise` SMALLINT NOT NULL,
  `id_muscle` TINYINT NOT NULL,
  `muscle_load` DECIMAL(1,1) NOT NULL,
  PRIMARY KEY (`id_exercise`, `id_muscle`),
  INDEX `fk_упражнение_has_мышца_мышца1_idx` (`id_muscle` ASC) VISIBLE,
  INDEX `fk_упражнение_has_мышца_упражнение_idx` (`id_exercise` ASC) VISIBLE,
  CONSTRAINT `fk_упражнение_has_мышца_упражнение1`
    FOREIGN KEY (`id_exercise`)
    REFERENCES `workouts`.`exercise` (`id_exercise`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_упражнение_has_мышца_мышца1`
    FOREIGN KEY (`id_muscle`)
    REFERENCES `workouts`.`muscle` (`id_muscle`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `workouts`.`workout_template`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `workouts`.`workout_template` (
  `id_workout` SMALLINT NOT NULL,
  `exercise_order` TINYINT NOT NULL,
  `id_exercise` SMALLINT NOT NULL,
  `plan_amount` TINYINT NOT NULL,
  `plan_load` SMALLINT NOT NULL,
  PRIMARY KEY (`id_workout`, `exercise_order`),
  INDEX `fk_упражнение_has_тренировка_трени_idx` (`id_workout` ASC) VISIBLE,
  INDEX `fk_упражнение_has_тренировка_упраж_idx` (`id_exercise` ASC) VISIBLE,
  CONSTRAINT `fk_упражнение_has_тренировка_упражн1`
    FOREIGN KEY (`id_exercise`)
    REFERENCES `workouts`.`exercise` (`id_exercise`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_упражнение_has_тренировка_тренир1`
    FOREIGN KEY (`id_workout`)
    REFERENCES `workouts`.`workout` (`id_workout`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `workouts`.`workout_journal`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `workouts`.`workout_journal` (
  `date_time` DATETIME NOT NULL,
  `id_workout` SMALLINT NOT NULL,
  `exercise_order` TINYINT NOT NULL,
  `real_amount` TINYINT NULL,
  `real_load` SMALLINT NULL,
  `complete` TINYINT(1) NULL,
  PRIMARY KEY (`date_time`, `id_workout`, `exercise_order`),
  INDEX `fk_workout_schedule_exirsize_in_workout1_idx` (`id_workout` ASC, `exercise_order` ASC) VISIBLE,
  CONSTRAINT `fk_workout_schedule_exirsize_in_workout1`
    FOREIGN KEY (`id_workout` , `exercise_order`)
    REFERENCES `workouts`.`workout_template` (`id_workout` , `exercise_order`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
