DELIMITER  //
create procedure if not exists exercise_for_muscle_group(muscle_group_name varchar(255))
begin
SELECT e.name as exercise, m.name, muscle_load 
from muscle_in_exercise mie 
join muscle m using(id_muscle)
join muscle_group mg using(id_muscle_group)
join exercise e using(id_exercise)
where mg.name = muscle_group_name
order by 1, muscle_load DESC;
end;
//

create procedure if not exists best_exercise_for_muscle(muscle_name varchar(255))
begin
select e.name, muscle_load
from muscle_in_exercise mie
join exercise e using(id_exercise)
join muscle m using(id_muscle)
where m.name = muscle_name
order by muscle_load DESC 
limit 5;
end;
//

create procedure if not exists fast_complete_workout(var_date_time datetime, workout_name varchar(255)) 
begin
	select id_workout into @id_workout from workout w where name = workout_name;
    update workout_journal set complete = 1 where date_time = var_date_time and id_workout = @id_workout;
    set @n = row_count();
    set @i = 1;
    while @i <= @n do
       select plan_amount, plan_load into @plan_amount, @plan_load 
       from workout_template wt 
       where id_workout = @id_workout  and exercise_order = @i;
      
       update workout_journal 
       set real_amount = @plan_amount, real_load = @plan_load 
       where date_time = var_date_time and exercise_order = @i and id_workout = @id_workout;
       set @i = @i + 1;
    end while;
end;
//

create procedure if not exists fast_workout_add(var_date_time datetime, var_workout_name varchar(255))
begin
	select id_workout into @id_workout from workout w where name = var_workout_name;

	SELECT count(*) 
	into @n 
	from workout_template wt 
	where wt.id_workout = @id_workout;

	set @i = 1;

	while @i <= @n do
		insert into workout_journal values (var_date_time, @id_workout, @i, null, null, 0);
		set	@i = @i + 1; 
	end while;
end;
//

CREATE PROCEDURE if not exists show_workout_in_journal(var_date_time datetime, workout_name varchar(255))
begin
select wj.date_time `date and time`, w.name `workout`, wj.exercise_order `order` , e.name `exercise`,
	wt.plan_load `planned load`, wj.real_load `real load`, lu.name  `load unit`,
	wt.plan_amount  `planned amount`, wj.real_amount `real amount` , au.name `amount unit`, wj.complete `completion`
from workout_template wt 
join workout_journal wj
	on wt.id_workout = wj.id_workout and wt.exercise_order = wj.exercise_order 
join workout w 
	on w.id_workout = wj.id_workout
join exercise e 
	on e.id_exercise = wt.id_exercise 
join load_unit lu using(id_load_unit)
join amount_unit au using(id_amount_unit)
where date_time = var_date_time and w.name = workout_name
order by date_time, wj.exercise_order;
end;//

-- самый лучший результат в упражнении
CREATE PROCEDURE best_result_in_exercise(exercise_name VARCHAR(255), amount_min DECIMAL(5,2))
BEGIN
    SELECT wj.real_load AS `best load result`, lu.name AS `load unit`, wj.real_amount AS `best amount`, au.name AS `amount unit`
    FROM workout_template wt
    JOIN workout_journal wj ON wt.id_workout = wj.id_workout AND wt.exercise_order = wj.exercise_order 
    JOIN exercise e using(id_exercise)
    JOIN load_unit lu using(id_load_unit)
    JOIN amount_unit au using(id_amount_unit)
    WHERE wj.real_amount >= amount_min AND e.name = exercise_name
    ORDER BY wj.real_load DESC, wj.real_amount DESC
    LIMIT 1;
END;//

create procedure complete_exercise(var_real_load decimal(5,2), var_real_amount decimal(5,2), var_date_time datetime, var_ex_order tinyint, workout_name varchar(255))
begin
	select id_workout into @id_workout from workout w where name = workout_name;
	update workout_journal 
	set real_load = var_real_load, real_amount = var_real_amount, complete = 1
	where date_time = var_date_time and exercise_order = var_ex_order and id_workout = @id_workout;
end;//