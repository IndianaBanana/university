drop database if exists workouts;
CREATE DATABASE IF NOT EXISTS workouts;

USE workouts;

CREATE TABLE IF NOT EXISTS workouts.load_unit (
  id_load_unit TINYINT NOT NULL AUTO_INCREMENT,
  name VARCHAR(255) NOT NULL unique,
  PRIMARY KEY (id_load_unit)
);

CREATE TABLE IF NOT EXISTS workouts.amount_unit (
  id_amount_unit TINYINT NOT NULL AUTO_INCREMENT,
  name VARCHAR(255) NOT NULL unique,
  PRIMARY KEY (id_amount_unit)
);

CREATE TABLE IF NOT EXISTS workouts.exercise (
  id_exercise SMALLINT NOT NULL AUTO_INCREMENT,
  name VARCHAR(255) NOT NULL unique,
  id_load_unit TINYINT NOT NULL,
  id_amount_unit TINYINT NOT NULL,
  PRIMARY KEY (id_exercise),
  FOREIGN KEY (id_load_unit) REFERENCES workouts.load_unit (id_load_unit),
  FOREIGN KEY (id_amount_unit) REFERENCES workouts.amount_unit (id_amount_unit)
);

CREATE TABLE IF NOT EXISTS workouts.muscle_group (
  id_muscle_group TINYINT NOT NULL AUTO_INCREMENT,
  name VARCHAR(255) NOT NULL unique,
  PRIMARY KEY (id_muscle_group)
);

CREATE TABLE IF NOT EXISTS workouts.muscle (
  id_muscle TINYINT NOT NULL AUTO_INCREMENT,
  name VARCHAR(255) NOT NULL,
  id_muscle_group TINYINT NOT NULL,
  PRIMARY KEY (id_muscle),
  FOREIGN KEY (id_muscle_group) REFERENCES workouts.muscle_group (id_muscle_group)
);

CREATE TABLE IF NOT EXISTS workouts.workout (
  id_workout SMALLINT NOT NULL AUTO_INCREMENT,
  name VARCHAR(255) NOT NULL unique,
  PRIMARY KEY (id_workout)
);

CREATE TABLE IF NOT EXISTS workouts.muscle_in_exercise (
  id_exercise SMALLINT NOT NULL,
  id_muscle TINYINT NOT NULL,
  muscle_load TINYINT NOT NULL, -- percents
  PRIMARY KEY (id_exercise, id_muscle),
  FOREIGN KEY (id_exercise) REFERENCES workouts.exercise (id_exercise),
  FOREIGN KEY (id_muscle) REFERENCES workouts.muscle (id_muscle)
);

CREATE TABLE IF NOT EXISTS workouts.workout_template (
  id_workout SMALLINT NOT NULL,
  exercise_order TINYINT NOT NULL check (exercise_order > 0),
  id_exercise SMALLINT NOT NULL,
  plan_amount DECIMAL(5,2) NOT NULL,
  plan_load DECIMAL(5,2) NOT NULL,
  PRIMARY KEY (id_workout, exercise_order),
  FOREIGN KEY (id_exercise) REFERENCES workouts.exercise (id_exercise),
  FOREIGN KEY (id_workout) REFERENCES workouts.workout (id_workout)
);

CREATE TABLE IF NOT EXISTS workouts.workout_journal (
  date_time DATETIME NOT NULL,
  id_workout SMALLINT NOT NULL,
  exercise_order TINYINT NOT NULL check (exercise_order > 0),
  real_amount DECIMAL(5,2) NULL,
  real_load DECIMAL(5,2) NULL,
  complete BOOLEAN NULL,
  PRIMARY KEY (date_time, id_workout, exercise_order),
  FOREIGN KEY (id_workout, exercise_order) REFERENCES workouts.workout_template (id_workout, exercise_order)
);




-- ---------------------------------
-- Inserting
-- ---------------------------------

insert into  load_unit (name) values
  ('kg'),
  ('lb'),
  ('km/h'),
  ('miles/h'),
  ('none');

insert into amount_unit (name) values  
  ('reps'),
  ('minutes');

insert into muscle_group (name)values
  ('Neck'),
  ('Shoulders'),
  ('Chest'),
  ('Back'),
  ('Biceps'),
  ('Forearms'),
  ('Triceps'), 
  ('Core'),
  ('Legs');


INSERT INTO muscle (name, id_muscle_group) VALUES
	('Pectoralis Major', (SELECT id_muscle_group FROM muscle_group WHERE name = 'Chest')),
	('Pectoralis Minor', (SELECT id_muscle_group FROM muscle_group WHERE name = 'Chest')),

	('Trapezius', (SELECT id_muscle_group FROM muscle_group WHERE name = 'Neck')),
  ('Omohyoid', (SELECT id_muscle_group FROM muscle_group WHERE name = 'Neck')),
  ('Sternohyoid', (SELECT id_muscle_group FROM muscle_group WHERE name = 'Neck')),

  
  ('Deltoid Anterior Head', (SELECT id_muscle_group FROM muscle_group WHERE name = 'Shoulders')),
  ('Deltoid Posterior Head', (SELECT id_muscle_group FROM muscle_group WHERE name = 'Shoulders')),
  ('Deltoid Middle Head', (SELECT id_muscle_group FROM muscle_group WHERE name = 'Shoulders')),


	('Latissimus Dorsi', (SELECT id_muscle_group FROM muscle_group WHERE name = 'Back')),
	('Rhomboids', (SELECT id_muscle_group FROM muscle_group WHERE name = 'Back')),
	('Erector Spinae', (SELECT id_muscle_group FROM muscle_group WHERE name = 'Back')),

	('Quadriceps', (SELECT id_muscle_group FROM muscle_group WHERE name = 'Legs')),
	('Hamstrings', (SELECT id_muscle_group FROM muscle_group WHERE name = 'Legs')),
	('Gluteus', (SELECT id_muscle_group FROM muscle_group WHERE name = 'Legs')),
	('Gastrocnemius', (SELECT id_muscle_group FROM muscle_group WHERE name = 'Legs')),
	('Soleus', (SELECT id_muscle_group FROM muscle_group WHERE name = 'Legs')),
	('Hip Flexors', (SELECT id_muscle_group FROM muscle_group WHERE name = 'Legs')),
  ('Triceps Surae', (SELECT id_muscle_group FROM muscle_group WHERE name = 'Legs')),
	('Triceps Longus', (SELECT id_muscle_group FROM muscle_group WHERE name = 'Legs')),


	('Biceps Brachii', (SELECT id_muscle_group FROM muscle_group WHERE name = 'Biceps')),
	('Brachialis', (SELECT id_muscle_group FROM muscle_group WHERE name = 'Biceps')),
	('Brachioradialis', (SELECT id_muscle_group FROM muscle_group WHERE name = 'Biceps')),

	('Triceps Medial Head', (SELECT id_muscle_group FROM muscle_group WHERE name = 'Triceps')),
	('Triceps Lateral Head', (SELECT id_muscle_group FROM muscle_group WHERE name = 'Triceps')),
	('Triceps Long Head', (SELECT id_muscle_group FROM muscle_group WHERE name = 'Triceps')),
	
	('Rectus Abdominis', (SELECT id_muscle_group FROM muscle_group WHERE name = 'Core')),
	('Obliques', (SELECT id_muscle_group FROM muscle_group WHERE name = 'Core'));

	

INSERT INTO exercise (name, id_load_unit, id_amount_unit) VALUES
  ('Rest', (SELECT id_load_unit FROM load_unit WHERE name = 'none'), (SELECT id_amount_unit FROM amount_unit WHERE name = 'minutes')),
-- chest
  ('Push-ups', (SELECT id_load_unit FROM load_unit WHERE name = 'kg'), (SELECT id_amount_unit FROM amount_unit WHERE name = 'reps')),
	('Bench Press', (SELECT id_load_unit FROM load_unit WHERE name = 'kg'), (SELECT id_amount_unit FROM amount_unit WHERE name = 'reps')),
	('Incline Dumbbell Fly', (SELECT id_load_unit FROM load_unit WHERE name = 'lb'), (SELECT id_amount_unit FROM amount_unit WHERE name = 'reps')),
	('Straight Arm Dumbbell Pullovers', (SELECT id_load_unit FROM load_unit WHERE name = 'lb'), (SELECT id_amount_unit FROM amount_unit WHERE name = 'reps')),
	('Dumbbell Fly', (SELECT id_load_unit FROM load_unit WHERE name = 'lb'), (SELECT id_amount_unit FROM amount_unit WHERE name = 'reps')),
	('Decline Bench Press', (SELECT id_load_unit FROM load_unit WHERE name = 'kg'), (SELECT id_amount_unit FROM amount_unit WHERE name = 'reps')),
	('Incline Bench Press', (SELECT id_load_unit FROM load_unit WHERE name = 'kg'), (SELECT id_amount_unit FROM amount_unit WHERE name = 'reps')),
	('Pec Fly', (SELECT id_load_unit FROM load_unit WHERE name = 'kg'), (SELECT id_amount_unit FROM amount_unit WHERE name = 'reps')),
	('Close Grip Bench Press', (SELECT id_load_unit FROM load_unit WHERE name = 'kg'), (SELECT id_amount_unit FROM amount_unit WHERE name = 'reps')),
	('Hammer Press', (SELECT id_load_unit FROM load_unit WHERE name = 'kg'), (SELECT id_amount_unit FROM amount_unit WHERE name = 'reps')),
-- shoulders
	('Side Dumbbell Raise', (SELECT id_load_unit FROM load_unit WHERE name = 'kg'), (SELECT id_amount_unit FROM amount_unit WHERE name = 'reps')),
	('Dumbbell Front Raise', (SELECT id_load_unit FROM load_unit WHERE name = 'kg'), (SELECT id_amount_unit FROM amount_unit WHERE name = 'reps')),
	('Dumbbell Shoulder Press', (SELECT id_load_unit FROM load_unit WHERE name = 'kg'), (SELECT id_amount_unit FROM amount_unit WHERE name = 'reps')),
	('Bent-Over Rear Raise', (SELECT id_load_unit FROM load_unit WHERE name = 'kg'), (SELECT id_amount_unit FROM amount_unit WHERE name = 'reps')),
	('Overhead Press', (SELECT id_load_unit FROM load_unit WHERE name = 'kg'), (SELECT id_amount_unit FROM amount_unit WHERE name = 'reps')),
	('Single-Arm Overhead Press', (SELECT id_load_unit FROM load_unit WHERE name = 'kg'), (SELECT id_amount_unit FROM amount_unit WHERE name = 'reps')),
	('Single-Arm Upright Row', (SELECT id_load_unit FROM load_unit WHERE name = 'kg'), (SELECT id_amount_unit FROM amount_unit WHERE name = 'reps')),
-- back
	('Pull-Ups', (SELECT id_load_unit FROM load_unit WHERE name = 'kg'), (SELECT id_amount_unit FROM amount_unit WHERE name = 'reps')),
	('T-bar', (SELECT id_load_unit FROM load_unit WHERE name = 'kg'), (SELECT id_amount_unit FROM amount_unit WHERE name = 'reps')),
	('Incline Row', (SELECT id_load_unit FROM load_unit WHERE name = 'kg'), (SELECT id_amount_unit FROM amount_unit WHERE name = 'reps')),
	('Barbell Row', (SELECT id_load_unit FROM load_unit WHERE name = 'kg'), (SELECT id_amount_unit FROM amount_unit WHERE name = 'reps')),
	('Superman Press', (SELECT id_load_unit FROM load_unit WHERE name = 'kg'), (SELECT id_amount_unit FROM amount_unit WHERE name = 'reps')),
	('Lat Pulldown', (SELECT id_load_unit FROM load_unit WHERE name = 'kg'), (SELECT id_amount_unit FROM amount_unit WHERE name = 'reps')),
	('Straight-Arm Cable Pulldown', (SELECT id_load_unit FROM load_unit WHERE name = 'kg'), (SELECT id_amount_unit FROM amount_unit WHERE name = 'reps')),
	('Two-seated Cable row', (SELECT id_load_unit FROM load_unit WHERE name = 'kg'), (SELECT id_amount_unit FROM amount_unit WHERE name = 'reps')),
	('Shrugs', (SELECT id_load_unit FROM load_unit WHERE name = 'kg'), (SELECT id_amount_unit FROM amount_unit WHERE name = 'reps')),
	('Single-Arm Cable Pulldown', (SELECT id_load_unit FROM load_unit WHERE name = 'kg'), (SELECT id_amount_unit FROM amount_unit WHERE name = 'reps')),
  -- biceps
	('Dumbbell Curls', (SELECT id_load_unit FROM load_unit WHERE name = 'kg'), (SELECT id_amount_unit FROM amount_unit WHERE name = 'reps')),
	('Reverse Curls', (SELECT id_load_unit FROM load_unit WHERE name = 'kg'), (SELECT id_amount_unit FROM amount_unit WHERE name = 'reps')),
	('Hammer Curls', (SELECT id_load_unit FROM load_unit WHERE name = 'kg'), (SELECT id_amount_unit FROM amount_unit WHERE name = 'reps')),
	('Cable Curls', (SELECT id_load_unit FROM load_unit WHERE name = 'kg'), (SELECT id_amount_unit FROM amount_unit WHERE name = 'reps')),
	('Concentration Curls', (SELECT id_load_unit FROM load_unit WHERE name = 'kg'), (SELECT id_amount_unit FROM amount_unit WHERE name = 'reps')),
	('Preacher Curls', (SELECT id_load_unit FROM load_unit WHERE name = 'kg'), (SELECT id_amount_unit FROM amount_unit WHERE name = 'reps')),
  -- Triceps
	('Tricep Dips', (SELECT id_load_unit FROM load_unit WHERE name = 'kg'), (SELECT id_amount_unit FROM amount_unit WHERE name = 'reps')),
	('Lying Triceps Extension', (SELECT id_load_unit FROM load_unit WHERE name = 'kg'), (SELECT id_amount_unit FROM amount_unit WHERE name = 'reps')),
	('Single-Arm Cable Extension', (SELECT id_load_unit FROM load_unit WHERE name = 'kg'), (SELECT id_amount_unit FROM amount_unit WHERE name = 'reps')),
	('Triceps Cable Extensions', (SELECT id_load_unit FROM load_unit WHERE name = 'kg'), (SELECT id_amount_unit FROM amount_unit WHERE name = 'reps')),
	('Overhead Cable Extensions', (SELECT id_load_unit FROM load_unit WHERE name = 'kg'), (SELECT id_amount_unit FROM amount_unit WHERE name = 'reps')),
  -- core 
	('Hanging Knee Raises', (SELECT id_load_unit FROM load_unit WHERE name = 'kg'), (SELECT id_amount_unit FROM amount_unit WHERE name = 'reps')),
	('HyperExtension', (SELECT id_load_unit FROM load_unit WHERE name = 'kg'), (SELECT id_amount_unit FROM amount_unit WHERE name = 'reps')),
	('Abdominal Crunch', (SELECT id_load_unit FROM load_unit WHERE name = 'kg'), (SELECT id_amount_unit FROM amount_unit WHERE name = 'reps')),
	('Leg Raise', (SELECT id_load_unit FROM load_unit WHERE name = 'kg'), (SELECT id_amount_unit FROM amount_unit WHERE name = 'reps')),
	('Plank', (SELECT id_load_unit FROM load_unit WHERE name = 'kg'), (SELECT id_amount_unit FROM amount_unit WHERE name = 'reps')),
  -- legs
	('Lunges', (SELECT id_load_unit FROM load_unit WHERE name = 'kg'), (SELECT id_amount_unit FROM amount_unit WHERE name = 'reps')),
	('Jogging', (SELECT id_load_unit FROM load_unit WHERE name = 'km/h'), (SELECT id_amount_unit FROM amount_unit WHERE name = 'minutes')),
	('Barbell Squats', (SELECT id_load_unit FROM load_unit WHERE name = 'kg'), (SELECT id_amount_unit FROM amount_unit WHERE name = 'reps')),
	('Deadlift', (SELECT id_load_unit FROM load_unit WHERE name = 'kg'), (SELECT id_amount_unit FROM amount_unit WHERE name = 'reps')),
	('Dumbbell Step-Up', (SELECT id_load_unit FROM load_unit WHERE name = 'kg'), (SELECT id_amount_unit FROM amount_unit WHERE name = 'reps')),
	('Calf Raises', (SELECT id_load_unit FROM load_unit WHERE name = 'kg'), (SELECT id_amount_unit FROM amount_unit WHERE name = 'reps')),
	('Lying Leg Curls', (SELECT id_load_unit FROM load_unit WHERE name = 'kg'), (SELECT id_amount_unit FROM amount_unit WHERE name = 'reps')),
	('Leg Press', (SELECT id_load_unit FROM load_unit WHERE name = 'kg'), (SELECT id_amount_unit FROM amount_unit WHERE name = 'reps')),
	('Seated Calf Raises', (SELECT id_load_unit FROM load_unit WHERE name = 'kg'), (SELECT id_amount_unit FROM amount_unit WHERE name = 'reps')),
	('Leg Extensions', (SELECT id_load_unit FROM load_unit WHERE name = 'kg'), (SELECT id_amount_unit FROM amount_unit WHERE name = 'reps')),
	('Sprint', (SELECT id_load_unit FROM load_unit WHERE name = 'km/h'), (SELECT id_amount_unit FROM amount_unit WHERE name = 'minutes'));

INSERT INTO muscle_in_exercise (id_exercise, id_muscle, muscle_load) VALUES 
  ((SELECT id_exercise FROM exercise WHERE name = 'Push-ups'), (SELECT id_muscle FROM muscle WHERE name = 'Pectoralis Major'), 50),
  ((SELECT id_exercise FROM exercise WHERE name = 'Push-ups'), (SELECT id_muscle FROM muscle WHERE name = 'Rectus Abdominis'), 9),
  ((SELECT id_exercise FROM exercise WHERE name = 'Push-ups'), (SELECT id_muscle FROM muscle WHERE name = 'Triceps Medial Head'), 25),
  ((SELECT id_exercise FROM exercise WHERE name = 'Push-ups'), (SELECT id_muscle FROM muscle WHERE name = 'Deltoid anterior head'), 9),
  ((SELECT id_exercise FROM exercise WHERE name = 'Push-ups'), (SELECT id_muscle FROM muscle WHERE name = 'Biceps Brachii'), 4),

  ((SELECT id_exercise FROM exercise WHERE name = 'Hammer Press'), (SELECT id_muscle FROM muscle WHERE name = 'Pectoralis Major'), 50),
  ((SELECT id_exercise FROM exercise WHERE name = 'Hammer Press'), (SELECT id_muscle FROM muscle WHERE name = 'Triceps Medial Head'), 15),
  ((SELECT id_exercise FROM exercise WHERE name = 'Hammer Press'), (SELECT id_muscle FROM muscle WHERE name = 'Triceps Lateral Head'), 15),
  ((SELECT id_exercise FROM exercise WHERE name = 'Hammer Press'), (SELECT id_muscle FROM muscle WHERE name = 'Triceps Long Head'), 15),

  ((SELECT id_exercise FROM exercise WHERE name = 'Pull-ups'), (SELECT id_muscle FROM muscle WHERE name = 'Rhomboids'), 22),
  ((SELECT id_exercise FROM exercise WHERE name = 'Pull-ups'), (SELECT id_muscle FROM muscle WHERE name = 'Erector Spinae'), 14),
  ((SELECT id_exercise FROM exercise WHERE name = 'Pull-ups'), (SELECT id_muscle FROM muscle WHERE name = 'Latissimus Dorsi'), 55),
  ((SELECT id_exercise FROM exercise WHERE name = 'Pull-ups'), (SELECT id_muscle FROM muscle WHERE name = 'Biceps Brachii'), 7),

  ((SELECT id_exercise FROM exercise WHERE name = 'Straight-Arm Cable Pulldown'), (SELECT id_muscle FROM muscle WHERE name = 'Rhomboids'), 6),
  ((SELECT id_exercise FROM exercise WHERE name = 'Straight-Arm Cable Pulldown'), (SELECT id_muscle FROM muscle WHERE name = 'Trapezius'), 7),
  ((SELECT id_exercise FROM exercise WHERE name = 'Straight-Arm Cable Pulldown'), (SELECT id_muscle FROM muscle WHERE name = 'Erector Spinae'), 23),
  ((SELECT id_exercise FROM exercise WHERE name = 'Straight-Arm Cable Pulldown'), (SELECT id_muscle FROM muscle WHERE name = 'Latissimus Dorsi'), 58),

  ((SELECT id_exercise FROM exercise WHERE name = 'Lat Pulldown'), (SELECT id_muscle FROM muscle WHERE name = 'Rhomboids'), 24),
  ((SELECT id_exercise FROM exercise WHERE name = 'Lat Pulldown'), (SELECT id_muscle FROM muscle WHERE name = 'Erector Spinae'), 18),
  ((SELECT id_exercise FROM exercise WHERE name = 'Lat Pulldown'), (SELECT id_muscle FROM muscle WHERE name = 'Latissimus Dorsi'), 50),
  ((SELECT id_exercise FROM exercise WHERE name = 'Lat Pulldown'), (SELECT id_muscle FROM muscle WHERE name = 'Biceps Brachii'), 7),

  ((SELECT id_exercise FROM exercise WHERE name = 'Bench Press'), (SELECT id_muscle FROM muscle WHERE name = 'Pectoralis Major'), 46),
  ((SELECT id_exercise FROM exercise WHERE name = 'Bench Press'), (SELECT id_muscle FROM muscle WHERE name = 'Pectoralis Minor'), 28),
  ((SELECT id_exercise FROM exercise WHERE name = 'Bench Press'), (SELECT id_muscle FROM muscle WHERE name = 'Triceps Medial Head'), 15),
  ((SELECT id_exercise FROM exercise WHERE name = 'Bench Press'), (SELECT id_muscle FROM muscle WHERE name = 'Deltoid Anterior Head'), 10),
  
  ((SELECT id_exercise FROM exercise WHERE name = 'Incline Dumbbell Fly'), (SELECT id_muscle FROM muscle WHERE name = 'Pectoralis Major'), 35),
  ((SELECT id_exercise FROM exercise WHERE name = 'Incline Dumbbell Fly'), (SELECT id_muscle FROM muscle WHERE name = 'Pectoralis Minor'), 48),
  
  ((SELECT id_exercise FROM exercise WHERE name = 'Dumbbell Fly'), (SELECT id_muscle FROM muscle WHERE name = 'Pectoralis Major'), 50),
  ((SELECT id_exercise FROM exercise WHERE name = 'Dumbbell Fly'), (SELECT id_muscle FROM muscle WHERE name = 'Pectoralis Minor'), 40),

  ((SELECT id_exercise FROM exercise WHERE name = 'Straight Arm Dumbbell Pullovers'), (SELECT id_muscle FROM muscle WHERE name = 'Pectoralis Major'), 45),
  ((SELECT id_exercise FROM exercise WHERE name = 'Straight Arm Dumbbell Pullovers'), (SELECT id_muscle FROM muscle WHERE name = 'Pectoralis Minor'), 45),
  ((SELECT id_exercise FROM exercise WHERE name = 'Straight Arm Dumbbell Pullovers'), (SELECT id_muscle FROM muscle WHERE name = 'Rhomboids'), 8),
  
  ((SELECT id_exercise FROM exercise WHERE name = 'Barbell Squats'), (SELECT id_muscle FROM muscle WHERE name = 'Gluteus'), 31),
  ((SELECT id_exercise FROM exercise WHERE name = 'Barbell Squats'), (SELECT id_muscle FROM muscle WHERE name = 'Quadriceps'), 40),
  ((SELECT id_exercise FROM exercise WHERE name = 'Barbell Squats'), (SELECT id_muscle FROM muscle WHERE name = 'Hamstrings'), 27),
  
  ((SELECT id_exercise FROM exercise WHERE name = 'Deadlift'), (SELECT id_muscle FROM muscle WHERE name = 'Erector Spinae'), 25),
  ((SELECT id_exercise FROM exercise WHERE name = 'Deadlift'), (SELECT id_muscle FROM muscle WHERE name = 'Rhomboids'), 10),
  ((SELECT id_exercise FROM exercise WHERE name = 'Deadlift'), (SELECT id_muscle FROM muscle WHERE name = 'Latissimus Dorsi'), 10),
  ((SELECT id_exercise FROM exercise WHERE name = 'Deadlift'), (SELECT id_muscle FROM muscle WHERE name = 'Gluteus'), 21),
  ((SELECT id_exercise FROM exercise WHERE name = 'Deadlift'), (SELECT id_muscle FROM muscle WHERE name = 'Quadriceps'), 10),
  ((SELECT id_exercise FROM exercise WHERE name = 'Deadlift'), (SELECT id_muscle FROM muscle WHERE name = 'Hamstrings'), 23),
  
  ((SELECT id_exercise FROM exercise WHERE name = 'Overhead Press'), (SELECT id_muscle FROM muscle WHERE name = 'Pectoralis Major'), 15),
  ((SELECT id_exercise FROM exercise WHERE name = 'Overhead Press'), (SELECT id_muscle FROM muscle WHERE name = 'Deltoid Anterior Head'), 40),
  ((SELECT id_exercise FROM exercise WHERE name = 'Overhead Press'), (SELECT id_muscle FROM muscle WHERE name = 'Rhomboids'), 30),
  
  ((SELECT id_exercise FROM exercise WHERE name = 'Dumbbell Step-Up'), (SELECT id_muscle FROM muscle WHERE name = 'Quadriceps'), 40),
  ((SELECT id_exercise FROM exercise WHERE name = 'Dumbbell Step-Up'), (SELECT id_muscle FROM muscle WHERE name = 'Hamstrings'), 25),
  ((SELECT id_exercise FROM exercise WHERE name = 'Dumbbell Step-Up'), (SELECT id_muscle FROM muscle WHERE name = 'Gluteus'), 14),
  ((SELECT id_exercise FROM exercise WHERE name = 'Dumbbell Step-Up'), (SELECT id_muscle FROM muscle WHERE name = 'Gastrocnemius'), 16),
  
  ((SELECT id_exercise FROM exercise WHERE name = 'Jogging'), (SELECT id_muscle FROM muscle WHERE name = 'Quadriceps'), 35),
  ((SELECT id_exercise FROM exercise WHERE name = 'Jogging'), (SELECT id_muscle FROM muscle WHERE name = 'Hamstrings'), 15),
  ((SELECT id_exercise FROM exercise WHERE name = 'Jogging'), (SELECT id_muscle FROM muscle WHERE name = 'Gluteus'), 8),
  ((SELECT id_exercise FROM exercise WHERE name = 'Jogging'), (SELECT id_muscle FROM muscle WHERE name = 'Gastrocnemius'), 35),
  
  ((SELECT id_exercise FROM exercise WHERE name = 'Sprint'), (SELECT id_muscle FROM muscle WHERE name = 'Quadriceps'), 35),
  ((SELECT id_exercise FROM exercise WHERE name = 'Sprint'), (SELECT id_muscle FROM muscle WHERE name = 'Hamstrings'), 25),
  ((SELECT id_exercise FROM exercise WHERE name = 'Sprint'), (SELECT id_muscle FROM muscle WHERE name = 'Gluteus'), 10),
  ((SELECT id_exercise FROM exercise WHERE name = 'Sprint'), (SELECT id_muscle FROM muscle WHERE name = 'Gastrocnemius'), 28),
  
  ((SELECT id_exercise FROM exercise WHERE name = 'Superman Press'), (SELECT id_muscle FROM muscle WHERE name = 'Erector Spinae'), 40),
  ((SELECT id_exercise FROM exercise WHERE name = 'Superman Press'), (SELECT id_muscle FROM muscle WHERE name = 'Rectus Abdominis'), 20),
  ((SELECT id_exercise FROM exercise WHERE name = 'Superman Press'), (SELECT id_muscle FROM muscle WHERE name = 'Rhomboids'), 10),
  ((SELECT id_exercise FROM exercise WHERE name = 'Superman Press'), (SELECT id_muscle FROM muscle WHERE name = 'Trapezius'), 15),
  
  ((SELECT id_exercise FROM exercise WHERE name = 'Two-seated Cable row'), (SELECT id_muscle FROM muscle WHERE name = 'Biceps Brachii'), 15),
  ((SELECT id_exercise FROM exercise WHERE name = 'Two-seated Cable row'), (SELECT id_muscle FROM muscle WHERE name = 'Erector Spinae'), 18),
  ((SELECT id_exercise FROM exercise WHERE name = 'Two-seated Cable row'), (SELECT id_muscle FROM muscle WHERE name = 'Latissimus Dorsi'), 20),
  ((SELECT id_exercise FROM exercise WHERE name = 'Two-seated Cable row'), (SELECT id_muscle FROM muscle WHERE name = 'Rhomboids'), 40),

  ((SELECT id_exercise FROM exercise WHERE name = 'Incline Row'), (SELECT id_muscle FROM muscle WHERE name = 'Biceps Brachii'), 17),
  ((SELECT id_exercise FROM exercise WHERE name = 'Incline Row'), (SELECT id_muscle FROM muscle WHERE name = 'Erector Spinae'), 8),
  ((SELECT id_exercise FROM exercise WHERE name = 'Incline Row'), (SELECT id_muscle FROM muscle WHERE name = 'Latissimus Dorsi'), 12),
  ((SELECT id_exercise FROM exercise WHERE name = 'Incline Row'), (SELECT id_muscle FROM muscle WHERE name = 'Rhomboids'), 46),

  ((SELECT id_exercise FROM exercise WHERE name = 'Shrugs'), (SELECT id_muscle FROM muscle WHERE name = 'Rhomboids'), 12),
  ((SELECT id_exercise FROM exercise WHERE name = 'Shrugs'), (SELECT id_muscle FROM muscle WHERE name = 'Trapezius'), 35),
  ((SELECT id_exercise FROM exercise WHERE name = 'Shrugs'), (SELECT id_muscle FROM muscle WHERE name = 'Omohyoid'), 10),
  ((SELECT id_exercise FROM exercise WHERE name = 'Shrugs'), (SELECT id_muscle FROM muscle WHERE name = 'Deltoid Anterior Head'), 19),
  ((SELECT id_exercise FROM exercise WHERE name = 'Shrugs'), (SELECT id_muscle FROM muscle WHERE name = 'Deltoid Posterior Head'), 8),
  ((SELECT id_exercise FROM exercise WHERE name = 'Shrugs'), (SELECT id_muscle FROM muscle WHERE name = 'Deltoid Middle Head'), 12),
  
  ((SELECT id_exercise FROM exercise WHERE name = 'T-bar'), (SELECT id_muscle FROM muscle WHERE name = 'Rhomboids'), 40),
  ((SELECT id_exercise FROM exercise WHERE name = 'T-bar'), (SELECT id_muscle FROM muscle WHERE name = 'Erector Spinae'), 10),
  ((SELECT id_exercise FROM exercise WHERE name = 'T-bar'), (SELECT id_muscle FROM muscle WHERE name = 'Deltoid Anterior Head'), 16),
  ((SELECT id_exercise FROM exercise WHERE name = 'T-bar'), (SELECT id_muscle FROM muscle WHERE name = 'Deltoid Posterior Head'), 12),
  ((SELECT id_exercise FROM exercise WHERE name = 'T-bar'), (SELECT id_muscle FROM muscle WHERE name = 'Trapezius'), 10),
  ((SELECT id_exercise FROM exercise WHERE name = 'T-bar'), (SELECT id_muscle FROM muscle WHERE name = 'Biceps Brachii'), 8),

  ((SELECT id_exercise FROM exercise WHERE name = 'HyperExtension'), (SELECT id_muscle FROM muscle WHERE name = 'Erector Spinae'), 35),
  ((SELECT id_exercise FROM exercise WHERE name = 'HyperExtension'), (SELECT id_muscle FROM muscle WHERE name = 'Hamstrings'), 25),
  ((SELECT id_exercise FROM exercise WHERE name = 'HyperExtension'), (SELECT id_muscle FROM muscle WHERE name = 'Gastrocnemius'), 10),
  ((SELECT id_exercise FROM exercise WHERE name = 'HyperExtension'), (SELECT id_muscle FROM muscle WHERE name = 'Gluteus'), 29),

  ((SELECT id_exercise FROM exercise WHERE name = 'Hammer Curls'), (SELECT id_muscle FROM muscle WHERE name = 'Biceps brachii'), 39),
  ((SELECT id_exercise FROM exercise WHERE name = 'Hammer Curls'), (SELECT id_muscle FROM muscle WHERE name = 'Brachialis'), 29),
  ((SELECT id_exercise FROM exercise WHERE name = 'Hammer Curls'), (SELECT id_muscle FROM muscle WHERE name = 'Brachioradialis'), 29),

  ((SELECT id_exercise FROM exercise WHERE name = 'Dumbbell Curls'), (SELECT id_muscle FROM muscle WHERE name = 'Biceps brachii'), 60),
  ((SELECT id_exercise FROM exercise WHERE name = 'Dumbbell Curls'), (SELECT id_muscle FROM muscle WHERE name = 'Brachialis'), 20),
  ((SELECT id_exercise FROM exercise WHERE name = 'Dumbbell Curls'), (SELECT id_muscle FROM muscle WHERE name = 'Brachioradialis'), 19),

  ((SELECT id_exercise FROM exercise WHERE name = 'Cable Curls'), (SELECT id_muscle FROM muscle WHERE name = 'Biceps brachii'), 68),
  ((SELECT id_exercise FROM exercise WHERE name = 'Cable Curls'), (SELECT id_muscle FROM muscle WHERE name = 'Brachialis'), 16),
  ((SELECT id_exercise FROM exercise WHERE name = 'Cable Curls'), (SELECT id_muscle FROM muscle WHERE name = 'Brachioradialis'), 11),

  ((SELECT id_exercise FROM exercise WHERE name = 'Concentration Curls'), (SELECT id_muscle FROM muscle WHERE name = 'Biceps brachii'), 72),
  ((SELECT id_exercise FROM exercise WHERE name = 'Concentration Curls'), (SELECT id_muscle FROM muscle WHERE name = 'Brachialis'), 14),
  ((SELECT id_exercise FROM exercise WHERE name = 'Concentration Curls'), (SELECT id_muscle FROM muscle WHERE name = 'Brachioradialis'), 13),
  
  ((SELECT id_exercise FROM exercise WHERE name = 'Preacher Curls'), (SELECT id_muscle FROM muscle WHERE name = 'Biceps brachii'), 90),

  ((SELECT id_exercise FROM exercise WHERE name = 'Reverse Curls'), (SELECT id_muscle FROM muscle WHERE name = 'Biceps brachii'), 50),
  ((SELECT id_exercise FROM exercise WHERE name = 'Reverse Curls'), (SELECT id_muscle FROM muscle WHERE name = 'Brachialis'), 24),
  ((SELECT id_exercise FROM exercise WHERE name = 'Reverse Curls'), (SELECT id_muscle FROM muscle WHERE name = 'Brachioradialis'), 24),

  ((SELECT id_exercise FROM exercise WHERE name = 'Tricep Dips'), (SELECT id_muscle FROM muscle WHERE name = 'Triceps Medial Head'), 45),
  ((SELECT id_exercise FROM exercise WHERE name = 'Tricep Dips'), (SELECT id_muscle FROM muscle WHERE name = 'Pectoralis Major'), 14),
  ((SELECT id_exercise FROM exercise WHERE name = 'Tricep Dips'), (SELECT id_muscle FROM muscle WHERE name = 'Trapezius'), 10),

  ((SELECT id_exercise FROM exercise WHERE name = 'Lying Triceps Extension'), (SELECT id_muscle FROM muscle WHERE name = 'Triceps Medial Head'), 20),
  ((SELECT id_exercise FROM exercise WHERE name = 'Lying Triceps Extension'), (SELECT id_muscle FROM muscle WHERE name = 'Triceps Lateral Head'), 35),
  ((SELECT id_exercise FROM exercise WHERE name = 'Lying Triceps Extension'), (SELECT id_muscle FROM muscle WHERE name = 'Triceps Long Head'), 35),

  ((SELECT id_exercise FROM exercise WHERE name = 'Triceps Cable Extensions'), (SELECT id_muscle FROM muscle WHERE name = 'Triceps Medial Head'), 20),
  ((SELECT id_exercise FROM exercise WHERE name = 'Triceps Cable Extensions'), (SELECT id_muscle FROM muscle WHERE name = 'Triceps Lateral Head'), 35),
  ((SELECT id_exercise FROM exercise WHERE name = 'Triceps Cable Extensions'), (SELECT id_muscle FROM muscle WHERE name = 'Triceps Long Head'), 35),

  ((SELECT id_exercise FROM exercise WHERE name = 'Overhead Cable Extensions'), (SELECT id_muscle FROM muscle WHERE name = 'Triceps Medial Head'), 35),
  ((SELECT id_exercise FROM exercise WHERE name = 'Overhead Cable Extensions'), (SELECT id_muscle FROM muscle WHERE name = 'Triceps Lateral Head'), 25),
  ((SELECT id_exercise FROM exercise WHERE name = 'Overhead Cable Extensions'), (SELECT id_muscle FROM muscle WHERE name = 'Triceps Long Head'), 35),

  ((SELECT id_exercise FROM exercise WHERE name = 'Hanging Knee Raises'), (SELECT id_muscle FROM muscle WHERE name = 'Rectus Abdominis'), 45),
  ((SELECT id_exercise FROM exercise WHERE name = 'Hanging Knee Raises'), (SELECT id_muscle FROM muscle WHERE name = 'Obliques'), 35),
  ((SELECT id_exercise FROM exercise WHERE name = 'Hanging Knee Raises'), (SELECT id_muscle FROM muscle WHERE name = 'Hip Flexors'), 20),

  ((SELECT id_exercise FROM exercise WHERE name = 'Leg Raise'), (SELECT id_muscle FROM muscle WHERE name = 'Rectus Abdominis'), 51),
  ((SELECT id_exercise FROM exercise WHERE name = 'Leg Raise'), (SELECT id_muscle FROM muscle WHERE name = 'Hip Flexors'), 20),
  ((SELECT id_exercise FROM exercise WHERE name = 'Leg Raise'), (SELECT id_muscle FROM muscle WHERE name = 'Hamstrings'), 15),
  ((SELECT id_exercise FROM exercise WHERE name = 'Leg Raise'), (SELECT id_muscle FROM muscle WHERE name = 'Erector Spinae'), 10),

  ((SELECT id_exercise FROM exercise WHERE name = 'Plank'), (SELECT id_muscle FROM muscle WHERE name = 'Rectus Abdominis'), 35),
  ((SELECT id_exercise FROM exercise WHERE name = 'Plank'), (SELECT id_muscle FROM muscle WHERE name = 'Obliques'), 30),
  ((SELECT id_exercise FROM exercise WHERE name = 'Plank'), (SELECT id_muscle FROM muscle WHERE name = 'Deltoid Middle Head'), 12),
  ((SELECT id_exercise FROM exercise WHERE name = 'Plank'), (SELECT id_muscle FROM muscle WHERE name = 'Gluteus'), 10),
  ((SELECT id_exercise FROM exercise WHERE name = 'Plank'), (SELECT id_muscle FROM muscle WHERE name = 'Hamstrings'), 10),

  ((SELECT id_exercise FROM exercise WHERE name = 'Calf Raises'), (SELECT id_muscle FROM muscle WHERE name = 'Gastrocnemius'), 70),
  ((SELECT id_exercise FROM exercise WHERE name = 'Calf Raises'), (SELECT id_muscle FROM muscle WHERE name = 'Soleus'), 28),

  ((SELECT id_exercise FROM exercise WHERE name = 'Seated Calf Raises'), (SELECT id_muscle FROM muscle WHERE name = 'Soleus'), 80),
  ((SELECT id_exercise FROM exercise WHERE name = 'Seated Calf Raises'), (SELECT id_muscle FROM muscle WHERE name = 'Gastrocnemius'), 15),

  ((SELECT id_exercise FROM exercise WHERE name = 'Leg Extensions'), (SELECT id_muscle FROM muscle WHERE name = 'Quadriceps'), 98),
  
  ((SELECT id_exercise FROM exercise WHERE name = 'Lying Leg Curls'), (SELECT id_muscle FROM muscle WHERE name = 'Hamstrings'), 80),
  ((SELECT id_exercise FROM exercise WHERE name = 'Lying Leg Curls'), (SELECT id_muscle FROM muscle WHERE name = 'Gastrocnemius'), 15),

  ((SELECT id_exercise FROM exercise WHERE name = 'Leg Press'), (SELECT id_muscle FROM muscle WHERE name = 'Quadriceps'), 38),
  ((SELECT id_exercise FROM exercise WHERE name = 'Leg Press'), (SELECT id_muscle FROM muscle WHERE name = 'Gluteus'), 25),
  ((SELECT id_exercise FROM exercise WHERE name = 'Leg Press'), (SELECT id_muscle FROM muscle WHERE name = 'Hamstrings'), 25),
  ((SELECT id_exercise FROM exercise WHERE name = 'Leg Press'), (SELECT id_muscle FROM muscle WHERE name = 'Gastrocnemius'), 10),

  ((SELECT id_exercise FROM exercise WHERE name = 'Side Dumbbell Raise'), (SELECT id_muscle FROM muscle WHERE name = 'Deltoid Middle Head'), 72),
  ((SELECT id_exercise FROM exercise WHERE name = 'Side Dumbbell Raise'), (SELECT id_muscle FROM muscle WHERE name = 'Deltoid Anterior Head'), 12),
  ((SELECT id_exercise FROM exercise WHERE name = 'Side Dumbbell Raise'), (SELECT id_muscle FROM muscle WHERE name = 'Deltoid Posterior Head'), 12),
  
  ((SELECT id_exercise FROM exercise WHERE name = 'Dumbbell Front Raise'), (SELECT id_muscle FROM muscle WHERE name = 'Deltoid Middle Head'), 12),
  ((SELECT id_exercise FROM exercise WHERE name = 'Dumbbell Front Raise'), (SELECT id_muscle FROM muscle WHERE name = 'Deltoid Anterior Head'), 72),
  ((SELECT id_exercise FROM exercise WHERE name = 'Dumbbell Front Raise'), (SELECT id_muscle FROM muscle WHERE name = 'Deltoid Posterior Head'), 12),
  
  ((SELECT id_exercise FROM exercise WHERE name = 'Bent-Over Rear Raise'), (SELECT id_muscle FROM muscle WHERE name = 'Deltoid Middle Head'), 15),
  ((SELECT id_exercise FROM exercise WHERE name = 'Bent-Over Rear Raise'), (SELECT id_muscle FROM muscle WHERE name = 'Deltoid Anterior Head'), 12),
  ((SELECT id_exercise FROM exercise WHERE name = 'Bent-Over Rear Raise'), (SELECT id_muscle FROM muscle WHERE name = 'Deltoid Posterior Head'), 60),

  ((SELECT id_exercise FROM exercise WHERE name = 'Single-Arm Upright Row'), (SELECT id_muscle FROM muscle WHERE name = 'Deltoid Middle Head'), 41),
  ((SELECT id_exercise FROM exercise WHERE name = 'Single-Arm Upright Row'), (SELECT id_muscle FROM muscle WHERE name = 'Deltoid Anterior Head'), 43),
  ((SELECT id_exercise FROM exercise WHERE name = 'Single-Arm Upright Row'), (SELECT id_muscle FROM muscle WHERE name = 'Deltoid Posterior Head'), 15);

insert into workout (name) values
  ('Chest and Biceps'),
  ('Back and Triceps'), 
  ('Legs');

insert into workout_template (id_workout, exercise_order, id_exercise, plan_amount, plan_load) values
  (1, 1, (SELECT id_exercise FROM exercise WHERE name = 'Bench Press'), 20, 20),
  (1, 2, (SELECT id_exercise FROM exercise WHERE name = 'Dumbbell Curls'), 15, 5),
  (1, 3, (SELECT id_exercise FROM exercise WHERE name = 'Rest'), 1, 0),
  
  (1, 4, (SELECT id_exercise FROM exercise WHERE name = 'Bench Press'), 10, 40),
  (1, 5, (SELECT id_exercise FROM exercise WHERE name = 'Dumbbell Curls'), 12, 7.5),
  (1, 6, (SELECT id_exercise FROM exercise WHERE name = 'Rest'), 2, 0),

  (1, 7, (SELECT id_exercise FROM exercise WHERE name = 'Bench Press'), 10, 50),
  (1, 8, (SELECT id_exercise FROM exercise WHERE name = 'Dumbbell Curls'), 12, 10),
  (1, 9, (SELECT id_exercise FROM exercise WHERE name = 'Rest'), 3, 0),

  (1, 10, (SELECT id_exercise FROM exercise WHERE name = 'Bench Press'), 10, 55),
  (1, 11, (SELECT id_exercise FROM exercise WHERE name = 'Dumbbell Curls'), 12, 10),
  (1, 12, (SELECT id_exercise FROM exercise WHERE name = 'Rest'), 3, 0),

  (1, 13, (SELECT id_exercise FROM exercise WHERE name = 'Bench Press'), 10, 55),
  (1, 14, (SELECT id_exercise FROM exercise WHERE name = 'Dumbbell Curls'), 12, 10),
  (1, 15, (SELECT id_exercise FROM exercise WHERE name = 'Rest'), 3, 0),

  (1, 16, (SELECT id_exercise FROM exercise WHERE name = 'Hammer Press'), 12, 10),
  (1, 17, (SELECT id_exercise FROM exercise WHERE name = 'Hammer Curls'), 12, 10),
  (1, 18, (SELECT id_exercise FROM exercise WHERE name = 'Rest'), 2.5, 0),

  (1, 19, (SELECT id_exercise FROM exercise WHERE name = 'Hammer Press'), 12, 12.5),
  (1, 20, (SELECT id_exercise FROM exercise WHERE name = 'Hammer Curls'), 12, 10),
  (1, 21, (SELECT id_exercise FROM exercise WHERE name = 'Rest'), 2.5, 0),

  (1, 22, (SELECT id_exercise FROM exercise WHERE name = 'Hammer Press'), 12, 12.5),
  (1, 23, (SELECT id_exercise FROM exercise WHERE name = 'Hammer Curls'), 12, 10),
  (1, 24, (SELECT id_exercise FROM exercise WHERE name = 'Rest'), 2.5, 0),

  (1, 25, (SELECT id_exercise FROM exercise WHERE name = 'Pec Fly'), 12, 18),
  (1, 26, (SELECT id_exercise FROM exercise WHERE name = 'Push-ups'), 15, 0),
  (1, 27, (SELECT id_exercise FROM exercise WHERE name = 'Rest'), 2.5, 0),

  (1, 28, (SELECT id_exercise FROM exercise WHERE name = 'Pec Fly'), 12, 18),
  (1, 29, (SELECT id_exercise FROM exercise WHERE name = 'Push-ups'), 15, 0),
  (1, 30, (SELECT id_exercise FROM exercise WHERE name = 'Rest'), 2.5, 0),

  (1, 31, (SELECT id_exercise FROM exercise WHERE name = 'Pec Fly'), 12, 18),
  (1, 32, (SELECT id_exercise FROM exercise WHERE name = 'Push-ups'), 15, 0);

insert into workout_template (id_workout, exercise_order, id_exercise, plan_amount, plan_load) values
  (2, 1, (SELECT id_exercise FROM exercise WHERE name = 'Pull-Ups'), 15, 0),
  (2, 2, (SELECT id_exercise FROM exercise WHERE name = 'Triceps Cable Extensions'), 15, 25),
  (2, 3, (SELECT id_exercise FROM exercise WHERE name = 'Hanging Knee Raises'), 15, 0),
  (2, 4, (SELECT id_exercise FROM exercise WHERE name = 'Rest'), 2, 0),

  (2, 5, (SELECT id_exercise FROM exercise WHERE name = 'Pull-Ups'), 15, 0),
  (2, 6, (SELECT id_exercise FROM exercise WHERE name = 'Triceps Cable Extensions'), 12, 35),
  (2, 7, (SELECT id_exercise FROM exercise WHERE name = 'Hanging Knee Raises'), 15, 0),
  (2, 8, (SELECT id_exercise FROM exercise WHERE name = 'Rest'), 2, 0),

  (2, 9, (SELECT id_exercise FROM exercise WHERE name = 'Pull-Ups'), 15, 0),
  (2, 10, (SELECT id_exercise FROM exercise WHERE name = 'Triceps Cable Extensions'), 12, 35),
  (2, 11, (SELECT id_exercise FROM exercise WHERE name = 'Hanging Knee Raises'), 15, 0),
  (2, 12, (SELECT id_exercise FROM exercise WHERE name = 'Rest'), 2, 0),

  (2, 13, (SELECT id_exercise FROM exercise WHERE name = 'Pull-Ups'), 15, 0),
  (2, 14, (SELECT id_exercise FROM exercise WHERE name = 'Triceps Cable Extensions'), 12, 35),  
  (2, 15, (SELECT id_exercise FROM exercise WHERE name = 'Hanging Knee Raises'), 15, 0),
  (2, 16, (SELECT id_exercise FROM exercise WHERE name = 'Rest'), 2, 0),

  (2, 17, (SELECT id_exercise FROM exercise WHERE name = 'Incline Row'), 12, 12.5),
  (2, 18, (SELECT id_exercise FROM exercise WHERE name = 'Overhead Cable Extensions'), 12, 30),
  (2, 19, (SELECT id_exercise FROM exercise WHERE name = 'Superman Press'), 15, 0),
  (2, 20, (SELECT id_exercise FROM exercise WHERE name = 'Rest'), 2.5, 0),

  (2, 21, (SELECT id_exercise FROM exercise WHERE name = 'Incline Row'), 12, 12.5),
  (2, 22, (SELECT id_exercise FROM exercise WHERE name = 'Overhead Cable Extensions'), 12, 30),
  (2, 23, (SELECT id_exercise FROM exercise WHERE name = 'Superman Press'), 15, 0),
  (2, 24, (SELECT id_exercise FROM exercise WHERE name = 'Rest'), 2.5, 0),

  (2, 25, (SELECT id_exercise FROM exercise WHERE name = 'Incline Row'), 12, 12.5),
  (2, 26, (SELECT id_exercise FROM exercise WHERE name = 'Overhead Cable Extensions'), 12, 30),
  (2, 27, (SELECT id_exercise FROM exercise WHERE name = 'Superman Press'), 15, 0),
  (2, 28, (SELECT id_exercise FROM exercise WHERE name = 'Rest'), 2.5, 0),

  (2, 29, (SELECT id_exercise FROM exercise WHERE name = 'Lat Pulldown'), 12, 48),
  (2, 30, (SELECT id_exercise FROM exercise WHERE name = 'Tricep Dips'), 15, 0),
  (2, 31, (SELECT id_exercise FROM exercise WHERE name = 'Rest'), 2.5, 0),

  (2, 32, (SELECT id_exercise FROM exercise WHERE name = 'Lat Pulldown'), 10, 54),
  (2, 33, (SELECT id_exercise FROM exercise WHERE name = 'Tricep Dips'), 15, 0),
  (2, 34, (SELECT id_exercise FROM exercise WHERE name = 'Rest'), 2.5, 0),

  (2, 35, (SELECT id_exercise FROM exercise WHERE name = 'Lat Pulldown'), 12, 48),
  (2, 36, (SELECT id_exercise FROM exercise WHERE name = 'Tricep Dips'), 15, 0);

insert into workout_journal (date_time, id_workout, exercise_order, real_amount, real_load, complete) values
  ('2024-05-11 11:00:00', 1, 1, 20, 20, 1),
  ('2024-05-11 11:00:00', 1, 2, 15, 5, 1),
  ('2024-05-11 11:00:00', 1, 3, 1, 0, 1),

  ('2024-05-11 11:00:00', 1, 4, 10, 40, 1),
  ('2024-05-11 11:00:00', 1, 5, 12, 7.5, 1),
  ('2024-05-11 11:00:00', 1, 6, 2, 0, 1),

  ('2024-05-11 11:00:00', 1, 7, 10, 50, 1),
  ('2024-05-11 11:00:00', 1, 8, 12, 10, 1),
  ('2024-05-11 11:00:00', 1, 9, 3, 0, 1),

  ('2024-05-11 11:00:00', 1, 10, 10, 55, 1),
  ('2024-05-11 11:00:00', 1, 11, 12, 10, 1),
  ('2024-05-11 11:00:00', 1, 12, 3, 0, 1),

  ('2024-05-11 11:00:00', 1, 13, 10, 55, 1),
  ('2024-05-11 11:00:00', 1, 14, 12, 10, 1),
  ('2024-05-11 11:00:00', 1, 15, 3, 0, 1),

  ('2024-05-11 11:00:00', 1, 16, 12, 10, 1),
  ('2024-05-11 11:00:00', 1, 17, 12, 10, 1),
  ('2024-05-11 11:00:00', 1, 18, 2.5, 0, 1),

  ('2024-05-11 11:00:00', 1, 19, 12, 12.5, 1),
  ('2024-05-11 11:00:00', 1, 20, 12, 10, 1),
  ('2024-05-11 11:00:00', 1, 21, 2.5, 0, 1),

  ('2024-05-11 11:00:00', 1, 22, 12, 12.5, 1),
  ('2024-05-11 11:00:00', 1, 23, 12, 10, 1),
  ('2024-05-11 11:00:00', 1, 24, 2.5, 0, 1),

  ('2024-05-11 11:00:00', 1, 25, 12, 18, 1),
  ('2024-05-11 11:00:00', 1, 26, 15, 0, 1),
  ('2024-05-11 11:00:00', 1, 27, 2.5, 0, 1),

  ('2024-05-11 11:00:00', 1, 28, 12, 18, 1),
  ('2024-05-11 11:00:00', 1, 29, 15, 0, 1),
  ('2024-05-11 11:00:00', 1, 30, 2.5, 0, 1),

  ('2024-05-11 11:00:00', 1, 31, 12, 18, 1),
  ('2024-05-11 11:00:00', 1, 32, 15, 0, 1);
