-- получаем список упражнений по группе мышц
SELECT e.name as exercise, m.name, muscle_load 
from muscle_in_exercise mie 
join muscle m using(id_muscle)
join muscle_group mg using(id_muscle_group)
join exercise e using(id_exercise)
order by e.name, muscle_load DESC;

-- получаем все запланированные на неделю вперед тренировки
select wj.date_time `date and time`, w.name `workout`, wj.exercise_order `order` , e.name `exercise`,
	wt.plan_load `planned load`, lu.name  `load unit`,
	wt.plan_amount  `planned amount`, au.name `amount unit`, wj.complete `completion`
from workout_template wt
join workout_journal wj 
	on wt.id_workout = wj.id_workout and wt.exercise_order = wj.exercise_order 
join workout w 
	on w.id_workout = wj.id_workout
join exercise e 
	on e.id_exercise = wt.id_exercise 
join load_unit lu using(id_load_unit)
join amount_unit au using(id_amount_unit)
where CURDATE() <= date(date_time)  and date(date_time) < DATE_ADD(CURDATE(), INTERVAL 1 WEEK)
order by date_time, wj.exercise_order;

-- выводит все группы мышц и мышцы относящиеся к ним
select mg.name `muscle goup`, m.name `muscle`
from muscle m 
join muscle_group mg using(id_muscle_group)
order by mg.name;

-- выводит все группы мышц
SELECT name from muscle_group;
-- выводит все тренировки
SELECT name from workout;

