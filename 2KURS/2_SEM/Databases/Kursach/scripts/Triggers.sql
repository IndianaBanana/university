delimiter //
CREATE TRIGGER if not exists is_leq_100_in_muscle_load
after INSERT ON muscle_in_exercise
FOR EACH ROW
BEGIN
    select sum(muscle_load) into @summa from muscle_in_exercise where id_exercise = NEW.id_exercise;
    if @summa is null then
        set @summa = 0;
    end if;
    IF ((@summa) > 100) THEN
        SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Muscle load must be less than or equal to 100';
    END IF;
END//

create trigger if not exists is_geq_0_in_muscle_load
before INSERT ON muscle_in_exercise
FOR EACH ROW
BEGIN
    IF ((NEW.muscle_load) < 0) THEN
        SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Muscle load must be greater than or equal to 0';
    END IF;
END//

create trigger if not exists is_geq_0_in_plan_load_and_plan_amount
before INSERT ON workout_template
FOR EACH ROW
BEGIN
    IF ((NEW.plan_load) < 0 or (NEW.plan_amount) < 0) THEN
        SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Plan load and plan amount must be greater than or equal to 0';
    END IF;
END//

create trigger if not exists is_geq_0_in_real_load_and_real_amount
before UPDATE ON workout_journal
FOR EACH ROW
BEGIN
    IF ((NEW.real_load) < 0 or (NEW.real_amount) < 0) THEN
        SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Real load and real amount must be greater than or equal to 0';
    END IF;
END//

create trigger if not exists is_date_time_unique
before INSERT ON workout_journal
FOR EACH ROW
BEGIN
    select id_workout into @id_workout from workout_journal where date_time = NEW.date_time;
    if @id_workout is not null then
        if NEW.id_workout != @id_workout then
            SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Date and time must be unique for each individual workout';
        end if;
    end if;
END//

