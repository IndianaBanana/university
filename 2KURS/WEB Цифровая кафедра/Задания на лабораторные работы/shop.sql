CREATE DATABASE IF NOT EXISTS shop;

USE shop;

CREATE TABLE IF NOT EXISTS category (
	id_category INT NOT NULL AUTO_INCREMENT,
    name VARCHAR(150) NOT NULL,
    CONSTRAINT pk_category PRIMARY KEY (id_category),
    CONSTRAINT unique_name UNIQUE (name)
);

CREATE TABLE IF NOT EXISTS tovar (
	artikul int NOT NULL,
    name VARCHAR(200) NOT NULL,
    proizv VARCHAR(100) NOT NULL,
    country VARCHAR(100) NOT NULL,
    cost FLOAT NOT NULL,
    ost INT NOT NULL,
    id_category INT NOT NULL,
    CONSTRAINT pk_tovar PRIMARY KEY (artikul),
    CONSTRAINT fk_tovar_category FOREIGN KEY (id_category)
		REFERENCES category (id_category)
        ON DELETE CASCADE
        ON UPDATE CASCADE
);

CREATE TABLE IF NOT EXISTS status (
	id_status INT NOT NULL AUTO_INCREMENT,
    name VARCHAR(45) NOT NULL,
    CONSTRAINT pk_status PRIMARY KEY (id_status)
);

CREATE TABLE IF NOT EXISTS client (
	id_client INT NOT NULL AUTO_INCREMENT,
    surname VARCHAR(45) NOT NULL,
    name VARCHAR(45) NOT NULL,
    otch VARCHAR(45) NOT NULL,
    phone VARCHAR(13) NOT NULL,
    email VARCHAR(100),
    CONSTRAINT pk_client PRIMARY KEY (id_client)
);

CREATE TABLE IF NOT EXISTS orders (
	id_order INT NOT NULL AUTO_INCREMENT,
    ord_date DATETIME NOT NULL,
    id_client INT NOT NULL,
    id_status INT NOT NULL,
    CONSTRAINT pk_orders PRIMARY KEY (id_order),
    CONSTRAINT fk_orders_client FOREIGN KEY (id_client)
		REFERENCES client (id_client)
        ON DELETE CASCADE
        ON UPDATE CASCADE,
	CONSTRAINT fk_orders_status FOREIGN KEY (id_status)
		REFERENCES status (id_status)
        ON DELETE CASCADE
        ON UPDATE CASCADE
);

CREATE TABLE IF NOT EXISTS order_tovar (
	artikul int NOT NULL,
    id_order INT NOT NULL,
    kolvo INT,
    CONSTRAINT pk_order_tovar PRIMARY KEY (artikul, id_order),
    CONSTRAINT fk_order_tovar_tovar FOREIGN KEY (id_order)
		REFERENCES orders (id_order)
        ON DELETE CASCADE
        ON UPDATE CASCADE,
	CONSTRAINT fk_order_tovar_order FOREIGN KEY (artikul)
		REFERENCES tovar (artikul)
        ON DELETE CASCADE
        ON UPDATE CASCADE
);


