
use shop;

select * from client;

select * from tovar;

select * from orders;

select * from status;

select * from order_tovar;

select * from category;


select * from tovar where tovar.cost <= 36;

select * from tovar where tovar.cost > 500;

select * from tovar where tovar.ost < 5;

select * from tovar where tovar.cost > 100 and tovar.ost < 10;

select * from tovar where tovar.cost < 50 and tovar.ost is not null;

select * from tovar where tovar.proizv = 'BRAUBERG' and cost >= 15 and cost <50;

update client set phone = "1111111" where id_client = 1;

update orders set id_status = 3;

update tovar set ost = 12 where cost < 15;

update tovar set ost = 2 where cost > 500;

update order_tovar 
set kolvo = kolvo + 2 
where artikul in (select artikul from tovar where tovar.proizv = "STAFF");

update tovar 
set ost = ost - (select kolvo from order_tovar  where id_order = 1001) 
where artikul = 143562;

DELETE FROM tovar
WHERE proizv = 'STAFF';

delete from tovar where proizv = "BRAUBERG" and cost > 500;

delete from order_tovar;

delete from orders where id_status in (select status.id_status from status where status.name="Выдан");

insert into orders (id_order, ord_date, id_client, id_status)
values (1006, '2022-12-15', 
(select id_client from client where client.surname = "Логвинов"), 
(select id_status from status where name = "Заказан"));


insert into tovar values
(222222, "bebebebe", "hehehehe", "veveveve", 34242, 234324, 
(select id_category from category where name = "Канцелярские мелочи"));

select * from client c where surname like "И%";

select * from client c where surname like "%ОВ";

select * from client c where phone is null;

select * from client c where phone is not null;

select * from tovar order by cost desc ;

select * from orders where ord_date between '2021-10-19' and '2021-10-22';

select client.surname, client.name, 
tovar.name, sum(tovar.cost * order_tovar.kolvo), 
orders.ord_date, 
status.name, 
order_tovar.kolvo
from tovar
join order_tovar using(artikul)
join orders using(id_order)
join client using(id_client)
join status using(id_status)
group by id_order ;








