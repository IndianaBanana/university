use журнал;

-- Фамилия и инициалы студентов
select concat(
		substring_index(фио, ' ', 1), ' ', 
		substring(фио, locate(' ', фио)+1, 1), '. ', 
		substring(фио, locate(' ', фио, locate(' ', фио)+1)+1, 1), '.'
		) as фио
from студент;

-- Выводит первый день студента в универе
select фио, concat_ws('-', год_поступления, '09', '01') as первый_день_в_универе
from студент;

-- Возраст студента
select фио, (year(current_date()) - год_рождения) as примерный_год_рождения
from студент;

-- Выводит семестр на котором обучается группа
select
    название,
    case
        when month(now()) < 9 then floor((year(now()) - год) * 2)
        else floor((year(now()) - год) * 2 + 1)
    end as семестр
from
    группа;
-- какую оценку поставить студенту не 22 ПГ
   select lower(название) as группа, фио, ceiling(rand()*4 + 1) as оценка
   from студент
   join группа
   on студент.группа = группа.id_группа
   where группа.название != "22ПГ"
   order by название, оценка;
  
-- нашествие ПИц
   select concat(название, "ц") группа
   from группа
   where название not like "%ПИц";
   
   