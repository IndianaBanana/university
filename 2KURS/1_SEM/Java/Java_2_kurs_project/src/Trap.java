import java.awt.*;

public class Trap extends Entity {
    private static final String IMAGE_NAME = "trap00";

    private final int damage;
    private int count = 0;
    private int num = 0;

    public Trap(int x, int y) {
        super(x, y);
        damage = (int)(Math.random() * 25) + 30;
        setSize(damage);
        setImageName(IMAGE_NAME + "0.png");
    }

    public int getDamage() {
        return damage;
    }

    @Override
    public void update() {
        if (count++ >= 13){
            count = 0;
            setImageName(IMAGE_NAME + (num++) + ".png");
            if (num > 2) num = 0;
        }
    }

    @Override
    public void draw(Graphics g) {
        g.drawImage(getImage(), (int) getX(),(int) getY(), getSize(), getSize(), null);
    }
}