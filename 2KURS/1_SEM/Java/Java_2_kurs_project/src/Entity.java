import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;


public abstract class Entity {

    private double x;
    private double y;

    private double centerX;
    private  double speed;
    private double centerY;

    private BufferedImage image = null;
    private int size = 10;

    public void setSize(int size) {
        this.size = size;
    }
    public int getSize() {
        return size;
    }

    public void setCenterX(double centerX) {
        this.centerX = centerX;
    }

    public void setCenterY(double centerY) {
        this.centerY = centerY;
    }

    public double getCenterX() {
        return centerX;
    }

    public double getCenterY() {
        return centerY;
    }

    public void setX(double x) {
        this.x = x;
    }

    public void setY(double y) {
        this.y = y;
    }

    public void setImageName(String imageName) {
        try {
            image = ImageIO.read(new File("Assets", imageName));
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }
    public BufferedImage getImage() {
        return image;
    }

    public void addX(double xCord) {
        x += xCord;
    }
    public double getX() {
        return x;
    }

    public void addY(double yCord) {
        y += yCord;
    }
    public double getY() {
        return y;
    }

    public Entity(int x, int y, int size) {
        this.x = x;
        this.y = y;
        this.size = size;
    }
    public Entity(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public abstract void draw(Graphics g);
    public abstract void update();

    public double getSpeed() {
        return speed;
    }

    public void setSpeed(double speed) {
        this.speed = speed;
    }
}
