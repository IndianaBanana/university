import javax.imageio.ImageIO;
import javax.sound.sampled.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class World extends Component {
    public static final int THREAD_SLEEP = 25;
    private static final File audioFile = new File("Assets/i_love_p++++.wav");

    private final List<Entity> entityList = new ArrayList<>();
    private final List<Trap> trapList = new ArrayList<>();
    private final List<PlayerBuff> playerBuffList = new ArrayList<>();
    private final Player player;

    private boolean pleaseStop = true;
    private PlayerMovementController movementController;
    private EntityFactory entityFactory;
    private BufferedImage imageOfGround;

    public World() {
        try {
            imageOfGround = ImageIO.read(new File("Assets", "Ground4.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }

        player = new Player(650, 640, 15);
    }
    public void start() {
        pleaseStop = false;
        playMusic();

        movementController = new PlayerMovementController(player, getWidth(), getHeight());
        addKeyListener(movementController);

        entityFactory = new EntityFactory(player, getWidth(), getHeight());
        entityList.add(player);
        entityList.add(new Enemy(200, (int) (player.getY() - Math.random() * 700 - 300), 30, player));

        while (!pleaseStop) {
            try {
                Thread.sleep(THREAD_SLEEP);
                update();

            } catch (InterruptedException e) {
                System.out.println("EXCEPTION IN THREAD");
            }
        }
        System.out.println("END");
    }

    private void update() {
        List<Entity> entities = entityFactory.getEntities();
        if (entities != null) {
            for (Entity entity : entities) {
                if (entity instanceof Enemy) entityList.add(entity);
                if (entity instanceof Trap) trapList.add((Trap) entity);
                if (entity instanceof PlayerBuff) playerBuffList.add((PlayerBuff) entity);
            }

        }
        for (Trap traps : trapList) traps.update();
        for (PlayerBuff buff : playerBuffList){
            buff.update();
            if (!buff.isStillThere()) {
                playerBuffList.remove(buff);
                return;
            }
        }

        if (!player.isTimeStopped()) for (int index = 1; index < entityList.size(); index++) entityList.get(index).update();
        movementController.update();
        player.update();

        repaint();
        isDead();
        isEnemyInEnemy();
        isEnemyInTrap();
        isPlayerBuffed();
    }

    @Override
    public void paint(Graphics g) {
        if (pleaseStop) {
            gameOver(g);
            return;
        }
        super.paint(g);

        int x = (getWidth() - imageOfGround.getWidth()) / 2;
        int y = (getHeight() - imageOfGround.getHeight()) / 2;
        g.drawImage(imageOfGround, x, y, this);

        for (Entity entity : trapList) entity.draw(g);
        for (Entity entity : playerBuffList) entity.draw(g);
        for (Entity entity : entityList) entity.draw(g);
    }

    private void isDead() {
        if (entityList.size() > 1) {
            double playerX = player.getX();
            double playerY = player.getY();
            double playerSize = player.getSize();

            for (int i = 1; i < (entityList.size()); i++) {
                double enemyX = entityList.get(i).getX();
                double enemyY = entityList.get(i).getY();
                double enemySize = entityList.get(i).getSize();
                if (collision(enemyX, enemyY, enemySize, playerX, playerY, playerSize)) {
                    pleaseStop = true;
                    repaint();
                    return;
                }
            }
        }
    }

    private void isPlayerBuffed(){
        if (playerBuffList.isEmpty()) return;
        double playerX = player.getX();
        double playerY = player.getY();
        double playerSize = player.getSize();

        for (PlayerBuff buff : playerBuffList){
            TypeOfPlayerBuff typeOfPlayerBuff = buff.getTypeOfPlayerBuff();
            double buffX = buff.getX();
            double buffY = buff.getY();
            double buffSize = buff.getSize();
            if (collision(buffX, buffY, buffSize, playerX, playerY, playerSize)) {
                switch (typeOfPlayerBuff){
                    case TIME_STOP -> {
                        playerBuffList.remove(buff);
                        player.setIsTimeStopped(true);
                        return;
                    }
                    case KILL_ALL_ENEMIES -> {
                        playerBuffList.remove(buff);
                        entityList.clear();
                        trapList.clear();
                        entityList.add(player);
                        return;
                    }
                }

            }
        }
    }

    private void isEnemyInEnemy() {
        if (entityList.size() >= 3) {
            for (int i = 1; i < entityList.size(); i++) {
                double enemyX = entityList.get(i).getX();
                double enemyY = entityList.get(i).getY();
                int enemySize = entityList.get(i).getSize();
                for (int j = i + 1; j < entityList.size(); j++) {
                    double enemy2X = entityList.get(j).getX();
                    double enemy2Y = entityList.get(j).getY();
                    int enemy2Size = entityList.get(j).getSize();
                    if (collision(enemyX, enemyY, enemySize, enemy2X, enemy2Y, enemy2Size)) {
                        double newX = (entityList.get(i).getX() + entityList.get(j).getX())/2;
                        double newY = (entityList.get(i).getY() + entityList.get(j).getY())/2;
                        if (Math.random() <= 0.5) {
                            entityList.remove(j);
                            entityList.get(i).setSize(enemy2Size + enemySize - (int) (Math.random() * 5) - 10);
                            entityList.get(i).setX(newX);
                            entityList.get(i).setY(newY);
                            return;
                        }
                        entityList.remove(i);
                        entityList.get(j - 1).setSize(enemy2Size + enemySize - (int) (Math.random() * 5) - 10);
                        entityList.get(j - 1).setX(newX);
                        entityList.get(j - 1).setY(newY);
                        return;
                    }
                }
            }
        }
    }

    private void isEnemyInTrap() {
        int entityListSize = entityList.size();
        int trapListSize = trapList.size();
        if (trapListSize > 0 && entityListSize > 1) {
            for (int i = 1; i < entityListSize; i++) {
                double enemyX = entityList.get(i).getX();
                double enemyY = entityList.get(i).getY();
                int enemySize = entityList.get(i).getSize();
                for (Trap trap : trapList) {
                    double trapX = trap.getX();
                    double trapY = trap.getY();
                    int trapSize = trap.getSize();
                    if (collision(enemyX, enemyY, enemySize, trapX, trapY, trapSize)) {
                        enemySize = enemySize - trap.getDamage();
                        if (enemySize < 15) {
                            entityList.remove(i);
                            trapList.remove(trap);
                            return;
                        }
                        entityList.get(i).setSize(enemySize);
                        trapList.remove(trap);
                        return;
                    }
                }
            }
        }
    }


    private void gameOver(Graphics g) {
        Font myFont = new Font("Serif", Font.BOLD, 150);
        FontMetrics fontMetrics = g.getFontMetrics(myFont);
        int textWidth = fontMetrics.stringWidth("Overlooked");
        g.setColor(Color.RED);
        g.setFont(myFont);
        g.drawString("Overlooked", (getWidth() - textWidth) / 2, getHeight() / 2);
    }
    private static boolean collision(double x1, double y1, double size1, double x2, double y2, double size2) {
        return x2 + size2 > x1 && x2 < x1 + size1 && y2 + size2 > y1 && y2 < y1 + size1;
    }
    private static void playMusic() {
        try {
            AudioInputStream audioStream = AudioSystem.getAudioInputStream(audioFile);

            Clip clip = AudioSystem.getClip();
            clip.open(audioStream);
            FloatControl gainControl = (FloatControl) clip.getControl(FloatControl.Type.MASTER_GAIN);
            gainControl.setValue(20f * (float) Math.log10(0.2));

            clip.addLineListener(event -> {
                if (event.getType() == LineEvent.Type.STOP) {
                    clip.close();
                    playMusic();
                }
            });

            clip.start();

        } catch (LineUnavailableException | UnsupportedAudioFileException | IOException e) {
            e.printStackTrace();
        }
    }


}
