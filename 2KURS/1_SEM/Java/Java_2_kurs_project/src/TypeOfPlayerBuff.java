public enum TypeOfPlayerBuff {
    KILL_ALL_ENEMIES,
    TIME_STOP;

    public static TypeOfPlayerBuff getRandomValue() {
        if(Math.random() < 0.3) return KILL_ALL_ENEMIES;
        return TIME_STOP;
    }
}
