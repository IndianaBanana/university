import java.awt.*;

public class Enemy extends Entity {
    private static final String IMAGE_NAME = "enemyEye";

    private final Player player;
    private int num = 1;
    private int count = 0;
    private final int red;
    private final int green;
    private final int blue;



    public Enemy(int x, int y, int size, Player player) {
        super(x, y, size);
        this.player = player;
        setSpeed((Math.random()*2.8+3.3)  / 10);
        red = (int) (Math.random() * 250);
        green = (int) (Math.random() * 250);
        blue = (int) (Math.random() * 250);
        setImageName(IMAGE_NAME + "1.png");
    }

    @Override
    public void draw(Graphics g) {
        g.setColor(new Color(red, green, blue));
        g.fillOval((int) getX(), (int) getY(),getSize(),getSize());
        g.setColor(Color.BLACK);
        g.drawOval((int) getX(),(int) getY(),getSize(),getSize());
        g.drawImage(getImage(),(int)(getCenterX()-getSize()*0.2), (int)(getCenterY() - getSize()*0.1),
                (int)(getImage().getWidth()*getSize()*0.04),
                (int)(getImage().getHeight()*getSize() * 0.04), null);

    }

    @Override
    public void update() {
        if ((count += (int) (Math.random() * 2) + 1) >= 100){
            count = 0;
            setImageName(IMAGE_NAME + (num++) + ".png");
            if (num == 4) num = 1;
        }
        setCenterX(getX() + (double) getSize() /2);
        setCenterY(getY() + (double) getSize() /2);
        double dx = player.getCenterX() - getCenterX();
        double dy = player.getCenterY() - getCenterY();
        double length = (Math.sqrt(dx * dx + dy * dy))*getSpeed();
        dx = (dx / length);
        dy = (dy / length);
        addX(dx);
        addY(dy);
    }
}
