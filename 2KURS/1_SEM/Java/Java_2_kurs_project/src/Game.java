import javax.swing.*;
import java.awt.*;


public class Game {
    public static void main(String[] args) {
        World world = new World();
        JFrame frame = new JFrame("Tangents");
        frame.setExtendedState(JFrame.MAXIMIZED_BOTH);


        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.add(world);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
        world.setFocusable(true);
        Container contentPane = frame.getContentPane();
        contentPane.setBackground(Color.darkGray);

        world.start();


    }
}