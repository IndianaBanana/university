#include <windows.h>
#include <iostream>
#include <string>
#include <algorithm>

HANDLE hDrawThread, hRunningStringThread, hPowerDrawThread, hPowerRunningStringThread;
bool thread1Paused = false;
bool thread2Paused = false;
HWND hwnd;
int x = 0;

LRESULT CALLBACK WindowProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam);
DWORD WINAPI drawThread(LPVOID lpParam);
DWORD WINAPI runningStringThread(LPVOID lpParam);
DWORD WINAPI powerDrawThread(LPVOID lpParam);
DWORD WINAPI powerRunningStringThread(LPVOID lpParam);

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
    WNDCLASS wc = {0};
    wc.lpfnWndProc = WindowProc;
    wc.hInstance = hInstance;
    wc.lpszClassName = "MyClass";
    RegisterClass(&wc);

    hwnd = CreateWindow("MyClass", "Multithreaded App", WS_OVERLAPPEDWINDOW, 100, 100, 1280, 500, NULL, NULL, hInstance, NULL);
    ShowWindow(hwnd, nCmdShow);

    MSG msg;
    while (GetMessage(&msg, NULL, 0, 0))
    {
        TranslateMessage(&msg);
        DispatchMessage(&msg);
    }

    return 0;
}

LRESULT CALLBACK WindowProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
    switch (uMsg)
    {
    case WM_CREATE:
        CreateWindow("BUTTON", "Thread 1", WS_VISIBLE | WS_CHILD | BS_DEFPUSHBUTTON, 10, 10, 200, 30, hwnd, (HMENU)1, NULL, NULL);
        CreateWindow("BUTTON", "Thread 2", WS_VISIBLE | WS_CHILD | BS_DEFPUSHBUTTON, 220, 10, 200, 30, hwnd, (HMENU)2, NULL, NULL);

        hDrawThread = CreateThread(NULL, 0, drawThread, NULL, 0, NULL);
        hRunningStringThread = CreateThread(NULL, 0, runningStringThread, NULL, 0, NULL);
        hPowerDrawThread = CreateThread(NULL, 0, powerDrawThread, NULL, 0, NULL);
        hPowerRunningStringThread = CreateThread(NULL, 0, powerRunningStringThread, NULL, 0, NULL);
        break;

    case WM_COMMAND:
        switch (LOWORD(wParam))
        {
        case 1:
            thread1Paused = !thread1Paused;
            break;

        case 2:
            thread2Paused = !thread2Paused;
            break;
        }
        break;

    case WM_DESTROY:
        TerminateThread(hDrawThread, 0);
        TerminateThread(hRunningStringThread, 0);
        CloseHandle(hDrawThread);
        CloseHandle(hRunningStringThread);
        PostQuitMessage(0);
        break;

    default:
        return DefWindowProc(hwnd, uMsg, wParam, lParam);
    }

    return 0;
}

DWORD WINAPI powerDrawThread(LPVOID lpParam) {
    while (true) {
        if (thread1Paused == false) ResumeThread(hDrawThread);
        else SuspendThread(hDrawThread);
    }
}

DWORD WINAPI powerRunningStringThread(LPVOID lpParam) {
    while (true) {
        if (thread2Paused == false) ResumeThread(hRunningStringThread);
        else SuspendThread(hRunningStringThread);
    }
}

DWORD WINAPI drawThread(LPVOID lpParam)
{
    HANDLE mutex = (HANDLE)lpParam;
    while (true)
    {
        WaitForSingleObject(mutex, INFINITE);
        HPEN hPen = CreatePen(PS_SOLID, 1, RGB(rand()%255, rand()%255, rand()%255));
        HDC hdc = GetDC(hwnd);
        SelectObject(hdc, hPen);
        MoveToEx(hdc, x, 50, NULL);
        LineTo(hdc, x++, rand()%620+100);
        ReleaseDC(hwnd, hdc);
        Sleep(100);

    }
    return 0;
}

DWORD WINAPI runningStringThread(LPVOID lpParam)
{
    std::string str = "     I`m string, i`m runnig     ";
    HANDLE mutex = (HANDLE)lpParam;
    while (true)
    {
        WaitForSingleObject(mutex, INFINITE);
        rotate(str.begin(), str.begin() + 1, str.end());
        SetWindowText(hwnd, str.c_str());
        Sleep(40);
    }

    return 0;
}
