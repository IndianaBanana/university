#include <windows.h>
#include <cstdio>
#include <iostream>
#include <vector>
#include <gdiplus.h>
#include <string>
#include <algorithm>

#pragma comment (lib,"Gdiplus.lib")

void InitGDIPlus(Gdiplus::GdiplusStartupInput& gdiplusStartupInput, ULONG_PTR& gdiplusToken);
void OnPaint(HDC hdc);
DWORD WINAPI ThreadProcMoveString(CONST LPVOID lpParam);
DWORD WINAPI ThreadProcDrawing(CONST LPVOID lpParam);

LRESULT CALLBACK WndProc(HWND hwnd, UINT Message, WPARAM wparam, LPARAM lparam);
HINSTANCE g_hInstance;
HWND g_hWnd;
HMODULE g_hModule;

Gdiplus::GdiplusStartupInput g_GdiplusStartupInput;
ULONG_PTR g_GdiplusToken;

int APIENTRY WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, PSTR pCmdLine, int nCmdShow)
{
	g_hInstance = hInstance;

	WNDCLASS wc = {};
	wc.hInstance = hInstance;
	wc.lpszClassName = "7l";
	wc.style = CS_HREDRAW | CS_VREDRAW;
	wc.lpfnWndProc = WndProc;
	wc.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
	RegisterClass(&wc);

	g_hWnd = CreateWindow("7l", "Main Window",
		WS_OVERLAPPEDWINDOW,
		0, 0,
		1280, 720,
		NULL, NULL,
		hInstance, NULL);

	ShowWindow(g_hWnd, SW_SHOWDEFAULT);
	UpdateWindow(g_hWnd);

	MSG msg;
	while (GetMessage(&msg, NULL, 0, 0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}

	return msg.wParam;
}



LRESULT CALLBACK WndProc(HWND hWnd, UINT Message, WPARAM wparam, LPARAM lparam)
{
	PAINTSTRUCT  ps;
	static HANDLE fth;
	static bool fthstop = false;
	static HANDLE sth;
	static bool sthstop = false;
	static HWND hwndButtonft;
	switch (Message)
	{
	case WM_DESTROY:
		PostQuitMessage(0);
		return 0;
	case WM_CREATE:
	{
		InitGDIPlus(g_GdiplusStartupInput, g_GdiplusToken);

		fth = CreateThread(NULL, 0, ThreadProcMoveString, NULL, 0, NULL);
		sth = CreateThread(NULL, 0, ThreadProcDrawing, NULL, 0, NULL);

		hwndButtonft = CreateWindow(
			"BUTTON",
			"First Thread",
			WS_TABSTOP | WS_VISIBLE | WS_CHILD | BS_DEFPUSHBUTTON,
			10,
			10,
			128,
			64,
			hWnd,
			NULL,
			g_hInstance,
			NULL);

		HWND hwndButtonst = CreateWindow(
			"BUTTON",
			"Second Thread",
			WS_TABSTOP | WS_VISIBLE | WS_CHILD | BS_DEFPUSHBUTTON,
			148,
			10,
			128,
			64,
			hWnd,
			NULL,
			g_hInstance,
			NULL);
	}
	return 0;
	case WM_COMMAND:
	{
		if (wparam == BN_CLICKED)
		{
			if ((HWND)lparam == hwndButtonft)
			{
				if (fthstop)
				{
					ResumeThread(fth);
					fthstop = false;
				}
				else
				{
					SuspendThread(fth);
					fthstop = true;
				}
			}
			else {
				if (sthstop)
				{
					ResumeThread(sth);
					sthstop = false;
				}
				else
				{
					SuspendThread(sth);
					sthstop = true;
				}
			}
		}

	}
	return 0;
	default:
		return DefWindowProc(hWnd, Message, wparam, lparam);
	}

	return 0;
}

void InitGDIPlus(Gdiplus::GdiplusStartupInput& gdiplusStartupInput, ULONG_PTR& gdiplusToken)
{
	Gdiplus::GdiplusStartup(&gdiplusToken, &gdiplusStartupInput, NULL);
}

void OnPaint(HDC hdc)
{
	static int i = 0;
	Gdiplus::Graphics graphics(hdc);
	Gdiplus::Pen curPen(Gdiplus::Color(255, 50, 150, 100), 3);
	graphics.DrawLine(&curPen, i, 256 + rand() % 256, i, 720);
	i++;
}

DWORD WINAPI ThreadProcMoveString(CONST LPVOID lpParam)
{
	HANDLE mutex = (HANDLE)lpParam;
	std::string str = "   Hello World!   ";
	int i = 0;
	while (1)
	{
		WaitForSingleObject(mutex, INFINITE);
		rotate(str.begin(), str.begin() + 1, str.end());
		SetWindowText(g_hWnd, str.c_str());
		Sleep(100);
		i += 1;
	}
	ExitThread(0);
}

DWORD WINAPI ThreadProcDrawing(CONST LPVOID lpParam)
{
	HANDLE mutex = (HANDLE)lpParam;
	while (1)
	{
		WaitForSingleObject(mutex, INFINITE);
		HDC hdc = GetDC(g_hWnd);
		OnPaint(hdc);
		ReleaseDC(g_hWnd, hdc);
		Sleep(100);
	}
}