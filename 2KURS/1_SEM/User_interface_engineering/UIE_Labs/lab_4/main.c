#include <windows.h>
//* g++ -shared -o GraphicsLibrary.dll GraphicsLibrary.c -lgdi32  компиляция DLL
//* g++ -o main.exe main.c -luser32 -lgdi32 -L. -lGraphicsLibrary компиляция EXE

// Прототипы функций из DLL
extern void DrawEllipse(HDC hdc, int x, int y, int width, int height, COLORREF color);
extern void DrawLine(HDC hdc, int x1, int y1, int x2, int y2, COLORREF color);
extern void DrawRectangle(HDC hdc, int x, int y, int width, int height, COLORREF color);

// Обработчик сообщений окна
LRESULT CALLBACK WindowProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam);

int APIENTRY WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, PSTR pCmdLine, int nCmdShow) {
    // Регистрация класса окна
    WNDCLASS windowClass = {};
    windowClass.lpfnWndProc = WindowProc;
    windowClass.hInstance = GetModuleHandle(NULL);
    windowClass.lpszClassName = "MainWindowClass";

    RegisterClass(&windowClass);

    // Создание окна
    HWND hwnd = CreateWindowEx(0, "MainWindowClass", "Graphics App",
                               WS_OVERLAPPEDWINDOW | WS_VISIBLE,
                               CW_USEDEFAULT, CW_USEDEFAULT, 800, 600,
                               0, 0, GetModuleHandle(NULL), 0);

    // Основной цикл обработки сообщений
    MSG msg = {};
    while (GetMessage(&msg, NULL, 0, 0)) {
        TranslateMessage(&msg);
        DispatchMessage(&msg);
    }

    return 0;
}

LRESULT CALLBACK WindowProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam) {
    static POINT startPt;
    static POINT endPt;
    static HDC hdc;
    static int shapeType = 0;  // 0 - line, 1 - ellipse, 2 - rectangle

    switch (uMsg) {
        case WM_CREATE:
            hdc = GetDC(hwnd);
            break;

        case WM_LBUTTONDOWN:
            shapeType = 0;  // Line
            startPt.x = LOWORD(lParam);
            startPt.y = HIWORD(lParam);
            break;

        case WM_MBUTTONDOWN:
            shapeType = 1;  // Ellipse
            startPt.x = LOWORD(lParam);
            startPt.y = HIWORD(lParam);
            break;

        case WM_RBUTTONDOWN:
            shapeType = 2;  // Rectangle
            startPt.x = LOWORD(lParam);
            startPt.y = HIWORD(lParam);
            break;

        case WM_LBUTTONUP:
        case WM_MBUTTONUP:
        case WM_RBUTTONUP:
            endPt.x = LOWORD(lParam);
            endPt.y = HIWORD(lParam);

            switch (shapeType) {
                case 0:
                    DrawLine(hdc, startPt.x, startPt.y, endPt.x, endPt.y, RGB(rand()%255, rand()%255, rand()%255));  // Blue line
                    break;
                case 1:
                    DrawEllipse(hdc, startPt.x, startPt.y, endPt.x - startPt.x, endPt.y - startPt.y, RGB(rand()%255, rand()%255, rand()%255));  // Red ellipse
                    break;
                case 2:
                    DrawRectangle(hdc, startPt.x, startPt.y, endPt.x - startPt.x, endPt.y - startPt.y, RGB(rand()%255, rand()%255, rand()%255));  // Green rectangle
                    break;
            }

            break;

        case WM_DESTROY:
            ReleaseDC(hwnd, hdc);
            PostQuitMessage(0);
            break;

        default:
            return DefWindowProc(hwnd, uMsg, wParam, lParam);
    }

    return 0;
}
