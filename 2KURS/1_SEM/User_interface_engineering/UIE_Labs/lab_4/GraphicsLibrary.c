// GraphicsLibrary.cpp

// #include <windows.h>
#include <GraphicsLibrary.h>

    __declspec(dllexport) void DrawEllipse(HDC hdc, int x, int y, int width, int height, COLORREF color)
    {
        HBRUSH hBrush = CreateSolidBrush(color);
        HGDIOBJ oldBrush = SelectObject(hdc, hBrush);

        Ellipse(hdc, x, y, x + width, y + height);

        SelectObject(hdc, oldBrush);
        DeleteObject(hBrush);
    }
    __declspec(dllexport) void DrawLine(HDC hdc, int x1, int y1, int x2, int y2, COLORREF color)
    {
        HPEN hPen = CreatePen(PS_SOLID, 2, color);
        HGDIOBJ oldPen = SelectObject(hdc, hPen);

        MoveToEx(hdc, x1, y1, NULL);
        LineTo(hdc, x2, y2);

        SelectObject(hdc, oldPen);
        DeleteObject(hPen);
    }
    __declspec(dllexport) void DrawRectangle(HDC hdc, int x, int y, int width, int height, COLORREF color)
    {
        HBRUSH hBrush = CreateSolidBrush(color);
        HGDIOBJ oldBrush = SelectObject(hdc, hBrush);

        Rectangle(hdc, x, y, x + width, y + height);

        SelectObject(hdc, oldBrush);
        DeleteObject(hBrush);
    }
