#include <windows.h>
#include <stdio.h>
#include <vector>
#include <limits>
#include <string>

LRESULT CALLBACK DrawWindow(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam);
LRESULT CALLBACK ListWindow(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam);

HINSTANCE hInst;
int lastY = std::numeric_limits<int>::max();
int lastX = std::numeric_limits<int>::max();
COLORREF lineColor = RGB(0, 0, 0);
COLORREF fillColor = RGB(255, 0, 0);
int lastFigureIndex = 0;
int selectedIndex = -1;
HWND hwndListBox, drawWindow;
bool drawing = false;
HWND listWindow;
int numberOfPoints = 0;
using namespace std;

class Figure
{
private:
    std::vector<POINT> points;
    bool collored = false;

public:
    bool wasCollored = false;
    Figure(){};
    Figure(Figure *figure)
    {
        collored = figure->isCollored();
        for (POINT point : figure->getPoints())
        {
            points.push_back({point.x, point.y});
        }
    };
    POINT getFirstPoint()
    {
        return points[0];
    }
    void setCollored()
    {
        collored = !collored;
        wasCollored = !wasCollored;
    }
    bool isCollored()
    {
        return collored;
    }

    POINT getLastPoint()
    {
        return points.back();
    }
    std::vector<POINT> getPoints()
    {
        return points;
    }
    void addPoint(POINT point)
    {
        points.push_back(point);
    }
    bool IsPointInsidePolygon(const POINT &point)
    {
        int crossings = 0;
        size_t numVertices = points.size();

        for (size_t i = 0; i < numVertices; i++)
        {
            const POINT &vertex1 = points[i];
            const POINT &vertex2 = points[(i + 1) % numVertices];

            if ((vertex1.y <= point.y && vertex2.y > point.y) ||
                (vertex1.y > point.y && vertex2.y <= point.y))
            {
                if (point.x < (vertex2.x - vertex1.x) * (point.y - vertex1.y) / (vertex2.y - vertex1.y) + vertex1.x)
                {
                    crossings++;
                }
            }
        }
        return (crossings % 2 != 0);
    }

    void draw(HWND hwnd, COLORREF fillColor2 = fillColor)
    {
        HDC hdc = GetDC(hwnd);
        HPEN hPen = CreatePen(PS_SOLID, 1, lineColor);
        HBRUSH hBrush = CreateSolidBrush(fillColor2);
        for (int i = 0; i < points.size() - 1; i++)
        {
            SelectObject(hdc, hPen);
            SelectObject(hdc, hBrush);
            MoveToEx(hdc, points[i].x, points[i].y, NULL);
            LineTo(hdc, points[i + 1].x, points[i + 1].y);
        }
        if (collored && points[0].x == points.back().x && points[0].y == points.back().y)
        {
            Polygon(hdc, points.data(), points.size() - 1);
        }
        DeleteObject(hPen);
        DeleteObject(hBrush);
        ReleaseDC(hwnd, hdc);
    }
};

void CreatePoint(HWND hwnd, vector<Figure *> figures, WPARAM wParam, LPARAM lParam);
void сreateFigure(HWND hwnd, vector<Figure *> figures, WPARAM wParam, LPARAM lParam);
void FillPolygon(HWND hwnd, vector<Figure *> figures, WPARAM wParam, LPARAM lParam);
void OpenArchive(HWND hwnd, vector<Figure *> figures, WPARAM wParam, LPARAM lParam);
void CloseArchive(HWND hwnd, vector<Figure *> figures, WPARAM wParam, LPARAM lParam);
void MoveUp(HWND hwnd, vector<Figure *> figures, WPARAM wParam, LPARAM lParam);
void MoveDown(HWND hwnd, vector<Figure *> figures, WPARAM wParam, LPARAM lParam);
void LoadFigure(HWND hwnd, vector<Figure *> figures, WPARAM wParam, LPARAM lParam);
void Finish(HWND hwnd, vector<Figure *> figures, WPARAM wParam, LPARAM lParam);

enum State
{
    NONE = 0,
    MAIN_WINDOW = 1,
    SECOND_POINT_WINDOW = 2,
    POINT_WINDOW = 3,
    DRAWED_FIGURES_WINDOW = 4,
    ARCHIVE = 5,
    FINISH_SUB = 6,
    FINISH_MAIN = 7
};

State Transition[7][8] = {
    {FINISH_MAIN, SECOND_POINT_WINDOW, NONE, NONE, NONE, NONE, NONE, NONE},
    {FINISH_MAIN, POINT_WINDOW, NONE, NONE, NONE, NONE, NONE, NONE},
    {FINISH_MAIN, POINT_WINDOW, MAIN_WINDOW, NONE, NONE, NONE, NONE, NONE},
    {FINISH_MAIN, SECOND_POINT_WINDOW, DRAWED_FIGURES_WINDOW, ARCHIVE, NONE, NONE, NONE, DRAWED_FIGURES_WINDOW},
    {NONE, NONE, NONE, NONE, ARCHIVE, ARCHIVE, DRAWED_FIGURES_WINDOW, NONE},
    {NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE},
    {NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE},
};

void (*Action[7][8])(HWND hwnd, vector<Figure *> figures, WPARAM wParam, LPARAM lParam) = {
    {Finish, CreatePoint, NULL, NULL, NULL, NULL, NULL, NULL},
    {Finish, CreatePoint, NULL, NULL, NULL, NULL, NULL, NULL},
    {Finish, CreatePoint, сreateFigure, NULL, NULL, NULL, NULL, NULL},
    {Finish, CreatePoint, FillPolygon, OpenArchive, NULL, NULL, NULL, LoadFigure},
    {NULL, NULL, NULL, NULL, MoveDown, MoveUp, CloseArchive, NULL},
    {NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL},
    {NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL}};

State currentState = MAIN_WINDOW;
static std::vector<Figure *> figures;

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
    WNDCLASS drawWindowClass = {0};
    drawWindowClass.lpfnWndProc = DrawWindow;
    drawWindowClass.hInstance = hInstance;
    drawWindowClass.lpszClassName = "drawWindowClass";
    RegisterClass(&drawWindowClass);
    drawWindow = CreateWindow("drawWindowClass", "Draw window", WS_OVERLAPPEDWINDOW,
                              100, 100, 800, 720, nullptr, nullptr, hInstance, nullptr);
    ShowWindow(drawWindow, nCmdShow);
    MSG msg = {0};
    while (GetMessage(&msg, nullptr, 0, 0))
    {
        TranslateMessage(&msg);
        DispatchMessage(&msg);
    }

    return 0;
}

LRESULT CALLBACK DrawWindow(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
{

    switch (message)
    {
    case WM_CREATE:
    {
        WNDCLASS listWindowClass = {0};

        listWindowClass.lpfnWndProc = ListWindow;
        listWindowClass.hInstance = hInst;
        listWindowClass.lpszClassName = "listWindowClass";
        RegisterClass(&listWindowClass);

        listWindow = CreateWindow(
            "listWindowClass",
            "Second Window",
            WS_OVERLAPPEDWINDOW & ~WS_SYSMENU,
            900, 100,
            400, 600,
            nullptr, nullptr, hInst, nullptr);
        hwndListBox = CreateWindow(
            "LISTBOX",
            "list",
            WS_CHILD | WS_VISIBLE | WS_VSCROLL | LBS_NOTIFY | LBS_WANTKEYBOARDINPUT,
            10, 10,
            100, 300,
            listWindow,
            NULL,
            hInst,
            NULL);

        figures.push_back(new Figure());
        return 0;
    }

    default:
    {
        switch (currentState)
        {
        case NONE:
        {
            break;
        }

        case MAIN_WINDOW:
        {
            printf("Main\n");
            switch (message)
            {
            case WM_LBUTTONDOWN:
            {
                Action[0][1](hwnd, figures, wParam, lParam);
                currentState = Transition[0][1];
                return 0;
            }
            case WM_DESTROY:
            {
                currentState = Transition[0][0];
                Action[0][0](hwnd, figures, wParam, lParam);
                return 0;
            }
            }
            return DefWindowProc(hwnd, message, wParam, lParam);
        }

        case SECOND_POINT_WINDOW:
        {
            printf("SecondP\n");
            switch (message)
            {
            case WM_LBUTTONDOWN:
            {
                Action[1][1](hwnd, figures, wParam, lParam);
                currentState = Transition[1][1];
                return 0;
            }

            case WM_MOUSEMOVE:
            {
                if (drawing)
                {
                    Sleep(40);
                    HDC hdc = GetDC(hwnd);
                    if (lastX != std::numeric_limits<int>::max() && lastY != std::numeric_limits<int>::max())
                    {
                        MoveToEx(hdc, figures.back()->getLastPoint().x, figures.back()->getLastPoint().y, NULL);
                        SetROP2(hdc, R2_NOTXORPEN);
                        HPEN hPen = CreatePen(PS_SOLID, 1, lineColor);
                        SelectObject(hdc, hPen);
                        LineTo(hdc, lastX, lastY);
                    }

                    MoveToEx(hdc, figures.back()->getLastPoint().x, figures.back()->getLastPoint().y, NULL);
                    LineTo(hdc, LOWORD(lParam), HIWORD(lParam));
                    lastX = LOWORD(lParam), lastY = HIWORD(lParam);
                    ReleaseDC(hwnd, hdc);
                }
                return 0;
            }

            case WM_DESTROY:
            {
                currentState = Transition[1][0];
                Action[1][0](hwnd, figures, wParam, lParam);
                return 0;
            }
            }
            return DefWindowProc(hwnd, message, wParam, lParam);
        }

        case POINT_WINDOW:
        {
            printf("Point\n");
            switch (message)
            {
            case WM_LBUTTONDOWN:
            {
                Action[2][1](hwnd, figures, wParam, lParam);
                currentState = Transition[2][1];
                return 0;
            }
            case WM_RBUTTONDOWN:
            {
                if (drawing && numberOfPoints > 1)
                {
                    Action[2][2](hwnd, figures, wParam, lParam);
                    currentState = Transition[2][2];
                    return 0;
                }
                return 0;
            }

            case WM_MOUSEMOVE:
            {
                if (drawing)
                {
                    Sleep(40);
                    HDC hdc = GetDC(hwnd);
                    if (lastX != std::numeric_limits<int>::max() && lastY != std::numeric_limits<int>::max())
                    {
                        MoveToEx(hdc, figures.back()->getLastPoint().x, figures.back()->getLastPoint().y, NULL);
                        SetROP2(hdc, R2_NOTXORPEN);
                        HPEN hPen = CreatePen(PS_SOLID, 1, lineColor);
                        SelectObject(hdc, hPen);
                        LineTo(hdc, lastX, lastY);
                    }

                    MoveToEx(hdc, figures.back()->getLastPoint().x, figures.back()->getLastPoint().y, NULL);
                    LineTo(hdc, LOWORD(lParam), HIWORD(lParam));
                    lastX = LOWORD(lParam), lastY = HIWORD(lParam);
                    ReleaseDC(hwnd, hdc);
                }
                return 0;
            }

            case WM_DESTROY:
            {
                currentState = Transition[2][0];
                Action[2][0](hwnd, figures, wParam, lParam);
                return 0;
            }
            }
            return DefWindowProc(hwnd, message, wParam, lParam);
        }

        case DRAWED_FIGURES_WINDOW:
        {
            printf("Drawed\n");
            switch (message)
            {
            case WM_LBUTTONDOWN:
            {
                Action[3][1](hwnd, figures, wParam, lParam);
                currentState = Transition[3][1];
                // SendMessage(drawWindow, WM_LBUTTONDOWN, wParam, lParam);
                return 0;
            }
            case WM_RBUTTONDOWN:
            {
                Action[3][2](hwnd, figures, wParam, lParam);
                currentState = Transition[3][2];
                return 0;
            }
            case WM_MBUTTONDOWN:
            {
                currentState = Transition[3][3];
                Action[3][3](hwnd, figures, wParam, lParam);
                return 0;
            }
            case WM_APP:
            {
                currentState = Transition[3][7];
                Action[3][7](hwnd, figures, wParam, lParam);
                return 0;
            }
            case WM_DESTROY:
            {
                currentState = Transition[3][0];
                Action[3][0](hwnd, figures, wParam, lParam);
                return 0;
            }
            }
            return DefWindowProc(hwnd, message, wParam, lParam);
        }
        }
    }
    }
}

LRESULT CALLBACK ListWindow(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    switch (currentState)
    {
    case ARCHIVE:
    {
        switch (message)
        {
        case WM_KEYDOWN:
        {
            if (drawing)
                return 0;
            switch (wParam)
            {
            case VK_UP:
            {
                currentState = Transition[4][5];
                Action[4][5](hwnd, figures, wParam, lParam);
                return 0;
            }

            case VK_DOWN:
            {
                currentState = Transition[4][4];
                Action[4][4](hwnd, figures, wParam, lParam);
                return 0;
            }
            case VK_RETURN:
            {
                currentState = Transition[4][6];
                Action[4][6](hwnd, figures, wParam, lParam);
                return 0;
            }
            default:
                break;
            }
            break;
        }
        }
        break;
    }

    default:
        break;
    }
}

void CreateFirstPoint(HWND hwnd, vector<Figure *> figures, WPARAM wParam, LPARAM lParam)
{
    figures.back()->addPoint({LOWORD(lParam), HIWORD(lParam)});
    numberOfPoints++;
    drawing = true;
}

void CreateSecondPoint(HWND hwnd, vector<Figure *> figures, WPARAM wParam, LPARAM lParam)
{
    figures.back()->addPoint({LOWORD(lParam), HIWORD(lParam)});
    numberOfPoints++;
    drawing = true;
}

void CreatePoint(HWND hwnd, vector<Figure *> figures, WPARAM wParam, LPARAM lParam)
{
    figures.back()->addPoint({LOWORD(lParam), HIWORD(lParam)});
    numberOfPoints++;
    drawing = true;
}

void сreateFigure(HWND hwnd, vector<Figure *> figures, WPARAM wParam, LPARAM lParam)
{
    numberOfPoints = 0;
    Figure *figure = figures.back();
    drawing = false;
    figures.back()->addPoint({LOWORD(lParam), HIWORD(lParam)});
    figure->addPoint(figure->getFirstPoint());
    figures.push_back(new Figure());
    lastX = lastY = std::numeric_limits<int>::max();
    figure->draw(hwnd);
    std::string numOfFigure = "Figure " + std::to_string(lastFigureIndex++);
    SendMessage(hwndListBox, LB_ADDSTRING, 0, (LPARAM)numOfFigure.c_str());
}

void FillPolygon(HWND hwnd, vector<Figure *> figures, WPARAM wParam, LPARAM lParam)
{
    POINT clickPoint = {LOWORD(lParam), HIWORD(lParam)};
    std::vector<Figure *> tmp = figures;
    int i = 0;
    for (Figure *figure : tmp)
    {
        if (figure == tmp.back() || figure->isCollored() || figure->wasCollored)
            continue;
        if (figure->IsPointInsidePolygon(clickPoint))
        {

            Figure *emptyFigure = figures.back();
            figure->wasCollored = true;
            figures.back() = new Figure(figure);
            figures.back()->setCollored();
            figures.back()->draw(hwnd);
            std::string numOfFigure = "Figure " + std::to_string(lastFigureIndex++);
            SendMessage(hwndListBox, LB_ADDSTRING, 0, (LPARAM)numOfFigure.c_str());
            figures.push_back(emptyFigure);
        }
    }
}

void OpenArchive(HWND hwnd, vector<Figure *> figures, WPARAM wParam, LPARAM lParam)
{
    ShowWindow(listWindow, SW_SHOW);
    SetFocus(listWindow);
}

void CloseArchive(HWND hwnd, vector<Figure *> figures, WPARAM wParam, LPARAM lParam)
{
    if (selectedIndex != -1)
    {
        int index = SendMessage(hwndListBox, LB_GETCURSEL, 0, 0);
        SendMessage(drawWindow, WM_APP, index, 0);
        SetFocus(drawWindow);
        ShowWindow(hwnd, SW_HIDE);
    }
}

void MoveUp(HWND hwnd, vector<Figure *> figures, WPARAM wParam, LPARAM lParam)
{
    if ((--selectedIndex) < 0)
        selectedIndex = 0;
    SendMessage(hwndListBox, LB_SETCURSEL, selectedIndex, 0);
}

void MoveDown(HWND hwnd, vector<Figure *> figures, WPARAM wParam, LPARAM lParam)
{
    int num = SendMessage(hwndListBox, LB_GETCOUNT, 0, 0) - 1;
    if (num < ++selectedIndex)
        selectedIndex = num;
    SendMessage(hwndListBox, LB_SETCURSEL, selectedIndex, 0);
}

void LoadFigure(HWND hwnd, vector<Figure *> figures, WPARAM wParam, LPARAM lParam)
{
    figures[wParam]->draw(hwnd, RGB(rand() % 255, rand() % 255, rand() % 255));
}

void Finish(HWND hwnd, vector<Figure *> figures, WPARAM wParam, LPARAM lParam)
{
    PostQuitMessage(0);
}
