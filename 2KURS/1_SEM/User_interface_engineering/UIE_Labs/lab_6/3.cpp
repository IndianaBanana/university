#include <windows.h>
#include <stdio.h>
#include <vector>
#include <limits>
#include <string>

BOOL RegClass(WNDPROC, LPCTSTR, UINT);
LRESULT CALLBACK WndProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam);
LRESULT CALLBACK ListWindowProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam);

using namespace std;

HINSTANCE hInst;
char szClassName[] = "DrawingAppClass";
char szClassName2[] = "FigureListClass";

COLORREF lineColor = RGB(0, 0, 0);
COLORREF fillColor = RGB(255, 0, 0);
int lastY = numeric_limits<int>::max();
int lastX = numeric_limits<int>::max();
HWND hwndListBox;
int lastFigureIndex = 0;
int selectedIndex = -1;
class Figure
{
    vector<POINT> points;

public:
    POINT getFirstPoint()
    {
        return points[0];
    }
    POINT getLastPoint()
    {
        return points.back();
    }
    void addPoint(POINT point)
    {
        points.push_back(point);
    }

    void draw(HWND hwnd)
    {
        HDC hdc = GetDC(hwnd);
        HPEN hPen = CreatePen(PS_SOLID, 1, lineColor);
        HBRUSH hBrush = CreateSolidBrush(fillColor);
        for (int i = 0; i < points.size() - 1; i++)
        {
            SelectObject(hdc, hPen);
            SelectObject(hdc, hBrush);
            MoveToEx(hdc, points[i].x, points[i].y, NULL);
            LineTo(hdc, points[i + 1].x, points[i + 1].y);
        }
        if (points[0].x == points.back().x && points[0].y == points.back().y)
        {
            Polygon(hdc, points.data(), points.size() - 1);
        }
        DeleteObject(hPen);
        DeleteObject(hBrush);
        ReleaseDC(hwnd, hdc);
    }
};
vector<Figure *> figures;

BOOL RegClass(WNDPROC Proc, LPCTSTR szName, UINT brBackground)
{
    WNDCLASS wc;
    wc.style = wc.cbClsExtra = wc.cbWndExtra = 0;
    wc.lpfnWndProc = Proc;
    wc.hInstance = hInst;
    wc.hIcon = LoadIcon(NULL, IDI_APPLICATION);
    wc.hCursor = LoadCursor(NULL, IDC_ARROW);
    wc.hbrBackground = (HBRUSH)(brBackground + 1);
    wc.lpszMenuName = NULL;
    wc.lpszClassName = szName;

    return (RegisterClass(&wc) != 0);
}

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
    MSG msg;
    HWND hwnd;
    hInst = hInstance;

    if (!RegClass(WndProc, szClassName, COLOR_WINDOW))
    {
        return FALSE;
    }

    hwnd = CreateWindow(
        szClassName,
        "Parent Application",
        WS_OVERLAPPEDWINDOW | WS_VISIBLE,
        CW_USEDEFAULT, CW_USEDEFAULT,
        500, 500,
        0,
        0,
        hInstance,
        NULL);
    if (!hwnd)
    {
        return FALSE;
    }
    while (GetMessage(&msg, NULL, 0, 0))
    {
        TranslateMessage(&msg);
        DispatchMessage(&msg);
    }

    return msg.wParam;
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
    static bool drawing = false;

    switch (msg)
    {
    case WM_CREATE:
    {
        if (!RegClass(ListWindowProc, szClassName2, COLOR_WINDOW))
        {
            return FALSE;
        }
        HWND hwndList = CreateWindow(
            szClassName2,
            "Figure List",
            WS_OVERLAPPEDWINDOW | WS_VISIBLE,
            1120, 200,
            400, 600,
            hwnd, NULL, hInst, NULL);
        hwndListBox = CreateWindow(
            "LISTBOX",
            "list",
            WS_CHILD | WS_VISIBLE | WS_VSCROLL | LBS_NOTIFY | LBS_WANTKEYBOARDINPUT,
            10, 10,
            200, 300,
            hwndList,
            NULL,
            hInst,
            NULL);

        figures.push_back(new Figure());

        return 0;
    }

    case WM_DESTROY:
        PostQuitMessage(0);
        return 0;

    case WM_LBUTTONDOWN:
        figures.back()->addPoint({LOWORD(lParam), HIWORD(lParam)});
        drawing = true;
        return 0;

    case WM_RBUTTONDOWN:
    {
        if (drawing)
        {
            Figure *figure = figures.back();
            drawing = false;
            figures.back()->addPoint({LOWORD(lParam), HIWORD(lParam)});
            figure->addPoint(figure->getFirstPoint());
            figures.push_back(new Figure());
            lastX = lastY = numeric_limits<int>::max();
            figure->draw(hwnd);
            string numOfFigure = "Figure " + to_string(lastFigureIndex++);
            SendMessage(hwndListBox, LB_ADDSTRING, 0, (LPARAM)numOfFigure.c_str());
        }

        return 0;
    }

    case WM_MOUSEMOVE:
    {
        if (drawing)
        {
            Sleep(40);
            HDC hdc = GetDC(hwnd);
            if (lastX != numeric_limits<int>::max() && lastY != numeric_limits<int>::max())
            {
                MoveToEx(hdc, figures.back()->getLastPoint().x, figures.back()->getLastPoint().y, NULL);
                SetROP2(hdc, R2_NOTXORPEN);
                HPEN hPen = CreatePen(PS_SOLID, 1, lineColor);
                SelectObject(hdc, hPen);
                LineTo(hdc, lastX, lastY);
            }

            MoveToEx(hdc, figures.back()->getLastPoint().x, figures.back()->getLastPoint().y, NULL);
            LineTo(hdc, LOWORD(lParam), HIWORD(lParam));
            lastX = LOWORD(lParam), lastY = HIWORD(lParam);
            ReleaseDC(hwnd, hdc);
        }
        return 0;
    }
    case WM_KEYDOWN:
    {
        switch (wParam)
        {
        case VK_UP:
        {
            // Обработка стрелки вверх
            if ((--selectedIndex) < 0)
                selectedIndex = 0;
            // printf("%d\n", selectedIndex);
            SendMessage(hwndListBox, LB_SETCURSEL, selectedIndex, 0);
            return 0;
        }

        case VK_DOWN:
        {
            // Обработка стрелки вниз
            int num = SendMessage(hwndListBox, LB_GETCOUNT, 0, 0) - 1;
            if (num < ++selectedIndex)
                selectedIndex = num;
            // printf("%d\n", selectedIndex);
            SendMessage(hwndListBox, LB_SETCURSEL, selectedIndex, 0);
            return 0;
        }
        case VK_RETURN:
        {

            if (selectedIndex != -1)
            {
                int index = SendMessage(hwndListBox, LB_GETCURSEL, 0, 0);
                printf("%d\n", index);
            }
            return 0;
        }
        default:
            break;
        }
        break;
    }

    default:
        return DefWindowProc(hwnd, msg, wParam, lParam);
    }
}

LRESULT CALLBACK ListWindowProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
    switch (uMsg)
    {
        // case WM_CREATE:
        // {

        //     hwndListBox = CreateWindow(
        //         "LISTBOX",
        //         "list",
        //         WS_CHILD | WS_VISIBLE | WS_VSCROLL | LBS_NOTIFY | LBS_WANTKEYBOARDINPUT,
        //         10, 10,
        //         300, 500,
        //         hwnd,
        //         NULL,
        //         hInst,
        //         NULL);
        //     break;
        // }

    case WM_SETFOCUS:
        break;

    case WM_KEYDOWN:
    {
        switch (wParam)
        {
        case VK_UP:
        {
            // Обработка стрелки вверх
            if ((--selectedIndex) < 0)
                selectedIndex = 0;
            // printf("%d\n", selectedIndex);
            SendMessage(hwndListBox, LB_SETCURSEL, selectedIndex, 0);
            return 0;
        }

        case VK_DOWN:
        {
            // Обработка стрелки вниз
            int num = SendMessage(hwndListBox, LB_GETCOUNT, 0, 0) - 1;
            if (num < ++selectedIndex)
                selectedIndex = num;
            // printf("%d\n", selectedIndex);
            SendMessage(hwndListBox, LB_SETCURSEL, selectedIndex, 0);
            return 0;
        }
        case VK_RETURN:
        {

            if (selectedIndex != -1)
            {
                int index = SendMessage(hwndListBox, LB_GETCURSEL, 0, 0);
                printf("%d\n", index);
            }
            return 0;
        }
        default:
            break;
        }
        break;
    }
        //   case WM_COMMAND:
        //     {
        //         // printf("FF");
        //         // Обработка сообщений от элементов управления
        //         switch (wParam)
        //         {
        //         printf("FF");

        //         case CBN_SELCHANGE:
        //         {
        //             // Получаем выбранный индекс
        //             // HWND hwndListBox = (HWND)lParam;
        //             int selectedIndex = SendMessage(hwndListBox, CB_GETCURSEL, 0, 0);
        //             printf("%sadasdsadd\n", selectedIndex);

        //             // Получаем текст выбранного элемента
        //             // char buffer[256];
        //             // SendMessage(hwndListBox, CB_GETLBTEXT, selectedIndex, (LPARAM)buffer);

        //             // Выводим информацию о выборе
        //             break;
        //         }

        //         default:
        //             break;
        //         }

        //         break;
        //     }

    case WM_DESTROY:
        PostQuitMessage(0);
        break;

    default:
        return DefWindowProc(hwnd, uMsg, wParam, lParam);
    }
}
