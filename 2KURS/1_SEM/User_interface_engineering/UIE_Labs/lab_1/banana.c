#include <windows.h>
#include <stdio.h>

BOOL RegClass(WNDPROC, LPCTSTR, UINT);
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

typedef struct
{
    long int x, y;
} Point;

Point pt;
HINSTANCE hInst;
char szClassName[] = "WindowsAppClass";
HWND g_hChildWnd = NULL;
int winHeight, winWidth;

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
    MSG msg;
    HWND hwnd;
    hInst = hInstance;

    if (!RegClass(WndProc, szClassName, COLOR_WINDOW))
        return FALSE;

    hwnd = CreateWindow(
        szClassName,
        "Parent Application",
        WS_OVERLAPPEDWINDOW | WS_VISIBLE,
        CW_USEDEFAULT, CW_USEDEFAULT,
        500, 500,
        0,
        0,
        hInstance,
        NULL);

    if (!hwnd)
        return FALSE;

    while (GetMessage(&msg, 0, 0, 0))
    {
        DispatchMessage(&msg);
    }

    return msg.wParam;
}

BOOL RegClass(WNDPROC Proc, LPCTSTR szName, UINT brBackground)
{
    WNDCLASS wc;
    wc.style = wc.cbClsExtra = wc.cbWndExtra = 0;
    wc.lpfnWndProc = Proc;
    wc.hInstance = hInst;
    wc.hIcon = LoadIcon(NULL, IDI_APPLICATION);
    wc.hCursor = LoadCursor(NULL, IDC_ARROW);
    wc.hbrBackground = (HBRUSH)(brBackground + 1);
    wc.lpszMenuName = NULL;
    wc.lpszClassName = szName;

    return (RegisterClass(&wc) != 0);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
    PAINTSTRUCT ps;
    static int pos;
    switch (msg)
    {
    case WM_LBUTTONDOWN:
    {
        if (g_hChildWnd)
        {
            DestroyWindow(g_hChildWnd);
            g_hChildWnd = NULL;
        }

        pt.x = LOWORD(lParam);
        pt.y = HIWORD(lParam);

        char buffer[50];
        sprintf(buffer, "X:%d\nY:%d", pt.x, pt.y);

        g_hChildWnd = CreateWindow(
            "static", // Класс окна
            buffer,   // Заголовок окна
            // WS_POPUPWINDOW | WS_CAPTION | WS_VISIBLE,
            WS_CHILD | WS_VISIBLE | SS_CENTER | WS_CAPTION | WS_BORDER,
            pt.x, pt.y, // Позиция окна
            100, 100,     // Размер окна
            hwnd,       // Родительское окно
            NULL,       // Меню
            hInst,      // Дескриптор приложения
            NULL        // Дополнительные параметры (указатель на POINT)
        );

        if (!g_hChildWnd)
        {
            MessageBox(hwnd, "Failed to create child window", "Error", MB_OK | MB_ICONERROR);
        }
        return 0;
    }
    case WM_SIZE:
    {
        winWidth = LOWORD(lParam) / 2;
        winHeight = HIWORD(lParam) / 2;
        break;
    }
    case WM_RBUTTONDOWN:
    {
        if (g_hChildWnd)
        {
            DestroyWindow(g_hChildWnd);
            g_hChildWnd = NULL;
        }
        pt.x = LOWORD(lParam);
        pt.y = HIWORD(lParam);

        int y = (winHeight - (int)pt.y) + winHeight;
        int x = (winWidth - (int)pt.x) + winWidth;

        char buffer[50];
        sprintf(buffer, "X:%d\nY:%d", x, y);

        g_hChildWnd = CreateWindow(
            "static", // Класс окна
            buffer,   // Заголовок окна
            WS_CHILD | WS_VISIBLE | SS_CENTER | WS_CAPTION | WS_BORDER,
            x, y,   // Позиция окна
            100, 100, // Размер окна
            hwnd,   // Родительское окно
            NULL,   // Меню
            hInst,  // Дескриптор приложения
            NULL    // Дополнительные параметры
        );

        if (!g_hChildWnd)
        {
            return FALSE;
        }
        return 0;
    }
    case WM_DESTROY:
    {
        PostQuitMessage(0);
        return 0;
    }
    default:
        return DefWindowProc(hwnd, msg, wParam, lParam);
    }
}
