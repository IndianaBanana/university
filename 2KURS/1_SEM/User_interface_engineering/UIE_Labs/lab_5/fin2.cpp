#include <windows.h>
#include <vector>
#include <limits>
#include <stdio.h>

#define BUTTON 1001

LRESULT CALLBACK WindowProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam);

using namespace std;

COLORREF lineColor = RGB(0, 0, 0);
COLORREF fillColor = RGB(255, 0, 0);
int lastY = numeric_limits<int>::max();
int lastX = numeric_limits<int>::max();
HINSTANCE hInst;
int numberOfPoints = 0;

class Figure
{
    vector<POINT> points;

public:
    bool collored = false;

    POINT getFirstPoint()
    {
        return points[0];
    }
    POINT getLastPoint()
    {
        return points.back();
    }
    void addPoint(POINT point)
    {
        points.push_back(point);
    }
    bool IsPointInsidePolygon(const POINT &point)
    {
        int crossings = 0;
        size_t numVertices = points.size();

        for (size_t i = 0; i < numVertices; i++)
        {
            const POINT &vertex1 = points[i];
            const POINT &vertex2 = points[(i + 1) % numVertices];

            if ((vertex1.y <= point.y && vertex2.y > point.y) ||
                (vertex1.y > point.y && vertex2.y <= point.y))
            {
                if (point.x < (vertex2.x - vertex1.x) * (point.y - vertex1.y) / (vertex2.y - vertex1.y) + vertex1.x)
                {
                    crossings++;
                }
            }
        }

        // Если число пересечений нечетное, то точка находится внутри многоугольника
        return (crossings % 2 != 0);
    }

    void draw(HWND hwnd)
    {
        HDC hdc = GetDC(hwnd);
        HPEN hPen = CreatePen(PS_SOLID, 1, lineColor);
        HBRUSH hBrush = CreateSolidBrush(fillColor);
        SelectObject(hdc, hPen);
        SelectObject(hdc, hBrush);
        for (int i = 0; i < points.size() - 1; i++)
        {
            MoveToEx(hdc, points[i].x, points[i].y, NULL);
            LineTo(hdc, points[i + 1].x, points[i + 1].y);
        }
        if (collored && points[0].x == points.back().x && points[0].y == points.back().y)
        {
            Polygon(hdc, points.data(), points.size() - 1);
        }
        DeleteObject(hPen);
        DeleteObject(hBrush);
        ReleaseDC(hwnd, hdc);
    }
};

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
    hInst = hInstance;
    WNDCLASS wc = {0};
    wc.lpfnWndProc = WindowProc;
    wc.hInstance = hInstance;
    wc.lpszClassName = "DrawingAppClass";

    RegisterClass(&wc);

    HWND hwnd = CreateWindowEx(
        0,
        "DrawingAppClass",
        "Drawing Application",
        WS_OVERLAPPEDWINDOW,
        CW_USEDEFAULT, CW_USEDEFAULT,
        800, 600,
        NULL, NULL, hInstance, NULL);

    ShowWindow(hwnd, nCmdShow);

    MSG msg = {0};
    while (GetMessage(&msg, NULL, 0, 0))
    {
        TranslateMessage(&msg);
        DispatchMessage(&msg);
    }

    return 0;
}

LRESULT CALLBACK WindowProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
    static vector<Figure *> figures;
    static bool drawing = false;
    static HWND GreenButton, BlueButton, button;

    switch (uMsg)
    {
    case WM_CREATE:
    {
        figures.push_back(new Figure());

        button = CreateWindow(
            "button",
            "Magic Collor",
            WS_VISIBLE | WS_CHILD | BS_DEFPUSHBUTTON,
            310, 10,
            100, 50,
            hwnd,
            (HMENU)BUTTON,
            hInst,
            NULL);
        return 0;
    }

    case WM_COMMAND:
    {
        if (drawing)
            return 0;
        switch (LOWORD(wParam))
        {
        case BUTTON:
            fillColor = RGB(rand() % 255, rand() % 255, rand() % 255);
            break;
        }
        return 0;
    }

    case WM_DESTROY:
        PostQuitMessage(0);
        return 0;

    case WM_LBUTTONDOWN:
    {
        figures.back()->addPoint({LOWORD(lParam), HIWORD(lParam)});
        numberOfPoints++;
        drawing = true;
        ShowWindow(button, SW_HIDE);
        return 0;
    }

    case WM_RBUTTONDOWN:
    {
        if (drawing && numberOfPoints > 1)
        {
            numberOfPoints = 0;
            Figure *figure = figures.back();
            drawing = false;
            ShowWindow(button, SW_SHOW);
            figures.back()->addPoint({LOWORD(lParam), HIWORD(lParam)});
            figure->addPoint(figure->getFirstPoint());
            figures.push_back(new Figure());
            lastX = lastY = numeric_limits<int>::max();
            figure->draw(hwnd);
            return 0;
        }
        POINT clickPoint = {LOWORD(lParam), HIWORD(lParam)};
        vector<Figure*> tmp = figures;
        for (Figure *figure : tmp)
        {
            if (figure == tmp.back()) continue;
            if (figure->IsPointInsidePolygon(clickPoint))
            {
                figures.back() = figure;
                figures.back()->collored = true;
                figures.back()->draw(hwnd);
                figures.push_back(new Figure());
            }
        }
        return 0;
    }

    case WM_MOUSEMOVE:
        if (drawing)
        {
            Sleep(40);
            HDC hdc = GetDC(hwnd);
            if (lastX != numeric_limits<int>::max() && lastY != numeric_limits<int>::max())
            {
                MoveToEx(hdc, figures.back()->getLastPoint().x, figures.back()->getLastPoint().y, NULL);
                SetROP2(hdc, R2_NOTXORPEN);
                HPEN hPen = CreatePen(PS_SOLID, 1, lineColor);
                SelectObject(hdc, hPen);
                LineTo(hdc, lastX, lastY);
            }

            MoveToEx(hdc, figures.back()->getLastPoint().x, figures.back()->getLastPoint().y, NULL);
            LineTo(hdc, LOWORD(lParam), HIWORD(lParam));
            lastX = LOWORD(lParam), lastY = HIWORD(lParam);
            ReleaseDC(hwnd, hdc);
        }
        return 0;

    default:
        return DefWindowProc(hwnd, uMsg, wParam, lParam);
    }
}
