#include <windows.h>

// Объявление глобальных переменных
HINSTANCE hInst;
int documentCount = 0;
char szChildClass[] = "MDIChild";
HWND hWndMDIClient;

// Прототипы функций
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);
LRESULT CALLBACK MDIChildWndProc(HWND, UINT, WPARAM, LPARAM);

// Точка входа
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow) {
    // Регистрация класса главного окна
     WNDCLASSEX wcex;
    wcex.cbSize = sizeof(WNDCLASSEX);
    wcex.style = CS_HREDRAW | CS_VREDRAW;
    wcex.lpfnWndProc = WndProc;
    wcex.cbClsExtra = 0;
    wcex.cbWndExtra = 0;
    wcex.hInstance = hInstance;
    wcex.hIcon = LoadIcon(hInstance, IDI_APPLICATION);
    wcex.hCursor = LoadCursor(NULL, IDC_ARROW);
    wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
    wcex.lpszMenuName = NULL;
    wcex.lpszClassName = "MDIParent";
    wcex.hIconSm = LoadIcon(wcex.hInstance, IDI_APPLICATION);

    if (!RegisterClassEx(&wcex)) {
        MessageBox(NULL, "Call to RegisterClassEx failed!", "Win32 Guided Tour", NULL);
        return 1;
    }

    // Создание главного окна
    HWND hWnd = CreateWindow("MDIParent", "MDI Parent Window", WS_OVERLAPPEDWINDOW,
        CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, NULL, NULL, hInstance, NULL);

    if (!hWnd) {
        MessageBox(NULL, "Call to CreateWindow failed!", "Win32 Guided Tour", NULL);
        return 1;
    }

    ShowWindow(hWnd, nCmdShow);
    UpdateWindow(hWnd);

    // Основной цикл сообщений
    MSG msg;
    while (GetMessage(&msg, NULL, 0, 0)) {
        TranslateMessage(&msg);
        DispatchMessage(&msg);
    }

    return (int)msg.wParam;
}

// Оконная процедура главного окна
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam) {
    switch (message) {
    case WM_CREATE: {
        CLIENTCREATESTRUCT ccs;
        ccs.hWindowMenu = GetSubMenu(GetMenu(hWnd), 2);
        ccs.idFirstChild = 50000;

        // Создание MDI клиента
        hWndMDIClient = CreateWindow("MDICLIENT", NULL,
            WS_CHILD | WS_CLIPCHILDREN | WS_VSCROLL | WS_HSCROLL,
            0, 0, 0, 0, hWnd, (HMENU)1, hInst, (LPVOID)&ccs);

        if (!hWndMDIClient) {
            MessageBox(hWnd, "Failed to create MDI client.", "Error", MB_OK | MB_ICONERROR);
        }
        break;
    }
    case WM_COMMAND: {
        int wmId = LOWORD(wParam);
        switch (wmId) {
        case 1: {
            documentCount++;
            MDICREATESTRUCT mcs;
            mcs.szClass = szChildClass;
            mcs.szTitle = "MDI Child";
            mcs.hOwner = hInst;
            mcs.x = CW_USEDEFAULT;
            mcs.y = CW_USEDEFAULT;
            mcs.cx = CW_USEDEFAULT;
            mcs.cy = CW_USEDEFAULT;
            mcs.style = 0;
            HWND hChild = (HWND)SendMessage(hWndMDIClient, WM_MDICREATE, 0, (LPARAM)(LPMDICREATESTRUCT)&mcs);
            break;
        }
        default:
            return DefFrameProc(hWnd, hWndMDIClient, message, wParam, lParam);
        }
        break;
    }
    case WM_DESTROY:
        PostQuitMessage(0);
        break;
    default:
        return DefFrameProc(hWnd, hWndMDIClient, message, wParam, lParam);
    }
    return 0;
}

// Оконная процедура MDI-окна
LRESULT CALLBACK MDIChildWndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam) {
    switch (message) {
    case WM_CREATE:
        break;
    case WM_CLOSE:
        DestroyWindow(hWnd);
        break;
    case WM_DESTROY:
        --documentCount;
        if (documentCount == 0) {
            PostQuitMessage(0);
        }
        break;
    default:
        return DefMDIChildProc(hWnd, message, wParam, lParam);
    }
    return 0;
}
