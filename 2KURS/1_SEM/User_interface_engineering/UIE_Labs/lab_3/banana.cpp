#include <Windows.h>
#include <vector>
// #include "resource.h"
 
HWND FrameWindow; 
HWND ClientWindow;
HWND ChildWindow;
HINSTANCE hInstance;
std::vector<HWND> vec;
wchar_t text[1000];
 
int x = GetSystemMetrics(SM_CXSCREEN), y = GetSystemMetrics(SM_CYSCREEN);
 
LRESULT CALLBACK MDIProc(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    switch (message)
    {
    case WM_CREATE:
    {
        HWND edit = CreateWindow(L"EDIT", NULL, WS_CHILD | WS_VISIBLE | WS_BORDER | ES_LEFT | ES_AUTOHSCROLL, 0, 0, 200, 200, hwnd, NULL, hInstance, NULL);
        SendMessage(hwnd, WM_APP, 0, 0);
 
        wchar_t* text = new wchar_t[100];
        GetWindowText(edit, text, 100);
        SetWindowLongPtr(ClientWindow, GWLP_USERDATA, (LONG_PTR)text);
        break;
    }
    case WM_COMMAND:
    {
        switch (HIWORD(wParam))
        {
        case EN_CHANGE:
        {
            HWND CurrentMDI = (HWND)SendMessage(ClientWindow, WM_MDIGETACTIVE, 0, 0);
            if (CurrentMDI != hwnd) break;
 
            HWND edit = FindWindowEx(hwnd, NULL, L"EDIT", NULL);
 
            wchar_t* text = new wchar_t[100];
            GetWindowText(edit, text, 100);
            SetWindowLongPtr(ClientWindow, GWLP_USERDATA, (LONG_PTR)text);
 
            for (HWND element : vec)
            {
                if (element == hwnd) continue;
                SendMessage(element, WM_APP, 0, 0);
            }
            break;
        }
        }
        break;
    }
    case WM_APP:
    {
        HWND edit = FindWindowEx(hwnd, NULL, L"EDIT", NULL);
        wchar_t* text = (wchar_t*)GetWindowLongPtr(ClientWindow, GWLP_USERDATA);
        SetWindowText(edit, text);
        break;
    }
    default:
        return DefMDIChildProc(hwnd, message, wParam, lParam);
    }
}
 
LRESULT CALLBACK FrameProc(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    switch (message)
    {
    case WM_CREATE:
        WNDCLASS wnd;
        memset(&wnd, 0, sizeof(WNDCLASS));
        wnd.lpszClassName = L"MDIWindow";
        wnd.lpfnWndProc = MDIProc;
        RegisterClass(&wnd);
 
        CLIENTCREATESTRUCT clcs;
        clcs.hWindowMenu = GetSubMenu(GetMenu(hwnd), IDM_MDI);
        clcs.idFirstChild = 500;
 
        ClientWindow = CreateWindow(L"MDICLIENT", NULL, WS_CHILD | WS_CLIPCHILDREN | WS_VISIBLE | WS_HSCROLL | WS_VSCROLL, 0, 0, 0, 0, hwnd, (HMENU)0xCAC, hInstance, (LPSTR)&clcs);
        ShowWindow(ClientWindow, SW_SHOW);
        break;
    case WM_COMMAND:
    {
        switch (wParam)
        {
        case IDM_EXIT:
            PostQuitMessage(0);
            break;
        case IDM_ABOUT:
            MessageBox(hwnd, L"Прикольное MDI-приложение", L"MDI", 0);
            break;
        case IDM_MDI:
            MDICREATESTRUCT mdics;
            memset(&mdics, 0, sizeof(mdics));
            mdics.szClass = L"MDIWindow";
            mdics.szTitle = L"MDI Window";
            mdics.hOwner = hInstance; 
            mdics.x = mdics.y = CW_USEDEFAULT;
            mdics.cx = mdics.cy = 200;
 
            ChildWindow = (HWND)SendMessage(ClientWindow, WM_MDICREATE, 0, (LPARAM)&mdics);
            vec.push_back(ChildWindow);
            break;
        }
        break;
    }
    case WM_DESTROY:
        PostQuitMessage(0);
        break;
    default:
        return DefFrameProc(hwnd, ClientWindow, message, wParam, lParam);
    }
}
 
int WINAPI WinMain(_In_ HINSTANCE This, _In_opt_ HINSTANCE Prev, _In_ LPSTR lpCmdLine, _In_ int nShowCmd)
{
    hInstance = This;
 
    MSG msg;
    WNDCLASS wnd;
    memset(&wnd, 0, sizeof(WNDCLASS));
    wnd.hInstance = This;
    wnd.lpszClassName = L"Window";
    wnd.lpfnWndProc = FrameProc;
    wnd.style = CS_HREDRAW | CS_VREDRAW;
    wnd.lpszMenuName = MAKEINTRESOURCE(109);
    RegisterClass(&wnd);
 
    FrameWindow = CreateWindow(L"Window", L"Frame Window", WS_OVERLAPPEDWINDOW, x / 6, y / 12, (double)2 / 3 * x, (double)5 / 6 * y, 0, 0, This, 0);
 
    ShowWindow(FrameWindow, nShowCmd);
    UpdateWindow(FrameWindow);
 
    while (GetMessage(&msg, 0, 0, 0))
    {
        if (!TranslateMDISysAccel(ClientWindow, &msg))
        {
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }
    }
    return 0;
}
 