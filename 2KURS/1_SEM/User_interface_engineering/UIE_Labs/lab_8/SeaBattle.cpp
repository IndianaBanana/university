#include "SeaBattle.h"

HWND new_game, start_game, one_deck, two_deck, three_deck, four_deck, restart, progress;
HDC hdc1;
bool nach_game = false, flag = false, vin = false;
int size = 0, one = 0, two = 0, three = 0, four = 0, xCord, yCord, playerProgress = 0, enemyProgress = 0, pos, res;
Mode mode = Mode::None;
std::vector<polygon> polygons_player;
std::vector<polygon> polygons_bot;
std::vector<polygon> polygons_ship;
std::vector<Ship> ships_player;
std::vector<Ship> ships_bot;

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow) {
    MSG msg = { 0 };
    if (!RegClass((HBRUSH)(COLOR_WINDOW + 1), LoadCursor(NULL, IDC_ARROW), hInstance, LoadIcon(NULL, IDI_APPLICATION), L"MainWindowClass", WndProc)) return FALSE;

    CreateWindow(L"MainWindowClass", L"Lab8", WS_OVERLAPPEDWINDOW | WS_VISIBLE, 200, 25, 900, 400, NULL, NULL, NULL, NULL);

    while (GetMessage(&msg, NULL, NULL, NULL)) {
        TranslateMessage(&msg);
        DispatchMessage(&msg);
    }
    return 0;
}

BOOL RegClass(HBRUSH BgColor, HCURSOR Cursor, HINSTANCE hInstance, HICON Icon, LPCWSTR Name, WNDPROC Procedure) {
    WNDCLASS NWC = { 0 };
    NWC.hCursor = Cursor;
    NWC.hIcon = Icon;
    NWC.hbrBackground = BgColor;
    NWC.hInstance = hInstance;
    NWC.lpfnWndProc = Procedure;
    NWC.lpszClassName = Name;
    return (RegisterClass(&NWC) != 0);
}

LRESULT CALLBACK WndProc(HWND hWnd, UINT Msg, WPARAM wParam, LPARAM lParam) {
    switch (Msg) {
    case WM_CREATE:
        new_game = CreateWindow(L"button", L"New Game", WS_CHILD | WS_VISIBLE, 300, 300, 250, 50, hWnd, (HMENU)NEW_GAME, NULL, NULL);
        break;
    case WM_COMMAND: {
        switch (wParam) {
        case NEW_GAME:
            SendMessage(new_game, WM_CLOSE, 0, 0);
            AddTable(hWnd);
            break;
        case ONE_DECK: mode = Mode::One; DisableButton(); break;
        case TWO_DECK: mode = Mode::Two; DisableButton(); break;
        case THREE_DECK: mode = Mode::Three; DisableButton(); break;
        case FOUR_DECK: mode = Mode::Four; DisableButton(); break;
        case RESTART:
            nach_game = false;
            hdc1 = GetDC(hWnd);
            for (int i = 0; i < polygons_player.size(); i++) {
                polygons_player[i].setShip(false);
                polygons_player[i].Fill(hdc1, RGB(255, 255, 255));
            }
            one = 0; two = 0; three = 0; four = 0; size = 0;
            EnableButton();
            ships_player.clear();
            break;
        case START_GAME:
            nach_game = true;
            Add_Two_Table(hWnd);
            break;
        }
    }break;
    case WM_LBUTTONDOWN:
        xCord = GET_X_LPARAM(lParam);
        yCord = GET_Y_LPARAM(lParam);
        hdc1 = GetDC(hWnd);
        if (nach_game == false) {
            if (xCord >= 50 && yCord >= 25 && xCord <= 300 && yCord <= 275) {
                if (mode == Mode::One) End_Ship(hWnd, lParam, 1);
                else if (mode == Mode::Two) {
                    if (size != 1) Ship_Building(hWnd, lParam);
                    else End_Ship(hWnd, lParam, 2);
                }
                else if (mode == Mode::Three) {
                    if (size != 2) Ship_Building(hWnd, lParam);
                    else End_Ship(hWnd, lParam, 3);
                }
                else if (mode == Mode::Four) {
                    if (size != 3) Ship_Building(hWnd, lParam);
                    else End_Ship(hWnd, lParam, 4);
                }
                else return DefWindowProc(hWnd, Msg, wParam, lParam);
            }
        }
        else {
            if (xCord >= 550 && yCord >= 25 && xCord <= 850 && yCord <= 275 && GetPixel(hdc1, xCord, yCord) == RGB(255, 255, 255)) {
                int l, j, g, h;
                for (l = 0; l < 100; l++) {
                    if (polygons_bot[l].getRect().left <= xCord && polygons_bot[l].getRect().top <= yCord
                        && polygons_bot[l].getRect().right >= xCord && polygons_bot[l].getRect().bottom >= yCord) break;
                }
                if (polygons_bot[l].getShip()) {
                    polygons_bot[l].Fill(hdc1, RGB(255, 0, 0));
                    for (j = 0; j < ships_bot.size(); j++) {
                        for (g = 0; g < ships_bot[j].get_polygon().size(); g++) {
                            if (ships_bot[j].get_polygon()[g].getRect().left <= xCord && ships_bot[j].get_polygon()[g].getRect().top <= yCord
                                && ships_bot[j].get_polygon()[g].getRect().right >= xCord && ships_bot[j].get_polygon()[g].getRect().bottom >= yCord) {flag = true; break;}
                        }
                        if (flag == true) { flag = false;  break;}
                    }
                    ships_bot[j].get_polygon()[g].setInjured(true);
                    ships_bot[j].setSize(ships_bot[j].get_size() - 1);
                    if (ships_bot[j].get_size() == 0) {
                        for (int k = 0; k < ships_bot[j].get_siz(); k++) {
                            for (h = 0; h < 100; h++) {
                                if (ships_bot[j].get_polygon()[k].getRect().left == polygons_bot[h].getRect().left && ships_bot[j].get_polygon()[k].getRect().top == polygons_bot[h].getRect().top
                                    && ships_bot[j].get_polygon()[k].getRect().right == polygons_bot[h].getRect().right && ships_bot[j].get_polygon()[k].getRect().bottom == polygons_bot[h].getRect().bottom) break;
                            }
                            if (h > 9 && h % 10 != 9) if (!polygons_bot[h - 9].getShip()) polygons_bot[h - 9].Fill(hdc1, RGB(255, 255, 0));
                            if (h > 9 && h % 10 != 0) if (!polygons_bot[h - 11].getShip()) polygons_bot[h - 11].Fill(hdc1, RGB(255, 255, 0));
                            if (h > 9) if (!polygons_bot[h - 10].getShip()) polygons_bot[h - 10].Fill(hdc1, RGB(255, 255, 0));
                            if (h < 90 && h % 10 != 0) if (!polygons_bot[h + 9].getShip()) polygons_bot[h + 9].Fill(hdc1, RGB(255, 255, 0));
                            if (h < 90 && h % 10 != 9) if (!polygons_bot[h + 11].getShip()) polygons_bot[h + 11].Fill(hdc1, RGB(255, 255, 0));
                            if (h < 90) if (!polygons_bot[h + 10].getShip()) polygons_bot[h + 10].Fill(hdc1, RGB(255, 255, 0));
                            if (h % 10 != 9) if (!polygons_bot[h + 1].getShip()) polygons_bot[h + 1].Fill(hdc1, RGB(255, 255, 0));
                            if (h % 10 != 0) if (!polygons_bot[h - 1].getShip()) polygons_bot[h - 1].Fill(hdc1, RGB(255, 255, 0));
                        }
                        playerProgress++;
                        SetWindowText(progress, L"Player: kill!");
                        if (playerProgress == 10) {
                            SetWindowText(progress, L"Player WIN");
                            MessageBox(hWnd, L"PLAYER WIN", NULL, NULL);
                            PostQuitMessage(0);
                        }
                    }
                    else SetWindowText(progress, L"Player: hit");
                }
                else { 
                    SetWindowText(progress, L"Player: missed");
                    polygons_bot[l].Fill(hdc1, RGB(255, 255, 0)); 
                    if (res == 0) {
                        pos = rand() % 99;
                        res = 0;
                        Move_Bot(hdc1, hWnd, pos);
                    }
                    Repite(hdc1, hWnd, pos);
                }
            }
        }
        break;
    case WM_DESTROY:
        PostQuitMessage(0);
        break;
    default: return DefWindowProc(hWnd, Msg, wParam, lParam);
        break;
    }
}

void Ship_Building(HWND hWnd, LPARAM lParam) {
    int rect = Fill_Polygon_Player(hWnd, lParam, RGB(0, 0, 255));
    if (rect != ERR) {
        polygons_ship.push_back(polygons_player[rect]);
        polygons_player[rect].setShip(true);
        size++;
    }
}

void End_Ship(HWND hWnd, LPARAM lParam, int s) {
    int rect = Fill_Polygon_Player(hWnd, lParam, RGB(0, 0, 255));
    if (rect != ERR) {
        polygons_ship.push_back(polygons_player[rect]);
        polygons_player[rect].setShip(true);
        Ship ship = Ship(polygons_ship, s);
        ships_player.push_back(ship);
        polygons_ship.clear();
        size = 0;
        switch (s) {
        case 1: one++; break;
        case 2: two++; break;
        case 3: three++; break;
        case 4: four++; break;
        }
        EnableButton();
        if (ships_player.size() == 10) {
            nach_game = true;
            EnableWindow(start_game, true);
        }
    }
}

int Fill_Polygon_Player(HWND hWnd, LPARAM lParam, COLORREF color) {
    HDC hdc = GetDC(hWnd);
    int xPos = GET_X_LPARAM(lParam);
    int yPos = GET_Y_LPARAM(lParam);
    int i;
    for (i = 0; i < 100; i++) {
        if (polygons_player[i].getRect().left <= xPos && polygons_player[i].getRect().top <= yPos 
            && polygons_player[i].getRect().right >= xPos && polygons_player[i].getRect().bottom >= yPos) break;
        if (i == 99) return ERR;
    }
    if (polygons_player[i].getShip() || !Check_Region_Polygon(i)) return ERR;
    if (size > 0) { if (!Check_Region(i)) return ERR; }
    polygons_player[i].Fill(hdc, color);
    return i;
}

void Fill_Polygon_Bot(){
    int a = 0, b = 0, c = 0, d = 0, current = 0, q = 4, ran, r;
    srand(time(0));
    while (current < 10) {
        switch (q) {
        case 1:
            while (a < 4) {
                r = rand() % 99;
                if (polygons_bot[r].getShip() || !Check_Bot(0, r, q)) break;
                else {
                    current++; a++;
                    polygons_ship.push_back(polygons_bot[r]);
                    polygons_bot[r].setShip(true);
                    Ship ship = Ship(polygons_ship, q);
                    ships_bot.push_back(ship);
                    polygons_ship.clear();
                }
            }
            if (a == 4) q--;
            break;
        case 2:
            while (b < 3) {
                ran = rand() % 2 + 1;
                r = rand() % 99;
                if (polygons_bot[r].getShip() || !Check_Bot(ran, r, q)) break;
                else {
                    if (ran == 1) {
                        current++; b++;
                        for (int j = 0; j < 2; j++) {
                            polygons_ship.push_back(polygons_bot[r + 10 * j]);
                            polygons_bot[r + 10 * j].setShip(true);
                        }
                        Ship ship = Ship(polygons_ship, q);
                        ships_bot.push_back(ship);
                        polygons_ship.clear();
                    }
                    else if (ran == 2) {
                        current++; b++;
                        for (int j = 0; j < 2; j++) {
                            polygons_ship.push_back(polygons_bot[r + 1 * j]);
                            polygons_bot[r + 1 * j].setShip(true);
                        }
                        Ship ship = Ship(polygons_ship, q);
                        ships_bot.push_back(ship);
                        polygons_ship.clear();
                    }
                }
            }
            if (b == 3) q--;
            break;
        case 3:
            while (c < 2) {
                ran = rand() % 2 + 1;
                r = rand() % 99;
                if (polygons_bot[r].getShip() || !Check_Bot(ran, r, q))  break;
                else {
                    if (ran == 1) {
                        current++; c++;
                        for (int j = 0; j < 3; j++) {
                            polygons_ship.push_back(polygons_bot[r + 10 * j]);
                            polygons_bot[r + 10 * j].setShip(true);
                        }
                        Ship ship = Ship(polygons_ship, q);
                        ships_bot.push_back(ship);
                        polygons_ship.clear();
                    }
                    else if (ran == 2) {
                        current++; c++;
                        for (int j = 0; j < 3; j++) {
                            polygons_ship.push_back(polygons_bot[r + 1 * j]);
                            polygons_bot[r + 1 * j].setShip(true);
                        }
                        Ship ship = Ship(polygons_ship, q);
                        ships_bot.push_back(ship);
                        polygons_ship.clear();
                    }
                }
            }
            if (c == 2) q--;
            break;
        case 4:
            while (d < 1) {
                ran = rand() % 2 + 1;
                r = rand() % 99;
                if (polygons_bot[r].getShip() || !Check_Bot(ran, r, q)) break;
                else {
                    if (ran == 1) {
                        current++; d++;
                        for (int j = 0; j < 4; j++) {
                            polygons_ship.push_back(polygons_bot[r + 10 * j]);
                            polygons_bot[r + 10 * j].setShip(true);
                        }
                        Ship ship = Ship(polygons_ship, q);
                        ships_bot.push_back(ship);
                        polygons_ship.clear();
                    }
                    else if (ran == 2) {
                            current++; d++;
                        for (int j = 0; j < 4; j++) {
                            polygons_ship.push_back(polygons_bot[r + 1 * j]);
                            polygons_bot[r + 1 * j].setShip(true);
                        }
                        Ship ship = Ship(polygons_ship, q);
                        ships_bot.push_back(ship);
                        polygons_ship.clear();
                    }
                }
            }
            if (d == 1) q--;
            break;
        }
    }
}

void AddTable(HWND hWnd) {
    HDC hdc = GetDC(hWnd);
    HPEN newPen = CreatePen(PS_SOLID, 2, RGB(0, 0, 0));
    SelectObject(hdc, newPen);
    int x = 50, y = 25;
    for (int i = 0; i < 10; i++) {
        for (int j = 0; j < 10; j++) {
            Rectangle(hdc, x, y, x + 25, y + 25);
            RECT rect;
            SetRect(&rect, x + 1, y + 1, x + 24, y + 24);
            polygon pol = polygon(rect);
            polygons_player.push_back(pol);
            x += 25;
        }
        y += 25; x = 50;
    }
    start_game = CreateWindow(L"button", L"Start", WS_CHILD | WS_VISIBLE, 50, 300, 125, 50, hWnd, (HMENU)START_GAME, NULL, NULL);
    EnableWindow(start_game, false);
    restart = CreateWindow(L"button", L"Restart", WS_CHILD | WS_VISIBLE, 175, 300, 125, 50, hWnd, (HMENU)RESTART, NULL, NULL);
    one_deck = CreateWindow(L"button", L"1 deck", WS_CHILD | WS_VISIBLE | BS_AUTORADIOBUTTON | WS_GROUP, 325, 25, 100, 15, hWnd, (HMENU)ONE_DECK, NULL, NULL);
    two_deck = CreateWindow(L"button", L"2 decks", WS_CHILD | WS_VISIBLE | BS_AUTORADIOBUTTON, 325, 50, 150, 15, hWnd, (HMENU)TWO_DECK, NULL, NULL);
    three_deck = CreateWindow(L"button", L"3 decks", WS_CHILD | WS_VISIBLE | BS_AUTORADIOBUTTON, 325, 75, 100, 15, hWnd, (HMENU)THREE_DECK, NULL, NULL);
    four_deck = CreateWindow(L"button", L"4 decks", WS_CHILD | WS_VISIBLE | BS_AUTORADIOBUTTON, 325, 100, 100, 15, hWnd, (HMENU)FOUR_DECK, NULL, NULL);
}

void Add_Two_Table(HWND hWnd) {
    SendMessage(one_deck, WM_CLOSE, 0, 0);
    SendMessage(two_deck, WM_CLOSE, 0, 0);
    SendMessage(three_deck, WM_CLOSE, 0, 0);
    SendMessage(four_deck, WM_CLOSE, 0, 0);
    SendMessage(start_game, WM_CLOSE, 0, 0);
    SendMessage(restart, WM_CLOSE, 0, 0);
    HDC hdc = GetDC(hWnd);
    HPEN newPen = CreatePen(PS_SOLID, 2, RGB(0, 0, 0));
    SelectObject(hdc, newPen);
    int x = 550, y = 25;
    for (int i = 0; i < 10; i++) {
        for (int j = 0; j < 10; j++) {
            Rectangle(hdc, x, y, x + 25, y + 25);
            RECT rect;
            SetRect(&rect, x, y, x + 24, y + 24);
            polygon pol = polygon(rect);
            polygons_bot.push_back(pol);
            x += 25;
        }
        y += 25; x = 550;
    }
    Fill_Polygon_Bot();
    progress = CreateWindowA("static", "", WS_VISIBLE | WS_CHILD, 300, 300, 250, 50, hWnd, NULL, NULL, NULL);
}

void DisableButton() {
    EnableWindow(one_deck, false);
    EnableWindow(two_deck, false);
    EnableWindow(three_deck, false);
    EnableWindow(four_deck, false);
}

void EnableButton() {
    if (one < 4) EnableWindow(one_deck, true);
    if (two < 3) EnableWindow(two_deck, true);
    if (three < 2) EnableWindow(three_deck, true);
    if (four < 1) EnableWindow(four_deck, true);
    mode = Mode::None;
}

bool Check_Region_Polygon(int r) {
    if (r < 90) {
        if (polygons_player[r+10].getShip() && !Check_Region_Ship(r+10)) return false;
        if (r % 10 != 0) {
            if (polygons_player[r + 9].getShip()) return false;
        }
        if (r % 10 != 9) {
            if (polygons_player[r + 11].getShip()) return false;
        }
    }
    if (r > 9) {
        if (polygons_player[r - 10].getShip() && !Check_Region_Ship(r-10)) return false;
        if (r % 10 != 0) {
            if (polygons_player[r - 11].getShip()) return false;
        }
        if (r % 10 != 9) {
            if (polygons_player[r - 9].getShip()) return false;
        }
    }
    if (r % 10 != 0) {
        if (polygons_player[r - 1].getShip() && !Check_Region_Ship(r-1)) return false;
    }
    if (r % 10 != 9) {
        if (polygons_player[r + 1].getShip() && !Check_Region_Ship(r+1)) return false;
    }
    return true;
}

bool Check_Region_Ship(int r) {
    for (int j = 0; j < polygons_ship.size(); j++) {
        if (polygons_player[r].getRect().left == polygons_ship[j].getRect().left && polygons_player[r].getRect().top == polygons_ship[j].getRect().top
            && polygons_player[r].getRect().right == polygons_ship[j].getRect().right && polygons_player[r].getRect().bottom == polygons_ship[j].getRect().bottom) return true;
    }
    return false;
}

bool Check_Region(int r) {
    if (r < 90) {
        if (polygons_player[r + 10].getShip() && Check_Region_Ship(r + 10)) return true;
    }
    if (r > 9) {
        if (polygons_player[r - 10].getShip() && Check_Region_Ship(r - 10)) return true;
    }
    if (r % 10 != 0) {
        if (polygons_player[r - 1].getShip() && Check_Region_Ship(r - 1)) return true;
    }
    if (r % 10 != 9) {
        if (polygons_player[r + 1].getShip() && Check_Region_Ship(r + 1)) return true;
    }
    return false;
}

bool Check_Region_Polygon_Bot(int r) {
    if (r < 90) {
        if (polygons_bot[r + 10].getShip()) return false;
        if (r % 10 != 0) {
            if (polygons_bot[r + 9].getShip()) return false;
        }
        if (r % 10 != 9) {
            if (polygons_bot[r + 11].getShip()) return false;
        }
    }
    if (r > 9) {
        if (polygons_bot[r - 10].getShip()) return false;
        if (r % 10 != 0) {
            if (polygons_bot[r - 11].getShip()) return false;
        }
        if (r % 10 != 9) {
            if (polygons_bot[r - 9].getShip()) return false;
        }
    }
    if (r % 10 != 0) {
        if (polygons_bot[r - 1].getShip()) return false;
    }
    if (r % 10 != 9) {
        if (polygons_bot[r + 1].getShip()) return false;
    }
    return true;
}

bool Check_Bot(int ran, int r, int size) {
    switch (size) {
    case 1: if (!Check_Region_Polygon_Bot(r)) return false; break;
    case 2:
        if (ran == 1 && r < 90) {
            for (int j = 0; j < 2; j++) {
                if (!Check_Region_Polygon_Bot(r + 10 * j)) return false;
            }
        }
        else if (ran == 2 && r % 10 < 8) {
            for (int j = 0; j < 2; j++) {
                if (!Check_Region_Polygon_Bot(r + 1 * j)) return false;
            }
        }
        else return false;
        break;
    case 3:
        if (ran == 1 && r < 80) {
            for (int j = 0; j < 3; j++) {
                if (!Check_Region_Polygon_Bot(r + 10 * j)) return false;
            }
        }
        else if (ran == 2 && r % 10 < 7) {
            for (int j = 0; j < 3; j++) { if (!Check_Region_Polygon_Bot(r + 1 * j)) return false; }
        }
        else return false;
        break;
    case 4:
        if (ran == 1 && r < 70) {
            for (int j = 0; j < 4; j++) {
                if (!Check_Region_Polygon_Bot(r + 10 * j)) return false;
            }
        }
        else if (ran == 2 && r % 10 < 6) {
            for (int j = 0; j < 4; j++) {
                if (!Check_Region_Polygon_Bot(r + 1 * j)) return false;
            }
        } 
        else return false;
        break;
    }
    return true;
}

void Move_Bot(HDC hdc, HWND hWnd, int pos) {
    if (GetPixel(hdc, pos / 10, pos % 10) == RGB(255, 255, 255)) {
        if (polygons_player[pos].getShip()) {
            int j, g;
            polygons_player[pos].Fill(hdc, RGB(255, 0, 0));
            for (j = 0; j < ships_player.size(); j++) {
                for (g = 0; g < ships_player[j].get_polygon().size(); g++) {
                    if (ships_player[j].get_polygon()[g].getRect().left == polygons_player[pos].getRect().left && ships_player[j].get_polygon()[g].getRect().top == polygons_player[pos].getRect().top
                        && ships_player[j].get_polygon()[g].getRect().right == polygons_player[pos].getRect().right && ships_player[j].get_polygon()[g].getRect().bottom == polygons_player[pos].getRect().bottom) {
                        flag = true; break;
                    }
                }
                if (flag == true) { flag = false;  break; }
            }
            ships_player[j].get_polygon()[g].setInjured(true);
            ships_player[j].setSize(ships_player[j].get_size() - 1);
            if (ships_player[j].get_size() == 0) {
                enemyProgress++;
                SetWindowText(progress, L"Bot: kill");
                if (enemyProgress == 10) {
                    SetWindowText(progress, L"Bot wins");
                    MessageBox(hWnd, L"Bot wins", NULL, NULL);
                    vin = true;
                }
                pos = rand() % 99;
                res = 0;
                Move_Bot(hdc, hWnd, pos);
            }
            else {
                SetWindowText(progress, L"Bot: hit");
                res = rand() % 4 + 1;
                Repite(hdc, hWnd, pos);
            }
        }
        else {
            SetWindowText(progress, L"Bot: missed");
            polygons_player[pos].Fill(hdc, RGB(255, 255, 0)); res = 0;
        }
    }
    else {
        pos = rand() % 99;
        res = 0;
        Move_Bot(hdc, hWnd, pos);
    }
}

int Repite(HDC hdc, HWND hWnd, int pos) {
    if (res == 0) return 0;
    if (res == 1) {
       if(pos % 10 != 9) Move_Bot(hdc, hWnd, pos + 1);
       else {
           res = rand() % 4 + 1;
           Repite(hdc, hWnd, pos);
       }
    }
    if (res == 2) {
        if (pos < 90) Move_Bot(hdc, hWnd, pos + 10);
        else {
            res = rand() % 4 + 1;
            Repite(hdc, hWnd, pos);
        }
    }
    if (res == 3) {
        if (pos % 10 != 0) Move_Bot(hdc, hWnd, pos - 1);
        else {
            res = rand() % 4 + 1;
            Repite(hdc, hWnd, pos);
        }
    }
    if (res == 4) {
        if (pos > 9) Move_Bot(hdc, hWnd, pos - 10);
        else {
            res = rand() % 4 + 1;
                Repite(hdc, hWnd, pos);
        }
    }
}