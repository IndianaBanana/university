package org.lab1;

import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.Base64;
import java.util.Objects;
import java.util.Scanner;

public class SMTP {
    public static void main(String[] args) {


        final String serverHost = "smtp.gmail.com"; // Адрес SMTP-сервера Gmail
        final int serverPort = 465; // Порт SMTP-сервера Gmail

        Scanner scanner = new Scanner(System.in);

        System.out.print("Введите ваш адрес электронной почты: ");
        String username = scanner.nextLine();

        System.out.print("Введите пароль: ");
        String password = scanner.nextLine();

        System.out.print("Введите адрес получателя: ");
        String recipient = scanner.nextLine();

        System.out.print("Введите тему письма: ");
        String subject = scanner.nextLine();

        System.out.print("Введите текст письма: ");
        String messageText = scanner.nextLine();

        scanner.close();

        try (SSLSocket socket = (SSLSocket) SSLSocketFactory.getDefault().createSocket(serverHost, serverPort);
             BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
             PrintWriter writer = new PrintWriter(socket.getOutputStream(), true)) {

            // Читаем ответ от сервера
            String response = reader.readLine();
            System.out.println("Сервер: " + response);
            if (!Objects.equals(response.split(" ")[0], "220")) {
                System.out.println("ERROR"); System.exit(-1);
            }

            // Отправляем приветственную команду
            writer.println("HELO gmail.com");
            response = reader.readLine();
            System.out.println("Сервер: " + response);
            if (!Objects.equals(response.split(" ")[0], "250")) {
                System.out.println("ERROR"); System.exit(-1);
            }

            // Отправляем команду AUTH LOGIN для аутентификации
            writer.println("AUTH LOGIN");

            // Кодируем логин и пароль в формате Base64

            String encodedUsername = Base64.getEncoder().encodeToString(username.getBytes());
            String encodedPassword = Base64.getEncoder().encodeToString(password.getBytes());

            writer.println(encodedUsername);
            response = reader.readLine();
            System.out.println("Сервер: " + response);
            if (!Objects.equals(response.split(" ")[0], "334")) {
                System.out.println("ERROR"); System.exit(-1);
            }

            writer.println(encodedPassword);
            response = reader.readLine();
            System.out.println("Сервер: " + response);
            if (!Objects.equals(response.split(" ")[0], "334")) {
                System.out.println("ERROR"); System.exit(-1);
            }


            // Отправляем команду MAIL FROM (замените на ваш адрес отправителя)
            writer.println("MAIL FROM: <" + username+ ">");
            response = reader.readLine();
            System.out.println("Сервер: " + response);
            if (!Objects.equals(response.split(" ")[0], "235")) {
                System.out.println("ERROR"); System.exit(-1);
            }

            // Отправляем команду RCPT TO (замените на адрес получателя)
            writer.println("RCPT TO: <" + recipient+ ">");
            response = reader.readLine();
            System.out.println("Сервер: " + response);
            if (!Objects.equals(response.split(" ")[0], "250")) {
                System.out.println("ERROR"); System.exit(-1);
            }

            // Отправляем команду DATA для начала передачи данных письма
            writer.println("DATA");
            response = reader.readLine();
            System.out.println("Сервер: " + response);
            if (!Objects.equals(response.split(" ")[0], "250")) {
                System.out.println("ERROR"); System.exit(-1);
            }

            // Отправляем заголовок и текст письма
            writer.println("Subject: " + subject);
            writer.println("From: <" + username+ ">");
            writer.println("To: <" + recipient+ ">");
            writer.println(); // Пустая строка между заголовком и текстом письма
            writer.println(messageText + ".");
            writer.println("."); // Завершение письма
            response = reader.readLine();
            System.out.println("Сервер: " + response);
            if (!Objects.equals(response.split(" ")[0], "354")) {
                System.out.println("ERROR"); System.exit(-1);
            }

            // Отправляем команду QUIT для завершения сессии
            writer.println("QUIT");
            response = reader.readLine();
            System.out.println("Сервер: " + response);
            if (!Objects.equals(response.split(" ")[0], "250")) {
                System.out.println("ERROR"); System.exit(-1);
            }

        } catch (IOException e) {
            e.printStackTrace();
            System.err.println("Ошибка: " + e.getMessage());
        }
    }
}
