package org.lab2;

import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.Scanner;

public class POP3 {
    public static void main(String[] args) {
        String username = ""; // Имя почтового ящика
        String password = ""; // Пароль
        int choose = 1;

        String pop3Server = "pop.gmail.com";
        int serverPort = 995;

        Scanner scanner = new Scanner(System.in);

        try (SSLSocket socket = (SSLSocket) SSLSocketFactory.getDefault().createSocket(pop3Server, serverPort);
             BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
             PrintWriter writer = new PrintWriter(socket.getOutputStream(), true)) {

            String response = reader.readLine();
            System.out.println(response);

            writer.println("USER " + username);
            response = reader.readLine();
            System.out.println(response);

            writer.println("PASS " + password);
            response = reader.readLine();
            System.out.println(response);

            while (choose != 0){
                System.out.println("""
                        Choose:
                        0: Quit
                        1: Show stats
                        2: Show List
                        3: (RETR) Show concrete message
                        4: Delete concrete message
                        5: Reset all deleted messages
                        6: (TOP) Show concrete message information
                        7: Check server""");
                choose = scanner.nextInt();
                switch (choose){
                    case 1:{
                        writer.println("STAT");
                        response = reader.readLine();
                        System.out.println(response);
                        break;
                    }
                    case 2:{
                        System.out.println("Wanna see list of all messages (1) or of a concrete one? (any other number)");
                        if (scanner.nextInt() == 1){
                            writer.println("LIST");
                            response = "";
                            while(!response.equals("."))  {
                                response = reader.readLine();
                                System.out.println(response);
                            }
                            break;
                        }
                        System.out.println("Enter number of message");
                        int number  = scanner.nextInt();
                        writer.println("LIST " + number);
                        response = reader.readLine();
                        System.out.println(response);
                        break;
                    }
                    case 3:{
                        System.out.println("(RETR) Enter number of message.");
                        int number  = scanner.nextInt();
                        writer.println("RETR " + number);
                        response = "";
                        while(!response.equals(".")){
                            response = reader.readLine();
                            System.out.println(response);
                        }
                        break;

                    }
                    case 4:{
                        System.out.println("Enter number of message.");
                        int number  = scanner.nextInt();
                        writer.println("DELE " + number);
                        response = reader.readLine();
                        System.out.println(response);
                        break;
                    }
                    case 5:{
                        writer.println("RSET");
                        response = reader.readLine();
                        System.out.println(response);
                        break;
                    }
                    case 6:{
                        System.out.println("(TOP) Enter number of message");
                        int number = scanner.nextInt();
                        System.out.println("Enter number of lines");
                        int numberOfLines = scanner.nextInt();
                        System.out.println(numberOfLines);
                        writer.println("TOP " + number + " " + numberOfLines);
                        response = "";
                        while(!response.equals("."))  {
                            response = reader.readLine();
                            System.out.println(response);
                        }
                        break;
                    }
                    case 7:{
                        writer.println("NOOP");
                        response = reader.readLine();
                        System.out.println(response);
                        break;
                    }
                    case 0:{
                        writer.println("QUIT");
                        response = reader.readLine();
                        System.out.println(response);
                        break;
                    }
                }
            }


        } catch (IOException e) {
            System.err.println("ERROR: " + e.getMessage());
        }
    }
}
