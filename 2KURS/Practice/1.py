import tkinter as tk
from tkinter import ttk
from tkinter import messagebox
import matplotlib.pyplot as plt
import numpy as np

class FunctionPlotter:
    def __init__(self, root):
        self.root = root
        self.root.title("Function Plotter")

        self.function_entries = []
        self.create_widgets()

    def create_widgets(self):
        main_frame = ttk.Frame(self.root)
        main_frame.pack(padx=10, pady=10, fill=tk.BOTH, expand=True)

        for i in range(3):
            label = ttk.Label(main_frame, text=f"Function {i+1}:")
            label.grid(row=i, column=0, padx=5, pady=5)
            entry = ttk.Entry(main_frame, width=50)
            entry.grid(row=i, column=1, padx=5, pady=5)
            self.function_entries.append(entry)

        label_x = ttk.Label(main_frame, text="X values (comma separated):")
        label_x.grid(row=3, column=0, padx=5, pady=5)
        self.entry_x = ttk.Entry(main_frame, width=50)
        self.entry_x.grid(row=3, column=1, padx=5, pady=5)

        plot_button = ttk.Button(main_frame, text="Plot", command=self.plot_functions)
        plot_button.grid(row=4, column=0, columnspan=2, pady=10)

    def plot_functions(self):
        x_values = self.entry_x.get()
        if not x_values:
            messagebox.showerror("Input Error", "Please enter X values.")
            return

        try:
            x = np.array([float(i) for i in x_values.split(',')])
        except ValueError:
            messagebox.showerror("Input Error", "Invalid X values.")
            return

        plt.figure()

        for entry in self.function_entries:
            function_str = entry.get()
            if function_str:
                try:
                    y = eval(function_str)
                    plt.plot(x, y, label=function_str)
                except Exception as e:
                    messagebox.showerror("Function Error", f"Error in function '{function_str}': {e}")
                    return

        plt.xlabel('X')
        plt.ylabel('Y')
        plt.legend()
        plt.title("Function Plotter")
        plt.ylim(-10, 10)  # Ограничение оси Y
        plt.grid(True)  # Добавление сетки
        plt.show()


if __name__ == "__main__":
    root = tk.Tk()
    app = FunctionPlotter(root)
    root.mainloop()
