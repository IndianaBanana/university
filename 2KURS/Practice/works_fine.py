import tkinter as tk
from tkinter import ttk
from tkinter import messagebox
import matplotlib.pyplot as plt
import numpy as np

class FunctionPlotter:
    def __init__(self, root):
        self.root = root
        self.root.title("Function Plotter")

        self.function_entries = []
        self.create_widgets()

    def create_widgets(self):
        main_frame = ttk.Frame(self.root)
        main_frame.pack(padx=10, pady=10, fill=tk.BOTH, expand=True)

        for i in range(3):
            label = ttk.Label(main_frame, text=f"Function {i+1}:")
            label.grid(row=i, column=0, padx=5, pady=5)
            entry = ttk.Entry(main_frame, width=50)
            entry.grid(row=i, column=1, padx=5, pady=5)
            self.function_entries.append(entry)

        label_x_min = ttk.Label(main_frame, text="X min:")
        label_x_min.grid(row=3, column=0, padx=5, pady=5)
        self.entry_x_min = ttk.Entry(main_frame, width=50)
        self.entry_x_min.grid(row=3, column=1, padx=5, pady=5)

        label_x_max = ttk.Label(main_frame, text="X max:")
        label_x_max.grid(row=4, column=0, padx=5, pady=5)
        self.entry_x_max = ttk.Entry(main_frame, width=50)
        self.entry_x_max.grid(row=4, column=1, padx=5, pady=5)

        label_step = ttk.Label(main_frame, text="Step:")
        label_step.grid(row=5, column=0, padx=5, pady=5)
        self.entry_step = ttk.Entry(main_frame, width=50)
        self.entry_step.grid(row=5, column=1, padx=5, pady=5)

        plot_button = ttk.Button(main_frame, text="Plot", command=self.plot_functions)
        plot_button.grid(row=6, column=0, columnspan=2, pady=10)

    def plot_functions(self):
        x_min = self.entry_x_min.get()
        x_max = self.entry_x_max.get()
        step = self.entry_step.get()

        if not x_min or not x_max or not step:
            messagebox.showerror("Input Error", "Please enter X min, X max, and step values.")
            return

        try:
            x_min = float(x_min)
            x_max = float(x_max)
            step = float(step)
        except ValueError:
            messagebox.showerror("Input Error", "Invalid X min, X max, or step values.")
            return

        if x_min >= x_max:
            messagebox.showerror("Input Error", "X min should be less than X max.")
            return

        if step <= 0:
            messagebox.showerror("Input Error", "Step should be a positive value.")
            return

        x = np.arange(x_min, x_max + step, step)

        plt.figure()

        for entry in self.function_entries:
            function_str = entry.get()
            if function_str:
                try:
                    y = eval(function_str)
                    plt.plot(x, y, label=function_str)
                except Exception as e:
                    messagebox.showerror("Function Error", f"Error in function '{function_str}': {e}")
                    return

        plt.xlabel('X')
        plt.ylabel('Y')
        plt.legend()
        plt.title("Function Plotter")
        plt.ylim(-10, 10)  # Ограничение оси Y
        plt.xlim(x_min, x_max)
        plt.grid(True)  # Добавление сетки
        plt.show()


if __name__ == "__main__":
    root = tk.Tk()
    app = FunctionPlotter(root)
    root.mainloop()
