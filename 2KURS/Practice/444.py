import tkinter as tk
from tkinter import ttk
from tkinter import messagebox
import matplotlib.pyplot as plt
import numpy as np
from decimal import Decimal, getcontext
from numpy import *

# Set the decimal precision
getcontext().prec = 20

class FunctionPlotter:
    def __init__(self, root):
        self.root = root
        self.root.title("Plotter")

        self.function_entries = []
        self.x_value_entries = []
        self.create_widgets()

    def create_widgets(self):
        main_frame = ttk.Frame(self.root)
        main_frame.pack(padx=10, pady=10, fill=tk.BOTH, expand=True)

        for i in range(3):
            label = ttk.Label(main_frame, text=f"Function {i+1}:")
            label.grid(row=i, column=0, padx=5, pady=5)
            entry = ttk.Entry(main_frame, width=30)
            entry.grid(row=i, column=1, padx=5, pady=5)
            self.function_entries.append(entry)

            label_x_val = ttk.Label(main_frame, text=f"X value {i+1}:")
            label_x_val.grid(row=i, column=2, padx=5, pady=5)
            entry_x_val = ttk.Entry(main_frame, width=10)
            entry_x_val.grid(row=i, column=3, padx=5, pady=5)
            self.x_value_entries.append(entry_x_val)

        label_x_min = ttk.Label(main_frame, text="X min:")
        label_x_min.grid(row=3, column=0, padx=5, pady=5)
        self.entry_x_min = ttk.Entry(main_frame, width=50)
        self.entry_x_min.grid(row=3, column=1, padx=5, pady=5)

        label_x_max = ttk.Label(main_frame, text="X max:")
        label_x_max.grid(row=4, column=0, padx=5, pady=5)
        self.entry_x_max = ttk.Entry(main_frame, width=50)
        self.entry_x_max.grid(row=4, column=1, padx=5, pady=5)

        label_step = ttk.Label(main_frame, text="Step:")
        label_step.grid(row=5, column=0, padx=5, pady=5)
        self.entry_step = ttk.Entry(main_frame, width=50)
        self.entry_step.grid(row=5, column=1, padx=5, pady=5)

        plot_button = ttk.Button(main_frame, text="Plot Functions", command=self.plot_functions)
        plot_button.grid(row=6, column=0, columnspan=4, pady=10)

        histogram_button = ttk.Button(main_frame, text="Plot Histogram", command=self.plot_histogram)
        histogram_button.grid(row=7, column=0, columnspan=4, pady=10)

        bar_graph_button = ttk.Button(main_frame, text="Plot Bar Graph", command=self.plot_bar_graph)
        bar_graph_button.grid(row=8, column=0, columnspan=4, pady=10)

    def plot_functions(self):
        x_min = self.entry_x_min.get()
        x_max = self.entry_x_max.get()
        step = self.entry_step.get()

        if not x_min or not x_max or not step:
            messagebox.showerror("Input Error", "Please enter X min, X max, and step values.")
            return

        try:
            x_min = Decimal(x_min)
            x_max = Decimal(x_max)
            step = Decimal(step)
        except ValueError:
            messagebox.showerror("Input Error", "Invalid X min, X max, or step values.")
            return

        if x_min >= x_max:
            messagebox.showerror("Input Error", "X min should be less than X max.")
            return

        if step <= 0:
            messagebox.showerror("Input Error", "Step should be a positive value.")
            return

        x = np.arange(float(x_min), float(x_max + step), float(step))

        plt.figure()

        y_values = []

        for entry in self.function_entries:
            function_str = entry.get()
            if function_str:
                try:
                    y = eval(function_str, {"x": x, "np": np, "Decimal": Decimal, **{k: getattr(np, k) for k in dir(np) if not k.startswith("_")}})
                    plt.plot(x, y, label=function_str)
                    y_values.append(y)
                except Exception as e:
                    messagebox.showerror("Function Error", f"Error in function '{function_str}': {e}")
                    return

        plt.xlabel('X')
        plt.ylabel('Y')
        plt.legend()
        plt.title("Function Plotter")
        plt.ylim(float(x_min), float(x_max))
        plt.xlim(float(x_min), float(x_max))
        plt.grid(True)
        plt.show(block=False)  # Non-blocking plot display

        self.show_y_values(y_values, x)

    def show_y_values(self, y_values, x):
        result_window = tk.Toplevel(self.root)
        result_window.title("Y Values at Specified X")

        for i, (entry, x_val_entry) in enumerate(zip(self.function_entries, self.x_value_entries)):
            function_str = entry.get()
            x_val = x_val_entry.get()
            if function_str and x_val:
                try:
                    x_val = Decimal(x_val)
                    if x_val < Decimal(x.min()) or x_val > Decimal(x.max()):
                        y_val = "X value out of range"
                    else:
                        y_index = np.abs(x - float(x_val)).argmin()
                        y_val = y_values[i][y_index]
                    label = ttk.Label(result_window, text=f"Function {i+1} ({function_str}) at X={x_val:.4f}: Y={y_val:.4f}")
                    label.pack(padx=10, pady=5)
                except ValueError:
                    label = ttk.Label(result_window, text=f"Function {i+1} ({function_str}): Invalid X value")
                    label.pack(padx=10, pady=5)
                except Exception as e:
                    label = ttk.Label(result_window, text=f"Function {i+1} ({function_str}): Error calculating Y value: {e}")
                    label.pack(padx=10, pady=5)

    def plot_histogram(self):
        x_min = self.entry_x_min.get()
        x_max = self.entry_x_max.get()
        step = self.entry_step.get()

        if not x_min or not x_max or not step:
            messagebox.showerror("Input Error", "Please enter X min, X max, and step values.")
            return

        try:
            x_min = Decimal(x_min)
            x_max = Decimal(x_max)
            step = Decimal(step)
        except ValueError:
            messagebox.showerror("Input Error", "Invalid X min, X max, or step values.")
            return

        if x_min >= x_max:
            messagebox.showerror("Input Error", "X min should be less than X max.")
            return

        if step <= 0:
            messagebox.showerror("Input Error", "Step should be a positive value.")
            return

        x = np.arange(float(x_min), float(x_max + step), float(step))
        colors = ['blue', 'green', 'red']
        plt.figure()

        for i, entry in enumerate(self.function_entries):
            function_str = entry.get()
            if function_str:
                try:
                    y = eval(function_str, {"x": x, "np": np, "Decimal": Decimal, **{k: getattr(np, k) for k in dir(np) if not k.startswith("_")}})
                    plt.hist(y, bins=30, color=colors[i], alpha=0.5, label=function_str, edgecolor='black')
                except Exception as e:
                    messagebox.showerror("Function Error", f"Error in function '{function_str}': {e}")
                    return

        plt.xlabel('Value')
        plt.ylabel('Frequency')
        plt.legend()
        plt.title("Histogram")
        plt.grid(True)
        plt.show(block=False)  # Non-blocking plot display

    def plot_bar_graph(self):
        x_min = self.entry_x_min.get()
        x_max = self.entry_x_max.get()
        step = self.entry_step.get()

        if not x_min or not x_max or not step:
            messagebox.showerror("Input Error", "Please enter X min, X max, and step values.")
            return

        try:
            x_min = Decimal(x_min)
            x_max = Decimal(x_max)
            step = Decimal(step)
        except ValueError:
            messagebox.showerror("Input Error", "Invalid X min, X max, or step values.")
            return

        if x_min >= x_max:
            messagebox.showerror("Input Error", "X min should be less than X max.")
            return

        if step <= 0:
            messagebox.showerror("Input Error", "Step should be a positive value.")
            return

        x = np.arange(float(x_min), float(x_max + step), float(step))
        colors = ['blue', 'green', 'red']
        width = float(step) / (len(self.function_entries) + 1)

        plt.figure()

        for i, entry in enumerate(self.function_entries):
            function_str = entry.get()
            if function_str:
                try:
                    y = eval(function_str, {"x": x, "np": np, "Decimal": Decimal, **{k: getattr(np, k) for k in dir(np) if not k.startswith("_")}})
                    plt.bar(x + i * width, y, width=width, color=colors[i], alpha=0.7, label=function_str, edgecolor='black')
                except Exception as e:
                    messagebox.showerror("Function Error", f"Error in function '{function_str}': {e}")
                    return

        plt.xlabel('X')
        plt.ylabel('Y')
        plt.legend()
        plt.title("Bar Graph")
        plt.grid(True)
        plt.show(block=False)  # Non-blocking plot display

if __name__ == "__main__":
    root = tk.Tk()
    app = FunctionPlotter(root)
    root.mainloop()
